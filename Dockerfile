FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential nodejs
ENV TZ=Asia/Ho_Chi_Minh
ENV RAILS_ENV=production
ENV RAILS_SERVE_STATIC_FILES=true
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN mkdir /pkh-api
WORKDIR /pkh-api
RUN mkdir /var/run/mysqld
RUN touch /var/run/mysqld/mysqld.sock
COPY Gemfile Gemfile.lock ./
RUN gem install bundler
RUN bundle install
COPY . ./
EXPOSE 3001
CMD ["bundle","exec","rails","server","-b","0.0.0.0","-p","3001"]
