require 'uri'
require 'net/http'
require 'net/https'

BVDHYD_HOST = (Rails.env == 'production') ? "http://dkkhambenh.umc.edu.vn:8080" : "http://dkkhambenh.umc.edu.vn:8080" # "http://bvdhyd-api.pkh.vn"
BVDHYD_API_ADVANCED_SEARCH_URL = BVDHYD_HOST + "/benhnhan/advanced_search"
BVDHYD_API_GETBYHSBN_URL = BVDHYD_HOST + "/benhnhan/getbyhsbn"
# BVDHYD_API_GETALL_DOCTOR_URL = BVDHYD_HOST + "/bacsi/getall"
BVDHYD_API_GETALL_DOCTOR_URL = BVDHYD_HOST + "/home/getdata"
# BVDHYD_API_DETAIL_DOCTOR_URL = BVDHYD_HOST + "/bacsi/detail"
# BVDHYD_API_GETALL_LICH_DOCTOR_URL = BVDHYD_HOST + "/lichkham/getlichbs"
BVDHYD_API_GETALL_LICH_DOCTOR_URL = BVDHYD_HOST + "/home/getdata"
# BVDHYD_API_GETALL_LICH_DOCTOR_THAYTHE_URL = BVDHYD_HOST + "/lichkham/getlichbsthaythe"
BVDHYD_API_GETALL_LICH_DOCTOR_THAYTHE_URL = BVDHYD_HOST + "/home/getdata"
# BVDHYD_API_GETALL_DUTRUKHAM = BVDHYD_HOST + "/lichkham/getdutrukham"
BVDHYD_API_GETALL_DUTRUKHAM = BVDHYD_HOST + "/home/getdata"
BVDHYD_API_GETALL_ROOM = BVDHYD_HOST + "/home/getdata"
BVDHYD_API_GETALL_KHOA = BVDHYD_HOST + "/home/getdata"
BVDHYD_API_GETALL_CLS = BVDHYD_HOST + "/home/getdata"
BVDHYD_API_GETALL_TOATHUOC = BVDHYD_HOST + "/home/getdata"

BVDHYD_API_INSERT_BN_UPDATE = BVDHYD_HOST + "/benhnhan/insert_bn_update"

BVDHYD_API_DATLICH_INSERTDSK = BVDHYD_HOST + "/datlich/insertDSK"
BVDHYD_API_DATLICH_DELETEDSK = BVDHYD_HOST + "/datlich/deleteDSK"
BVDHYD_API_DATLICH_INSERTTT = BVDHYD_HOST + "/datlich/insertTT"
BVDHYD_API_DATLICH_INSERTDL = BVDHYD_HOST + "/datlich/insertDL"
BVDHYD_API_DATLICH_GETBOOKEDDSK = BVDHYD_HOST + "/datlich/getbookedDSK"

BVDHYD_API_GETALL_TAIKHAM_URL = BVDHYD_HOST + "/home/getdata"

BVDHYD_API_SEARCHSINGLE_VIENPHI_URL = BVDHYD_HOST + "/vienphi/searchsingle"
BVDHYD_API_SEARCHMULTI_VIENPHI_URL = BVDHYD_HOST + "/vienphi/searchmulti"
BVDHYD_API_SEARCH_HISTORY_URL = BVDHYD_HOST + "/vienphi/history_by_sohs"
BVDHYD_API_VIENPHI_INSERT_TT = BVDHYD_HOST + "/vienphi/insert"
BVDHYD_API_UPDATE_TAMUNG_URL = BVDHYD_HOST + "/vienphi/updatetamung"

BVDHYD_API_BENHNHAN_GET_SOHS_FROM_TTTK = BVDHYD_HOST + "/datlich/get_sohs_from_tttk"
BVDHYD_API_CLS_GETBYSOHS = BVDHYD_HOST + "/cls/search_by_sohs"
BVDHYD_API_TOATHUOC_GETBYSOHS = BVDHYD_HOST + "/toathuoc/search_by_sohs"
BVDHYD_API_GET_HOADON = BVDHYD_HOST + "/hoadon/gethd"
BVDHYD_API_HOADON_GETBYSOHS = BVDHYD_HOST + "/hoadon/getbysohs"