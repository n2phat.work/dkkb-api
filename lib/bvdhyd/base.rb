class Bvdhyd::Base
    def self.send_get(url = '', params = {})
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host,uri.port)
        unless Rails.env == 'production'
            http.set_debug_output $stderr
        end
        http.read_timeout = 20

        uri.query = URI.encode_www_form(params)
        res = http.request_get(uri)
        
        if res.code.to_i == 200 
            data = ::Util.json_decode(res.body, false)
            return data["data"]
        else
            return nil;
        end
    end

    def self.send_post(url = '', params = {})
        uri = URI.parse(url)

        http = Net::HTTP.new(uri.host, uri.port)
        unless Rails.env == 'production'
            http.set_debug_output $stderr
        end
        http.read_timeout = 20

        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = JSON.generate(params)
        request["Content-Type"] = "application/json"
        
        response = http.request(request)
        re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
        if re
            return re
        else
            return nil
        end
    end
end