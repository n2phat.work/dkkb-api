require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Patient < Bvdhyd::Base
    def self.advanced_search(params = {})
        uri = URI.parse(BVDHYD_API_ADVANCED_SEARCH_URL)

        http = Net::HTTP.new(uri.host, uri.port)
        http.read_timeout = 20

        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = JSON.generate(params)
        request["Content-Type"] = "application/json"
        
        response = http.request(request)
        re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
        if re
            result = re["data"]
            return result
        else
            return nil
        end
    end

    def self.getbyhsbn(sohs = "")
        params = {
            :sohs => sohs
        }
        data = self.send_get(BVDHYD_API_GETBYHSBN_URL, params)
        data = data.first if data.kind_of?(Array)
        return data
    end

    def self.getall(limit = 10, offset = 0)
        params = {
            limit: limit,
            offset: offset,
            table_name: 'mtb_BenhNhan'
        }
        return self.send_get('http://dkkhambenh.umc.edu.vn:8080/home/getdata', params)
    end

    def self.insert_bn_update(data = {})
        params = {
            SoHS: data[:msbn],
            DiDong: data[:mobile],
            DienThoai: data[:mobile],
            Email: data[:email],
            MedProID: data[:medpro_id]
        }
        return self.send_post(BVDHYD_API_INSERT_BN_UPDATE, params)
    end

    def self.get_sohs_from_tttk(data = {})
        params = {
            medpro_id: data[:medpro_id]
        }
        return self.send_get(BVDHYD_API_BENHNHAN_GET_SOHS_FROM_TTTK, params)
    end
end