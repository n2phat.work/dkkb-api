require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Hoadon < Bvdhyd::Base

    def self.gethd(hd_id = nil)
        params = {
            eInvoiceID: hd_id
        }
        return self.send_get(BVDHYD_API_GET_HOADON, params)
    end

    def self.get_by_sohs(sohs)
        params = {
            SoHS: sohs
        }
        return self.send_get(BVDHYD_API_HOADON_GETBYSOHS, params)
    end
end