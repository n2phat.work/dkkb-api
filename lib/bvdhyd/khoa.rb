require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Khoa < Bvdhyd::Base

    def self.getall(limit = 10, offset = 0)
        params = {
            table_name: "mtb_DM_DonVi",
            limit: limit,
            offset: offset
        }
        return self.send_get(BVDHYD_API_GETALL_KHOA, params)
    end
end