require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Room < Bvdhyd::Base

    def self.getall(limit = 10, offset = 0)
        params = {
            table_name: "mtb_DM_DonViCT",
            limit: limit,
            offset: offset
        }
        return self.send_get(BVDHYD_API_GETALL_ROOM, params)
    end
end