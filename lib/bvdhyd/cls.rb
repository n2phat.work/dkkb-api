require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Cls < Bvdhyd::Base
    
    def self.getall(limit = 10, offset = 0)
        params = {
            limit: limit,
            offset: offset,
            table_name: "utb_KetQuaCLS"
        }
        return self.send_get(BVDHYD_API_GETALL_CLS, params)
    end

    def self.getallindex(limit = 10, offset = 0)
        params = {
            limit: limit,
            offset: offset,
            table_name: "utb_CLS"
        }
        return self.send_get(BVDHYD_API_GETALL_CLS, params)
    end

    def self.get_by_sohs(sohs)
        params = {
            SoHS: sohs
        }
        return self.send_get(BVDHYD_API_CLS_GETBYSOHS, params)
    end
end