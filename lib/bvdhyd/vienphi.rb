require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Vienphi < Bvdhyd::Base
    def self.searchmulti(msbn = nil, msnv = nil)
        params = {
            SoHS: msbn,
            SoNV: msnv
        }
        return self.send_get(BVDHYD_API_SEARCHMULTI_VIENPHI_URL, params)
    end

    def self.searchsingle(sohoadon = nil)
        params = {
            SoPhieu: sohoadon
        }
        return self.send_get(BVDHYD_API_SEARCHSINGLE_VIENPHI_URL, params)
    end

    def self.detail(id = 0)
        # params = {
        #     id: id
        # }
        # return self.send_get(BVDHYD_API_DETAIL_DOCTOR_URL, params)
    end

    def self.search_history(msbn = nil)
        params = {
            sohs: msbn
        }
        return self.send_get(BVDHYD_API_SEARCH_HISTORY_URL, params)
    end

    def self.insert_thanhtoan(data = {})
        params = {
            IDVienPhi: data[:id],
            SoHS: data[:msbn],
            SoNV: data[:msnv],
            Ngay: data[:date],
            TraiBenh: data[:subject_code],
            SoTien: data[:amount],
            GhiChu: data[:note],
            TrangThai: data[:status],
            TransactionID_TT: data[:transaction_code_tt]
        }
        return self.send_post(BVDHYD_API_VIENPHI_INSERT_TT, params)
    end

    def self.update_tamung(data = {})
        params = {
            id: data[:id],
            status: data[:status]
        }
        return self.send_get(BVDHYD_API_UPDATE_TAMUNG_URL, params)
    end
end