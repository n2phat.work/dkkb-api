require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Doctor < Bvdhyd::Base
    def self.getall(limit = 10, offset = 0)
        params = {
            limit: limit,
            offset: offset,
            table_name: "mtb_DM_BacSi"
        }
        return self.send_get(BVDHYD_API_GETALL_DOCTOR_URL, params)
    end

    def self.detail(id = 0)
        # params = {
        #     id: id
        # }
        # return self.send_get(BVDHYD_API_DETAIL_DOCTOR_URL, params)
    end
end