class Bvdhyd::Slottime
    def initialize(options = {})
        @start_time = Chronic.parse(options[:strTime])
        @max_slot = options[:max_slot].to_i
        @time_per_slot = options[:time_per_slot].to_i
        @start_number = options[:start_number]
        if options[:first].to_i == 0 && options[:second].to_i == 0
            @step = 1
        else
            @step = options[:first].to_i + options[:second].to_i
        end

        @@sang = [
            { from: "6", to: "7", calc_from: "06:00", calc_to: "06:59", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "7", to: "8", calc_from: "07:00", calc_to: "07:59", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "8", to: "9", calc_from: "08:00", calc_to: "08:59", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "9", to: "10", calc_from: "09:00", calc_to: "09:59", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "10", to: "11", calc_from: "10:00", calc_to: "10:59", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "11", to: "12", calc_from: "11:00", calc_to: "12:00", max_slot: 0, number_from: nil, number_to: nil, step: nil }
        ]
        @@chieu = [
            { from: "13.5", to: "14.5", calc_from: "13:00", calc_to: "14:30", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "14.5", to: "15.5", calc_from: "14:31", calc_to: "15:30", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            { from: "15.5", to: "16.5", calc_from: "15:31", calc_to: "16:30", max_slot: 0, number_from: nil, number_to: nil, step: nil },
            # { from: "16.5", to: "17.5", calc_from: "16:31", calc_to: "17:30", max_slot: 0, number_from: nil, number_to: nil, step: nil }
        ]
    end

    def get_list
        # @start_time = "2018-01-01 00:00:00"
        time = @start_time
        (@start_number .. @start_number + @step * @max_slot - @step).step(@step).each do |e|
            time = time.strftime('%H:%M')
            if time <= "11:59"
                @@sang.each_with_index do |s, k|
                    if time.to_s >= s[:calc_from] && time.to_s <= s[:calc_to]
                        @@sang[k][:max_slot] += 1
                        @@sang[k][:number_from] = @@sang[k][:number_from].nil? ? e : @@sang[k][:number_from]
                        @@sang[k][:number_to] = e
                        @@sang[k][:step] = @step
                    end
                end
            else
                @@chieu.each_with_index do |s, k|
                    if time.to_s >= s[:calc_from] && time.to_s <= s[:calc_to]
                        @@chieu[k][:max_slot] += 1
                        @@chieu[k][:number_from] = @@chieu[k][:number_from].nil? ? e : @@chieu[k][:number_from]
                        @@chieu[k][:number_to] = e
                        @@chieu[k][:step] = @step
                    end
                end
            end
            time = Chronic.parse(time) + @time_per_slot.minutes
        end
        return {
            sang: @@sang,
            chieu: @@chieu
        }
    end

    def get_sang
        ap @@sang
    end

    def get_chieu
        ap @@chieu
    end
end