require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Toathuoc < Bvdhyd::Base
    def self.getall(limit = 10, offset = 0)
        params = {
            limit: limit,
            offset: offset,
            table_name: "utb_Toathuoc"
        }
        return self.send_get(BVDHYD_API_GETALL_TOATHUOC, params)
    end

    def self.get_by_sohs(sohs)
        params = {
            SoHS: sohs
        }
        return self.send_get(BVDHYD_API_TOATHUOC_GETBYSOHS, params)
    end
end