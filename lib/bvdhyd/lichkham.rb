require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Lichkham < Bvdhyd::Base

    def self.get_dutru_kham(limit = 10, offset = 0)
        params = {
            table_name: 'mtb_DM_STTKB',
            limit: limit,
            offset: offset
        }
        return self.send_get(BVDHYD_API_GETALL_DUTRUKHAM, params)
    end

    def self.getall_lich_bs(limit = 10, offset = 0)
        params = {
            table_name: 'mtb_LichKhamBenh',
            limit: limit,
            offset: offset
        }
        return self.send_get(BVDHYD_API_GETALL_LICH_DOCTOR_URL, params)
    end

    def self.getall_lich_bsthaythe(limit = 10, offset = 0)
        params = {
            table_name: 'mtb_LichKhamThe',
            limit: limit,
            offset: offset
        }
        return self.send_get(BVDHYD_API_GETALL_LICH_DOCTOR_THAYTHE_URL, params)
    end

    def self.getbookedDSK(data = {})
        params = {
            MaDonVi: data[:khoa_code],
            RIDBuoiKham: data[:buoi],
            IDBS: data[:doctor_id],
            NgayKham: data[:booking_date]
        }
        return self.send_get(BVDHYD_API_DATLICH_GETBOOKEDDSK, params)
    end

    def self.insertDSK(data = {})
        params = {
            MaDonVi: data[:khoa_code],
            RIDBuoiKham: data[:buoi],
            IDBS: data[:doctor_id],
            NgayKham: data[:booking_date],
            SoTT: data[:booking_number],
            IDDonViCT: data[:bvdhyd_room_id],
            GioKham: data[:gio_kham],
            TransactionID_GD: data[:transaction_code_gd],
            TransactionID_TT: data[:transaction_code_tt],
            Loai: data[:loai] ? data[:loai] : 3,
            CDate: Time.now.strftime("%Y-%m-%d %H:%M:%S")
        }
        return self.send_post(BVDHYD_API_DATLICH_INSERTDSK, params)
    end

    def self.deleteDSK(data = {})
        params = {
            transaction_code_gd: data[:transaction_code_gd]
        }
        return self.send_get(BVDHYD_API_DATLICH_DELETEDSK,params)
    end

    def self.insertTT(data = {})
        params = {
            TransactionID_TT: data[:transaction_code_tt],
            SoTienThucThu: data[:real_price],
            MaThe: data[:bank_code],
            LoaiThe: data[:loai_the],
            NgayGioTT: data[:date_update],
            PhiDichVu: data[:medpro_price],
            TrangThai: data[:status],
            TongTien: data[:total_price]
        }
        return self.send_post(BVDHYD_API_DATLICH_INSERTTT, params)
    end

    def self.insertDL(data = {})
        params = {
            NgayKham: data[:booking_date],
            SoHS: data[:bvdhyd_msbn],
            RIDBuoiKham: data[:buoi],
            MaDonVi: data[:ma_khoa],
            IDBS: data[:doctor_id],
            SoTT: data[:booking_number],
            Ho: data[:surname],
            NamSinh: data[:birthyear],
            GioiTinh: data[:sex],
            BHYT: data[:bhyt],
            GhiChu: data[:note],
            Ten: data[:name],
            NgaySinh: data[:birthdate],
            SoTien: data[:price],
            TrangThai: data[:status],
            MedProID: data[:medpro_id],
            IDDonViCT: data[:bvdhyd_room_id],
            DiaChi: data[:address],
            DienThoai: data[:mobile],
            MaQuocGia: data[:country_code],
            IDTinh: data[:city_id],
            IDQuanHuyen: data[:district_id],
            IDPhuongXa: data[:ward_id],
            IDNgheNghiep: data[:profession_id],
            IDDanToc: data[:dantoc_id],
            Email: data[:email],
            TransactionID_GD: data[:transaction_code_gd],
            TransactionID_TT: data[:transaction_code_tt],
            GioKham: data[:bv_booking_time]
        }
        return self.send_post(BVDHYD_API_DATLICH_INSERTDL, params)
    end
end