class Bvdhyd::TimeCalc
    @start_time = nil
    @max_slot = 0
    @time_per_slot = 0
    @step = 0
    @start_number = 0
    M = 60

    def initialize(options = {})
        # strTime, start_number = 1, max_slot = 0, time_per_slot = 0, first = 0, second = 0
        @start_time = Chronic.parse(options[:strTime])
        @max_slot = options[:max_slot].to_i
        @time_per_slot = options[:time_per_slot].to_i
        @start_number = options[:start_number]
        if options[:first].to_i == 0 && options[:second].to_i == 0
            @step = 1
        else
            @step = options[:first].to_i + options[:second].to_i
        end
    end

    def get_start_time
        @start_time
    end

    def get_slot_list
        arrReturn = []
        # slot = (M / @time_per_slot).to_i
        # if slot <= @max_slot
            obj = {
                from: time_to_float(@start_time).round(0),
                to: time_to_float(@start_time + (@time_per_slot * @max_slot).to_i.minutes).round(0),
                number_from:  @start_number,
                number_to: @start_number + (@step * (@max_slot - 1)),
                max_slot: @max_slot,
                step: @step,
                time_per_slot: @time_per_slot
            }
            offset_time = obj[:to] - obj[:from]
            if  offset_time > 1
                offset_slot = (@max_slot.to_f / offset_time.to_f).to_i
                left_slot = obj[:max_slot]
                from = obj[:number_from]
                hour_from = obj[:from]
                while offset_time > 0
                    r = {
                        from: hour_from,
                        to: hour_from + 1,
                        number_from: from,
                        # number_to: from + ((@step * ((hour_from + 1 == obj[:to]) ? left_slot : offset_slot)) - @step) - @step,
                        number_to: from + ((((hour_from + 1 == obj[:to]) ? left_slot : offset_slot) - 1) * @step),
                        step: @step,
                        time_per_slot: @time_per_slot
                    }
                    r[:max_slot] = count_slot(r[:number_from],r[:number_to],r[:step])
                    arrReturn << r
                    left_slot = left_slot - offset_slot
                    hour_from = r[:to]
                    from = r[:number_to] + @step
                    offset_time = offset_time - 1
                end
            else
                arrReturn << obj
            end
            return arrReturn
        # else
        #     while slot > 0
        #         ap slot
        #         slot = slot - @max_slot
        #     end
        # end
    end

    def count_slot(from, to, step)
        return ((to.to_i - from.to_i) / step.to_i).to_i + 1
    end

    def time_to_float(time)
        hour = time.strftime("%H").to_i
        minute = time.strftime("%M").to_i
        return (minute % 60) != 0 ? (hour.to_f + (minute.to_f / 60).round(1)) : hour
    end
end