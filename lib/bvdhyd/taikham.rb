require "#{Rails.root}/lib/bvdhyd/define.rb"

class Bvdhyd::Taikham < Bvdhyd::Base
    def self.getall(limit = 10, offset = 0)
        params = {
            limit: limit,
            offset: offset,
            table_name: "mtb_LichHenTaiKham"
        }
        return self.send_get(BVDHYD_API_GETALL_TAIKHAM_URL, params)
    end
end