class Payoo::MainOffline

    def self.create_offline_order(params = {})
        expired_date = (params[:expired_date] ? params[:expired_date] : (Time.now + 1.days)).to_time
        data = {
            OrderNo: params[:transaction_code_tt],
            ShopID: PKB_PAYOO_SHOP_ID,
            FromShipDate: Time.now.strftime('%d/%m/%Y').to_s,
            ShipNumDay: 1.to_s,
            Description: params[:description] ? params[:description] : 'Description',
            CyberCash: params[:amount],
            PaymentExpireDate: expired_date.strftime('%Y%m%d%H%M%S'),
            NotifyUrl: PKB_PAYOO_NOTI_URL,
            InfoEx: ::Util.url_encode("<InfoEx><CustomerEmail>phatn1@yahoo.com</CustomerEmail><Title>Thanh toán payoo</Title> <CustomerPhone>01698665336</CustomerPhone><CustomerName>PP</CustomerName>><CustomerAddress>97 Dien Bien Phu</CustomerAddress></InfoEx>")
        }
        checksum = make_checksum(data.to_json)
        ap checksum
        requestData = {
            RequestData: data.to_json,
            Signature: checksum
        }
        uri = URI.parse("#{PKB_PAYOO_BUSINESS_URL}/GetBillingCode")
        http = Net::HTTP.new(uri.host,uri.port)
        if Rails.env == 'development'
            http.set_debug_output $stderr
        end
        http.read_timeout = 20

        if true
            http.use_ssl = true
            #unless Rails.env == 'production'
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            #end
        end

        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = requestData.to_json
        request["Content-Type"] = "application/json"
        request["APIUsername"] = PKB_PAYOO_API_USERNAME
        request["APIPassword"] = PKB_PAYOO_API_PASSWORD
        request["APISignature"] = PKB_PAYOO_API_SIGNATURE

        response = http.request(request)
        ap response.body
        re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
        code = nil
        if re["ResponseData"]
            tmp = ::Util.json_decode(re["ResponseData"])
            code = tmp["BillingCode"] ? tmp["BillingCode"] : nil
        end
        return {
            code: code,
            expired_date: expired_date.strftime("%Y-%m-%d %H:%M:%S")
        }
    end

    def self.ipnchecksum(str)
        data = Hash.from_xml(str)
        obj = nil
        hash_obj = nil
        signature = data["PayooConnectionPackage"]["Signature"]
        arr_key = data["PayooConnectionPackage"]["KeyFields"].split("|")
        if data["PayooConnectionPackage"]["Data"]
            obj = ::Util.base64_decode(data["PayooConnectionPackage"]["Data"])
            if obj
                hash_obj = Hash.from_xml(obj)
                arr_checksum = []
                if hash_obj.key?("PaymentNotification")
                    if hash_obj["PaymentNotification"].key?("shops")
                        tag = nil
                        arr_key.each do |e|
                            case e
                            when "PaymentMethod"
                                arr_checksum << hash_obj["PaymentNotification"]["PaymentMethod"]
                            when "State"
                                arr_checksum << hash_obj["PaymentNotification"]["State"]
                            when "Session"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["session"]
                            when "BusinessUsername"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["username"]
                            when "ShopID"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_id"]
                            when "ShopTitle"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_title"]
                            when "ShopDomain"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_domain"]
                            when "ShopBackUrl"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_back_url"]
                            when "OrderNo"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_no"]
                            when "OrderCashAmount"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_cash_amount"]
                            when "StartShippingDate"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_ship_date"]
                            when "ShippingDays"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_ship_days"]
                            when "OrderDescription"
                                arr_checksum << ::Util.url_decode(hash_obj["PaymentNotification"]["shops"]["shop"]["order_description"])
                            when "NotifyUrl"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["notify_url"]
                            when "PaymentExpireDate"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["validity_time"]
                            end
                        end
                    elsif hash_obj["PaymentNotification"].key?("ShopId")
                        tag = 'offline'
                        arr_key.each do |e|
                        case e #State|ShopID|OrderNo|OrderCashAmount|ShippingDays|BillingCode
                            when "PaymentMethod"
                                arr_checksum << hash_obj["PaymentNotification"]["PaymentMethod"]
                            when "State"
                                arr_checksum << hash_obj["PaymentNotification"]["State"]
                            when "ShopID"
                                arr_checksum << hash_obj["PaymentNotification"]["ShopId"]
                            when "OrderNo"
                                arr_checksum << hash_obj["PaymentNotification"]["OrderNo"]
                            when "OrderCashAmount"
                                arr_checksum << hash_obj["PaymentNotification"]["OrderCashAmount"]
                            when "ShippingDays"
                                arr_checksum << hash_obj["PaymentNotification"]["ShippingDays"].to_i
                            when "BillingCode"
                                arr_checksum << hash_obj["PaymentNotification"]["BillingCode"]
                            end
                        end
                    end
                end
                # return arr_checksum
                str_checksum = PKB_PAYOO_SECRET_KEY + "|" + arr_checksum.join("|")
                checksum = ::Util.sha512(str_checksum)
                if signature.downcase == checksum.downcase
                    if tag
                        return {
                            session: hash_obj["PaymentNotification"]["OrderNo"],
                            amount: hash_obj["PaymentNotification"]["OrderCashAmount"],
                            status: hash_obj["PaymentNotification"]["State"],
                            order_no: hash_obj["PaymentNotification"]["OrderNo"]
                        }
                    else
                        return {
                            session: hash_obj["PaymentNotification"]["shops"]["shop"]["session"],
                            amount: hash_obj["PaymentNotification"]["shops"]["shop"]["order_cash_amount"],
                            status: hash_obj["PaymentNotification"]["State"],
                            order_no: hash_obj["PaymentNotification"]["shops"]["shop"]["order_no"]
                        }
                    end
                else
                    return nil
                end
            end
        end
        return nil
    end

    def self.make_checksum(str)
        ::Util.sha512(PKB_PAYOO_SECRET_KEY + str)
    end
end