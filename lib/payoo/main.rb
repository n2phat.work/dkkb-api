class Payoo::Main
    def self.get_payment_url(params = {})
        obj = {
            shop: {
                session: Time.now.to_i.to_s,
                username: PAYOO_ACCOUNT_NAME,
                shop_id: PAYOO_SHOP_ID,
                shop_title: PAYOO_SHOP_TITLE,
                shop_domain: PAYOO_SHOP_DOMAIN,
                shop_back_url: PAYOO_POSTBACK_PAYMENT,
                order_no: Time.now.to_i.to_s,
                order_cash_amount: 1000.to_s,
                order_ship_date: Time.now.strftime('%d/%m/%Y').to_s,
                order_ship_days: 0.to_s,
                # order_description: params[:description] ? params[:description] : 'Description',
                order_description: self.make_description({
                    sohoadon: params[:description],
                    patient_name: params[:patient_name],
                    msbn: params[:msbn],
                    amount: params[:amount]
                }),
                validity_time: (Time.now + 1.days).strftime('%Y%m%d%H%M%S').to_s,
                notify_url: PAYOO_NOTI_URL,
                customer: {
                    name: PAYOO_CUS_NAME,
                    phone: PAYOO_CUS_PHONE,
                    address: PAYOO_CUS_ADDRESS,
                    city: PAYOO_CUS_CITY,
                    email: PAYOO_CUS_EMAIL
                }
            }
        }

        if params[:amount]
            obj[:shop][:order_cash_amount] = params[:amount].to_s
        else
            return nil
        end

        if params[:transaction_code_tt]
            obj[:shop][:session] = params[:transaction_code_tt]
        else
            return nil
        end
        xml = obj.to_xml(:skip_instruct => true, :root => 'shops',:indent => 0,:dasherize => false)
        # ap xml
        postData = {
            data: xml,
            checksum: self.make_checksum(xml),
            refer: BASE_URL_PAYOO,
            method: 'bank-payment'
        }
        # ap postData
        if params[:code]
            postData[:bc] = params[:code]
        end
        ap postData
        post = ::Util.curl_form_post(PAYOO_PAYMENT_URL, postData, true,{
            "Referer" => BASE_URL_PAYOO,
            "Origin" => BASE_URL_PAYOO
        })
        ap post
        if post["result"] == 'success'
            return post["order"]["payment_url"]
        end
        return nil
    end

    def self.create_order(params = {})
        obj = {
            shop: {
                session: Time.now.to_i.to_s,
                username: PAYOO_ACCOUNT_NAME,
                shop_id: PAYOO_SHOP_ID,
                shop_title: PAYOO_SHOP_TITLE,
                shop_domain: PAYOO_SHOP_DOMAIN + "/web",
                shop_back_url: PAYOO_POSTBACK_PAYMENT_VP,
                order_no: Time.now.to_i.to_s,
                order_cash_amount: 1000.to_s,
                order_ship_date: Time.now.strftime('%d/%m/%Y').to_s,
                order_ship_days: 0.to_s,
                # order_description: params[:description] ? params[:description] : 'Description',
                order_description: self.make_description({
                    sohoadon: params[:description],
                    patient_name: params[:patient_name],
                    msbn: params[:msbn],
                    amount: params[:amount]
                }),
                validity_time: (Time.now + 1.days).strftime('%Y%m%d%H%M%S').to_s,
                notify_url: PAYOO_NOTI_URL,
                customer: {
                    name: PAYOO_CUS_NAME,
                    phone: PAYOO_CUS_PHONE,
                    address: PAYOO_CUS_ADDRESS,
                    city: PAYOO_CUS_CITY,
                    email: PAYOO_CUS_EMAIL
                }
            }
        }

        if params[:amount]
            obj[:shop][:order_cash_amount] = params[:amount].to_s
        else
            return nil
        end

        if params[:transaction_code_tt]
            obj[:shop][:session] = params[:transaction_code_tt]
        else
            return nil
        end
        xml = obj.to_xml(:skip_instruct => true, :root => 'shops',:indent => 0,:dasherize => false)
        # ap xml
        postData = {
            data: xml,
            checksum: self.make_checksum(xml),
            refer: BASE_URL_PAYOO,
            # method: 'cc-payment'
            method: 'bank-payment'
        }
        # ap postData
        if params[:code]
            postData[:bc] = params[:code]
        end
        if ['VISA','MASTER','JCB'].include?(params[:code])
            postData[:method] = 'cc-payment'
        end
        ap postData
        post = ::Util.curl_form_post(PAYOO_PAYMENT_URL, postData, true,{
            "Referer" => BASE_URL_PAYOO,
            "Origin" => BASE_URL_PAYOO
        })
        ap post
        if post["result"] == 'success'
            return post["order"]
        end
        return nil
    end

    def self.create_mobile_order(params = {})
        data = {
            shop: {
                session: Time.now.to_i.to_s,
                username: PAYOO_ACCOUNT_NAME,
                shop_id: PAYOO_SHOP_ID,
                shop_title: PAYOO_SHOP_TITLE,
                shop_domain: PAYOO_SHOP_DOMAIN,
                # shop_back_url: PAYOO_POSTBACK_PAYMENT_VP,
                order_no: Time.now.to_i.to_s,
                order_cash_amount: 1000.to_s,
                order_ship_date: Time.now.strftime('%d/%m/%Y').to_s,
                order_ship_days: 0.to_s,
                # order_description: params[:description] ? params[:description] : 'Description',
                order_description: self.make_description({
                    sohoadon: params[:description],
                    patient_name: params[:patient_name],
                    msbn: params[:msbn],
                    amount: params[:amount]
                }),
                validity_time: (Time.now + 1.days).strftime('%Y%m%d%H%M%S').to_s,
                notify_url: PAYOO_NOTI_URL,
                customer: {
                    name: nil,
                    phone: nil,
                    address: nil,
                    city: nil,
                    email: nil
                }
            }
        }
        if params[:amount]
            data[:shop][:order_cash_amount] = params[:amount].to_s
        else
            return nil
        end

        if params[:transaction_code_tt]
            data[:shop][:session] = params[:transaction_code_tt]
        else
            return nil
        end

        xml = data.to_xml(:skip_instruct => true, :root => 'shops',:indent => 0,:dasherize => false)
        checksum = self.make_checksum(xml)
        return {
            orderInfo: xml,
            checksum: checksum
        }
    end

    def self.create_mpos_order(params = {})
        pattern = {
            MerchantName: 'sangnguyen',
            PurchaseOrderNo: params[:transaction_code_tt],
            MoneyTotal: params[:amount],
            PayExpireDate: (Time.now + 1.days).strftime('%Y%m%d%H%M%S').to_s,
            CustomerFullName: PAYOO_CUS_NAME,
            CustomerAddress: PAYOO_CUS_ADDRESS,
            CustomerPhone: PAYOO_CUS_PHONE,
            CustomerEmail: PAYOO_CUS_EMAIL,
            Note: "",
            Description: params[:description] ? params[:description] : 'Description',
            IsCreated: false,
            PaymentMethodType: -1
        }
        str = params[:operation_name].to_s + pattern.to_json + '123456'
        checksum = ::Util.sha512(str)
        return {
            Checksum: checksum,
            RequestData: pattern
        }
    end

    def self.getorderinfo(id)
        post_data = {
            OrderID: id,
            ShopId: PAYOO_SHOP_ID
        }

        uri = URI.parse("#{PAYOO_BUSINESS_URL}/GetOrderInfo")
        http = Net::HTTP.new(uri.host,uri.port)
        if Rails.env == 'development'
            http.set_debug_output $stderr
        end
        http.read_timeout = 20

        if true
            http.use_ssl = true
            #unless Rails.env == 'production'
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            #end
        end

        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = {
            RequestData: post_data.to_json, 
            Signature: self.make_checksum(post_data.to_json)
        }.to_json
        request["Content-Type"] = "application/json"
        request["APIUsername"] = PAYOO_API_USERNAME
        request["APIPassword"] = PAYOO_API_PASSWORD
        request["APISignature"] = PAYOO_API_SIGNATURE

        response = http.request(request)
        ap response.body
        re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
        
        return re
    end

    def self.create_offline_order(params = {})
        expired_date = (Time.now + 1.days)
        data = {
            OrderNo: params[:transaction_code_tt],
            ShopID: PAYOO_SHOP_ID,
            FromShipDate: Time.now.strftime('%d/%m/%Y').to_s,
            ShipNumDay: 1.to_s,
            # Description: params[:description] ? params[:description] : 'Description',
            Description: self.make_description({
                sohoadon: params[:description],
                patient_name: params[:patient_name],
                msbn: params[:msbn],
                amount: params[:amount]
            }),
            CyberCash: params[:amount],
            PaymentExpireDate: expired_date.strftime('%Y%m%d%H%M%S').to_s,
            NotifyUrl: PAYOO_NOTI_URL,
            InfoEx: ::Util.url_encode("<InfoEx><CustomerEmail>phatn1@yahoo.com</CustomerEmail><Title>Thanh toán viện phí</Title> <CustomerPhone>01698665336</CustomerPhone><CustomerName>PP</CustomerName>><CustomerAddress>97 Dien Bien Phu</CustomerAddress></InfoEx>")
        }
        checksum = make_checksum(data.to_json)
        ap checksum
        requestData = {
            RequestData: data.to_json,
            Signature: checksum
        }
        uri = URI.parse("#{PAYOO_BUSINESS_URL}/GetBillingCode")
        http = Net::HTTP.new(uri.host,uri.port)
        if Rails.env == 'development'
            http.set_debug_output $stderr
        end
        http.read_timeout = 20

        if true
            http.use_ssl = true
            #unless Rails.env == 'production'
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            #end
        end

        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = requestData.to_json
        request["Content-Type"] = "application/json"
        request["APIUsername"] = PAYOO_API_USERNAME
        request["APIPassword"] = PAYOO_API_PASSWORD
        request["APISignature"] = PAYOO_API_SIGNATURE

        response = http.request(request)
        ap response.body
        re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
        code = nil
        if re["ResponseData"]
            tmp = ::Util.json_decode(re["ResponseData"])
            code = tmp["BillingCode"] ? tmp["BillingCode"] : nil
        end
        return {
            code: code,
            expired_date: expired_date.strftime("%Y-%m-%d %H:%M:%S")
        }
    end

    def self.ipnchecksum(str)
        data = Hash.from_xml(str)
        obj = nil
        hash_obj = nil
        signature = data["PayooConnectionPackage"]["Signature"]
        arr_key = data["PayooConnectionPackage"]["KeyFields"].split("|")
        if data["PayooConnectionPackage"]["Data"]
            obj = ::Util.base64_decode(data["PayooConnectionPackage"]["Data"])
            if obj
                hash_obj = Hash.from_xml(obj)
                arr_checksum = []
                if hash_obj.key?("PaymentNotification")
                    if hash_obj["PaymentNotification"].key?("shops")
                        tag = nil
                        arr_key.each do |e|
                            case e
                            when "PaymentMethod"
                                arr_checksum << hash_obj["PaymentNotification"]["PaymentMethod"]
                            when "State"
                                arr_checksum << hash_obj["PaymentNotification"]["State"]
                            when "Session"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["session"]
                            when "BusinessUsername"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["username"]
                            when "ShopID"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_id"]
                            when "ShopTitle"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_title"]
                            when "ShopDomain"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_domain"]
                            when "ShopBackUrl"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["shop_back_url"]
                            when "OrderNo"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_no"]
                            when "OrderCashAmount"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_cash_amount"]
                            when "StartShippingDate"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_ship_date"]
                            when "ShippingDays"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["order_ship_days"]
                            when "OrderDescription"
                                arr_checksum << ::Util.url_decode(hash_obj["PaymentNotification"]["shops"]["shop"]["order_description"])
                            when "NotifyUrl"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["notify_url"]
                            when "PaymentExpireDate"
                                arr_checksum << hash_obj["PaymentNotification"]["shops"]["shop"]["validity_time"]
                            end
                        end
                    elsif hash_obj["PaymentNotification"].key?("ShopId")
                        tag = 'offline'
                        arr_key.each do |e|
                        case e #State|ShopID|OrderNo|OrderCashAmount|ShippingDays|BillingCode
                            when "PaymentMethod"
                                arr_checksum << hash_obj["PaymentNotification"]["PaymentMethod"]
                            when "State"
                                arr_checksum << hash_obj["PaymentNotification"]["State"]
                            when "ShopID"
                                arr_checksum << hash_obj["PaymentNotification"]["ShopId"]
                            when "OrderNo"
                                arr_checksum << hash_obj["PaymentNotification"]["OrderNo"]
                            when "OrderCashAmount"
                                arr_checksum << hash_obj["PaymentNotification"]["OrderCashAmount"]
                            when "ShippingDays"
                                arr_checksum << hash_obj["PaymentNotification"]["ShippingDays"].to_i
                            when "BillingCode"
                                arr_checksum << hash_obj["PaymentNotification"]["BillingCode"]
                            end
                        end
                    end
                end
                # return arr_checksum
                str_checksum = PAYOO_SECRET_KEY + "|" + arr_checksum.join("|")
                checksum = ::Util.sha512(str_checksum)
                if signature.downcase == checksum.downcase
                    if tag
                        return {
                            session: hash_obj["PaymentNotification"]["OrderNo"],
                            amount: hash_obj["PaymentNotification"]["OrderCashAmount"],
                            status: hash_obj["PaymentNotification"]["State"],
                            order_no: hash_obj["PaymentNotification"]["OrderNo"]
                        }
                    else
                        return {
                            session: hash_obj["PaymentNotification"]["shops"]["shop"]["session"],
                            amount: hash_obj["PaymentNotification"]["shops"]["shop"]["order_cash_amount"],
                            status: hash_obj["PaymentNotification"]["State"],
                            order_no: hash_obj["PaymentNotification"]["shops"]["shop"]["order_no"]
                        }
                    end
                else
                    return nil
                end
            end
        end
        return nil
    end

    def self.make_checksum(str)
        ::Util.sha512(PAYOO_SECRET_KEY + str)
    end

    def self.make_description(params = {})
        str = ''
        str += '<table border="1">'
        str += "<tr><td>Hóa đơn</td><td>Viện phí BV DH Y DƯỢC</td></tr>"
        str += "<tr><td>Mã thanh toán</td><td>#{params[:sohoadon]}</td></tr>"
        str += "<tr><td>Tên khách hàng</td><td>#{params[:patient_name]} (#{params[:msbn]})</td></tr>"
        str += "<tr><td>Số tiền</td><td>#{params[:amount]}</td></tr>"
        str += '</table>'
        return ::Util.url_encode(str)
    end
end