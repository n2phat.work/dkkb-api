module Cybersource
    class Main
        SECRET_KEY = CYBS_SECRET
        def self.pay(params= {})
            if !params[:amount] || !params[:transaction_code_tt]
                return nil
            end
            data = {
                access_key: CYBS_ACCESS_KEY,
                amount: params[:amount],
                currency: 'vnd',
                locale: 'vi-VN',
                profile_id: CYBS_PROFILE_ID,
                reference_number: params[:transaction_code_tt],
                signature: nil,
                signed_date_time: nil,
                signed_field_names: 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency',
                transaction_type: 'sale',
                transaction_uuid: SecureRandom.hex(16),
                unsigned_field_names: nil,
                # bill_to_address_city: "Ho Chi Minh",
                # bill_to_address_country: "VN",
                # bill_to_address_line1: "97 Dien Bien Phu",
                # bill_to_address_line2: "97 Dien Bien Phu",
                # bill_to_company_name: "MP",
                # bill_to_email: "no-reply@medpro.com.vn",
                # bill_to_forename: "User",
                # bill_to_surname: "U",
                # bill_to_phone: "+841698665336",
                # ship_to_address_city: "HCM",
                # ship_to_address_country: "VN",
                # ship_to_address_line1: "97 Dien Bien Phu",
                # ship_to_address_line2: "97 Dien Bien Phu",
                # ship_to_company_name: "MP",
                # ship_to_forename: "User",
                # ship_to_surname: "U",
                # ship_to_phone: "+841698665336"
            }
            current_utc_xml_date_time = Time.now.utc.strftime "%Y-%m-%dT%H:%M:%S%z"
            current_utc_xml_date_time = current_utc_xml_date_time[0, current_utc_xml_date_time.length-5]
            current_utc_xml_date_time << 'Z'
            data[:signed_date_time] = current_utc_xml_date_time
            data[:signature] = ::Cybersource::Security.generate_signature(data, SECRET_KEY)
            return data
        end
    end
end