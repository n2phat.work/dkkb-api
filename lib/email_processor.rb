require 'base64'

class EmailProcessor
    def initialize(email)
      @email = email
    end
  
    def process
      arr_file = []
      TestEmail.new do |e|
        e.from = @email.from[:email]
        e.to = @email.to.map{|t| t[:email]}.join(",")
        e.cc = @email.cc.map{|t| t[:email]}.join(",")
        e.bcc = @email.bcc.map{|t| t[:email]}.join(",")
        e.body = @email.body
        # e.raw_body = @email.raw_body
        e.subject = @email.subject
        @email.attachments.each do |f|
          File.open(f.path, 'r') do |h|
            arr_file << Base64.strict_encode64(h.read)
          end
        end
        e.attachment = arr_file.join(",")
        e.save!
      end
    end
  end