class Vnpay::Main
    def self.lookup_payment(params = {})
        data = {
            vnp_Version: '2.0.0',
            vnp_Command: 'querydr',
            vnp_TxnRef: params[:transaction_code_tt],
            vnp_OrderInfo: params[:info] ? params[:info] : "lookup-" + ::Util.md5(::Util.milisec),
            vnp_TransDate: params[:create_date] ? params[:create_date] : (::Util.getCurrentDateTime),
            vnp_CreateDate: ::Util.getCurrentDateTime,
            vnp_IpAddr: '127.0.0.1'
        }
        if params[:vnp_tmncode]
            data[:vnp_TmnCode] = params[:vnp_tmncode]
        else
            return nil
        end

        if params[:vnp_secret]
            data[:vnp_secret] = params[:vnp_secret]
        else
            return nil
        end

        return self.make_link(data)
    end

    def self.cancel_payment(params = {})

    end

    def self.get_payment_url(params = {})
        data = {
            vnp_Version: '2.0.0',
            vnp_Amount: (params[:amount] ? params[:amount] : 1000).to_i * VNPAY_MULTIPLIER,
            vnp_Command: 'pay',
            vnp_CreateDate: params[:create_date] ? params[:create_date] : (::Util.getCurrentDateTime),
            vnp_CurrCode: 'VND',
            vnp_IpAddr: '127.0.0.1',
            vnp_Locale: 'vn',
            vnp_OrderInfo: params[:info] ? params[:info] : "info-" + ::Util.md5(::Util.milisec),
            vnp_OrderType: 270001,
            vnp_ReturnUrl: PAYMENT_POSTBACK_URL
        }
        if params[:transaction_code_tt]
            data[:vnp_TxnRef] = params[:transaction_code_tt]
        else
            return nil
        end

        if params[:vnp_tmncode]
            data[:vnp_TmnCode] = params[:vnp_tmncode]
        else
            return nil
        end

        if params[:vnp_secret]
            data[:vnp_secret] = params[:vnp_secret]
        else
            return nil
        end

        if params[:code]
            data[:vnp_BankCode] = params[:code]
        end
        # ap data
        return self.make_link(data)
    end

    def self.make_link(params = {})
        vnp_HashSecret = params[:vnp_secret] ? params[:vnp_secret] : VNP_HASHSECRET
        params = self.sort_hash(params)
        i = 0
        str = ''
        query = ''
        params.each do |k,v|
            if k.to_s != "vnp_secret"
                if i == 1
                    str += '&' + k.to_s + "=" + v.to_s
                else
                    str += k.to_s + "=" + v.to_s
                    i = 1
                end
                query += ::Util.url_encode(k.to_s) + "=" + ::Util.url_encode(v.to_s) + "&"
            end
        end
        url = VNPAY_PAYMENT_URL + "?" + query
        if vnp_HashSecret
            hash = ::Util.md5(vnp_HashSecret + str)
            url += "vnp_SecureHashType=MD5&vnp_SecureHash=" + hash
        end
        return url
    end

    def self.sort_hash(hash = {})
        data = {}
        tmp =  hash.sort_by { |k, v| k.to_s }
        tmp.each do |e|
            data[e[0]] = e[1]
        end
        return data
    end

    def self.check_hash(params = {})
        case params["vnp_TmnCode"]
        when VNPAY_TMN_CODE
            vnp_HashSecret = VNP_HASHSECRET
        when VNPAY_TMN_CODE_TKB
            vnp_HashSecret = VNP_HASHSECRET_TKB
        else
            vnp_HashSecret = VNP_HASHSECRET
        end

        data = {}
        i = 0
        str = ''
        params.each do |k,v|
            unless ["vnp_SecureHashType","vnp_SecureHash","controller","action","payment"].include?(k)
                if i == 1
                    str += '&' + k.to_s + "=" + v.to_s
                else
                    str += k.to_s + "=" + v.to_s
                    i = 1
                end
            end
        end
        hash = ::Util.md5(vnp_HashSecret + str)
        params["cal_hash"] = hash
        return params.deep_symbolize_keys
    end
end