require 'uri'
require 'net/http'
require 'net/https'

class Facebook::SMS
    FB_ACCESSTOKEN_URL = 'https://graph.accountkit.com/v1.1/access_token'
    FB_INFO_URL = 'https://graph.accountkit.com/v1.1/me'
    FB_APP_ID = '153422365349408'
    FB_KIT_SECRET = 'd813cf6426f250ce3d12835671223e6d'

    def self.get_user_info(code)
        uri = URI.parse(FB_INFO_URL)
        https = Net::HTTP.new(uri.host,uri.port)
        https.use_ssl = true
        json_access_token = self.get_access_token(code)
        if json_access_token["error"]
            return json_access_token
        else
            access_token = json_access_token["access_token"]
        end

        params = {
            :access_token => access_token
        }
        uri.query = URI.encode_www_form(params)
        res = Net::HTTP.get_response(uri)
        result = ActiveSupport::JSON.decode(res.body)
        if !result["error"]
            return result
        end
        return result
    end

    private
    def self.get_access_token(code)
        uri = URI.parse(FB_ACCESSTOKEN_URL)
        https = Net::HTTP.new(uri.host,uri.port)
        https.use_ssl = true
        params = {
            :grant_type => 'authorization_code',
            :code => code,
            :access_token => "AA|#{FB_APP_ID}|#{FB_KIT_SECRET}"
        }
        uri.query = URI.encode_www_form(params)
        res = Net::HTTP.get_response(uri)
        return ActiveSupport::JSON.decode(res.body)
    end
end
