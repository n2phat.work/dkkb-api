module Momo
    class Main
        def initialize
            @access_key   = MOMO_ACCESS_KEY
            @secret_key   = MOMO_SECRET_KEY
            @return_url   = MOMO_RETURN_URL
            @notify_url   = MOMO_NOTIFY_URL
            @partner_code = MOMO_PARTNER_CODE
        end

        def create_order(data = {})
            params = {
                :partnerCode => @partner_code,
                :accessKey   => @access_key,
                :requestId   => data[:transaction_code_tt],
                :amount      => data[:amount] ? data[:amount].to_s : "10000" ,
                :orderId     => data[:transaction_code_tt],
                :orderInfo   => data[:info] ? data[:info] : "info-" + ::Util.md5(::Util.milisec),
                :returnUrl   => @return_url,
                :notifyUrl   => @notify_url,
                :extraData   => data[:extra] ? data[:extra] : "extra data",
                :requestType => "captureMoMoWallet"
            }
            signature = create_order_params(params, @secret_key)
            params[:signature] = signature
            data = ::Util.curl_post(MOMO_END_POINT, params, true)
            return data
        end

        def order_status(data = {})
            params = {
                :partnerCode => @partner_code,
                :accessKey   => @access_key,
                :requestId   => data[:transaction_code_tt],
                :orderId     => data[:transaction_code_tt],
                :requestType => "transactionStatus"
            }
            signature = order_status_params(params, @secret_key)
            params[:signature] = signature
            data = ::Util.curl_post(MOMO_END_POINT, params, true)
            return data
        end

        def refund(data = {})
            params = {
                :partnerCode => @partner_code,
                :accessKey   => @access_key,
                :requestId   => data[:transaction_code_tt],
                :amount      => data[:amount],
                :orderId     => data[:transaction_code_tt],
                :transId     => data[:momo_id],
                :requestType => "refundMoMoWallet"
            }
            signature = refund_params(params, @secret_key)
            params[:signature] = signature
            data = ::Util.curl_post(MOMO_END_POINT, params, true)
            return data
        end

        def sdk_deeplink(data = {})
            return_data = {
                action: 'gettoken',
                partner: 'merchant',
                appScheme: 'momo1qnv20190417',
                amount: data[:amount] ? data[:amount] : 10000,
                description: "Thanh toán đơn hàng #{data[:transaction_code_tt]}",
                merchantcode: 'MOMO1QNV20190417',
                merchantname: "Phat Nguyen",
                merchantnamelabel: "Thanh toán cho Phat Nguyen",
                language: 'vi',
                fee: 0,
                username: "",
                orderLabel: "",
                orderId: "",
                extra: "",
            }
            return return_data
        end

        def refund_params(data = {}, serectkey)
            rawSignature = "partnerCode=#{data[:partnerCode]}&accessKey=#{data[:accessKey]}&requestId=#{data[:requestId]}&amount=#{data[:amount]}&orderId=#{data[:orderId]}&transId=#{data[:transId]}&requestType=#{data[:requestType]}"
            signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), serectkey, rawSignature)
            return signature
        end

        def order_status_params(data = {}, serectkey)
            rawSignature = "partnerCode=#{data[:partnerCode]}&accessKey=#{data[:accessKey]}&requestId=#{data[:requestId]}&orderId=#{data[:orderId]}&requestType=#{data[:requestType]}"
            signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), serectkey, rawSignature)
            return signature
        end

        def create_order_params(data = {}, serectkey)
            rawSignature = "partnerCode=#{data[:partnerCode]}&accessKey=#{data[:accessKey]}&requestId=#{data[:requestId]}&amount=#{data[:amount]}&orderId=#{data[:orderId]}&orderInfo=#{data[:orderInfo]}&returnUrl=#{data[:returnUrl]}&notifyUrl=#{data[:notifyUrl]}&extraData=#{data[:extraData]}"
            signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), serectkey, rawSignature)
            return signature
        end

        def self.payment_recieve_params_noti(data = {})
            #partnerCode=$partnerCode&accessKey=$accessKey&requestId=$requestId&orderId=$orderId&errorCode=$errorCode&message=$message&responseTime=responseTime&extraData=extraData
            rawSignature = "partnerCode=#{data[:partnerCode]}&accessKey=#{data[:accessKey]}&requestId=#{data[:requestId]}&orderId=#{data[:orderId]}&errorCode=#{data[:errorCode]}&message=#{data[:message]}&responseTime=#{data[:responseTime]}&extraData=#{data[:extraData]}"
            signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), MOMO_SECRET_KEY, rawSignature)
            return signature
        end

        def self.payment_recieve_params_postback(data = {})
            rawSignature = "partnerCode=#{data[:partnerCode]}&accessKey=#{data[:accessKey]}&requestId=#{data[:requestId]}&amount=#{data[:amount]}&orderId=#{data[:orderId]}&orderInfo=#{data[:orderInfo]}&orderType=#{data[:orderType]}&transId=#{data[:transId]}&message=#{data[:message]}&localMessage=#{data[:localMessage]}&responseTime=#{data[:responseTime]}&errorCode=#{data[:errorCode]}&payType=#{data[:payType]}&extraData=#{data[:extraData]}"
            ap rawSignature
            signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), MOMO_SECRET_KEY, rawSignature)
            return signature
        end
    end
end