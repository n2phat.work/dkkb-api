module Firebase
    FIREBASE_PROJECT_ID = 'medpro-1e443'

    class Main
        def self.decode_access_token(*args)
            data = args[0]
            access_token = data[:access_token]
            begin
                decoded_token = JWT.decode access_token, nil, false,
                {
                    verify_iat: true,
                    verify_aud: true, 
                    aud: FIREBASE_PROJECT_ID,
                    verify_iss: true,
                    iss: "https://securetoken.google.com/" + FIREBASE_PROJECT_ID 
                    # iss: "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com"
                }
            rescue JWT::DecodeError
                decoded_token = nil
            end
        end

        def self.get_phone(access_token = nil)
            phone = nil
            if access_token
                data = self.decode_access_token(access_token: access_token)
                if data
                    phone = data.first["phone_number"]
                end
            end
            return phone
        end
    end
end