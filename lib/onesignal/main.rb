class Onesignal::Main
    AUTHORIZE = "Basic YjkwMmViNDctZmU2NC00NTU1LWE5NmUtODFhMDc5YTgyOTgx"
    APP_ID = "8f39ea58-cd31-4262-9f01-87052405e573"
    def self.sendpush(push_ids = nil, title = "", content = "", data = "")
        data = ::Util.json_decode(data,true)

        uri = URI.parse('https://onesignal.com/api/v1/notifications')

        http = Net::HTTP.new(uri.host, uri.port)
        if Rails.env != 'production'
            http.set_debug_output $stderr
        end
        http.read_timeout = 20
        http.use_ssl = true

        request = Net::HTTP::Post.new(uri.request_uri)
        push_data = {
            app_id: self::APP_ID,
            data: data,
            contents: {
                en: content,
                vi: content
            },
            headings: {
                en: title,
                vi: title
            }
        }
        if push_ids
            push_data[:include_player_ids] = push_ids
        else
            
            push_data[:included_segments] = "All" if Rails.env == "production"
        end
        request.body = JSON.generate(push_data)
        request["Content-Type"] = "application/json"
        request["Authorization"] = self::AUTHORIZE
        
        response = http.request(request)
        ap response
        re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
        if re
            return re
        else
            return nil
        end
    end
end