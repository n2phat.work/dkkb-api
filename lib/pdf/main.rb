module Pdf
    class Main
        include RenderAnywhere
        @data = nil
        @type = nil # cls || toathuoc
        def initialize(data)
            @data = data
        end

        def create_cls(params = {})

        end

        def create_toathuoc(params = {})

        end

        def to_pdf
            @pdf = WickedPdf.new.pdf_from_string(
                    as_html,
                    :margin => { 
                        :top => 0, :bottom => 0, :left => 0, :right => 0
                    },
                    :background => true,
                    :page_size => "A4",
                    # :footer => {:content => as_footer}
                )
            return @pdf
            begin
                save_path = "#{Rails.root}/tmp/#{SecureRandom.urlsafe_base64}.pdf"
                File.open(save_path, 'wb') do |file|
                    file << @pdf
                end
            rescue => exception
                ap exception.message
            end
            return save_path
        end

        private def as_html
            render template: "_cls/test", layout: nil, encoding: 'utf8', locals: {cls: @data}
        end
    end
end