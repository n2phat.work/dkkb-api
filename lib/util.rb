require 'uri'
require 'net/http'
require 'net/https'
require "base64"

class Util
    TIME_MILISEC ||= "%Y-%m-%dT%H:%M:%S.%L"
    TIME_DATETIME ||= "%Y%m%d%H%M%S"

    def self.base64_encode(str)
        Base64.encode64(str)
    end

    def self.base64_decode(str)
        Base64.decode64(str)
    end

    def self.md5(str)
        Digest::MD5.hexdigest(str)
    end

    def self.sha512(str)
        Digest::SHA2.new(512).hexdigest(str)
    end

    def self.sha256(str)
        Digest::SHA2.new(256).hexdigest(str)
    end

    def self.json_decode(str, to_hash = false)
        if to_hash
            ActiveSupport::JSON.decode(str).symbolize_keys 
        else    
            ActiveSupport::JSON.decode(str)
        end
    end

    def self.json_encode(data)
        return ActiveSupport::JSON.encode(data)
    end

    def self.number_format(number)
        return ActiveSupport::NumberHelper.number_to_delimited(number)
    end

    def self.milisec()
        return Time.now.strftime(TIME_MILISEC)
    end

    def self.getCurrentDateTime()
        return Time.now.strftime(TIME_DATETIME)
    end

    def self.url_encode(url = nil)
        return url ? CGI.escape(url) : nil
    end

    def self.url_decode(url = nil)
        return url ? CGI.unescape(url) : nil
    end

    def self.curl_post(url = nil, params = {}, https = false)
        if url
            uri = URI.parse(url)
            http = Net::HTTP.new(uri.host,uri.port)
            http.read_timeout = 20

            if https
                http.use_ssl = true
                unless Rails.env == 'production'
                    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
                end
            end

            request = Net::HTTP::Post.new(uri.request_uri)
            request.body = JSON.generate(params)
            request["Content-Type"] = "application/json"
            
            response = http.request(request)
            re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
            
            return re
        else
            return nil
        end
    end

    def self.curl_form_post(url = nil, params = {}, https = false, header = {})
        if url
            uri = URI.parse(url)
            http = Net::HTTP.new(uri.host,uri.port)
            http.set_debug_output $stderr
            http.read_timeout = 20

            if https
                http.use_ssl = true
                unless Rails.env == 'production'
                    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
                end
            end

            request = Net::HTTP::Post.new(uri.request_uri)
            request.body = params.to_query
            request["Content-Type"] = "application/x-www-form-urlencoded"
            request["Referer"] = header["Referer"] ? header["Referer"] : BASE_URL
            request["User-Agent"] = header["User-Agent"] ? header["User-Agent"] : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
            request["Origin"] = header["Origin"] ? header["Origin"] : BASE_URL

            ap request.inspect
            response = http.request(request)
            ap response.body
            re = response.code.to_i == 200 ? ::Util.json_decode(response.body, false) : nil
            
            return re
        else
            return nil
        end
    end

    def self.remove_dau(str)
        str = str.gsub(/[ăâáàảạãắẳẵằặấầẩẫậ]/,"a")
        str = str.gsub(/[éèẻẽẹêếềểễệ]/,"e")
        str = str.gsub(/[đ]/,"d")
        str = str.gsub(/[íìỉĩị]/,"i")
        str = str.gsub(/[óòỏõọôốồổỗộơớờởỡợ]/,"o")
        str = str.gsub(/[úùủũụưứừữửự]/,"u")
        str = str.gsub(/[ýỳỷỹỵ]/,"y")
        str = str.gsub(/[ĂÂÁÀẢẠÃẮẲẴẰẶẤẦẨẪẬ]/,"A")
        str = str.gsub(/[ÉÈẺẼẸÊẾỀỂỄỆ]/,"E")
        str = str.gsub(/[Đ]/,"D")
        str = str.gsub(/[ÍÌỈĨỊ]/,"I")
        str = str.gsub(/[ÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]/,"O")
        str = str.gsub(/[ÚÙỦŨỤƯỨỪỮỬỰ]/,"U")
        str = str.gsub(/[ÝỲỶỸỴ]/,"Y")
        return str
    end
end
