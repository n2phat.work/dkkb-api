class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  # before_action :cors_before_filter
  # before_action :cors_preflight_check
  before_action :log_activity

  def initialize
    super
    @sysconfig = SystemConfig.first
  end

  def log_activity
    begin
      Activity.new do |e|
        e.ip = (request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip)
        e.user_id = params[:user_id]
        e.controller = params[:controller]
        e.action = params[:action]
        e.params = params.permit!.except(:controller,:action).to_json
        e.save!
      end
    rescue => exception
      SystemException.new({content: exception.to_s, trace: exception.backtrace}).save!
    end
  end

  # CORS process for OPTIONS
  def cors_before_filter
    headers['Access-Control-Allow-Origin'] = request.headers['Origin']
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # CORS process for OPTIONS
  def cors_preflight_check
    if request.method == 'OPTIONS'
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token'
      headers['Access-Control-Max-Age'] = '1728000'
    end
  end

  def check_sign_in
    if !params[:user_id] || !params[:access_token] 
      render json: {error_code: t(:user_params_missing_user_id_access_token_code),error_message: t(:user_params_missing_user_id_access_token)}
      return
    end
    user_ban = UserBan.find_by(user_id: params[:user_id])
    if user_ban
      render json: {error_code: -113, error_message: t(:user_session_expired) + " B!"}
      return
    end
    session = Session.find_by(access_token: params[:access_token], user_id: params[:user_id])
    if session.nil?
      render json: {error_code: t(:user_session_expired_code), error_message: t(:user_session_expired)}
      return
    end
    if params[:user_id] && params[:platform] && params[:onesignal_id]
      begin
        PushDevice.find_or_create_by(user_id: params[:user_id], onesignal_id: params[:onesignal_id], platform: params[:platform]) do |e|
          e.onesignal_token = params[:onesignal_token]
          e.save
        end
      rescue => e
        ap e
      end
    end
  end

  def make_hash(str,salt)
    str ||= ""
    salt ||= ""
    return Digest::MD5.hexdigest(str+salt)
  end

  def make_salt(length = 16)
    pattern = 'abcdefghijklmnopqrstuvwxyz0123456789'
		i = 0
		str = ''
		while i < length
			str = str + pattern[Random.rand(pattern.length - 1)]
			i = i + 1
		end
		return str
  end

  def append_info_to_payload(payload)
    super
    payload[:remote_ip] = request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip
  end
end
