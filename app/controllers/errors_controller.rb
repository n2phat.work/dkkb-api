class ErrorsController < ActionController::Base
    protect_from_forgery with: :null_session
  
    def not_found
      render json: {error_code: 404}
      return
    end
  
    def internal_server_error
      render json: {error_code: 500}
      return
    end
  end