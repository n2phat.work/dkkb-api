class ApidocsController < ApplicationController
    include Swagger::Blocks

    swagger_root swagger: '2.0',
        info: {
            version: '1.0.0',
            title: 'MedPro API',
            description: 'API của MedPro',
            termsOfService: '',
            contact: {
                name: 'PP'
            },
            license: {
                name: 'MedPro'
            }
        },    
        host: HOST,
        basePath: '/api',
        consumes: ['application/x-www-form-urlencoded'],
        produces: ['application/json']


    swagger_schema :ErrorModel do
        key :required, [:code, :message]
        property :error_code do
            key :type, :integer
            key :format, :int32
        end
        property :error_message do
            key :type, :string
        end
    end

    # A list of all classes that have swagger_* declarations.
    SWAGGERED_CLASSES = [
        self, Booking, BookingTime, Doctor, Hospital, Patient, Payment, Room, Support, Banner, Relative, 
        Subject, Time, User, Country, City, District, Ward, Dantoc, Faq, 
        FaqCategory, ContactMessage, Profession,
        Api::HospitalController,
        Api::DoctorController,
        Api::PatientController,
        Api::BookingController,
        Api::RoomController,
        Api::UserController,
        Api::ResourceController,
        Api::PaymentController,
        Api::FaqController,
        Api::SupportController,
        Api::CmessageController,
        Api::NotiController,
        Api::RebookingController,
        Api::RelativeController,
        Api::FeeController,
        Api::SupporterController,
        Api::PrescribeController,
        Api::ClinicController,
        Api::InvoiceController
    ]

    def index
        render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
    end
end
