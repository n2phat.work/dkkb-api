class Nd1docsController < ApplicationController
    include Swagger::Blocks

    swagger_root swagger: '2.0',
        info: {
            version: '1.0.0',
            title: 'MedPro API - BVND1',
            description: 'API của MedPro BVND1',
            termsOfService: '',
            contact: {
                name: 'PP'
            },
            license: {
                name: 'MedPro'
            }
        },    
        host: HOST,
        basePath: '/nd1',
        consumes: ['application/x-www-form-urlencoded'],
        produces: ['application/json']


    swagger_schema :ErrorModel do
        key :required, [:code, :message]
        property :error_code do
            key :type, :integer
            key :format, :int32
        end
        property :error_message do
            key :type, :string
        end
    end

    # A list of all classes that have swagger_* declarations.
    SWAGGERED_CLASSES = [
        self,
        Nd1::UserController,
    ]

    def index
        render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
    end
end
