class SkindocsController < ApplicationController
    include Swagger::Blocks

    swagger_root swagger: '2.0',
        info: {
            version: '1.0.0',
            title: 'MedPro API - BVDL',
            description: 'API của MedPro BVDL',
            termsOfService: '',
            contact: {
                name: 'PP'
            },
            license: {
                name: 'MedPro'
            }
        },    
        host: HOST,
        basePath: '/skin',
        consumes: ['application/x-www-form-urlencoded'],
        produces: ['application/json']


    swagger_schema :ErrorModel do
        key :required, [:code, :message]
        property :error_code do
            key :type, :integer
            key :format, :int32
        end
        property :error_message do
            key :type, :string
        end
    end

    # A list of all classes that have swagger_* declarations.
    SWAGGERED_CLASSES = [
        self,
        Skin::BookingController,
        Skin::PatientController,
        Skin::SubjectController,
        Skin::NotiController,
        Skin::ResourceController,
        Skin::ResourceController,
    ]

    def index
        render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
    end
end
