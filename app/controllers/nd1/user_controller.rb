class Nd1::UserController < SkinBaseController
    def index
        render json: {status: true, msg: "bvdl"}
        return
    end

    def fbsmslogin
        if params[:code]
            info = ::Facebook::Nd1Sms.get_user_info params[:code]
            if info.nil?
                render json: {error_code: 2,error_message: "no response"}
                return
            elsif info["error"]
                render json: {error_code: 1,error_message: info["error"]["message"]}
                return
            else
                res = {
                    number: "+999999999",
                    username: "+84999999999",
                    fullname: "Lê Văn A",
                    user_id: nil,
                    email: nil,
                    access_token: nil,
                    history_booking_count: 0
                }
                if info["phone"]
                    user = get_user_by_phone(info["phone"]["number"])
                    if !user
                        user = Skin::User.new do |e|
                            e.username = info["phone"]["number"]
                            e.phone = info["phone"]["number"]
                            e.salt = self.make_salt
                            e.hashpwd = self.make_hash(e.salt, e.salt)
                            e.save
                        end
                    end
                    
                elsif info["email"]
                    user = get_user_by_email(info["email"]["address"])
                    if !user
                        user = Skin::User.new do |e|
                            e.username = info["email"]["address"]
                            e.email = info["email"]["address"]
                            e.salt = self.make_salt
                            e.hashpwd = self.make_hash(e.salt, e.salt)
                            e.save
                        end
                    end
                end
                session = Nd1::Session.select('user_id, access_token').find_by(user_id: user.id)
                unless session
                    session = Nd1::Session.new(user_id: user.id, access_token: ::Util.md5(::Util.milisec))
                    session.save
                end

                res[:username] = user.username
                res[:fullname] = user.fullname
                res[:number] = user.phone
                res[:user_id] = user.id
                res[:email] = user.email
                res[:access_token] = session.access_token
                res[:history_booking_count] = user.booking.count rescue 0
                res[:patient_count] = user.patient.count
                render json: res
                return
            end
        else
            render json: {error_code: t(:user_no_code_found_code),error_message: t(:user_no_code_found)}
            return
        end
    end

    private def get_user_by_phone(phone)
        return Nd1::User.find_by(phone: phone)
    end

    private def get_user_by_email(email)
        return Nd1::User.find_by(email: email)
    end
end