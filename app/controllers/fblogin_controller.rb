class FbloginController < ApplicationController
    def fbcallbacksms
        Fbcallbacksms.new do |e|
            e.response = params.to_json
            e.save
        end

        if params[:code]
            info = ::Facebook::SMS.get_user_info params[:code]
            if info.empty?
                render json: {error_code: 2,error_message: "no response"}
                return
            else
                if info["error"]
                    render json: {error_code: 1,error_message: info["error"]["message"]}
                    return
                else
                    if info["phone"]
                        render json: info["phone"]
                        return
                    else
                        render json: info
                        return
                    end
                end
            end
        else
            render json: {error_code: 1,error_message: "no code"}
            return
        end
    end
end
