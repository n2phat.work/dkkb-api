class Skin::CmessageController < ApplicationController

    def insert
        cm = Skin::ContactMessage.new do |e|
            e.name = params[:name]
            e.company_name = params[:company_name]
            e.email = params[:email]
            e.content = params[:content]
            e.mobile = params[:mobile]
            e.type = params[:type]
            e.save!
        end

        Thread.new do
            send_data = {
                to: Rails.env == 'production' ? ['support@medpro.com.vn'] : ['phat.n.nguyen@pkh.vn'],
                cc: [cm.email, 'hotro@medpro.com.vn'],
                bcc: ['khiet.m.nguyen@pkh.vn'],
                subject: "Bệnh viện Da Liễu - CSKH - Tin nhắn ##{cm.id}",
                content: cm.content
            }
            if cm.type == 1 && Rails.env == 'production'
                send_data[:to] << 'ctxh@bvdl.org.vn'
            end
            SkinMailer.sendmail(send_data).deliver_now
        end

        render json: cm
        return
    end

    include Swagger::Blocks
    swagger_path '/cmessage/insert' do
        operation :post do
            key :summary, 'Insert new contact message'
            key :description, 'Insert new contact message'
            key :operationId, 'cmessageinsert'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'cmessage'
            ]
            parameter  name: :email, in: :query, description: 'Email Address'
            parameter  name: :name, in: :query, description: 'Tên'
            parameter  name: :company_name, in: :query, description: 'Tên công ty'
            parameter  name: :content, in: :query, description: 'Nội dung tin nhắn'
            parameter  name: :mobile, in: :query, description: 'Số ĐT'
            parameter  name: :type, in: :query, description: 'Loại: 1-Chuyên môn, 2-Kỹ thuật, 3-Khác'
                
            response 200 do
                key :description, 'country response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
