class Skin::FaqController < ApplicationController
    def getall
        cats = FaqCategory.all.includes(:faq)
        cats = cats.where('faq.status = 1').references(:faq)
        cats = cats.where('faq.question LIKE ?',"%#{params[:q]}%") if params[:q] && params[:q] != ''
        render json: cats.as_json(include: [{
                faq: {
                    except: [:date_create, :date_update]
                }
            }]
        )
    end

    include Swagger::Blocks
    swagger_path '/faq/getall' do
        operation :get do
            key :summary, 'All FAQ'
            key :description, 'Returns all FAQ'
            key :operationId, 'allfaq'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'faq'
            ]
            parameter name: :q, in: :query, description: 'search string'
            response 200 do
                key :description, 'FAQ response'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :FaqCategory
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
