class Skin::PatientController < SkinBaseController
    before_action :check_sign_in

    def insert
        user = Skin::User.find_by(id: params[:user_id])
        count = user.patient.count
        if count < USER_MAX_PATIENT
            patient = params[:bv_id] ? Skin::Patient.find_by(bv_id: params[:bv_id]) : nil
            if patient.nil?
                unless !params[:name].nil? && params[:name] != ""
                    render json: { error_code: t(:params_missing_name_code), error_message: t(:params_missing_name) }
                    return
                end
                unless !params[:surname].nil? && params[:surname] != ""
                    render json: { error_code: t(:params_missing_surname_code), error_message: t(:params_missing_surname) }
                    return
                end
                unless !params[:sex].nil? && params[:sex] != ""
                    render json: { error_code: t(:params_missing_sex_code), error_message: t(:params_missing_sex) }
                    return
                end
                unless !params[:birthyear].nil? && params[:birthyear] != ""
                    render json: { error_code: t(:params_missing_birthyear_code), error_message: t(:params_missing_birthyear) }
                    return
                end
                Skin::UserPatient.where(user_id: params[:user_id]).update(is_default: 0) if params[:is_default]
                
                medpro_id = self.make_msbn
                patient = Skin::Patient.new do |e|
                    e.name = params[:name].upcase
                    e.surname = params[:surname].upcase
                    e.cmnd = params[:cmnd]
                    e.sex = params[:sex]
                    e.mobile = params[:mobile]
                    e.email = params[:email]
                    e.dantoc_id = params[:dantoc_id]
                    e.profession_id = params[:profession_id]
                    e.bhyt = params[:bhyt]
                    e.country_code = (params[:country_code] != nil && params[:country_code] != '') ? params[:country_code] : "VIE"
                    e.city_id = params[:city_id]
                    e.district_id = params[:district_id]
                    e.ward_id = params[:ward_id]
                    e.address = params[:address]
                    e.note = params[:note]
                    e.birthdate = params[:birthdate]
                    e.birthyear = params[:birthyear]
                    e.bv_id = params[:bv_id]
                    e.medpro_id = medpro_id
                    e.save!
                end
                patient.bv_id = Time.now.strftime("%y") + "8" + patient.id.to_s
                patient.save!
            else
                patient.mobile = (params[:mobile] && /^\d{10,11}$/.match?(params[:mobile])) ? params[:mobile] : patient.mobile
                patient.email = params[:email] ? params[:email] : patient.email
                # patient.cmnd = params[:cmnd] ? params[:cmnd] : patient.cmnd
                # patient.dantoc_id = params[:dantoc_id] ? params[:dantoc_id] : patient.dantoc_id
                # patient.profession_id = params[:profession_id] ? params[:profession_id] : patient.profession_id
                # patient.bhyt = params[:bhyt] ? params[:bhyt] : patient.bhyt
                # patient.country_code = params[:country_code] ? params[:country_code] : patient.country_code
                # patient.district_id = params[:district_id] ? params[:district_id] : patient.district_id
                # patient.ward_id = params[:ward_id] ? params[:ward_id] : patient.ward_id
                # patient.address = params[:address] ? params[:address] : patient.address
                # patient.note = params[:note] ? params[:note] : patient.note
                # patient.birthdate = params[:birthdate] ? params[:birthdate] : patient.birthdate
                patient.save!
            end
            up = Skin::UserPatient.find_or_create_by(user_id: params[:user_id], skin_patient_id: patient.id) do |e|
                if params[:is_default] != nil
                    e.is_default = params[:is_default]
                end
                e.save!
            end

            if params[:relative_name] && params[:relative_type_id]
                patient.relative = Skin::Relative.find_or_create_by({
                    name: params[:relative_name],
                    mobile: params[:relative_mobile],
                    email: params[:relative_email],
                    relative_type_id: params[:relative_type_id],
                    skin_patient_id: patient.id
                })
                patient.save!
            end

            render json: patient
            return
        else
            render json: {error_code: t(:maximum_patient_code), error_message: t(:maximum_patient)}
            return
        end
    end

    def detail
        if(params[:user_id])
            patient = Skin::Patient.joins(:user_patient).find_by(id: params[:patient_id], skin_user_patient: {user_id: params[:user_id]})
            if patient
                up = Skin::UserPatient.find_or_create_by(user_id: params[:user_id], skin_patient_id: patient.id)
                patient.is_default = up.is_default ? up.is_default : 0

                render json: patient
                return
            else
                render json: {error_code: 1,error_message: "patient not found by this user_id"}
                return
            end
        else
            render json: {error_code: 1,error_message: "params missing"}
            return
        end
    end

    def getbymsbn
        count = 0 #PatientSearchLog.where(user_id: params[:user_id], date: Time.now.strftime("%Y-%m-%d")).count
        if Rails.env == "production" && count >= 10
            render json: {error_code: t(:patient_not_found_code), error_message: t(:patient_not_found)}
            return
        end
        if params[:sign].nil?
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            msbn = data[0]["msbn"]
            patient = nil
            patient = Skin::Patient.find_by(bv_id: msbn.upcase) if data[0]["rebooking"].nil? || data[0]["rebooking"] == ""
            if patient
                # begin
                #     PatientSearchLog.new({user_id: params[:user_id], date: Time.now.strftime("%Y-%m-%d")}).save!
                # rescue => exception
                #     SystemException.new({content: exception.to_s, trace: exception.backtrace}).save!
                # end

                render json: patient
                return
            else
                render json: {error_code: t(:patient_not_found_code), error_message: t(:patient_not_found)}
                return
            end
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def setdefault
        render json: nil
        return
    end

    def update
        if params[:user_id] && params[:patient_id]
            patient = Skin::Patient.joins(:user_patient).find_by(skin_user_patient: {user_id: params[:user_id]}, id: params[:patient_id])
            if patient
                unless !params[:name].nil? && params[:name] != ""
                    render json: { error_code: t(:params_missing_name_code), error_message: t(:params_missing_name) }
                    return
                end
                unless !params[:surname].nil? && params[:surname] != ""
                    render json: { error_code: t(:params_missing_surname_code), error_message: t(:params_missing_surname) }
                    return
                end
                unless !params[:sex].nil? && params[:sex] != ""
                    render json: { error_code: t(:params_missing_sex), error_message: t(:params_missing_sex) }
                    return
                end
                unless !params[:birthyear].nil? && params[:birthyear] != ""
                    render json: { error_code: t(:params_missing_birthyear_code), error_message: t(:params_missing_birthyear) }
                    return
                end
                Skin::UserPatient.where(user_id: params[:user_id]).update_all(is_default: 0) if params[:is_default]
                old_bk = Skin::Booking.where(skin_patient_id: params[:patient_id]).where('status >= 1').first
                if old_bk
                    patient.transaction do
                        e = patient.lock!
                        e.mobile = (params[:mobile] && /^\d{10,11}$/.match?(params[:mobile])) ? params[:mobile] : e.mobile
                        e.email = params[:email] ? params[:email] : e.email
                        e.save!
                    end
                else
                    patient.transaction do
                        e = patient.lock!
                        if patient.bv_id.nil?
                            e.name = params[:name] ? params[:name].upcase : e.name
                            e.surname = params[:surname] ? params[:surname].upcase : e.surname
                            e.sex = !params[:sex].nil? ? params[:sex] : e.sex
                            e.birthyear = params[:birthyear] ? params[:birthyear] : e.birthyear
                            e.city_id = params[:city_id] ? params[:city_id] : e.city_id
                        end
                        e.cmnd = params[:cmnd] ? params[:cmnd] : e.cmnd
                        e.dantoc_id = params[:dantoc_id] ? params[:dantoc_id] : e.dantoc_id
                        e.profession_id = params[:profession_id] ? params[:profession_id] : e.profession_id
                        e.bhyt = params[:bhyt] ? params[:bhyt] : e.bhyt
                        e.country_code = params[:country_code] ? params[:country_code] : e.country_code
                        e.district_id = params[:district_id] ? params[:district_id] : e.district_id
                        e.ward_id = params[:ward_id] ? params[:ward_id] : e.ward_id
                        e.address = params[:address] ? params[:address] : e.address
                        e.note = params[:note] ? params[:note] : e.note
                        e.birthdate = params[:birthdate] ? params[:birthdate] : e.birthdate
                        e.mobile = (params[:mobile] && /^\d{10,11}$/.match?(params[:mobile])) ? params[:mobile] : e.mobile
                        e.email = params[:email] ? params[:email] : e.email
                        e.save!
                    end
                end
                up = Skin::UserPatient.find_by(user_id: params[:user_id], skin_patient_id: patient.id)
                unless params[:is_default].nil?
                    up.is_default = params[:is_default]
                    up.save!
                end
                patient.is_default = up.is_default ? up.is_default : 0

                if params[:relative_name] && params[:relative_type_id]
                    patient.relative = Skin::Relative.find_or_create_by({
                        name: params[:relative_name],
                        mobile: params[:relative_mobile],
                        email: params[:relative_email],
                        relative_type_id: params[:relative_type_id],
                        skin_patient_id: patient.id
                    })
                    patient.save!
                end

                render json: patient
                return
            else
                render json: {error_code: t(:patient_not_found_code),error_message: t(:patient_not_found)}
                return
            end
        else
            render json: {error_code: t(:params_missing_patient_id_code),error_message: t(:params_missing_patient_id)}
            return
        end
    end

    def getbyuserid
        if params[:user_id]
            patients = Skin::Patient.joins(:user_patient).where(skin_user_patient: {user_id: params[:user_id]})
            patients.each do |e|
                up = Skin::UserPatient.find_by(user_id: params[:user_id], skin_patient_id: e.id)
                e.is_default = up.is_default ? up.is_default : 0
            end
            render json: patients
            return
        else
            render json: {error_code: t(:user_params_missing_user_id_access_token_code), error_message: t(:user_params_missing_user_id_access_token)}
            return
        end
    end

    def delete
        if params[:user_id] && params[:patient_id]
            up = Skin::UserPatient.find_by(user_id: params[:user_id], skin_patient_id: params[:patient_id])
            if up
                up.destroy

                render json: { status: true }
                return
            else
                render json: { error_code: 1,error_message: "patient not found with this user" }
                return
            end
        else
            render json: { error_code: 1,error_message: "params missing" }
            return
        end
    end

    def make_msbn(length = 6)
        pattern = '1234567890'
		i = 0
		str = ''
		while i < length
			str = str + pattern[Random.rand(pattern.length - 1)]
			i = i + 1
        end
        str = "MDL-" + Time.now.strftime("%y%m%d").to_s + str
        patient = Skin::Patient.find_by(medpro_id: str)
        if patient
            self.make_msbn
        else
            return str
        end
    end

    include Swagger::Blocks
    swagger_path '/patient/getbyuserid' do
        operation :get do
            key :summary, 'Get patient list for user_id'
            key :description, 'Get patient list for user_id'
            key :operationId, 'patientlist'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true

            response 200 do
                key :description, 'list patient response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/patient/setdefault' do
        operation :get do
            key :summary, 'Set default patient for user'
            key :description, 'Set default patient for user'
            key :operationId, 'setdefault'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :patient_id, in: :query, description: 'patient_id', required: true

            response 200 do
                key :description, 'list patient response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/patient/delete' do
        operation :post do
            key :summary, 'Delete patient for User'
            key :description, 'Delete patient for User'
            key :operationId, 'deleteuser'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :patient_id, in: :query, description: 'patient_id', required: true
            response 200 do
                key :description, 'list patient response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/patient/getbymsbn' do
        operation :get do
            key :summary, 'Get patient list for msbn'
            key :description, 'Get patient list for msbn'
            key :operationId, 'patientlist'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :msbn, in: :query, description: 'MSBN user', required: true

            response 200 do
                key :description, 'list patient response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/patient/detail' do
        operation :get do
            key :summary, 'Get detail patient'
            key :description, 'Returns detail patient'
            key :operationId, 'detailpatient'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :patient_id, in: :query, description: 'ID patient', required: true

            response 200 do
                key :description, 'patient response'
                schema do
                    
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/patient/insert' do
        operation :post do
            key :summary, 'Insert patient'
            key :description, 'Inserts detail patient. Nếu đi từ luồng tự nhập thông tin thì ko cần truyền bv_id. Nếu đi luồng quên BN hoặc search từ Mã Khám bệnh sẽ có bv_id nên Insert nhận params này.'
            key :operationId, 'insertpatient'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :name, in: :query, description: 'Patient\'s name', required: true
            parameter name: :surname, in: :query, description: 'Patient\'s surname', required: true
            parameter name: :bv_id, in: :query, description: 'SoHS -> msbn ben BN nếu có'
            parameter name: :sex, in: :query, description: 'Patient\'s sex: 0 - nữ; 1 - nam', required: true
            parameter name: :birthyear, in: :query, description: 'Patient\'s birthyear', required: true
            parameter name: :cmnd, in: :query, description: 'Patient\'s cmnd'
            parameter name: :birthdate, in: :query, description: 'Patient\'s birthdate Y-m-d'
            parameter name: :bhyt, in: :query, description: 'Patient\'s bhyt'
            parameter name: :mobile, in: :query, description: 'Patient\'s mobile'
            parameter name: :email, in: :query, description: 'Patient\'s email'
            parameter name: :dantoc_id, in: :query, description: 'Dan toc'
            parameter name: :profession_id, in: :query, description: 'Nghe Nghiep'
            parameter name: :city_id, in: :query, description: 'Patient\'s City'
            parameter name: :country_code, in: :query, description: 'Patient\'s Country'
            parameter name: :district_id, in: :query, description: 'Patient\'s District'
            parameter name: :ward_id, in: :query, description: 'Patient\'s Ward'
            parameter name: :address, in: :query, description: 'Patient\'s Address'
            parameter name: :note, in: :query, description: 'Some notes'
            parameter name: :is_default, in: :query, description: 'Set default'
            parameter name: :relative_type_id, in: :query, description: 'Relative Type ID'
            parameter name: :relative_name, in: :query, description: 'Relative\'s name'
            # parameter name: :relative_mobile, in: :query, description: 'Relative\'s mobile'
            # parameter name: :relative_email, in: :query, description: 'Relative\'s email'
            response 200 do
                key :description, 'patient response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/patient/update' do
        operation :post do
            key :summary, 'Update patient'
            key :description, 'Update detail patient'
            key :operationId, 'updatepatient'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'patient'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :patient_id, in: :query, description: 'PatientID', required: true
            parameter name: :name, in: :query, description: 'Patient\'s name', required: true
            parameter name: :surname, in: :query, description: 'Patient\'s surname', required: true
            parameter name: :sex, in: :query, description: 'Patient\'s sex: 0 - nữ; 1 - nam', required: true
            parameter name: :birthyear, in: :query, description: 'Patient\'s birthyear', required: true
            parameter name: :bhyt, in: :query, description: 'Patient\'s bhyt'
            parameter name: :mobile, in: :query, description: 'Patient\'s mobile'
            parameter name: :email, in: :query, description: 'Patient\'s email'
            parameter name: :country_code, in: :query, description: 'Patient\'s Country'
            parameter name: :dantoc_id, in: :query, description: 'Dan toc'
            parameter name: :profession_id, in: :query, description: 'Nghe Nghiep'
            parameter name: :city_id, in: :query, description: 'Patient\'s City'
            parameter name: :district_id, in: :query, description: 'Patient\'s District'
            parameter name: :ward_id, in: :query, description: 'Patient\'s Ward'
            parameter name: :address, in: :query, description: 'Patient\'s Address'
            parameter name: :note, in: :query, description: 'Some notes'
            parameter name: :is_default, in: :query, description: 'Set default'
            parameter name: :relative_type_id, in: :query, description: 'Relative Type ID'
            parameter name: :relative_name, in: :query, description: 'Relative\'s name'
            # parameter name: :relative_mobile, in: :query, description: 'Relative\'s mobile'
            # parameter name: :relative_email, in: :query, description: 'Relative\'s email'
            response 200 do
                key :description, 'patient response'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Patient
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end