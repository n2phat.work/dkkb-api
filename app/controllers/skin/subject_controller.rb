class Skin::SubjectController < SkinBaseController

    def getall
        render json: {
            data: Skin::Subject.where(status: 1).where(id: Skin::Schedule.where('status = 1').pluck(:skin_subject_id))
        }
        return
    end

    def detail
        # params[:subject_id]
        # params[:date]
        day_of_week = C.parse(params[:date])&.wday
        tmp = Skin::Btime.where(skin_subject_id: params[:subject_id])
        if day_of_week == 6
            tmp = tmp.where(buoi: 1)
        end
        tmp.each do |e|
            e.used_slot = Skin::Booking.where('is_deleted = 0').where(skin_time_id: e.id, booking_date: params[:date]).count
        end
        render json: {
            data: tmp
        }
        return
    end

    include Swagger::Blocks
    swagger_path '/subject/getall' do
        operation :get do
            key :summary, 'Get all subject'
            key :description, 'Get all subject'
            key :operationId, 'subject/getall'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'subject'
            ]

            response 200 do
                key :description, 'subject response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/subject/detail' do
        operation :get do
            key :summary, 'Get detail subject by hospital'
            key :description, 'Get detail subject by hospital'
            key :operationId, 'subject/detail'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'subject'
            ]
            parameter name: :subject_id, in: :query, description: 'subject_id', required: true
            parameter name: :date, in: :query, description: 'date', required: true

            response 200 do
                key :description, 'subject response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end