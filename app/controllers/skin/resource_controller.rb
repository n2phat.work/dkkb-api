class Skin::ResourceController < SkinBaseController

    def faq
        @faq = Faq.all.order('category_id ASC')
    end

    def getholiday
        render json: Holiday.all.pluck(:date)
        return
    end

    def getconfig
        # params[:platform]
        app_config = Skin::AppConfig.where(platform: params[:platform]).where(status: 1)
        render json: app_config ? app_config : []
        return
    end

    def checkversion
        # params[:version_number]
        # params[:version_code]
        # params[:platform]
        case params[:platform]
        when 'android'
            app_version = Skin::AppVersion.where(platform: params[:platform]).where(status: 1)
            app_version = app_version.order('version_code DESC').first
        when 'ios'
            app_version = Skin::AppVersion.where(platform: params[:platform]).where(status: 1)
            app_version = app_version.order('version_number DESC').first
        end
        render json: app_version ? app_version : {}
        return
    end

    def getallbanner
        banners = Skin::Banner.all.pluck(:image)
        banners.each_with_index do |e, k|
            banners[k] = BASE_URL + '/st/banner-dl/' + e.to_s
        end
        render json: banners
        return
    end

    def allbanner
        # params[:type]
        banners = Skin::BannerWeb.where.not(status: 0).where(type: params[:type])
        banners.each do |e|
            e.image_url = !e.image_url.nil? ? (DL_WEB_STATIC_URL + e.image_url) : nil
        end
        render json: banners
        return
    end

    def getallcountry
        render json: Country.select(:id,:code,:name).all
        return
    end

    def getcity
        render json: City.select(:id,:name).where(country_code: params[:country_code])
        return
    end

    def getdistrict
        render json: District.on.select(:id,:name).where(city_id: params[:city_id])
        return
    end

    def getward
        render json: Ward.on.select(:id,:name).where(district_id: params[:district_id])
        return
    end

    def getdantoc
        render json: Dantoc.select(:id,:name).all
        return
    end

    def getallprofession
        render json: Profession.select(:id,:name).all.order(:name)
        return
    end

    def getarticle
        # params[:type]
        article = Skin::Article.find_by(sub_id: params[:sub_id])
        render json: article
        return
    end

    def barcode
        # params[:bvdhyd_msbn]
        if params[:bvdhyd_msbn]
            width = params[:width].to_i > 0 ? params[:width].to_i: 240
            height = params[:height].to_i > 0 ? params[:height].to_i: 80
            barcode = Barby::Code128.new(params[:bvdhyd_msbn].to_s).to_image(margin:0).resize(width,height)
            @barcode = barcode.to_blob
            send_data @barcode, type: 'image/png', disposition: 'inline'
            return
        else
            render json: nil
            return
        end
    end

    include Swagger::Blocks

    swagger_path '/resource/checkversion' do
        operation :get do
            key :summary, 'Check New Version'
            key :description, 'Check New Version'
            key :operationId, 'checkversion'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :version_number, in: :query, description: 'version_number'
            parameter name: :version_code, in: :query, description: 'version_code'
            parameter name: :platform, in: :query, description: 'platform'

            response 200 do
                key :description, 'app_version response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getconfig' do
        operation :get do
            key :summary, 'Get Config'
            key :description, 'Get Config'
            key :operationId, 'getconfig'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :platform, in: :query, description: 'platform'

            response 200 do
                key :description, 'app_config response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getarticle' do
        operation :get do
            key :summary, 'Get Article'
            key :description, 'Get Article'
            key :operationId, 'getarticle'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :sub_id, in: :query, description: 'Loai ID Bai Viet', required: true
            response 200 do
                key :description, 'article response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getallcountry' do
        operation :get do
            key :summary, 'All Country'
            key :description, 'Returns all country'
            key :operationId, 'allcountry'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            
            response 200 do
                key :description, 'country response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getcity' do
        operation :get do
            key :summary, 'All City in Country'
            key :description, 'Returns all city in country'
            key :operationId, 'allcity'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :country_code, in: :query, description: 'Country Code', required: true
            response 200 do
                key :description, 'city response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :City
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/resource/getdistrict' do
        operation :get do
            key :summary, 'All District in City'
            key :description, 'Returns all district in city'
            key :operationId, 'alldistrict'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :city_id, in: :query, description: 'City ID', required: true
            response 200 do
                key :description, 'district response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :District
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/resource/getward' do
        operation :get do
            key :summary, 'All Ward in District'
            key :description, 'Returns all ward in district'
            key :operationId, 'allward'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :district_id, in: :query, description: 'District ID', required: true
            response 200 do
                key :description, 'ward response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Ward
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getdantoc' do
        operation :get do
            key :summary, 'All Dantoc'
            key :description, 'Returns all dantoc'
            key :operationId, 'alldantoc'
            key :tags, [
                'resource'
            ]
            response 200 do
                key :description, 'dantoc response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Dantoc
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getallbanner' do
        operation :get do
            key :summary, 'All Banners'
            key :description, 'Returns all banners'
            key :operationId, 'getallbanner'
            key :tags, [
                'resource'
            ]
            response 200 do
                key :description, 'banner response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/allbanner' do
        operation :get do
            key :summary, 'All Banners'
            key :description, 'Returns all banners'
            key :operationId, 'allbanner'
            key :tags, [
                'resource'
            ]
            parameter name: :type, in: :query, description: 'Type: 1 - web, 2 - mobile', required: true
            response 200 do
                key :description, 'banner response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getallprofession' do
        operation :get do
            key :summary, 'All Profession'
            key :description, 'Returns all Profession'
            key :operationId, 'allprofession'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            
            response 200 do
                key :description, 'profession response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Profession
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getholiday' do
        operation :get do
            key :summary, 'All Holidays'
            key :description, 'Returns all Holidays'
            key :operationId, 'allholiday'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            
            response 200 do
                key :description, 'profession response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end