class Skin::PaymentController < SkinBaseController

    def getallpayment
        # params[:hospital_id]
        params[:hospital_id] ||= 2
        # tkb = Bank.where(status: 1).where("priority = 0 AND hospital_id = :hospital_id", {hospital_id: params[:hospital_id]}).order("priority ASC")
        # tkb.each do |e|
        #     e.rate = 0
        #     e.const_rate = 0
        #     e.medpro_fee = MEDPRO_SERVICE_FEE
        #     e.image = BASE_URL + '/st/bank/' + e.image
        # end

        # atm = Bank.where(status: 1).where("is_intcard = 0 AND priority != 0 AND hospital_id = :hospital_id", {hospital_id: params[:hospital_id]}).order("priority ASC")
        # atm.each do |e|
        #     e.rate = RATE_ATM
        #     e.const_rate = ADD_ATM 
        #     e.medpro_fee = MEDPRO_SERVICE_FEE
        #     e.image = BASE_URL + '/st/bank/' + e.image
        # end

        # vis = Bank.where(status: 1).where("is_intcard = 1 AND hospital_id = :hospital_id",{hospital_id: params[:hospital_id]}).order("priority ASC")
        # vis.each do |e|
        #     e.rate = RATE_INT_CARD
        #     e.const_rate = ADD_INT_CARD
        #     e.medpro_fee = MEDPRO_SERVICE_FEE
        #     e.image = BASE_URL + '/st/bank/' + e.image
        # end
        return_data = [
            # {
            #     id: 1,
            #     name: "Thanh toán bằng thẻ khám bệnh",
            #     rate: 0,
            #     const_rate: 0,
            #     medpro_fee: MEDPRO_SERVICE_FEE,
            #     data: tkb,
            #     type: 1
            # },
            # {
            #     id: 2,
            #     rate: RATE_INT_CARD,
            #     const_rate: ADD_INT_CARD,
            #     medpro_fee: MEDPRO_SERVICE_FEE,
            #     name: "Thanh toán bằng Thẻ quốc tế Visa, Master, JCB",
            #     data: vis,
            #     type: 2
            # },
            # {
            #     id: 3,
            #     rate: RATE_ATM,
            #     const_rate: ADD_ATM,
            #     medpro_fee: MEDPRO_SERVICE_FEE,
            #     name: "Thanh toán bằng Thẻ ATM nội địa/Internet Banking",
            #     data: atm,
            #     type: 1
            # }
        ]
        if true #Rails.env == "development"
            return_data += [
                {
                    id: 7,
                    rate: 0,
                    const_rate: 0,
                    medpro_fee: MEDPRO_SERVICE_FEE,
                    name: "Thanh toán bằng Ví MoMo",
                    data: [
                        {
                            id: 10000,
                            name: "Thanh toán bằng Ví MoMo",
                            rate: 0,
                            const_rate: 0,
                            medpro_fee: MEDPRO_SERVICE_FEE,
                            image: BASE_URL + '/st/bank/momo.png'
                        }
                    ],
                    type: 6
                }
            ]
        end
        if params[:api_version].to_i >= @sysconfig.api_version
            return_data += [
                # {
                #     id: 5,
                #     rate: VP_RATE_OFFLINE,
                #     const_rate: 0,
                #     medpro_fee: MEDPRO_SERVICE_FEE,
                #     name: "Thanh toán tại đại lý",
                #     data: [
                #         {
                #             id: 2001,
                #             name: "Đến đại lý thanh toán.",
                #             rate: VP_RATE_OFFLINE,
                #             const_rate: 0,
                #             medpro_fee: MEDPRO_SERVICE_FEE,
                #             image: BASE_URL + '/st/bank/home.png',
                #             min_fee: 5500,
                #             max_fee: 550000
                #         }
                #     ],
                #     type: 4
                # },
                # {
                #     id: 6,
                #     rate: RATE_ATM,
                #     const_rate: ADD_ATM,
                #     medpro_fee: MEDPRO_SERVICE_FEE,
                #     name: "Thanh toán bằng Chuyển khoản",
                #     data: [
                #         {
                #             id: 9999,
                #             name: "Chuyển khoản đến MedPro.",
                #             rate: RATE_ATM,
                #             const_rate: ADD_ATM,
                #             medpro_fee: MEDPRO_SERVICE_FEE,
                #             image: BASE_URL + '/st/bank/tt-chuyen-khoan.png'
                #         }
                #     ],
                #     type: 5
                # }
            ]
        end
        render json: return_data
        return
    end

    def getinfobooking
        bookings = Skin::Booking.joins(:payment).where('skin_payment.transaction_code_tt = ?', params[:payment]).where('skin_booking.user_id = ?', params[:user_id]).where('skin_payment.status = 1')
        render json: bookings
        return
    end

    def momopostback
        data = params.permit!
        final_step_url = nil
        if final_step_url =~ URI::regexp
            final_step_success_url = final_step_url + "/booking-new-final-step"
        else
            final_step_success_url = DL_WEB_FINAL_STEP + "/booking-new-final-step"
            final_step_url = DL_WEB_FINAL_STEP
        end
        
        data[:calc_sign] = ::Momo::SkinMain.payment_recieve_params_postback(data)
        if (data[:calc_sign] == data[:signature])
            payment = Skin::Payment.find_by(transaction_code_tt: data[:orderId])
            if payment
                if data[:message].downcase == 'success'
                    if payment.status == 0
                        payment.status = PAYMENT_SUCCESS
                        payment.result = params.permit!.to_json
                        payment.save!
                        bookings = Skin::Booking.joins(:payment).where('skin_payment.id = ?', payment.id)
                        bookings.each do |bk|
                            #bk.booking_number = _give_number(bk)
                            #bk.save!
                            
                            # _send_socket(bk)
                            _send_mail(bk)
                            Skin::Noti.new do |noti|
                                noti.type = NOTI_NORMAL
                                noti.user_id = bk.user_id
                                noti.skin_booking_id = bk.id
                                noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                                # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                                noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/><i>(Vui lòng đến bàn tiếp nhận)</i><br/>Trân trọng cảm ơn!"
                                noti.save!
                            end
                        end
                    end
                    ## redirect luon vi da thanh cong
                    if data[:extraData] == 'android'
                        @android_final_step_url = "medpro://dalieu?tt=#{payment.transaction_code_tt}"
                        # render template: "momo/android", layout: nil, encoding: 'utf8', locals: {link: @android_final_step_url}
                        # return
                        redirect_to @android_final_step_url
                        return
                    else
                        redirect_to final_step_success_url + "?payment=" + (payment.transaction_code_tt ? payment.transaction_code_tt.to_s : "")
                        return
                    end
                else
                    redirect_to final_step_url + "/payment_error"
                    return
                end
            else
                redirect_to final_step_url + "/payment_error"
                return
            end
        else
            redirect_to final_step_url + "/payment_error"
            return
        end
        render json: data
        return
    end

    def momonoti
        data = params.permit!
        data[:calc_sign] = ::Momo::Main.payment_recieve_params_noti(data)
        render json: data
        return
    end

    def momo_sdk_payment
        # params[:phonenumber]
        # params[:token]
        # params[:transaction_code_tt]
        final_step_url = nil
        if final_step_url =~ URI::regexp
            final_step_success_url = final_step_url + "/booking-new-final-step"
        else
            final_step_success_url = DL_WEB_FINAL_STEP + "/booking-new-final-step"
            final_step_url = DL_WEB_FINAL_STEP
        end
        payment = Skin::Payment.find_by(transaction_code_tt: params[:transaction_code_tt])
        result = nil
        if payment
            momo = Momo::SkinMain.new
            result = momo.sdk_payment({
                amount: payment.amount,
                transaction_code_tt: payment.transaction_code_tt,
                phonenumber: params[:phonenumber],
                token: params[:token]
            })
            if result
                ap result
                raw_sign = "status=#{result["status"]}&message=#{result["message"]}&amount=#{result["amount"]}&transid=#{result["transid"]}"
                calc_sign = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), MOMO_SECRET_KEY_DL, raw_sign)
                if result["status"].to_i == 0 && calc_sign.downcase == result["signature"].downcase
                    payment.status = PAYMENT_SUCCESS
                    payment.result = result.to_json
                    payment.save!
                    bookings = Skin::Booking.joins(:payment).where('skin_payment.id = ?', payment.id)
                    bookings.each do |bk|
                        # _send_socket(bk)
                        _send_mail(bk)
                        Skin::Noti.new do |noti|
                            noti.type = NOTI_NORMAL
                            noti.user_id = bk.user_id
                            noti.skin_booking_id = bk.id
                            noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                            noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/><i>(Vui lòng đến bàn tiếp nhận)</i><br/>Trân trọng cảm ơn!"
                            noti.save!
                        end
                    end
                    render json: {status: true}
                    return
                else
                    render json: {error_code: -35, error_message: result["message"]}
                    return
                end
            else
                render json: {error_code: -35, error_message: result["message"]}
                return
            end
        else
            render json: {error_code: -35, error_message: result["message"]}
            return
        end
    end

    private def _format_name(surname = '', name = '')
        return surname.to_s + ' ' + name.to_s
    end

    private def _send_mail(bk)
        patient = Skin::Patient.find_by(id: bk.skin_patient_id)
        if patient
            if patient.email && patient.email != ""
                begin
                    Skin::BookingMailJob.perform_later(bk.patient.email, "BVDL - Xác nhận Phiếu Khám Bệnh: #{bk.transaction_code_gd}", bk.id)
                rescue  => e
                    ap e
                end
            end
        end
    end
end