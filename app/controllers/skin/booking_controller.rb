class Skin::BookingController < SkinBaseController
    before_action :check_sign_in
    
    def getbyuserid
        if params[:user_id]
            data = nil #$redis.get('booking_list_' + params[:user_id].to_s)

            if data
                render json: data
                return
            else
                booking = Skin::Booking.where(user_id: params[:user_id]).where.not(status: 0).where.not(status: -1)
                # if params[:patient_id]
                #     booking = booking.where(patient_id: params[:patient_id])
                # end
                booking = booking.order('id DESC')
                result = booking.to_json

                # $redis.set('booking_list_' + params[:user_id].to_s, result)
                # $redis.expire('booking_list_' + params[:user_id].to_s, REDIS_BOOKING_LIST_CACHE_TIME.minute.to_i)
                render json: result
                return
            end
        else
            render json: {error_code: t(:user_params_missing_user_id_access_token_code),error_message: t(:user_params_missing_user_id_access_token)}
            return
        end
    end

    def resend_code
        # params[:booking_id]
        # params[:mobile]
        render json: {}
        return
        if !params[:booking_id]
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        else
            booking = Booking.find_by(id: params[:booking_id], user_id: params[:user_id])
            if booking
                mobile = params[:mobile] ? params[:mobile] : (booking.patient ? (booking.patient.mobile ? booking.patient.mobile : booking.booking_phone) : booking.booking_phone)
                # send sms
                if booking.sms_count < 2
                    mobile = mobile.sub("+84","0")
                    sms_msg = "BN #{_smsname(booking.patient.surname, booking.patient.name)}, Ma so: #{booking.transaction_code_gd}, Kham #{Chronic.parse(booking.booking_date).strftime('%d/%m/%Y')}, #{_smsgiokham(booking.booking_time.from,booking.booking_time.to)}, STT: #{booking.booking_number}, khoa #{::Util.remove_dau(booking.schedule.subject.name)}, #{::Util.remove_dau(booking.schedule.room.name)}. Vui long lay STT tai #{_end_text(booking)}."
                    sms_msg = "BN #{_smsname(booking.patient.surname, booking.patient.name, true)}, Ma so: #{booking.transaction_code_gd}, Kham #{Chronic.parse(booking.booking_date).strftime('%d/%m/%Y')}, #{_smsgiokham(booking.booking_time.from,booking.booking_time.to)}, STT: #{booking.booking_number}, khoa #{::Util.remove_dau(booking.schedule.subject.name)}, #{::Util.remove_dau(booking.schedule.room.name)}. Vui long lay STT tai #{_end_text(booking)}." if sms_msg.length > 160

                    client = Savon.client(wsdl: SMS_SOAP_URL)
                    logger.info(sms_msg)
                    result = client.call(:send_sms, message: {
                        authenticate_user: SMS_USER, 
                        authenticate_pass: SMS_AUTHEN, 
                        brand_name: SMS_BRAND_NAME,
                        message: sms_msg,
                        receiver: mobile.to_s,
                        type: 1
                    })
                    # LogSms.new do |e|
                    #     e.booking_id = booking.id
                    #     e.mobile = mobile
                    #     e.content = sms_msg
                    #     e.response = result
                    #     e.save!
                    # end
                else
                    render json: { error_code: t(:sms_max_code), error_message: t(:sms_max) }
                    return
                end
                # send email 
                if booking.patient && booking.status == BOOKING_PAYMENT_DONE
                    if booking.patient.email && booking.patient.email != ""
                        # email_params = {
                        #     to: booking.patient.email,
                        #     subject: "BVDHYD - Xác nhận Phiếu Khám Bệnh",
                        #     booking_id: params[:booking_id]
                        # }
                        # Api::CurlJob.perform_later(MAIL_SERVER_URL, email_params)
                        # ::Util.curl_post(MAIL_SERVER_URL, email_params)
                        # Api::BookingMailer.sendmail(booking.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh: #{booking.transaction_code_gd}", params[:booking_id]).deliver_now
                        begin
                            Api::BookingMailJob.perform_later(booking.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh: #{booking.transaction_code_gd}", params[:booking_id])
                        rescue => e
                            ap e
                        end
                    end
                end
                # end
                begin
                    # Redis::BookingJob.perform_later(booking.user_id)
                rescue => e
                    ap e
                end
                render json: {
                    mobile: params[:mobile] ? params[:mobile] : booking.patient.mobile,
                    mobile_censored: booking.patient.mobile_censored
                }
                return
            else
                render json: { error_code: t(:booking_not_found_code), error_message: t(:booking_not_found) }
                return
            end
        end
    end

    def cancel
        # params[:booking_id]
        if !params[:booking_id]
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        else
            booking = Skin::Booking.find_by(id: params[:booking_id], user_id: params[:user_id], status: BOOKING_PAYMENT_DONE)

            if booking
                if Time.now.strftime("%H:%M").to_s >= "15:30"
                    current_time = (Time.now + 1.days).strftime("%Y-%m-%d").to_s 
                else
                    current_time = Time.now.strftime("%Y-%m-%d").to_s
                end
                if booking.booking_date.to_s <= current_time
                    render json: { error_code: t(:booking_cancel_on_same_day_code), error_message: t(:booking_cancel_on_same_day) }
                    return
                end
                payment = Skin::Payment.find_by(id: booking.skin_payment_id)
                if payment
                    # payment auto callback update booking
                    payment.update(status: PAYMENT_CANCEL_LATER, refund_status: 1)
                end
            end
            render json: { status: 1 }
            return
        end
    end

    def detail
        if params[:booking_id]
            booking = Skin::Booking.find_by(id: params[:booking_id], user_id: params[:user_id])
            if booking
                booking.related = Skin::Booking.where(skin_payment_id: booking.skin_payment_id).where.not(id: booking.id).pluck(:transaction_code_gd)
                render json: booking
                return
            else
                render json: { error_code: t(:booking_not_found_code), error_message: t(:booking_not_found) }
                return
            end
        else
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        end
    end

    def insertmulti
        # {
        #     "bookings" : [{
        #         "subject_id": 2,
        #         "bhyt": 1,
        #         "booking_time_id": 1,
        #         "booking_phone": "0123456789",
        #         "buoi": 1
        #     }],
        #     "date":"2018-06-01",
        #     "patient_id": 1,
        #     "user_id": 1,
        #     "access_token": "",
        #     "bank_id": 1,
        #     "method_id": 1,
        #     "platform":"web",
        #     "hoadon_info"
        # }

        arr_rest_days = ['0101', '0209', '3004', '0105']
        arr_holidays = Holiday.all.pluck(:date)
        i = 0
        bookings = []
        booking_id_list = []
        total = 0
        unless params[:date] && params[:date] != ''
            render json: {error_code: t(:params_missing_date_code), error_message: t(:params_missing_date)}
            return
        end
        tmp_date = Time.parse(params[:date])
        tmp_30_day = Time.now + 30.days
        tmp_date_1day = Time.parse(params[:date].to_s + " 15:30:00")
        tmp_date_now_1day = Time.now + 1.days
        if tmp_date >= tmp_30_day || tmp_date_now_1day > tmp_date_1day
            render json: {error_code: t(:out_of_30_days_code), error_message: t(:out_of_30_days)}
            return
        end
        if tmp_date <= Time.now
            render json: {error_code: t(:date_in_past_code), error_message: t(:date_in_past)}
            return
        end
        if arr_rest_days.include? tmp_date.strftime("%d%m")
            render json: {error_code: -33, error_message: "Không thể đặt khám vào ngày nghỉ."}
            return
        end
        if arr_holidays.include? tmp_date.strftime("%Y-%m-%d")
            render json: {error_code: -33, error_message: "Không thể đặt khám vào ngày nghỉ."}
            return
        end
        unless params[:patient_id] && params[:patient_id] != ''
            render json: {error_code: t(:params_missing_patient_id_code), error_message: t(:params_missing_patient_id)}
            return
        end
        patient = Skin::Patient.find_by(id: params[:patient_id])
        unless patient
            render json: {error_code: t(:patient_not_found_code), error_message: t(:patient_not_found)}
            return
        end
        bank = Bank.find_by(id: params[:bank_id])
        if bank.nil? && params[:bank_id].to_i != 9999 && params[:bank_id].to_i != 2001 && params[:bank_id].to_i != 10000
            render json: {error_code: t(:bank_not_found_code), error_message: t(:bank_not_found)}
            return
        end

        if params[:bookings].length > 3
            render json: {error_code: -34, error_message: "Không thể đặt khám nhiều hơn 3 chuyên khoa."}
            return
        end
        params[:bookings].each do |e|
            unless e["subject_id"] && e["subject_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_subject_id) + ": #{i}" }
                return
            end
            unless e["booking_time_id"] && e["booking_time_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_booking_time_id) + ": #{i}" }
                return
            end
            
            schedule = Skin::Schedule.find_by(
                skin_subject_id: e["subject_id"]
            )
            if schedule
                count = Skin::Booking.where('status >= 0 AND is_deleted != 1').where(booking_date: params[:date], skin_schedule_id: schedule.id, skin_time_id: e["booking_time_id"]).count
                subject = Skin::Subject.find_by(id: e["subject_id"])
                btime = Skin::Btime.find_by(id: e["booking_time_id"])
                max_slot = btime ? btime.max_slot : 0
                if count.to_i < max_slot
                    booking = Skin::Booking.new do |b|
                        b.platform = params[:platform]
                        # b.transaction_code_gd = SecureRandom.uuid
                        b.booking_date = params[:date]
                        b.user_id = params[:user_id]
                        b.skin_schedule_id = schedule.id
                        b.skin_time_id = e["booking_time_id"]
                        b.skin_patient_id = params["patient_id"]
                        b.booking_phone = e["booking_phone"]
                        b.bhyt_accept = e["bhyt"].to_i == 3 ? 0 : e["bhyt"]
                        b.email = e["email"]
                        b.status = BOOKING_NEW
                        b.save!
                    end
                    if booking
                        booking_day = Skin::BookingDay.new do|e|
                            e.booking_id = booking.id
                            e.save!
                        end
                        if booking_day
                            if params[:platform].nil? || params[:platform] == 'web'
                                prefix = 'W'
                            else
                                prefix = 'A'
                            end
                            booking.transaction_code_gd = prefix + Time.now.strftime("%y%m%d") + _format_id(booking_day.id.to_s)
                            booking.save!
                        end
                    end

                    bookings << booking
                    total += booking.subject.price
                    booking_id_list << booking.id
                else
                    render json: {error_code: t(:element_out_of_slot_code), error_message: t(:element_out_of_slot) + ": #{i}"}
                    return
                end
            else
                render json: {error_code: t(:element_schedule_not_found_code), error_message: t(:element_schedule_not_found) + ": #{i}"}
                return
            end
            i += 1
        end
        # payment
        amount = 0
        amount_service = MEDPRO_SERVICE_FEE
        amount_gate = 0
        rate_percent = 0
        rate_const = 0

        if bank
            case bank.vnpay_code
            when 'VISA'
                rate_percent = RATE_INT_CARD
                rate_const = ADD_INT_CARD
                method = 1
            when 'VIETINBANK'
                if (params[:method_id].to_i == 1)
                    method = 1
                else
                    rate_percent = RATE_ATM
                    rate_const = ADD_ATM
                    method = 1
                end
            else
                rate_percent = RATE_ATM
                rate_const = ADD_ATM
                method = 1
            end
            amount_gate = rate_percent * (total + amount_service) + rate_const
            amount = total + amount_service + amount_gate

        else
            case params[:method_id].to_i
            when 7 # Momo
                rate_percent = 0
                rate_const = 0
                method = 1

                amount_gate = rate_percent * (total + amount_service) + rate_const
                amount = total + amount_service + amount_gate
            when 6
                rate_percent = RATE_ATM
                rate_const = ADD_ATM
                method = 1

                amount_gate = rate_percent * (total + amount_service) + rate_const
                amount = total + amount_service + amount_gate
            when 5
                rate_percent = VP_RATE_OFFLINE
                rate_const = 0
                method = 1

                amount_gate = rate_percent * total + rate_const
                amount_gate = 5500 if amount_gate < 5500
                amount_gate = 550000 if amount_gate > 550000
                amount = total + amount_service + amount_gate
            end
        end

        payment = Skin::Payment.new do |pm|
            pm.transdate =  ::Util.getCurrentDateTime
            pm.method_id = params[:method_id]
            pm.amount = amount
            pm.amount_original = total
            pm.amount_service = amount_service
            pm.amount_gate = amount_gate
            pm.rate_percent = rate_percent
            pm.rate_const = rate_const
            pm.card_code = params[:bank_code]
            pm.user_id = params[:user_id]
            pm.hoadon_info = params[:hoadon_info] ? params[:hoadon_info].strip : nil
            pm.save!
        end
        if payment
            offline_code = nil
            payment_url = nil
            offline_msg = nil
            momo_deeplink =  nil
            momo_deeplinkWebInApp = nil
            payment.transaction_code_tt = (Rails.env == 'production' ? "TTDL-" : "TTDLDev-") + Time.now.strftime("%y%m%d").to_s + payment.id.to_s
            payment.save!
            Skin::Booking.where(id: booking_id_list).update_all(skin_payment_id: payment.id)
            case method
            when 1
                if params[:method_id].to_i == 2
                    payment_url = BASE_URL + '/api/resource/cs?' + {transaction_code_tt: payment.transaction_code_tt, amount: payment.amount, p_redirect: request.headers['origin'].to_s}.to_query
                elsif params[:method_id].to_i == 6
                    # render json: {error_code: 123, error_message: PKH_TRANSFER_TEXT + payment.transaction_code_tt}
                    # return
                    offline_msg = PKH_TRANSFER_TEXT + '<b>' + payment.transaction_code_tt + '<b/>' + ".<br/>Hết hạn: #{(tmp_date_now_1day > (tmp_date_1day - 1.days) ? expired_date - 30.minutes : tmp_date_now_1day).strftime("%H:%M %d-%m-%Y")}"
                elsif params[:method_id].to_i == 5
                    # render json: {error_code: 123, error_message: "Đã chọn thanh toán đại lý"}
                    # return
                    offline_code = ::Payoo::MainOffline.create_offline_order({
                        amount: payment.amount, 
                        transaction_code_tt: payment.transaction_code_tt,
                        expired_date: expired_date
                    })
                    if offline_code[:code]
                        payment.payment_payoo = PaymentPayoo.new({
                            offline_code: offline_code[:code]
                        })
                        payment.save!
                        Noti.new do |noti|
                            noti.type = 5
                            noti.user_id = payment.user_id
                            noti.booking_id = nil
                            noti.title = "Bạn vừa tạo mã thanh toán phiếu khám bệnh tại đại lý."
                            noti.content = "Mã thanh toán của bạn là: #{offline_code[:code]}<br/>(Hết hạn: #{offline_code[:expired_date].to_time.strftime("%H:%M %d-%m-%Y")})"
                            noti.save!
                        end
                    end
                elsif params[:method_id].to_i == 7
                    momo = ::Momo::SkinMain.new
                    momo_data = momo.create_order({
                        amount: payment.amount,
                        transaction_code_tt: payment.transaction_code_tt,
                        info: "Thanh toán Phiếu khám bệnh",
                        extra: params[:platform]
                    })
                    momo_sdk_data = momo.sdk_deeplink({
                        amount: payment.amount,
                        transaction_code_tt: payment.transaction_code_tt,
                        info: "Thanh toán Phiếu khám bệnh #{payment.transaction_code_tt}",
                        extra: params[:platform]
                    })
                    if momo_data["payUrl"]
                        payment_url = (params[:platform] == 'ios' || params[:platform] == 'android') ? momo_data["deeplink"] : momo_data["payUrl"]
                        momo_deeplink =  momo_data["deeplink"]
                        momo_deeplinkWebInApp = momo_data["deeplinkWebInApp"]
                    else
                        render json: {error_code: -35, error_message: "Có lỗi xảy ra khi thanh toán bằng Momo."}
                        return
                    end
                else
                    payment_url = ::Vnpay::Main.get_payment_url({
                        amount: payment.amount,
                        transaction_code_tt: payment.transaction_code_tt,
                        info: request.headers['origin'].to_s + "-" + ::Util.md5(::Util.milisec),
                        create_date: payment.transdate,
                        vnp_secret: params[:method_id].to_i == 1 ? VNP_HASHSECRET_TKB : VNP_HASHSECRET,
                        vnp_tmncode: params[:method_id].to_i == 1 ? VNPAY_TMN_CODE_TKB : VNPAY_TMN_CODE,
                        code: Rails.env == 'production' ? bank.vnpay_code : "NCB",
                        # code: bank.vnpay_code
                    })
                end
            when 2
                payment_url = ::Payoo::Main.get_payment_url({
                    amount: payment.amount,
                    transaction_code_tt: payment.transaction_code_tt,
                    code: bank.payoo_code
                })
            end
        end

        # end payment
        render json: {
            total: total,
            payment: payment,
            list: booking_id_list,
            payment_url: payment_url,
            bookings: bookings,
            offline_code: offline_code,
            offline_msg: offline_msg,
            momo_deeplink: momo_deeplink,
            momo_deeplinkWebInApp: momo_deeplinkWebInApp,
            momo_sdk_data: momo_sdk_data ? momo_sdk_data : {}
        }
        return
    end

    private def _format_id(str = "")
        result = ""
        default_length = 4
        if str.length < default_length
            (default_length - str.length).times do
                result += "0"
            end
        end
        result += str
        return result
    end

    include Swagger::Blocks

    swagger_path '/booking/cancel' do
        operation :post do
            key :summary, 'Cancel booking'
            key :description, 'Cancel booking'
            key :operationId, 'cancelbooking'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'User access_token', required: true
            parameter name: :booking_id, in: :query, description: 'User access_token', required: true
            response 200 do
                key :description, 'booking cancel response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/resend_code' do
        operation :post do
            key :summary, 'Resend booking code'
            key :description, 'Resend booking code'
            key :operationId, 'resend_code'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'User access_token', required: true
            parameter name: :booking_id, in: :query, description: 'User access_token', required: true
            response 200 do
                key :description, 'booking cancel response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/getbyuserid' do
        operation :get do
            key :summary, 'Get history by user_id'
            key :description, 'Get history by user_id'
            key :operationId, 'getbyuserid'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'User access_token', required: true
            response 200 do
                key :description, 'booking response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/detail' do
        operation :get do
            key :summary, 'Get detail booking'
            key :description, 'Get detail booking'
            key :operationId, 'bookingdetail'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'User access_token', required: true
            parameter name: :booking_id, in: :query, description: 'Booking ID', required: true
            response 200 do
                key :description, 'booking response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end