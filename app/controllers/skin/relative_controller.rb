class Skin::RelativeController < SkinBaseController

    def getalltype
        data = ::RelativeType.all.order('priority ASC, name ASC')
        render json: data
        return
    end

    include Swagger::Blocks
    swagger_path '/relative/getalltype' do
        operation :get do
            key :summary, 'Get all type for Relative'
            key :description, 'Get all type for Relative'
            key :operationId, 'getalltype'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'relative'
            ]
            parameter do
                key :name, :user_id
                key :in, :query
                key :description, 'ID user'
                key :required, true
                schema do
                    
                end
            end
            parameter do
                key :name, :access_token
                key :in, :query
                key :description, 'Access Token'
                key :required, true
                schema do
                    
                end
            end
            response 200 do
                key :description, 'list relative type response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

end