class Adm::BookingController < ApplicationController
    http_basic_authenticate_with name: SIDEKIQ_ADMIN, password: "pkh123!?"
    layout 'adm'
    def index
        case params[:command]
        when 'index'
            self.allpayment
        when 'do_pay'
            # render json:params
            # return
            payment = Payment.find_by(transaction_code_tt: params[:transaction_code_tt])
            if payment
                if payment.status == 0
                    if payment.payment_payoo
                        if payment.payment_payoo.status != 'PAYMENT_RECEIVED'
                            render json: {status: false, msg: "Không thể thanh toán đơn hàng này lúc này."}
                            return
                        end
                    end
                    url = ::Vnpay::Main.get_payment_url({
                        amount: payment.amount,
                        transaction_code_tt: payment.transaction_code_tt,
                        info: payment.transaction_code_tt + "-" + ::Util.milisec,
                        create_date: payment.transdate,
                        vnp_secret: payment.method_id.to_i == 1 ? VNP_HASHSECRET_TKB : VNP_HASHSECRET,
                        vnp_tmncode: payment.method_id.to_i == 1 ? VNPAY_TMN_CODE_TKB : VNPAY_TMN_CODE,
                        # code: Rails.env == 'production' ? "TECHCOMBANK" : "NCB",
                        # code: bank.vnpay_code
                    })
                    redirect_to url
                    return
                else
                    render json: {status: false, msg: "Đã thanh toán hoặc đã hủy."}
                    return
                end
            else
                render json: {status: false, msg: "Không tìm thấy lượt thanh toán."}
                return
            end
        when 'do_pay_again'
            render json: {status: false, msg: "Tính năng sắp triển khai."}
            return
        when 'cancel'
            payment = Payment.find_by(transaction_code_tt: params[:transaction_code_tt])
            payment.method_id = 3
            payment.save!
            redirect_to '/adm/booking'
            return
        else
            self.allpayment
        end
    end
    
    def allpayment
        @payments = Payment.joins(:booking).where(status: 0, method_id: [6]).where('payment.date_create >= ?', Time.now - 1.days).where('booking.date_expired >= ?', Time.now).distinct
        @payments = @payments.where(transaction_code_tt: params[:transaction_code_tt].strip) if !params[:transaction_code_tt].nil? && params[:transaction_code_tt] != ''
        @payments = @payments.where(booking: {patient_id: Patient.where(bvdhyd_msbn: params[:sohs].strip).pluck(:id)}) if !params[:sohs].nil? && params[:sohs] != ''
        @payments = @payments.where(booking: {patient_id: Patient.where('CONCAT(TRIM(surname), " ", TRIM(name)) LIKE ?',"%#{params[:patient_name].strip}%").pluck(:id)}) if !params[:patient_name].nil? && params[:patient_name] != ''
        @payments = @payments.order('id DESC')
        @params = params.permit!
    end
end
