class Adm::TestController < ApplicationController
    http_basic_authenticate_with name: "admin", password: "admin"
    layout "test"

    def cls
        @data = []
        if params[:sbn] && params[:sct]
            @cls = BvdhydCls.find_by(SoBienNhan: params[:sbn], SoChungTu: params[:sct])
            if @cls
                case @cls.IDChuongTrinh.to_i
                when 167
                    view = "xquang"
                when 164
                    view = "noisoi"
                else
                    render json: "Không biết view KQ nào"
                    return
                end
                render view
            end
        else
            limit = params[:limit] || 10
            offset = params[:offset] || 0
            tmp_data = Bvdhyd::Cls.getall(limit, offset)
            if tmp_data
                tmp_data.each do |e|
                    cls = BvdhydCls.find_by(SoBienNhan: e["SoBienNhan"], SoChungTu: e["SoChungTu"])
                    unless cls
                        new_cls = {
                            :SoBienNhan => e["SoBienNhan"],
                            :SoChungTu => e["SoChungTu"],
                            :SoHS => e["SoHS"],
                            :NgayThucHien => e["NgayThucHien"],
                            :DVCode => e["DVCode"],
                            :LoaiBenhNhan => e["LoaiBenhNhan"],
                            :IDChuongTrinh => e["IDChuongTrinh"],
                            :FileXML => e["FileXML"],
                        }
                        cls = BvdhydCls.new(new_cls)
                        cls.save!
                    end
                    @data << cls
                end
            end
        end
    end
end