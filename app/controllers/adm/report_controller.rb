class Adm::ReportController < ApplicationController
    http_basic_authenticate_with name: SIDEKIQ_ADMIN, password: "pkh123!?"
    layout 'adm'
    
    def initialize
        @bookings = nil
        @params = {}
        super
    end

    def index
        page = params[:page] ? params[:page].to_i : 1
        limit = params[:limit] ? params[:limit].to_i : 25
        if params[:include_delete]
            bks = Booking.unscoped
                    .left_outer_joins(:payment)
                    .left_outer_joins(:user)
                    .left_outer_joins(:hospital)
                    .left_outer_joins(:subject)
                    .left_outer_joins(:patient)
        else
            bks = Booking
                    .left_outer_joins(:payment)
                    .left_outer_joins(:user)
                    .left_outer_joins(:hospital)
                    .left_outer_joins(:subject)
                    .left_outer_joins(:patient)
        end
        if params[:keyword] && !params[:keyword].blank?
            @bookings || bks
            @bookings = bks.where('CONCAT(TRIM(patient.surname)," ",TRIM(patient.name)) LIKE ?', "%#{params[:keyword]}%")
            @bookings = @bookings.or(bks.where('patient.bvdhyd_msbn LIKE ?',"%#{params[:keyword]}%"))
            @bookings = @bookings.or(bks.where('patient.medpro_id LIKE ?',"%#{params[:keyword]}%"))
            @bookings = @bookings.or(bks.where('patient.mobile LIKE ?',"%#{params[:keyword]}%"))
            @bookings = @bookings.or(bks.where('user.username LIKE ?',"%#{params[:keyword]}%"))
            @bookings = @bookings.or(bks.where('payment.transaction_code_tt LIKE ?',"%#{params[:keyword]}%"))
            @bookings = @bookings.or(bks.where('booking.transaction_code_gd LIKE ?',"%#{params[:keyword]}%"))
            @bookings = @bookings.or(bks.where('CONCAT(booking.booking_date) LIKE ?',"%#{params[:keyword]}%"))
        end
        if params[:from] && params[:to] && !params[:from].blank? && !params[:to].blank?
            from = C.parse(params[:from] + " 00:00:00")
            to = C.parse(params[:to] + " 23:59:59")
            @bookings ||= bks
            @bookings = @bookings.where('booking.date_create BETWEEN ? AND ?',from, to)
        end
        if !params[:status].blank?
            @bookings ||= bks
            @bookings = @bookings.where('booking.status = ?',params[:status])
        end
        @bookings = (@bookings || bks).page(page).per(limit).group('booking.id').order('booking.id DESC')
        @params = {
            page: page,
            limit: limit,
            keyword: params[:keyword],
            include_delete: params[:include_delete],
            form: params[:from],
            to: params[:to],
            status: params[:status]
        }
    end

    private def list

    end

    private def search

    end

    private def detail

    end
end