class PartnerController < ApplicationController
    def patient_update
        render json: {status: true}
        return
    end

    def patient_detail
        patient = Patient.where(medpro_id: params[:IDMedpro])
        render json: patient
        return
    end

    def payment_getlist
        render json: []
        return
    end

    def payment_detail
        render json: []
        return
    end

    def booking_update
        render json: {status: true}
        return
    end
end
