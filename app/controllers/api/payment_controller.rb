require 'uri'

class Api::PaymentController < ApplicationController
    before_action :check_sign_in, only: [:create]
    
    def create
        # params[:code]
        # params[:booking_id_list]
        render json: nil
        return
    end

    def getlistbank
        banks = Bank.where("vnpay_code != 'VISA' AND payoo_code IS NOT NULL").order("priority ASC")
        banks.each do |e|
            e.image = BASE_URL + '/st/bank/' + e.image
        end
        render json: banks
        return
    end

    def getlistvisa
        banks = Bank.where("vnpay_code = 'VISA'").order("priority ASC")
        banks.each do |e|
            e.image = BASE_URL + '/st/bank/' + e.image
        end
        render json: banks
        return
    end

    def getallpayment
        # params[:hospital_id]
        params[:hospital_id] ||= 2
        tkb = Bank.where(status: 1).where("priority = 0 AND hospital_id = :hospital_id", {hospital_id: params[:hospital_id]}).order("priority ASC")
        tkb.each do |e|
            e.rate = 0
            e.const_rate = 0
            e.medpro_fee = MEDPRO_SERVICE_FEE
            e.image = BASE_URL + '/st/bank/' + e.image
        end

        atm = Bank.where(status: 1).where("is_intcard = 0 AND priority != 0 AND hospital_id = :hospital_id", {hospital_id: params[:hospital_id]}).order("priority ASC")
        atm.each do |e|
            e.rate = RATE_ATM
            e.const_rate = ADD_ATM 
            e.medpro_fee = MEDPRO_SERVICE_FEE
            e.image = BASE_URL + '/st/bank/' + e.image
        end

        vis = Bank.where(status: 1).where("is_intcard = 1 AND hospital_id = :hospital_id",{hospital_id: params[:hospital_id]}).order("priority ASC")
        vis.each do |e|
            e.rate = RATE_INT_CARD
            e.const_rate = ADD_INT_CARD
            e.medpro_fee = MEDPRO_SERVICE_FEE
            e.image = BASE_URL + '/st/bank/' + e.image
        end
        return_data = [
            {
                id: 1,
                name: "Thanh toán bằng thẻ khám bệnh",
                rate: 0,
                const_rate: 0,
                medpro_fee: MEDPRO_SERVICE_FEE,
                data: tkb,
                type: 1
            },
            {
                id: 2,
                rate: RATE_INT_CARD,
                const_rate: ADD_INT_CARD,
                medpro_fee: MEDPRO_SERVICE_FEE,
                name: "Thanh toán bằng Thẻ quốc tế Visa, Master, JCB",
                data: vis,
                type: 2
            },
            {
                id: 3,
                rate: RATE_ATM,
                const_rate: ADD_ATM,
                medpro_fee: MEDPRO_SERVICE_FEE,
                name: "Thanh toán bằng Thẻ ATM nội địa/Internet Banking",
                data: atm,
                type: 1
            }
        ]
        if Rails.env == "development"
            return_data += [
                {
                    id: 7,
                    rate: 0,
                    const_rate: 0,
                    medpro_fee: MEDPRO_SERVICE_FEE,
                    name: "Thanh toán bằng Ví MoMo",
                    data: [
                        {
                            id: 10000,
                            name: "Thanh toán bằng Ví MoMo",
                            rate: 0,
                            const_rate: 0,
                            medpro_fee: MEDPRO_SERVICE_FEE,
                            image: BASE_URL + '/st/bank/momo.png'
                        }
                    ],
                    type: 6
                }
            ]
        end
        if params[:api_version].to_i >= @sysconfig.api_version
            return_data += [
                # {
                #     id: 5,
                #     rate: VP_RATE_OFFLINE,
                #     const_rate: 0,
                #     medpro_fee: MEDPRO_SERVICE_FEE,
                #     name: "Thanh toán tại đại lý",
                #     data: [
                #         {
                #             id: 2001,
                #             name: "Đến đại lý thanh toán.",
                #             rate: VP_RATE_OFFLINE,
                #             const_rate: 0,
                #             medpro_fee: MEDPRO_SERVICE_FEE,
                #             image: BASE_URL + '/st/bank/home.png',
                #             min_fee: 5500,
                #             max_fee: 550000
                #         }
                #     ],
                #     type: 4
                # },
                {
                    id: 6,
                    rate: RATE_ATM,
                    const_rate: ADD_ATM,
                    medpro_fee: MEDPRO_SERVICE_FEE,
                    name: "Thanh toán bằng Chuyển khoản",
                    data: [
                        {
                            id: 9999,
                            name: "Chuyển khoản đến MedPro.",
                            rate: RATE_ATM,
                            const_rate: ADD_ATM,
                            medpro_fee: MEDPRO_SERVICE_FEE,
                            image: BASE_URL + '/st/bank/tt-chuyen-khoan.png'
                        }
                    ],
                    type: 5
                }
            ]
        end
        render json: return_data
        return
    end

    def vnpayipn
        begin
            data = ::Vnpay::Main.check_hash(params.permit!.to_hash)
            if data[:vnp_SecureHash].downcase != data[:cal_hash].downcase
                render json: {
                    RspCode: "97",
                    Message: "Invalid signature"
                }
                return
            else
                payment = Payment.find_by(transaction_code_tt: data[:vnp_TxnRef])
                if payment
                    if payment.status.to_i == 0
                        if params[:vnp_ResponseCode].to_i.zero?
                            payment.status = PAYMENT_SUCCESS
                            payment.result = params.permit!.to_json
                            payment.save!
                            bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                            bookings.each do |bk|
                                #bk.booking_number = _give_number(bk)
                                #bk.save!
                                
                                # _send_socket(bk)
                                _send_mail(bk)
                                _send_tracking(bk)
                                Noti.new do |noti|
                                    noti.type = NOTI_NORMAL
                                    noti.user_id = bk.user_id
                                    noti.booking_id = bk.id
                                    noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                                    # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                                    noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{bk.booking_number.to_i}<br/><i>(#{bk.bhyt_accept >= 1 ? 'Vui lòng xác minh thông tin tại quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'})</i><br/>Trân trọng cảm ơn!"
                                    noti.save!
                                end
                            end
                        end
                        render json: {
                            RspCode: "00",
                            Message: "Confirm Success"
                        }
                        return
                    else
                        render json: {
                            RspCode: "02",
                            Message: "Order already confirmed"
                        }
                        return
                    end
                else
                    render json: {
                        RspCode: "01",
                        Message: "Order not found"
                    }
                end
            end
        rescue  => e
            render json: {
                RspCode: "99",
                Message: "Lỗi chung..."
            }
            return
        end
    end

    def postback
        data = ::Vnpay::Main.check_hash(params.permit!.to_hash)
        payment = Payment.find_by(transaction_code_tt: data[:vnp_TxnRef])
        final_step_url = nil
        final_step_url = data[:vnp_OrderInfo].to_s.split("-").first if data[:vnp_OrderInfo]
        if final_step_url =~ URI::regexp
            final_step_success_url = final_step_url + "/booking-new-final-step"
        else
            final_step_success_url = WEB_FINAL_STEP + "/booking-new-final-step"
            final_step_url = WEB_FINAL_STEP
        end

        if data[:vnp_SecureHash].downcase != data[:cal_hash].downcase
            redirect_to final_step_url + "/payment_error"
            return
        end

        if !payment.nil?
            if params[:vnp_ResponseCode].to_i.zero?
                if payment.status == 0
                    payment.status = PAYMENT_SUCCESS
                    payment.result = params.permit!.to_json
                    payment.save!
                    bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                    bookings.each do |bk|
                        #bk.booking_number = _give_number(bk)
                        #bk.save!
                        
                        # _send_socket(bk)
                        _send_mail(bk)
                        _send_tracking(bk)
                        Noti.new do |noti|
                            noti.type = NOTI_NORMAL
                            noti.user_id = bk.user_id
                            noti.booking_id = bk.id
                            noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                            # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                            noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{bk.booking_number.to_i}<br/><i>(#{bk.bhyt_accept >= 1 ? 'Vui lòng xác minh thông tin tại quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'})</i><br/>Trân trọng cảm ơn!"
                            noti.save!
                        end
                    end
                end
                ## redirect luon vi da thanh cong
                redirect_to final_step_success_url + "?payment=" + (payment.transaction_code_tt ? payment.transaction_code_tt.to_s : "")
                return
            else
                payment.result = params.permit!.to_json
                payment.save!
                bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                bookings.each do |bk|
                    bk.status = BOOKING_PAYMENT_CANCEL
                    bk.save!
                    # xoa DSK ben BV
                    Bvdhyd::Lichkham.deleteDSK({transaction_code_gd: bk.transaction_code_gd}) if Rails.env == "production"
                end
                redirect_to final_step_url + "/payment_error"
                return
            end
        else
            redirect_to final_step_url + "/payment_error"
            return
        end
    end

    def payoochecksum(in_checksum, session, order_no, status, secret)
        str = session.to_s + "." + order_no + '.' + status
        checksum = ::Util.sha512(secret + str)
        return (checksum == in_checksum)
    end

    def payoo_postback_pkb

    end

    def payoo_noti_pkb
        ap params.permit!
        payoo = ::Payoo::MainOffline.ipnchecksum(params[:NotifyData])
        payment = Payment.find_by(transaction_code_tt: payoo[:session])
        # ap payoo
        if payment
            if payment.payment_payoo
                payment.payment_payoo.status = payoo[:status]
                payment.payment_payoo.result = payoo.to_json
                payment.payment_payoo.save!
            else
                payment.payment_payoo = PaymentPayoo.new({
                    status: payoo[:status],
                    result: payoo.to_json
                })
            end
            if payoo[:status].downcase == 'payment_received'
                if payment.status == 0
                    payment.status = PAYMENT_SUCCESS
                    payment.result = payoo.to_json
                    payment.save!
                    bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                    bookings.each do |bk|
                        _send_mail(bk)
                        _send_tracking(bk)
                        Noti.new do |noti|
                            noti.type = NOTI_NORMAL
                            noti.user_id = bk.user_id
                            noti.booking_id = bk.id
                            noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                            noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{bk.booking_number.to_i}<br/><i>(#{bk.bhyt_accept >= 1 ? 'Vui lòng xác minh bảo hiểm y tế tại các quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'})</i><br/>Trân trọng cảm ơn!"
                            noti.save!
                        end
                    end
                end
            end
        end
        render plain: "NOTIFY_RECEIVED"
        return
    end

    def payoopostbackvp
        browser = Browser.new(request.user_agent)
        if payoochecksum(params[:checksum], params[:session], params[:order_no], params[:status], PAYOO_SECRET_KEY)
            @params = params.permit!.to_query
            pf = PaymentFee.find_by(transaction_code_tt: params[:session])
            if pf
                if params[:status].to_i == 1
                    if pf.status == 0
                        pf.result = params.permit!.to_json
                        pf.status = 1
                        pf.save!

                        tmp_arr = pf.fee_id.to_s.split(",")
                        Fee.where(id: tmp_arr).update(status: 1)
                        fees = Fee.where(id: tmp_arr)
                        contents = fees.pluck(:content).join(", ")
                        mtts = fees.pluck(:sohoadon).join(", ")
                        patient = Patient.find_by(bvdhyd_msbn: fees.first.msbn)
                        name_sms = patient ? (_smsname(patient.surname, patient.name)) : ""
                        name_noti = patient ? (patient.surname.to_s + " " + patient.name.to_s) : ""
                        Noti.new do |noti|
                            noti.type = 5
                            noti.user_id = pf.user_id
                            noti.booking_id = nil
                            #Xac Nhan: BN {m1} da thanh toan thanh cong so tien {m2}. Noi dung: {m3}. Ma thanh toan: {m4}. Tran trong!
                            noti.title = "Thanh toán viện phí thành công."
                            noti.content = "Xác Nhận: BN #{name_noti} đã thanh toán thành công số tiền #{pf.amount_original}. Nội dung: #{contents}. Mã thanh toán: #{mtts}. <br/>Trân trọng!."
                            noti.save!
                        end
                        #send SMS
                        user = User.find_by(id: pf.user_id)
                        mobile = user.phone.sub("+84","0")
                        sms_msg = ::Util.remove_dau("BN #{name_sms} da thanh toan thanh cong so tien #{pf.amount_original}. Noi dung: #{contents}. MTT: #{mtts}. Tran trong!")
                        client = Savon.client(wsdl: SMS_SOAP_URL)
                        logger.info(sms_msg)
                        result = client.call(:send_sms, message: {
                            authenticate_user: SMS_USER, 
                            authenticate_pass: SMS_AUTHEN, 
                            brand_name: SMS_BRAND_NAME,
                            message: sms_msg,
                            receiver: mobile.to_s,
                            type: 1
                        })
                        LogSms.new do |e|
                            e.mobile = mobile
                            e.content = sms_msg
                            e.response = result
                            e.save!
                        end
                        #end
                    end
                    
                    if browser.device.mobile?
                        @url = "payoosdk764://postbacksdkurl/index?" + @params
                    else
                        @url = VP_FINAL_URL + '?list_id=' + pf.fee_id.to_s
                    end
                else
                    @url = WEB_FINAL_STEP_ERROR
                end
            else
                @url = WEB_FINAL_STEP_ERROR
            end
        else
            @url = WEB_FINAL_STEP_ERROR
        end
    end

    def payoonoti
        begin
            ap params.permit!
            payoo = ::Payoo::Main.ipnchecksum(params[:NotifyData])
            if payoo
                pf = PaymentFee.find_by(transaction_code_tt: payoo[:session])
                if pf
                    if payoo[:status].downcase == 'payment_received'
                        if pf.status == 0
                            pf.result = payoo.to_json
                            pf.status = 1
                            pf.save!

                            tmp_arr = pf.fee_id.to_s.split(",")
                            Fee.where(id: tmp_arr).update(status: 1)

                            fees = Fee.where(id: tmp_arr)
                            contents = fees.pluck(:content).join(", ")
                            mtts = fees.pluck(:sohoadon).join(", ")
                            patient = Patient.find_by(bvdhyd_msbn: fees.first.msbn)
                            name_sms = patient ? (_smsname(patient.surname, patient.name)) : ""
                            name_noti = patient ? (patient.surname.to_s + " " + patient.name.to_s) : ""
                            Noti.new do |noti|
                                noti.type = 5
                                noti.user_id = pf.user_id
                                noti.booking_id = nil
                                #Xac Nhan: BN {m1} da thanh toan thanh cong so tien {m2}. Noi dung: {m3}. Ma thanh toan: {m4}. Tran trong!
                                noti.title = "Thanh toán viện phí thành công."
                                noti.content = "Xác Nhận: BN #{name_noti} đã thanh toán thành công số tiền #{pf.amount_original}. Nội dung: #{contents}. Mã thanh toán: #{mtts}. <br/>Trân trọng!."
                                noti.save!
                            end
                            #send SMS
                            user = User.find_by(id: pf.user_id)
                            mobile = user.phone.sub("+84","0")
                            sms_msg = ::Util.remove_dau("BN #{name_sms} da thanh toan thanh cong so tien #{pf.amount_original}. Noi dung: #{contents}. MTT: #{mtts}. Tran trong!")
                            client = Savon.client(wsdl: SMS_SOAP_URL)
                            logger.info(sms_msg)
                            result = client.call(:send_sms, message: {
                                authenticate_user: SMS_USER, 
                                authenticate_pass: SMS_AUTHEN,
                                brand_name: SMS_BRAND_NAME,
                                message: sms_msg,
                                receiver: mobile.to_s,
                                type: 1
                            })
                            LogSms.new do |e|
                                e.mobile = mobile
                                e.content = sms_msg
                                e.response = result
                                e.save!
                            end
                            #end
                        end
                    end
                end
            end
            render plain: "NOTIFY_RECEIVED"
            return
        rescue => e
            render json: e
            return
        end
    end

    def payoopostback
        payment = Payment.find_by(transaction_code_tt: params[:session])
        if !payment.nil?
            if params[:status].to_i == 1
                payment.status = PAYMENT_SUCCESS
                payment.result = params.permit!.to_json
                payment.save!
                bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                bookings.each do |bk|
                    #bk.booking_number = _give_number(bk)
                    #bk.save!
                    
                    # _send_socket(bk)
                    _send_mail(bk)
                    Noti.new do |noti|
                        noti.type = NOTI_NORMAL
                        noti.user_id = bk.user_id
                        noti.booking_id = bk.id
                        noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                        # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                        noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{bk.booking_number.to_i}<br/><i>(#{bk.bhyt_accept >= 1 ? 'Vui lòng xác minh bảo hiểm y tế tại các quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'})</i><br/>Trân trọng cảm ơn!"
                        noti.save!
                    end
                end
            else
                payment.result = params.permit!.to_json
                payment.save!
                bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                bookings.each do |bk|
                    bk.status = BOOKING_PAYMENT_CANCEL
                    bk.save!
                end

                redirect_to WEB_FINAL_STEP_ERROR
                return
            end
        else
            render json: {error_code: t(:payment_not_found_code),error_message: t(:payment_not_found)}
            return
        end
        # render json: payment
        redirect_to WEB_FINAL_STEP + "?payment=" + (payment.transaction_code_tt ? payment.transaction_code_tt.to_s : "")
        return
    end

    def getinfobooking
        bookings = Booking.joins(:payment).where('payment.transaction_code_tt = ?', params[:payment]).where('booking.user_id = ?', params[:user_id])
        render json: bookings
        return
    end

    def cybersourcepostback
        final_step_url = cookies[:p_redirect] ? cookies[:p_redirect] : ''
        if final_step_url =~ URI::regexp
            final_step_success_url = final_step_url + "/booking-new-final-step"
        else
            final_step_success_url = WEB_FINAL_STEP + "/booking-new-final-step"
            final_step_url = WEB_FINAL_STEP
        end
        payment = Payment.find_by(transaction_code_tt: params[:req_reference_number])
        signature = ::Cybersource::Security.generate_signature(params.permit!, CYBS_SECRET)

        if signature != params[:signature]
            redirect_to final_step_url + "/payment_error"
            return
        end
        if payment
            if params[:auth_response]
                if payment.status == 0
                    if params[:auth_response].strip == "00" && params[:decision].to_s.strip.upcase == "ACCEPT"
                        payment.status = PAYMENT_SUCCESS
                        payment.result = params.permit!.to_json
                        payment.save!
                        bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                        bookings.each do |bk|
                            #bk.booking_number = _give_number(bk)
                            #bk.save!
                            
                            # _send_socket(bk)
                            _send_mail(bk)
                            _send_tracking(bk)
                            Noti.new do |noti|
                                noti.type = NOTI_NORMAL
                                noti.user_id = bk.user_id
                                noti.booking_id = bk.id
                                noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                                # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                                noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{bk.booking_number.to_i}<br/><i>(#{bk.bhyt_accept >= 1 ? 'Vui lòng xác minh thông tin tại quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'})</i><br/>Trân trọng cảm ơn!"
                                noti.save!
                            end
                        end
                        redirect_to final_step_success_url + "?payment=" + (payment.transaction_code_tt ? payment.transaction_code_tt.to_s : "")
                        return
                    else
                        # send noti
                        Noti.new do |noti|
                            noti.user_id = payment.user_id
                            noti.title = "Thanh toán KHÔNG THÀNH CÔNG."
                            noti.content = "Thanh toán <b>không thành công.</b><br/>Vui lòng thử lại hoặc liên hệ <b>19002267</b> để được hỗ trợ nếu đã bị trừ tiền."
                            noti.type = 4
                            noti.save!
                        end
                        # end noti
                        redirect_to final_step_url + "/payment_error"
                        return
                    end
                else
                    redirect_to final_step_url + "/payment_error"
                    return
                end
            else
                # ko co auth_response
                # check some case
                if params[:decision].to_s.strip.upcase == "DECLINE"
                    # send noti
                    Noti.new do |noti|
                        noti.user_id = payment.user_id
                        noti.title = "Thanh toán KHÔNG THÀNH CÔNG."
                        noti.content = "Thanh toán <b>không thành công.</b><br/>Vui lòng thử lại hoặc liên hệ <b>19002267</b> để được hỗ trợ nếu đã bị trừ tiền."
                        noti.type = 4
                        noti.save!
                    end
                    # end noti
                else
                    payment.result = params.permit!.to_json
                    payment.save!
                    bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                    bookings.each do |bk|
                        bk.status = BOOKING_PAYMENT_CANCEL
                        bk.save!
                        # xoa DSK ben BV
                        Bvdhyd::Lichkham.deleteDSK({transaction_code_gd: bk.transaction_code_gd}) if Rails.env == "production"
                    end
                end
                redirect_to final_step_url + "/payment_error"
                return
            end
        else
            redirect_to final_step_url + "/payment_error"
            return
        end
    end

    def momopostback
        data = params.permit!
        final_step_url = nil
        if final_step_url =~ URI::regexp
            final_step_success_url = final_step_url + "/booking-new-final-step"
        else
            final_step_success_url = WEB_FINAL_STEP + "/booking-new-final-step"
            final_step_url = WEB_FINAL_STEP
        end
        
        data[:calc_sign] = ::Momo::Main.payment_recieve_params_postback(data)
        if (data[:calc_sign] == data[:signature])
            payment = Payment.find_by(transaction_code_tt: data[:orderId])
            if payment
                if data[:message].downcase == 'success'
                    if payment.status == 0
                        payment.status = PAYMENT_SUCCESS
                        payment.result = params.permit!.to_json
                        payment.save!
                        bookings = Booking.joins(:payment).where('payment.id = ?', payment.id)
                        bookings.each do |bk|
                            #bk.booking_number = _give_number(bk)
                            #bk.save!
                            
                            # _send_socket(bk)
                            _send_mail(bk)
                            _send_tracking(bk)
                            Noti.new do |noti|
                                noti.type = NOTI_NORMAL
                                noti.user_id = bk.user_id
                                noti.booking_id = bk.id
                                noti.title = "Bạn đã đăng ký khám bệnh thành công. Mã phiếu: #{bk.transaction_code_gd}."
                                # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                                noti.content = "Bệnh nhân: #{_format_name(bk.patient.surname,bk.patient.name)}<br/>Chuyên khoa: #{bk.subject ? bk.subject.name : ""}<br/>Ngày khám: #{bk.booking_date ? bk.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{bk.booking_number.to_i}<br/><i>(#{bk.bhyt_accept >= 1 ? 'Vui lòng xác minh thông tin tại quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'})</i><br/>Trân trọng cảm ơn!"
                                noti.save!
                            end
                        end
                    end
                    ## redirect luon vi da thanh cong
                    if data[:extraData] == 'android'
                        @android_final_step_url = "medpro://pkh?tt=#{payment.transaction_code_tt}"
                        # render template: "momo/android", layout: nil, encoding: 'utf8', locals: {link: @android_final_step_url}
                        # return
                        redirect_to @android_final_step_url
                        return
                    else
                        redirect_to final_step_success_url + "?payment=" + (payment.transaction_code_tt ? payment.transaction_code_tt.to_s : "")
                        return
                    end
                else
                    redirect_to final_step_url + "/payment_error"
                    return
                end
            else
                redirect_to final_step_url + "/payment_error"
                return
            end
        else
            redirect_to final_step_url + "/payment_error"
            return
        end
        render json: data
        return
    end

    def momonoti
        data = params.permit!
        data[:calc_sign] = ::Momo::Main.payment_recieve_params_noti(data)
        render json: data
        return
    end

    private def _smsname(surname, name, short = false)
        if short
            arr = surname.split(" ")
            r = []
            arr.each do |e|
                r << e[0].upcase
            end
            result = ""
            unless r.empty?
                result = r.join(".")
            end
            return result + "." + ::Util.remove_dau(name)
        else
            return ::Util.remove_dau(surname.to_s) + " " + ::Util.remove_dau(name.to_s)
        end
    end

    private def _format_name(surname = '', name = '')
        return surname.to_s + ' ' + name.to_s
    end

    private def _give_number(bk)
        if bk
            bk_time = BookingTime.find_by(id: bk.booking_time_id)
            if bk_time && bk_time.step.to_i > 0
                arr_used_number = Booking.where(booking_date: bk.booking_date, schedule_id: bk.schedule_id, booking_time_id: bk.booking_time_id).where('`status` >= 0').order(:booking_number).pluck(:booking_number)
                cursor = bk_time.number_from
                number = 0
                loop do
                    if cursor > bk_time.number_to
                        break
                    end
                    unless arr_used_number.include?(cursor)
                        number = cursor
                        break
                    end
                    cursor = cursor + bk_time.step
                end
                return number
            else
                return nil
            end
        else
            return nil
        end
    end

    private def _send_tracking(bk)
        if Rails.env == "production"
            Dhyd::TrackJob.perform_later(booking_id: bk.id)
        end
    end

    private def _send_socket(bk)
        slot_time = BookingTime.where(
            "(`from` <= :hour_from AND `to` > :hour_from) 
            OR (`from` >= :hour_from AND `to` <= :hour_to) 
            OR (`from` < :hour_to AND `to` >= :hour_to)", 
            {
                hour_from: bk.schedule.ptime.hour_from, 
                hour_to: bk.schedule.ptime.hour_to
            }
        ).where("room_id = ?", bk.schedule.room_id)
        slot_time.each do |e|
            e.used_slot = e.booking.where('booking_date IS NOT NULL AND status >= 0 AND is_deleted != 1 AND booking_date = ?', bk.booking_date).size
        end

        begin
            ActionCable.server.broadcast "slottime_" + bk.schedule.room_id.to_s + "_" + bk.booking_date.to_s, slot_time.as_json
        rescue => e
            ap e
        end
    end

    private def _send_mail(bk)
        patient = Patient.find_by(id: bk.patient_id)
        if patient
            if patient.email && patient.email != ""
                # email_params = {
                #     to: bk.patient.email,
                #     subject: "BVDHYD - Xác nhận Phiếu Khám Bệnh",
                #     booking_id: bk.id
                # }
                # Api::CurlJob.perform_later(MAIL_SERVER_URL, email_params)
                # ::Util.curl_post(MAIL_SERVER_URL, email_params)
                # Api::BookingMailer.sendmail(booking.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh", params[:booking_id]).deliver_now
                begin
                    Api::BookingMailJob.perform_later(bk.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh: #{bk.transaction_code_gd}", bk.id)
                rescue  => e
                    ap e
                end
            end
        end
    end

    def make_transaction_code_tt(length = 6)
        str = "TT-" + Payment.maximum(:id).to_i.next.to_s
        booking = Payment.find_by(transaction_code_tt: str)
        if booking 
            self.make_transaction_code_tt
        else
            return str
        end
    end

    include Swagger::Blocks
    swagger_path '/payment/getlistvisa' do
        operation :post do
            key :summary, 'Get list VISA card'
            key :description, 'Returns all VISA card'
            key :operationId, 'getlistvisa'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'payment'
            ]
            response 200 do
                key :description, 'VISA list'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Room
                    end
                end
            end

            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/payment/getlistbank' do
        operation :post do
            key :summary, 'Get list bank'
            key :description, 'Returns all bank'
            key :operationId, 'getlistbank'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'payment'
            ]
            response 200 do
                key :description, 'Bank list'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Room
                    end
                end
            end
            
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/payment/getallpayment' do
        operation :post do
            key :summary, 'Get all payment method'
            key :description, 'Get all payment method & all bank'
            key :operationId, 'getallpayment'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'payment'
            ]
            parameter name: :hospital_id, in: :query, description: 'hospital_id'
            response 200 do
                key :description, 'Bank list'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Room
                    end
                end
            end
            
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
