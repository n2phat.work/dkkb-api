class Api::FeeController < ApplicationController
    before_action :check_sign_in, except: [:getbank,:search_by_code,:bill_code_ipn,:check_transaction,:momo_search_code,:momo_ipn]
    before_action :is_admin_mpos, only: [:getordermpos]

    def momo_search_code
        # params[:bill_code]
        # sign = md5(secret + bill_code)
        secret = Rails.env == 'production' ? "wjgu456sxjg4mp96" : "bvdhyd_vp_secret"
        sign = ::Util.md5(secret.to_s + params[:bill_code].to_s)
        if sign.downcase == params[:sign].to_s.downcase
            newfee = Fee.new
            params[:bill_code] = params[:bill_code].gsub(/TA/i,"TA-") if params[:bill_code] =~ /TA\d/i
            params[:bill_code] = params[:bill_code].gsub(/TX/i,"-TX-") if params[:bill_code] =~ /TX\d/i
            ap params[:bill_code]
            data = newfee.crawl({sohoadon: params[:bill_code], paid_status: 0})
            if data.length > 0
                fee = data.first
                amount_fee = 0
                amount = fee.amount + MEDPRO_VP_SERVICE_FEE + amount_fee
                data[0].amount = amount
            else
                data = Fee.new.crawl({sohoadon: params[:bill_code], paid_status: 1})
                if data.length > 0
                    render json: {status: false, error_code: 2, error_message: "Already paid."}
                    return
                else
                    render json: {status: false, error_code: 4, error_message: "Code not found."}
                    return
                end
            end
            render json: {
                status: true,
                data: data,
                patient: data.length > 0 ? data.first.patient : Patient.where("bvdhyd_msbn != '' AND bvdhyd_msbn IS NOT NULL").where(bvdhyd_msbn: params[:msbn]).first
            }
            return
        else
            render json: {status: false, error_code: 1, error_message: "Sign failed."}
            return
        end
    end

    def momo_ipn
        # params[:bill_code]
        # params[:amount]
        # params[:pay_date]
        # params[:transaction_id]
        # sign = md5(secret + amount + bill_code + pay_date + transaction_id)
        secret = Rails.env == 'production' ? "wjgu456sxjg4mp96" : "bvdhyd_vp_secret"
        sign = ::Util.md5(secret.to_s + params[:amount].to_s + params[:bill_code].to_s + params[:pay_date].to_s + params[:transaction_id].to_s)
        if sign.downcase == params[:sign].to_s.downcase
            data = Fee.new.crawl({sohoadon: params[:bill_code], paid_status: 0})
            if data.length > 0
                fee = data.first
                amount_add = 0
                amount = fee.amount + MEDPRO_VP_SERVICE_FEE + amount_add
                pf = PaymentFee.new do |e|
                    e.method_id = 7
                    e.amount = amount
                    e.amount_original = fee.amount
                    e.amount_rate = VP_RATE_OFFLINE
                    e.amount_add = 0
                    e.amount_medpro = MEDPRO_VP_SERVICE_FEE
                    e.transaction_id = params[:transaction_id]
                    e.fee_id = fee.id
                    e.save!
                end
                pf.transaction_code_tt = (Rails.env == "production" ? "VP-" : "VPDev-") + Time.now.strftime("%y%m%d").to_s + pf.id.to_s
                pf.save!
                if pf.amount.to_i == params[:amount].to_i
                    pf.status = 1
                    pf.save!

                    Fee.where(id: fee.id).update(status: 1)
                    render json: {status: true}
                    return
                else
                    render json: {status: false, error_code: 3, error_message: "Money amount not match."}
                    return
                end
            else
                data = Fee.new.crawl({sohoadon: params[:bill_code], paid_status: 1})
                ap data.length
                if data.length > 0
                    render json: {status: false, error_code: 2, error_message: "Already paid."}
                    return
                else
                    render json: {status: false, error_code: 4, error_message: "Code not found."}
                    return
                end
            end
        else
            render json: {status: false, error_code: 1, error_message: "Sign failed."}
            return
        end
    end

    def check_transaction
        # params[:bill_code]
        # params[:transaction_id]
        # sign = md5(secret + bill_code + transaction_id)
        secret = Rails.env == 'production' ? "wjgu456sxjg4mp96" : "bvdhyd_vp_secret"
        sign = ::Util.md5(secret.to_s + params[:bill_code].to_s + params[:transaction_id].to_s)
        if sign.downcase == params[:sign].to_s.downcase
            fee = Fee.find_by(sohoadon: params[:bill_code])
            if fee.nil?
                render json: {status: false, error_code: 4, error_message: "Code not found."}
                return
            end
            pf = PaymentFee.find_by(fee_id: fee.id, transaction_id: params[:transaction_id])
            if pf
                if pf.status == 1
                    render json: {status: true}
                    return
                else
                    render json: {status: false, error_code: 5, error_message: "Payment has not been received."}
                    return
                end
            else
                render json: {status: false, error_code: 5, error_message: "Payment has not been received."}
                return
            end
        else
            render json: {status: false, error_code: 1, error_message: "Sign failed."}
            return
        end
    end

    def search_by_code
        # params[:bill_code]
        # sign = md5(secret + bill_code)
        secret = Rails.env == 'production' ? "wjgu456sxjg4mp96" : "bvdhyd_vp_secret"
        sign = ::Util.md5(secret.to_s + params[:bill_code].to_s)
        if sign.downcase == params[:sign].to_s.downcase
            newfee = Fee.new
            params[:bill_code] = params[:bill_code].gsub(/TA/i,"TA-") if params[:bill_code] =~ /TA\d/i
            params[:bill_code] = params[:bill_code].gsub(/TX/i,"-TX-") if params[:bill_code] =~ /TX\d/i
            data = newfee.crawl({sohoadon: params[:bill_code], paid_status: 0})
            if data.length > 0
                fee = data.first
                amount_fee = VP_RATE_OFFLINE * (fee.amount)
                amount_fee = 5500 if amount_fee < 5500
                amount_fee = 550000 if amount_fee > 550000
                amount = fee.amount + MEDPRO_VP_SERVICE_FEE + amount_fee
                data[0].amount = amount
            else
                data = Fee.new.crawl({sohoadon: params[:bill_code], paid_status: 1})
                if data.length > 0
                    render json: {status: false, error_code: 2, error_message: "Already paid."}
                    return
                else
                    render json: {status: false, error_code: 4, error_message: "Code not found."}
                    return
                end
            end
            render json: {
                status: true,
                data: data,
                patient: data.length > 0 ? data.first.patient : Patient.where("bvdhyd_msbn != '' AND bvdhyd_msbn IS NOT NULL").where(bvdhyd_msbn: params[:msbn]).first
            }
            return
        else
            render json: {status: false, error_code: 1, error_message: "Sign failed."}
            return
        end
    end

    def bill_code_ipn
        # params[:bill_code]
        # params[:amount]
        # params[:pay_date]
        # params[:transaction_id]
        # sign = md5(secret + amount + bill_code + pay_date + transaction_id)
        secret = Rails.env == 'production' ? "wjgu456sxjg4mp96" : "bvdhyd_vp_secret"
        sign = ::Util.md5(secret.to_s + params[:amount].to_s + params[:bill_code].to_s + params[:pay_date].to_s + params[:transaction_id].to_s)
        if sign.downcase == params[:sign].to_s.downcase
            data = Fee.new.crawl({sohoadon: params[:bill_code], paid_status: 0})
            if data.length > 0
                fee = data.first
                amount_add = VP_RATE_OFFLINE * (fee.amount)
                amount_add = 5500 if amount_add < 5500
                amount_add = 550000 if amount_add > 550000
                amount = fee.amount + MEDPRO_VP_SERVICE_FEE + amount_add
                pf = PaymentFee.new do |e|
                    e.method_id = 6
                    e.amount = amount
                    e.amount_original = fee.amount
                    e.amount_rate = VP_RATE_OFFLINE
                    e.amount_add = 0
                    e.amount_medpro = MEDPRO_VP_SERVICE_FEE
                    e.transaction_id = params[:transaction_id]
                    e.fee_id = fee.id
                    e.save!
                end
                pf.transaction_code_tt = (Rails.env == "production" ? "VP-" : "VPDev-") + Time.now.strftime("%y%m%d").to_s + pf.id.to_s
                pf.save!
                if pf.amount.to_i == params[:amount].to_i
                    pf.status = 1
                    pf.save!

                    Fee.where(id: fee.id).update(status: 1)
                    render json: {status: true}
                    return
                else
                    render json: {status: false, error_code: 3, error_message: "Money amount not match."}
                    return
                end
            else
                data = Fee.new.crawl({sohoadon: params[:bill_code], paid_status: 1})
                ap data.length
                if data.length > 0
                    render json: {status: false, error_code: 2, error_message: "Already paid."}
                    return
                else
                    render json: {status: false, error_code: 4, error_message: "Code not found."}
                    return
                end
            end
        else
            render json: {status: false, error_code: 1, error_message: "Sign failed."}
            return
        end
    end

    def is_admin_mpos
        @user = User.find_by(id: params[:user_id])
        mobile = @user.phone.sub("+84","0")
        if FeeMposAdmin.find_by(mobile: mobile)
            return true
        else
            render json: {error_code: 9990, error_message: "Not permission!"}
            return
        end
    end

    def search
        # params[:msbn]
        # params[:msnv]
        # params[:sohoadon]
        newfee = Fee.new
        data = nil
        bv_history = []
        fee = []
        if params[:msbn] && params[:msnv]
            data = newfee.crawl({msbn: params[:msbn], msnv: params[:msnv], paid_status: 0})
            tmp_data = Bvdhyd::Vienphi.search_history(params[:msbn])
            tmp_data.each do |e|
                if e["SoNV"].to_s.strip == params[:msnv].to_s.strip
                    e["msbn"] = e["SoHS"]
                    e["msnv"] = e["SoNV"]
                    e["sohoadon"] = e["SoPhieu"]
                    e["amount"] = e["SoTien"]
                    e["content"] = e["NoiDung"]
                    e["ngay"] = Time.strptime(e["Ngay"].to_s,'%Y-%m-%dT%H:%M:%S').localtime.strftime("%Y-%m-%d %H:%M:%S")
                    bv_history << e
                end
            end
            bv_history.sort! {|a,b| a["Ngay"] <=> b["Ngay"]}
            pf = PaymentFee.joins("LEFT JOIN fee ON FIND_IN_SET(fee.id, payment_fee.fee_id)").where("payment_fee.status = 1 AND fee.msbn = ? AND fee.msnv = ?", params[:msbn], params[:msnv])
            tmp = []
            if pf.length > 0
                pf.each do |e|
                    tmp += e.fee_id.split(",")
                end
                fee = Fee.where(status: 1, id: tmp).order('date_update')
                fee = fee.limit(params[:limit]) if params[:limit]
                fee = fee.offset(params[:offset]) if params[:offset]
                # fee.each do |f|
                    # f.patient = Patient.find_by(bvdhyd_msbn: f.msbn)
                # end
            end
        elsif params[:sohoadon]
            data = newfee.crawl({sohoadon: params[:sohoadon], paid_status: 0})
            pf = PaymentFee.joins("LEFT JOIN fee ON FIND_IN_SET(fee.id, fee_id)").where("payment_fee.status = 1 AND fee.sohoadon = ?", [params[:sohoadon]])
            tmp = []
            if pf.length > 0
                pf.each do |e|
                    tmp += e.fee_id.split(",")
                end
                fee = Fee.where(status: 1, id: tmp).order('date_update')
                fee = fee.limit(params[:limit]) if params[:limit]
                fee = fee.offset(params[:offset]) if params[:offset]
                # fee.each do |f|
                    # f.patient = Patient.find_by(bvdhyd_msbn: f.msbn)
                # end
            end
        else
            render json: {error_code: t(:params_missing_fee_code), error_message: t(:params_missing_fee)}
            return
        end
        render json: {
            status: true,
            data: data,
            patient: data.length > 0 ? data.first.patient : Patient.where("bvdhyd_msbn != '' AND bvdhyd_msbn IS NOT NULL").where(bvdhyd_msbn: params[:msbn]).first,
            bv_history: bv_history,
            history: fee
        }
        return
    end

    def search_history
        # params["msbn"]
        # params["msnv"]
        if params[:msbn] && params[:msnv]
            data = Bvdhyd::Vienphi.search_history(params[:msbn])
            return_data = []
            data.each do |e|
                if e["SoNV"].strip == params[:msnv].strip
                    return_data << e
                end
            end
            return_data.sort! {|a,b| a["Ngay"] <=> b["Ngay"]}
        else
            render json: {error_code: t(:params_missing_fee_code), error_message: t(:params_missing_fee)}
            return
        end
        render json: {
            bv_history: return_data,
            patient: nil,
            history: nil
        }
        return
    end

    def getbank
        params[:hospital_id] ||= 2
        banks = Bank.where(hospital_id: params[:hospital_id])
        tkb = banks.where(is_tkb: 1).order("priority ASC").each do |e|
            e.rate = VP_RATE_ATM
            e.const_rate = 0
            e.medpro_fee = MEDPRO_VP_SERVICE_FEE
            e.image = BASE_URL + '/st/bank/' + e.image
        end
        vis = banks.where(is_intcard: 1).order("priority ASC").each do |e|
            e.rate = VP_RATE_INT_CARD
            e.const_rate = VP_ADD_INT_CARD
            e.medpro_fee = MEDPRO_VP_SERVICE_FEE
            e.image = BASE_URL + '/st/bank/' + e.image
        end
        atm = banks.where.not(is_intcard: 1).where.not(is_tkb: 1).where.not(payoo_code: nil).order("priority ASC").each do |e|
            e.rate = VP_RATE_ATM
            e.const_rate = VP_ADD_ATM
            e.medpro_fee = MEDPRO_VP_SERVICE_FEE
            e.image = BASE_URL + '/st/bank/' + e.image
        end
        return_data = [
            {
                id: 1,
                name: "Thanh toán bằng thẻ khám bệnh",
                rate: VP_RATE_ATM,
                const_rate: 0,
                medpro_fee: MEDPRO_VP_SERVICE_FEE,
                data: tkb,
                type: 1 # type: 1=ATM, 2=VISA, 3=mPOS
            },
            {
                id: 2,
                rate: VP_RATE_INT_CARD,
                const_rate: VP_ADD_INT_CARD,
                medpro_fee: MEDPRO_VP_SERVICE_FEE,
                name: "Thanh toán bằng Thẻ quốc tế Visa, Master, JCB",
                data: vis,
                type: 2
            },
            {
                id: 3,
                rate: VP_RATE_ATM,
                const_rate: VP_ADD_ATM,
                medpro_fee: MEDPRO_VP_SERVICE_FEE,
                name: "Thanh toán bằng Thẻ ATM nội địa/Internet Banking",
                data: atm,
                type: 1
            },
            # {
            #     id: 4,
            #     rate: 0,
            #     const_rate: 0,
            #     medpro_fee: MEDPRO_VP_SERVICE_FEE,
            #     name: "Thanh toán bằng mPOS",
            #     data: [{
            #         id: 1000,
            #         name: "Thẻ nội địa",
            #         rate: VP_MPOS_INTERNAL,
            #         const_rate: 0,
            #         medpro_fee: MEDPRO_VP_SERVICE_FEE,
            #         image: BASE_URL + '/st/bank/card.png'
            #     },
            #     {
            #         id: 1001,
            #         name: "Thẻ quốc tế phát hành tại VN",
            #         rate: VP_MPOS_INTER_VN,
            #         const_rate: 0,
            #         medpro_fee: MEDPRO_VP_SERVICE_FEE,
            #         image: BASE_URL + '/st/bank/card.png'
            #     },
            #     {
            #         id: 1002,
            #         name: "Thẻ quốc tế phát hành tại nước ngoài",
            #         rate: VP_MPSO_INTER_EX,
            #         const_rate: 0,
            #         medpro_fee: MEDPRO_VP_SERVICE_FEE,
            #         image: BASE_URL + '/st/bank/card.png'
            #     }],
            #     type: 3
            # },
            {
                id: 5,
                rate: VP_RATE_OFFLINE,
                const_rate: 0,
                medpro_fee: MEDPRO_VP_SERVICE_FEE,
                name: "Thanh toán tại đại lý",
                data: [
                    {
                        id: 2001,
                        name: "Đến đại lý thanh toán.",
                        rate: VP_RATE_OFFLINE,
                        const_rate: 0,
                        medpro_fee: MEDPRO_VP_SERVICE_FEE,
                        image: BASE_URL + '/st/bank/home.png',
                        min_fee: 5500,
                        max_fee: 550000
                    }
                ],
                type: 4
            }
        ]
        render json: return_data
        return
    end

    def getorderoffline
        if params[:list_id]
            tmp_arr = params[:list_id].split(",")
            fees = Fee.where(id: tmp_arr)
            amount = 0
            fees.each do |e|
                if e.status != 0
                    render json: {error_code: 2223, error_message: "Không thể thanh toán cho phiếu tạm ứng này"}
                    return
                end
                amount += e.amount
            end
            if amount == 0
                render json: {error_code: 2223, error_message: "Không thể thanh toán cho phiếu tạm ứng này: 0đ"}
                return
            end
            amount_original = amount
            amount_add = 0
            rate = 0
            amount_medpro = MEDPRO_VP_SERVICE_FEE
            case params[:method_id].to_i
            when 1
                amount_add = VP_ADD_ATM
                rate = VP_RATE_ATM
            when 2
                amount_add = VP_ADD_INT_CARD
                rate = VP_RATE_INT_CARD
            when 3
                amount_add = VP_ADD_ATM
                rate = VP_RATE_ATM
            when 5
                amount_add = 0
                rate = VP_RATE_OFFLINE
            else
                render json: {error_code: t(:bank_not_found_code), error_message: t(:bank_not_found)}
                return
            end
            amount_fee = (amount_original) * rate
            amount_fee = 5500 if amount_fee < 5500
            amount_fee = 550000 if amount_fee > 550000
            amount = amount_original + amount_fee + amount_medpro
            amount = amount.to_i
            # lam tron 500
            # amount = ((amount + 499) / 500).to_i * 500

            pf = PaymentFee.new do |e|
                e.method_id = params[:method_id]
                e.amount = amount
                e.amount_original = amount_original
                e.amount_add = amount_add
                e.amount_rate = rate
                e.amount_medpro = amount_medpro
                e.user_id = params[:user_id]
                e.fee_id = tmp_arr.join(",")
                e.save!
            end
            if pf
                pf.transaction_code_tt = (Rails.env == "production" ? "VP-" : "VPDev-") + Time.now.strftime("%y%m%d").to_s + pf.id.to_s
                pf.save!
            end
            patient = Patient.find_by(bvdhyd_msbn: fees.first.msbn)
            result = ::Payoo::Main.create_offline_order({
                amount: amount, 
                transaction_code_tt: pf.transaction_code_tt,
                description: fees.pluck(:sohoadon).join(","),
                msbn: fees.first.msbn,
                patient_name: patient ? (patient.surname.to_s + " " + patient.name.to_s) : nil
            })
            if result[:code]
                Noti.new do |noti|
                    noti.type = 5
                    noti.user_id = pf.user_id
                    noti.booking_id = nil
                    noti.title = "Bạn vừa tạo mã thanh toán viện phí tại đại lý."
                    noti.content = "Mã thanh toán của bạn là: #{result[:code]}<br/>Mã sẽ hết hạn vào: #{result[:expired_date].to_time.strftime("%H:%M %d-%m-%Y")}"
                    noti.save!
                end
            end
            render json: result
            return
        else
            render json: {error_code: 123, error_message: 'list_id missing'}
            return
        end
        
    end

    def getordermpos
        # params[:list_id]
        # params[:user_id]
        # params[:method_id]
        # params[:bank_id]
        if !params[:method_id]
            render json: {error_code: 123, error_message: 'Chưa có phương thức thanh toán'}
            return
        end
        if ![1000,1001,1002].include?(params[:bank_id].to_i)
            render json: {error_code: t(:bank_not_found_code), error_message: t(:bank_not_found)}
            return
        end
        if !params[:operation_name]
            render json: {error_code: 124, error_message: 'Chưa có operation_name'}
            return
        end
        if params[:list_id]
            tmp_arr = params[:list_id].split(",")
            fees = Fee.where(id: tmp_arr)
            amount = 0
            fees.each do |e|
                if e.status != 0
                    render json: {error_code: 2223, error_message: "Không thể thanh toán cho phiếu tạm ứng này"}
                    return
                end
                amount += e.amount
            end
            if amount == 0
                render json: {error_code: 2223, error_message: "Không thể thanh toán cho phiếu tạm ứng này: 0đ"}
                return
            end
            amount_original = amount
            amount_add = 0
            rate = 0
            amount_medpro = MEDPRO_VP_SERVICE_FEE
            amount = amount_original + (amount_original * rate) + amount_add + amount_medpro
            # lam tron 500
            # amount = ((amount + 499) / 500).to_i * 500

            pf = PaymentFee.new do |e|
                e.method_id = params[:method_id]
                e.amount = amount
                e.amount_original = amount_original
                e.amount_add = amount_add
                e.amount_rate = rate
                e.amount_medpro = amount_medpro
                e.user_id = params[:user_id]
                e.fee_id = tmp_arr.join(",")
                e.save!
            end
            if pf
                pf.transaction_code_tt = (Rails.env == "production" ? "VP-" : "VPDev-") + Time.now.strftime("%y%m%d").to_s + pf.id.to_s
                pf.save!
            end
            patient = Patient.find_by(bvdhyd_msbn: fees.first.msbn)
            data = ::Payoo::Main.create_mpos_order({
                amount: amount,
                transaction_code_tt: pf.transaction_code_tt,
                operation_name: params[:operation_name],
                description: fees.pluck(:sohoadon).join(","),
                msbn: fees.first.msbn,
                patient_name: patient ? (patient.surname.to_s + " " + patient.name.to_s) : nil
            })
            render json: {
                status: true,
                data: data
            }
            return
        else
            render json: {error_code: 123, error_message: 'list_id missing'}
            return
        end
    end

    def getorder
        # params[:list_id]
        # params[:bank_id]
        # params[:method_id]
        if !params[:method_id]
            render json: {error_code: 123, error_message: 'Chưa có phương thức thanh toán'}
            return
        end
        bank = Bank.find_by(id: params[:bank_id])
        unless bank
            render json: {error_code: t(:bank_not_found_code), error_message: t(:bank_not_found)}
            return
        end
        order = nil
        if params[:list_id]
            tmp_arr = params[:list_id].split(",")
            fees = Fee.where(id: tmp_arr)
            amount = 0
            fees.each do |e|
                if e.status != 0
                    render json: {error_code: 2223, error_message: "Không thể thanh toán cho phiếu tạm ứng này"}
                    return
                end
                amount += e.amount
            end
            if amount == 0
                render json: {error_code: 2223, error_message: "Không thể thanh toán cho phiếu tạm ứng này: 0đ"}
                return
            end
            amount_original = amount
            amount_add = 0
            rate = 0
            amount_medpro = MEDPRO_VP_SERVICE_FEE
            case params[:method_id].to_i
            when 1
                amount_add = VP_ADD_ATM
                rate = VP_RATE_ATM
            when 2
                amount_add = VP_ADD_INT_CARD
                rate = VP_RATE_INT_CARD
            when 3
                amount_add = VP_ADD_ATM
                rate = VP_RATE_ATM
            else
                render json: {error_code: t(:bank_not_found_code), error_message: t(:bank_not_found)}
                return
            end
            amount = amount_original + (amount_original * rate) + amount_add + amount_medpro
            amount = amount.to_i
            # lam tron 500
            # amount = ((amount + 499) / 500).to_i * 500

            pf = PaymentFee.new do |e|
                e.method_id = params[:method_id]
                e.amount = amount
                e.amount_original = amount_original
                e.amount_add = amount_add
                e.amount_rate = rate
                e.amount_medpro = amount_medpro
                e.user_id = params[:user_id]
                e.fee_id = tmp_arr.join(",")
                e.save!
            end
            if pf
                pf.transaction_code_tt = (Rails.env == "production" ? "VP-" : "VPDev-") + Time.now.strftime("%y%m%d").to_s + pf.id.to_s
                pf.save!
            end
            patient = Patient.find_by(bvdhyd_msbn: fees.first.msbn)
            order = ::Payoo::Main.create_order({
                amount: amount, 
                transaction_code_tt: pf.transaction_code_tt, 
                code: bank.payoo_code,
                description: fees.pluck(:sohoadon).join(","),
                msbn: fees.first.msbn,
                patient_name: patient ? (patient.surname.to_s + " " + patient.name.to_s) : nil
            })
        end
        render json: {
            mobile: ::Payoo::Main.create_mobile_order({
                    amount: amount, 
                    transaction_code_tt: pf.transaction_code_tt,
                    description: fees.pluck(:sohoadon).join(","),
                    msbn: fees.first.msbn,
                    patient_name: patient ? (patient.surname.to_s + " " + patient.name.to_s) : nil
                }),
            order: order
        }
        return
    end

    def resultmpos
        if params[:PurchaseOrderNo]
            pf = PaymentFee.find_by(transaction_code_tt: params[:PurchaseOrderNo])
            if pf
                if pf.status == 0
                    pf.result = params.permit!.to_json
                    pf.status = 1
                    pf.save!

                    tmp_arr = pf.fee_id.to_s.split(",")
                    fees = Fee.where(id: tmp_arr)
                    fees.each do |e|
                        e.patient = Patient.find_by(bvdhyd_msbn: e.msbn)
                    end
                    fees.update(status: 1)
                    Noti.new do |noti|
                        noti.type = 5
                        noti.user_id = pf.user_id
                        noti.booking_id = nil
                        noti.title = "Bạn đã thanh toán viện phí thành công."
                        # noti.content = "Đặt phiếu khám bệnh <span style='color: #398BE8;font-weight: 700;'>(#{bk.transaction_code_gd})</span> thành công."
                        noti.content = "Bạn đã thanh toán viện phí thành công."
                        noti.save!
                    end
                end
            end
        end
        render json: {status: true, data: fees}
        return
    end

    def detail
        # params[:list_id]
        tmp_arr = params[:list_id].split(",")
        pf = PaymentFee.where(user_id: params[:user_id], fee_id: tmp_arr.join(","), status: 1)
        if pf.length == 0
            render json: {error_code: 8888, error_message: "Không tìm thấy kết quả giao dịch"}
            return
        end
        fee = Fee.where(id: tmp_arr)
        return_data = {
            data: fee,
            patient: fee.length > 0 ? Patient.find_by(bvdhyd_msbn: fee.first.msbn) : nil,
            payment: pf
        }
        render json: return_data
        return
    end

    def history
        pf = PaymentFee.where(user_id: params[:user_id], status: 1)
        tmp = []
        fee = []
        if pf.length > 0
            pf.each do |e|
                tmp += e.fee_id.split(",")
            end
            fee = Fee.where(status: 1, id: tmp).order('date_update')
            fee = fee.limit(params[:limit]) if params[:limit]
            fee = fee.offset(params[:offset]) if params[:offset]
            fee.each do |f|
                f.patient = Patient.find_by(bvdhyd_msbn: f.msbn)
            end
        end
        return_data = {
            data: fee,
            patient: nil
        }
        render json: return_data
        return
    end

    def payoochecksum(in_checksum, str, secret)
        checksum = ::Util.sha512(secret + str)
        ap checksum
        return (checksum.downcase == in_checksum.downcase)
    end

    include Swagger::Blocks
    swagger_path '/fee/search' do
        operation :get do
            key :summary, 'Fee Search'
            key :description, 'Returns all Fee'
            key :operationId, 'allfee'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :msbn, in: :query, description: 'msbn'
            parameter name: :msnv, in: :query, description: 'msnv'
            parameter name: :sohoadon, in: :query, description: 'sohoadon'

            response 200 do
                key :description, 'Fee response'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Fee
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/search_history' do
        operation :get do
            key :summary, 'Search History by MSBN & MSNV'
            key :description, 'Returns all History'
            key :operationId, 'search_history'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :msbn, in: :query, description: 'msbn'
            parameter name: :msnv, in: :query, description: 'msnv'

            response 200 do
                key :description, 'History response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/getorder' do
        operation :get do
            key :summary, 'Create Order to Pay'
            key :description, 'Create Order to Pay'
            key :operationId, 'getorder'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :list_id, in: :query, description: 'Danh sách cần thanh toán 1,2,3...'
            parameter name: :bank_id, in: :query, description: 'bank_id'
            parameter name: :method_id, in: :query, description: 'method_id'

            response 200 do
                key :description, 'Fee response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/getorderoffline' do
        operation :get do
            key :summary, 'Create Order Code to Pay'
            key :description, 'Create Order Code to Pay'
            key :operationId, 'getorderoffline'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :list_id, in: :query, description: 'Danh sách cần thanh toán 1,2,3...'
            parameter name: :method_id, in: :query, description: 'method_id'

            response 200 do
                key :description, 'Fee response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/getordermpos' do
        operation :get do
            key :summary, 'Create Order to Pay'
            key :description, 'Create Order to Pay'
            key :operationId, 'getordermpos'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :list_id, in: :query, description: 'Danh sách cần thanh toán 1,2,3...'
            parameter name: :bank_id, in: :query, description: 'bank_id'
            parameter name: :method_id, in: :query, description: 'method_id'
            parameter name: :operation_name, in: :query, description: 'dùng cái OperationName'

            response 200 do
                key :description, 'Fee response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/resultmpos' do
        operation :post do
            key :summary, 'Create Order to Pay'
            key :description, 'Create Order to Pay'
            key :operationId, 'getresultmpos'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]

            response 200 do
                key :description, 'Fee response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/detail' do
        operation :get do
            key :summary, 'Detail Fee sau khi thanh toán xong'
            key :description, 'Detail Fee sau khi thanh toán xong'
            key :operationId, 'detail'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :list_id, in: :query, description: 'Danh sách cần thanh toán 1,2,3...'

            response 200 do
                key :description, 'Fee response:  Note: type: 1=ATM, 2=VISA'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/history' do
        operation :get do
            key :summary, 'VP Pay History'
            key :description, 'VP Pay History'
            key :operationId, 'history'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'

            response 200 do
                key :description, 'Fee response'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Fee
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/fee/getbank' do
        operation :get do
            key :summary, 'Get all banks'
            key :description, 'Get all banks'
            key :operationId, 'getbank'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'fee'
            ]

            response 200 do
                key :description, 'Fee response: Note: type: 1=ATM, 2=VISA'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Fee
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
