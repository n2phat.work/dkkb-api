class Api::ClinicController < ApplicationController
    before_action :check_sign_in

    def getall
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            msbn = data["msbn"]
            BvdhydCls.crawl({msbn: msbn})
            data = BvdhydCls.where(SoHS: msbn).group(:NgayThucHien).order('NgayThucHien DESC').pluck(:NgayThucHien)
            render json: {data: data}
            return
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def detail_by_date
        # params["date"]
        # params["msbn"]
        # params["user_id"]
        # params["access_token"]
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            msbn = data["msbn"]
            date = data["date"]
            data = BvdhydCls.where(SoHS: msbn, NgayThucHien: date)
            render json: {data: data.as_json({methods: [:cls_name]})}
            return
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def pdf
        # params[:cls_id]
        # params["user_id"]
        # params["access_token"]
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            cls_id = data["cls_id"]
            cls = BvdhydCls.find(cls_id) rescue nil
            # render json: @cls.result
            # return
            if cls
                # pdf_obj = ::Pdf::Main.new(cls)
                # pdf = pdf_obj.to_pdf
                
                # send_data pdf, :filename => "abc.pdf"
                # send_file pdf, :filename => "abc.pdf"
                render layout: 'test', template: 'adm/test/noisoi_pdf', encoding: "utf-8", locals: {cls: cls} 
            else
                render json: "Không tìm thấy kết quả"
                return
            end
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    include Swagger::Blocks
    swagger_path '/clinic/getall' do
        operation :get do
            key :summary, 'Get all clinics'
            key :description, 'Get all clinics, có dùng jwt để mã hóa'
            key :operationId, 'clinicgetall'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'clinic - Cận Lâm Sàng'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :msbn, in: :query, description: 'msbn', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'clinics response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/clinic/detail_by_date' do
        operation :get do
            key :summary, 'Get detail clinics by date'
            key :description, 'Get detail clinics by date, có dùng jwt để mã hóa'
            key :operationId, 'detail_by_date'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'clinic - Cận Lâm Sàng'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :msbn, in: :query, description: 'msbn', required: true
            parameter  name: :date, in: :query, description: 'date', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'clinics response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/clinic/pdf' do
        operation :get do
            key :summary, 'Detail CLS...'
            key :description, 'Detail CLS, có dùng jwt để mã hóa'
            key :operationId, 'clinicpdf'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'clinic - Cận Lâm Sàng'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :cls_id, in: :query, description: 'cls_id', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'clinics response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
