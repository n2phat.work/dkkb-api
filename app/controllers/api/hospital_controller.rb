class Api::HospitalController < ApplicationController
    before_action :check_sign_in
    
    def getall
        hospitals = Hospital.all
        render json: hospitals.to_json(include: [
            { city: { except: [:date_create, :date_update] } }
        ])
    end

    def detail
        # arr_subject = Subject.where(id: Schedule.select(:hospital_subject_id).distinct.pluck(:hospital_subject_id))
        hospital = Hospital.find_by(id: params[:hospital_id])
        render json: hospital.to_json(methods: :subject, include: [
            { city: { except: [:date_create, :date_update] } }
        ])
    end

    include Swagger::Blocks
    swagger_path '/hospital/getall' do
        operation :get do
            key :summary, 'All Hospitals'
            key :description, 'Returns all hospitals'
            key :operationId, 'allhospital'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'hospital'
            ]
            response 200 do
                key :description, 'hospital response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Hospital
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/hospital/detail' do
        operation :get do
            key :summary, 'Get detail hospital'
            key :description, 'Returns detail hospital'
            key :operationId, 'detailhospital'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'hospital'
            ]

            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :hospital_id, in: :query, description: 'ID hospital', required: true
            response 200 do
                key :description, 'hospital response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Hospital
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end