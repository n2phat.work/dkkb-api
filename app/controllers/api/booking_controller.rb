class Api::BookingController < ApplicationController
    before_action :check_sign_in
    before_action :check_ip, only: [:insertmulti]

    def check_ip
        if (Time.now < C.parse('2018-09-15 07:18:00'))
            ip = request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip
            arr_allow_ip = %w(118.69.62.199 172.16.11.61 172.16.11.52 118.69.51.16 118.69.51.17 118.69.51.18 118.69.51.19 118.69.51.20 118.69.51.21 118.69.51.22 125.234.101.185 125.234.101.186 125.234.101.187 125.234.101.188 125.234.101.189 125.234.101.190 125.234.96.102 125.234.96.104 125.234.101.177 125.234.101.178 125.234.101.179 125.234.101.180 125.234.101.181 125.234.101.182 203.205.32.196)
            if !arr_allow_ip.include?(ip)
                render json: { error_code: 1, error_message: "IP not allowed" }
                return
            end
        end
    end

    private def _smsname(surname, name, short = false)
        if short
            arr = surname.split(" ")
            r = []
            arr.each do |e|
                r << e[0].upcase
            end
            result = ""
            unless r.empty?
                result = r.join(".")
            end
            return result + "." + ::Util.remove_dau(name)
        else
            return ::Util.remove_dau(surname.to_s) + " " + ::Util.remove_dau(name.to_s)
        end
    end

    private def _smsbuoi(id)
        unless id.to_i == 1
            "CHIEU"
        end
        "SANG"
    end

    private def _smsbsname(str)
        arr = str.split(" ")
        return ::Util.remove_dau(arr.last)
    end

    private def _smsgiokham(from,to)
        _strtotime(from) + "-" + _strtotime(to)
    end

    private def _strtotime(str)
        num = str.to_f
        hour = (num * 60 / 60).to_i
        min = (num * 60 % 60).to_i
        return hour.to_s + "h" + (min != 0 ? min.to_s : "")
    end

    private def _end_text(bk)
        if bk.bhyt_accept >= 1
            return "quay 12, 13, 14"
        else
            return "may phat so tu dong"
        end
    end

    def resend_code
        # params[:booking_id]
        # params[:mobile]
        if !params[:booking_id]
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        else
            booking = Booking.find_by(id: params[:booking_id], user_id: params[:user_id])
            if booking
                mobile = params[:mobile] ? params[:mobile] : (booking.patient ? (booking.patient.mobile ? booking.patient.mobile : booking.booking_phone) : booking.booking_phone)
                # send sms
                if booking.sms_count < 2
                    mobile = mobile.sub("+84","0")
                    sms_msg = "BN #{_smsname(booking.patient.surname, booking.patient.name)}, Ma so: #{booking.transaction_code_gd}, Kham #{Chronic.parse(booking.booking_date).strftime('%d/%m/%Y')}, #{_smsgiokham(booking.booking_time.from,booking.booking_time.to)}, STT: #{booking.booking_number}, khoa #{::Util.remove_dau(booking.schedule.subject.name)}, #{::Util.remove_dau(booking.schedule.room.name)}. Vui long lay STT tai #{_end_text(booking)}."
                    sms_msg = "BN #{_smsname(booking.patient.surname, booking.patient.name, true)}, Ma so: #{booking.transaction_code_gd}, Kham #{Chronic.parse(booking.booking_date).strftime('%d/%m/%Y')}, #{_smsgiokham(booking.booking_time.from,booking.booking_time.to)}, STT: #{booking.booking_number}, khoa #{::Util.remove_dau(booking.schedule.subject.name)}, #{::Util.remove_dau(booking.schedule.room.name)}. Vui long lay STT tai #{_end_text(booking)}." if sms_msg.length > 160

                    client = Savon.client(wsdl: SMS_SOAP_URL)
                    logger.info(sms_msg)
                    result = client.call(:send_sms, message: {
                        authenticate_user: SMS_USER, 
                        authenticate_pass: SMS_AUTHEN, 
                        brand_name: SMS_BRAND_NAME,
                        message: sms_msg,
                        receiver: mobile.to_s,
                        type: 1
                    })
                    LogSms.new do |e|
                        e.booking_id = booking.id
                        e.mobile = mobile
                        e.content = sms_msg
                        e.response = result
                        e.save!
                    end
                else
                    render json: { error_code: t(:sms_max_code), error_message: t(:sms_max) }
                    return
                end
                # send email 
                if booking.patient && booking.status == BOOKING_PAYMENT_DONE
                    if booking.patient.email && booking.patient.email != ""
                        # email_params = {
                        #     to: booking.patient.email,
                        #     subject: "BVDHYD - Xác nhận Phiếu Khám Bệnh",
                        #     booking_id: params[:booking_id]
                        # }
                        # Api::CurlJob.perform_later(MAIL_SERVER_URL, email_params)
                        # ::Util.curl_post(MAIL_SERVER_URL, email_params)
                        # Api::BookingMailer.sendmail(booking.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh: #{booking.transaction_code_gd}", params[:booking_id]).deliver_now
                        begin
                            Api::BookingMailJob.perform_later(booking.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh: #{booking.transaction_code_gd}", params[:booking_id])
                        rescue => e
                            ap e
                        end
                    end
                end
                # end
                begin
                    # Redis::BookingJob.perform_later(booking.user_id)
                rescue => e
                    ap e
                end
                render json: {
                    mobile: params[:mobile] ? params[:mobile] : booking.patient.mobile,
                    mobile_censored: booking.patient.mobile_censored
                }
                return
            else
                render json: { error_code: t(:booking_not_found_code), error_message: t(:booking_not_found) }
                return
            end
        end
    end

    def cancel
        # params[:booking_id]
        if !params[:booking_id]
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        else
            booking = Booking.find_by(id: params[:booking_id], user_id: params[:user_id], status: BOOKING_PAYMENT_DONE)
            if booking
                if Time.now.strftime("%H:%M").to_s >= "16:30"
                    current_time = (Time.now + 1.days).strftime("%Y-%m-%d").to_s 
                else
                    current_time = Time.now.strftime("%Y-%m-%d").to_s
                end
                if booking.booking_date.to_s <= current_time
                    render json: { error_code: t(:booking_cancel_on_same_day_code), error_message: t(:booking_cancel_on_same_day) }
                    return
                end
                payment = Payment.find_by(id: booking.payment_id)
                if payment
                    # payment auto callback update booking
                    payment.update(status: PAYMENT_CANCEL_LATER, status_process: 0, refund_status: 1)
                end
            end
            render json: { status: 1 }
            return
        end
    end
    
    def getbyuserid
        if params[:user_id]
            data = nil #$redis.get('booking_list_' + params[:user_id].to_s)

            if data
                render json: data
                return
            else
                booking = Booking.where(user_id: params[:user_id]).where.not(status: 0).where.not(status: -1)
                # if params[:patient_id]
                #     booking = booking.where(patient_id: params[:patient_id])
                # end
                booking = booking.order('id DESC')
                result = booking.to_json

                # $redis.set('booking_list_' + params[:user_id].to_s, result)
                # $redis.expire('booking_list_' + params[:user_id].to_s, REDIS_BOOKING_LIST_CACHE_TIME.minute.to_i)
                render json: result
                return
            end
        else
            render json: {error_code: t(:user_params_missing_user_id_access_token_code),error_message: t(:user_params_missing_user_id_access_token)}
            return
        end
    end

    def detail
        if params[:booking_id]
            booking = Booking.find_by(id: params[:booking_id], user_id: params[:user_id])
            if booking
                booking.related = Booking.where(payment_id: booking.payment_id).where.not(id: booking.id).pluck(:transaction_code_gd)
                render json: booking
                return
            else
                render json: { error_code: t(:booking_not_found_code), error_message: t(:booking_not_found) }
                return
            end
        else
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        end
    end

    def edit

    end

    def back_delete
        # params[:user_id]
        # params[:access_token]
        # params[:transaction_code_tt]
        if params[:transaction_code_tt] && params[:transaction_code_tt] != ''
            payment = Payment.find_by(transaction_code_tt: params[:transaction_code_tt])
            if payment && payment.status == 0
                bookings = Booking.where(payment_id: payment.id)
                bookings.each do |e|
                    # e.status = BOOKING_PAYMENT_CANCEL
                    e.is_deleted = 1
                    e.save!
                    # xoa DSK ben BV
                    Bvdhyd::Lichkham.deleteDSK({transaction_code_gd: e.transaction_code_gd}) if Rails.env == "production"
                end
                render json: {status: true}
                return
            end
        else
            render json: {error_code: t(:params_missing_code), error_message: t(:params_missing)}
            return
        end
    end

    def insertmulti
        # {
        #     "bookings" : [{
        #         "doctor_id": 1,
        #         "room_id": 2,
        #         "hospital_id":2,
        #         "subject_id": 2,
        #         "bhyt": 1,
        #         "booking_time_id": 1,
        #         "booking_phone": "0123456789"
        #     },
        #     {
        #         "doctor_id": 1,
        #         "room_id": 2,
        #         "hospital_id":2,
        #         "subject_id": 2,
        #         "bhyt": 1,
        #         "booking_time_id": 1,
        #         "booking_phone": "0123456789"
        #     }],
        #     "date":"2018-06-01",
        #     "patient_id": 1,
        #     "user_id": 1,
        #     "access_token": "",
        #     "bank_id": 1,
        #     "method_id": 1,
        #     "platform":"web"
        # }
        arr_rest_days = ['0101', '0209', '3004', '0105']
        arr_holidays = Holiday.all.pluck(:date)
        i = 0
        bookings = []
        booking_id_list = []
        total = 0
        unless params[:date] && params[:date] != ''
            render json: {error_code: t(:params_missing_date_code), error_message: t(:params_missing_date)}
            return
        end
        tmp_date = Time.parse(params[:date])
        tmp_30_day = Time.now + 30.days
        tmp_date_1day = Time.parse(params[:date].to_s + " 16:30:00")
        tmp_date_now_1day = Time.now + 1.days
        if tmp_date >= tmp_30_day || tmp_date_now_1day > tmp_date_1day
            render json: {error_code: t(:out_of_30_days_code), error_message: t(:out_of_30_days)}
            return
        end
        if tmp_date <= Time.now
            render json: {error_code: t(:date_in_past_code), error_message: t(:date_in_past)}
            return
        end
        if arr_rest_days.include? tmp_date.strftime("%d%m")
            render json: {error_code: -33, error_message: "Không thể đặt khám vào ngày nghỉ."}
            return
        end
        if arr_holidays.include? tmp_date.strftime("%Y-%m-%d")
            render json: {error_code: -33, error_message: "Không thể đặt khám vào ngày nghỉ."}
            return
        end
        unless params[:patient_id] && params[:patient_id] != ''
            render json: {error_code: t(:params_missing_patient_id_code), error_message: t(:params_missing_patient_id)}
            return
        end
        patient = Patient.find_by(id: params[:patient_id])
        unless patient
            render json: {error_code: t(:patient_not_found_code), error_message: t(:patient_not_found)}
            return
        end
        bank = Bank.find_by(id: params[:bank_id])
        if bank.nil? && params[:bank_id].to_i != 9999 && params[:bank_id].to_i != 2001 && params[:bank_id].to_i != 10000
            render json: {error_code: t(:bank_not_found_code), error_message: t(:bank_not_found)}
            return
        end
        # tinh toan thoi gian expired voi thanh toan dai ly va thanh toan chuyen khoan
        expired_date = nil
        if params[:method_id].to_i == 5 || params[:method_id].to_i == 6
            expired_date = Time.new(Time.now.year, Time.now.month, Time.now.day, 23,59,59)
            if (Time.now + 1.days) > (tmp_date_1day - 1.days)
                expired_date = tmp_date_1day - 1.days
            end
        end
        if params[:bookings].length > 3
            render json: {error_code: -34, error_message: "Không thể đặt khám nhiều hơn 3 chuyên khoa."}
            return
        end
        params[:bookings].each do |e|
            unless e["hospital_id"] && e["hospital_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_hospital_id) + ": #{i}" }
                return
            end
            unless e["subject_id"] && e["subject_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_subject_id) + ": #{i}" }
                return
            end
            unless e["booking_time_id"] && e["booking_time_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_booking_time_id) + ": #{i}" }
                return
            end
            unless e["doctor_id"] && e["doctor_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_doctor_id) + ": #{i}" }
                return
            end
            unless e["room_id"] && e["room_id"] != ''
                render json: {error_code: t(:element_missing_code),error_message: t(:element_missing_room_id) + ": #{i}" }
                return
            end
            
            schedule = Schedule.with_hospital_subject(
                e["hospital_id"], 
                e["subject_id"]
            ).find_by(
                room_id: e["room_id"], 
                doctor_id: e["doctor_id"]
            )
            if schedule
                count = Booking.where('status >= 0 AND is_deleted != 1').where(booking_date: params[:date], schedule_id: schedule.id, booking_time_id: e["booking_time_id"]).count
                bk_time = BookingTime.find_by(id: e["booking_time_id"])
                if count.to_i < bk_time.max_slot.to_i
                    booking = Booking.new do |b|
                        b.platform = params[:platform]
                        b.booking_date = params[:date]
                        b.user_id = params[:user_id]
                        b.schedule_id = schedule.id
                        b.booking_time_id = e["booking_time_id"]
                        b.patient_id = params["patient_id"]
                        b.booking_phone = e["booking_phone"]
                        b.bhyt_accept = e["bhyt"].to_i == 3 ? 0 : e["bhyt"]
                        b.email = e["email"]
                        b.status = BOOKING_NEW
                        b.date_expired = expired_date
                        b.save!
                    end
                    if booking
                        booking_day = BookingDay.new do|e|
                            e.booking_id = booking.id
                            e.save!
                        end
                        if booking_day
                            if params[:platform].nil? || params[:platform] == 'web'
                                prefix = 'W'
                            else
                                prefix = 'A'
                            end
                            booking.transaction_code_gd = prefix + Time.now.strftime("%y%m%d") + _format_id(booking_day.id.to_s)
                            booking.save!
                        end
                    end
                    # check reserve number
                    #if Rails.env == 'production'
                        number = _give_number_bk(booking.booking_time_id, booking.booking_date, booking.schedule.room_id)
                        if number == 0
                            render json: {error_code: t(:out_of_slot_online_check_code),error_message: t(:out_of_slot_online_check) + ": #{i}"}
                            return
                        end
                        booking.booking_number = number
                        booking.save!
                    #end
                    # pre-insert
                    Dhyd::TrackJob.perform_now(booking_id: booking.id, booking_number: number)

                    bookings << booking
                    total += booking.subject.price
                    booking_id_list << booking.id
                else
                    render json: {error_code: t(:element_out_of_slot_code), error_message: t(:element_out_of_slot) + ": #{i}"}
                    return
                end
            else
                render json: {error_code: t(:element_schedule_not_found_code), error_message: t(:element_schedule_not_found) + ": #{i}"}
                return
            end
            i += 1
        end
        # payment
        amount = 0
        amount_service = MEDPRO_SERVICE_FEE
        amount_gate = 0
        rate_percent = 0
        rate_const = 0

        if bank
            case bank.vnpay_code
            when 'VISA'
                rate_percent = RATE_INT_CARD
                rate_const = ADD_INT_CARD
                method = 1
            when 'VIETINBANK'
                if (params[:method_id].to_i == 1)
                    method = 1
                else
                    rate_percent = RATE_ATM
                    rate_const = ADD_ATM
                    method = 1
                    # method = 2
                end
            else
                rate_percent = RATE_ATM
                rate_const = ADD_ATM
                method = 1
                # method = 2
            end
            amount_gate = rate_percent * (total + amount_service) + rate_const
            amount = total + amount_service + amount_gate
            # lam tron 500
            # amount = ((amount + 499) / 500).to_i * 500
            # end lam tron
        else
            case params[:method_id].to_i
            when 7 # Momo
                rate_percent = 0
                rate_const = 0
                method = 1

                amount_gate = rate_percent * (total + amount_service) + rate_const
                amount = total + amount_service + amount_gate
            when 6
                rate_percent = RATE_ATM
                rate_const = ADD_ATM
                method = 1

                amount_gate = rate_percent * (total + amount_service) + rate_const
                amount = total + amount_service + amount_gate
            when 5
                rate_percent = VP_RATE_OFFLINE
                rate_const = 0
                method = 1

                amount_gate = rate_percent * total + rate_const
                amount_gate = 5500 if amount_gate < 5500
                amount_gate = 550000 if amount_gate > 550000
                amount = total + amount_service + amount_gate
            end
        end

        payment = Payment.new do |pm|
            pm.transdate =  ::Util.getCurrentDateTime
            pm.method_id = params[:method_id]
            pm.amount = amount
            pm.amount_original = total
            pm.amount_service = amount_service
            pm.amount_gate = amount_gate
            pm.rate_percent = rate_percent
            pm.rate_const = rate_const
            pm.card_code = params[:bank_code]
            pm.user_id = params[:user_id]
            pm.save!
        end
        if payment
            offline_code = nil
            payment_url = nil
            offline_msg = nil
            momo_deeplink =  nil
            momo_deeplinkWebInApp = nil
            payment.transaction_code_tt = (Rails.env == 'production' ? "TT-" : "TTDev-") + Time.now.strftime("%y%m%d").to_s + payment.id.to_s
            payment.save!
            Booking.where(id: booking_id_list).update_all(payment_id: payment.id)
            case method
            when 1
                if params[:method_id].to_i == 2
                    payment_url = BASE_URL + '/api/resource/cs?' + {transaction_code_tt: payment.transaction_code_tt, amount: payment.amount, p_redirect: request.headers['origin'].to_s}.to_query
                elsif params[:method_id].to_i == 6
                    # render json: {error_code: 123, error_message: PKH_TRANSFER_TEXT + payment.transaction_code_tt}
                    # return
                    offline_msg = PKH_TRANSFER_TEXT + '<b>' + payment.transaction_code_tt + '</b>' + "</p><p>Số tiền: <b>#{ActiveSupport::NumberHelper::number_to_delimited(payment.amount, delimiter: ".")} VNĐ</b></p><p><i>(Mã TT sẽ hết hạn vào 23:59:59 hôm nay hoặc sẽ hết hạn sau 16:00:00 hôm nay nếu đặt khám vào ngày mai)</i></p><p><font color=\"red\"><b>NGƯỜI BỆNH KHÔNG TỰ Ý CHUYỂN TIỀN VÀO TÀI KHOẢN BỆNH VIỆN!</b></font></p>"
                elsif params[:method_id].to_i == 5
                    # render json: {error_code: 123, error_message: "Đã chọn thanh toán đại lý"}
                    # return
                    offline_code = ::Payoo::MainOffline.create_offline_order({
                        amount: payment.amount, 
                        transaction_code_tt: payment.transaction_code_tt,
                        expired_date: expired_date
                    })
                    if offline_code[:code]
                        payment.payment_payoo = PaymentPayoo.new({
                            offline_code: offline_code[:code]
                        })
                        payment.save!
                        Noti.new do |noti|
                            noti.type = 5
                            noti.user_id = payment.user_id
                            noti.booking_id = nil
                            noti.title = "Bạn vừa tạo mã thanh toán phiếu khám bệnh tại đại lý."
                            noti.content = "Mã thanh toán của bạn là: #{offline_code[:code]}<br/>(Hết hạn: #{offline_code[:expired_date].to_time.strftime("%H:%M %d-%m-%Y")})"
                            noti.save!
                        end
                    end
                elsif params[:method_id].to_i == 7
                    momo = ::Momo::Main.new
                    momo_data = momo.create_order({
                        amount: payment.amount,
                        transaction_code_tt: payment.transaction_code_tt,
                        info: "Thanh toán Phiếu khám bệnh",
                        extra: params[:platform]
                    })
                    if momo_data["payUrl"]
                        payment_url = (params[:platform] == 'ios' || params[:platform] == 'android') ? momo_data["deeplink"] : momo_data["payUrl"]
                        momo_deeplink =  momo_data["deeplink"]
                        momo_deeplinkWebInApp = momo_data["deeplinkWebInApp"]
                    else
                        render json: {error_code: -35, error_message: "Có lỗi xảy ra khi thanh toán bằng Momo."}
                        return
                    end
                else
                    payment_url = ::Vnpay::Main.get_payment_url({
                        amount: payment.amount,
                        transaction_code_tt: payment.transaction_code_tt,
                        info: request.headers['origin'].to_s + "-" + ::Util.md5(::Util.milisec),
                        create_date: payment.transdate,
                        vnp_secret: params[:method_id].to_i == 1 ? VNP_HASHSECRET_TKB : VNP_HASHSECRET,
                        vnp_tmncode: params[:method_id].to_i == 1 ? VNPAY_TMN_CODE_TKB : VNPAY_TMN_CODE,
                        code: Rails.env == 'production' ? bank.vnpay_code : "NCB",
                        # code: bank.vnpay_code
                    })
                end
            when 2
                payment_url = ::Payoo::Main.get_payment_url({
                    amount: payment.amount,
                    transaction_code_tt: payment.transaction_code_tt,
                    code: bank.payoo_code
                })
            end
        end

        # end payment
        render json: {
            total: total,
            payment: payment,
            list: booking_id_list,
            payment_url: payment_url,
            bookings: bookings,
            offline_code: offline_code,
            offline_msg: offline_msg,
            momo_deeplink: momo_deeplink,
            momo_deeplinkWebInApp: momo_deeplinkWebInApp
        }
        return
    end

    def insert
        render json: {}
        return
    end

    def update
        # params[:booking_id]
        # params[:hospital_id]
        # params[:subject_id]
        # params[:booking_time_id]
        # params[:doctor_id]
        # params[:room_id]
        # params[:date]
        tmp_date = Time.parse(params[:date])
        tmp_30_day = Time.now + 30.days
        tmp_date_1day = Time.parse(params[:date].to_s + " 16:30:00")
        tmp_date_now_1day = Time.now + 1.days
        if tmp_date >= tmp_30_day || tmp_date_now_1day > tmp_date_1day
            render json: {error_code: t(:out_of_30_days_code), error_message: t(:out_of_30_days)}
            return
        end
        if tmp_date <= Time.now
            render json: {error_code: t(:date_in_past_code), error_message: t(:date_in_past)}
            return
        end
        unless params[:booking_id] && params[:booking_id] != ''
            render json: {error_code: t(:params_missing_booking_id_code), error_message: t(:params_missing_booking_id)}
            return
        end
        unless params[:hospital_id] && params[:hospital_id] != ''
            render json: {error_code: t(:params_missing_hospital_id_code), error_message: t(:params_missing_hospital_id)}
            return
        end
        unless params[:subject_id] && params[:subject_id] != ''
            render json: {error_code: t(:params_missing_subject_id_code), error_message: t(:params_missing_subject_id)}
            return
        end
        unless params[:booking_time_id] && params[:booking_time_id] != ''
            render json: {error_code: t(:params_missing_booking_time_id_code), error_message: t(:params_missing_booking_time_id)}
            return
        end
        unless params[:doctor_id] && params[:doctor_id] != ''
            render json: {error_code: t(:params_missing_doctor_id_code), error_message: t(:params_missing_doctor_id)}
            return
        end
        unless params[:room_id] && params[:room_id] != ''
            render json: {error_code: t(:params_missing_room_id_code), error_message: t(:params_missing_room_id)}
            return
        end
        unless params[:date] && params[:date] != ''
            render json: {error_code: t(:params_missing_date_code), error_message: t(:params_missing_date)}
            return
        end
        schedule = Schedule.with_hospital_subject(
            params[:hospital_id], 
            params[:subject_id]
        ).find_by(
            room_id: params[:room_id], 
            doctor_id: params[:doctor_id]
        )

        booking = Booking.find_by(id: params[:booking_id], user_id: params[:user_id], status: 1)
        if booking
            count = Booking.where(booking_date: params[:date], schedule_id: booking.schedule_id, booking_time_id: params[:booking_time_id]).count
            bk_time = BookingTime.find_by(id: params[:booking_time_id])
            if count.to_i < bk_time.max_slot.to_i
                booking.booking_date = (params[:date] && params[:date] != '') ? params[:date] : booking.booking_date
                booking.booking_time_id = params[:booking_time_id] ? params[:booking_time_id] : booking.booking_time_id 
                booking.schedule_id = schedule.id
                booking.bhyt_accept = params[:bhyt] ? params[:bhyt] : booking.bhyt_accept
                # beware: insert + nha so, not done yet !!!!!!
                booking.booking_number = _give_number(booking)
                #
                booking.is_changed = 1
                booking.is_editable = 0
                booking.save!

                render json: booking
                return
            else
                render json: {error_code: t(:no_slot_left_code),error_message: t(:no_slot_left)}
                return
            end
        else
            render json: {error_code: t(:booking_not_found_code),error_message: t(:booking_not_found)}
            return
        end
    end

    private def _format_id(str = "")
        result = ""
        default_length = 4
        if str.length < default_length
            (default_length - str.length).times do 
                result += "0"
            end
        end
        result += str
        return result
    end

    private def _give_number(bk)
        if bk
            bk_time = BookingTime.find_by(id: bk.booking_time_id)
            if bk_time && bk_time.step.to_i > 0
                arr_used_number = Booking.where(booking_date: bk.booking_date, schedule_id: bk.schedule_id, booking_time_id: bk.booking_time_id).where('`status` >= 0').order(:booking_number).pluck(:booking_number)
                cursor = bk_time.number_from
                number = 0
                loop do
                    if cursor > bk_time.number_to
                        break
                    end
                    unless arr_used_number.include?(cursor)
                        number = cursor
                        break
                    end
                    cursor = cursor + bk_time.step
                end
                return number
            else
                return nil
            end
        else
            return nil
        end
    end

    private def _give_number_bk(booking_time_id, booking_date, room_id)
        number = 0
        bk_time = BookingTime.find_by(id: booking_time_id)
        if bk_time && bk_time.step.to_i > 0
            schedule = Schedule.find_by(room_id: room_id)
            if schedule
                arr_used_number = Booking.where(booking_date: booking_date, schedule_id: schedule.id, booking_time_id: booking_time_id).where('`status` >= 0').order(:booking_number).pluck(:booking_number)
                arr_used_number_bv = []
                result = Bvdhyd::Lichkham.getbookedDSK({
                    khoa_code: schedule.subject.code, 
                    doctor_id: schedule.doctor_id, 
                    buoi: schedule.ptime.buoi, 
                    booking_date: booking_date
                })
                result.each do |e|
                    arr_used_number_bv << e["SoTT"]
                end
                cursor = bk_time.number_from
                loop do
                    if cursor > bk_time.number_to
                        break
                    end
                    unless arr_used_number.include?(cursor) 
                        unless arr_used_number_bv.include?(cursor)
                            number = cursor
                            break
                        end
                    end
                    cursor = cursor + bk_time.step
                end
            end
        end
        return number
    end

    def make_transaction_code_gd(length = 6)
        str = "M-" + Booking.maximum(:id).to_i.next.to_s
        booking = Booking.find_by(transaction_code_gd: str)
        if booking 
            self.make_transaction_code_gd
        else
            return str
        end
    end

    include Swagger::Blocks

    swagger_path '/booking/cancel' do
        operation :post do
            key :summary, 'Cancel booking'
            key :description, 'Cancel booking'
            key :operationId, 'cancelbooking'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            parameter name: :booking_id,
                in: :query,
                description: 'User access_token',
                required: true
            response 200 do
                key :description, 'booking cancel response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/back_delete' do
        operation :post do
            key :summary, 'Cancel booking on back'
            key :description, 'Cancel booking on back'
            key :operationId, 'cancelbookingonback'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            parameter name: :transaction_code_tt,
                in: :query,
                description: 'transaction_code_tt',
                required: true

            response 200 do
                key :description, 'booking cancel response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/resend_code' do
        operation :post do
            key :summary, 'Resend booking code'
            key :description, 'Resend booking code'
            key :operationId, 'resend_code'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            parameter name: :booking_id,
                in: :query,
                description: 'User access_token',
                required: true
            response 200 do
                key :description, 'booking cancel response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/getbyuserid' do
        operation :get do
            key :summary, 'Get history by user_id'
            key :description, 'Get history by user_id'
            key :operationId, 'getbyuserid'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            response 200 do
                key :description, 'booking response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/detail' do
        operation :get do
            key :summary, 'Get detail booking'
            key :description, 'Get detail booking'
            key :operationId, 'bookingdetail'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            parameter name: :booking_id,
                in: :query,
                description: 'Booking ID',
                required: true
            response 200 do
                key :description, 'booking response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/insert' do
        operation :post do
            key :summary, 'Insert booking'
            key :description, 'Insert booking'
            key :operationId, 'insert'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            parameter name: :date,
                in: :query,
                description: 'Ngày khám Y-m-d',
                required: true
            parameter name: :patient_id,
                in: :query,
                description: 'ID Bệnh nhân',
                required: true
            parameter name: :booking_time_id,
                in: :query,
                description: 'ID Slot giờ',
                required: true
            parameter name: :booking_phone,
                in: :query,
                description: 'Số Điện thoại người đặt',
                required: true
            parameter name: :bhyt,
                in: :query,
                description: 'BHYT Có/Không: 1/0',
                required: true
            parameter name: :hospital_id,
                in: :query,
                description: 'Hospital ID',
                required: true
            parameter name: :subject_id,
                in: :query,
                description: 'ID Khoa',
                required: true
            parameter name: :email,
                in: :query,
                description: 'Email người đặt',
                required: true
            parameter name: :doctor_id,
                in: :query,
                description: 'Doctor ID',
                required: true
            parameter name: :room_id,
                in: :query,
                description: 'Room ID',
                required: true
            response 200 do
                key :description, 'patient response'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Booking
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/booking/update' do
        operation :post do
            key :summary, 'Update booking'
            key :description, 'Update booking'
            key :operationId, 'updatebooking'
            key :tags, [
                'booking'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true
            parameter name: :access_token,
                in: :query,
                description: 'User access_token',
                required: true
            parameter name: :booking_id,
                in: :query,
                description: 'ID Booking',
                required: true
            parameter name: :date,
                in: :query,
                description: 'Date Booking',
                required: true
            parameter name: :hospital_id,
                in: :query,
                description: 'ID hospital',
                required: true
            parameter name: :subject_id,
                in: :query,
                description: 'ID Khoa',
                required: true
            parameter name: :doctor_id,
                in: :query,
                description: 'ID Doctor',
                required: true
            parameter name: :room_id,
                in: :query,
                description: 'ID Room',
                required: true
            parameter name: :booking_time_id,
                in: :query,
                description: 'ID Booking Time',
                required: true
            parameter name: :bhyt,
                in: :query,
                description: 'Accept BHYT',
                required: false
            response 200 do
                key :description, 'booking response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
