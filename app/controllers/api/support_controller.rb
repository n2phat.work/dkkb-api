class Api::SupportController < ApplicationController

    def insert
        unless params[:email]
            render json: {error_code: t(:params_missing_email_code), error_message: t(:params_missing_email)}
            return
        end

        unless params[:question]
            render json: {error_code: t(:params_missing_question_code), error_message: t(:params_missing_question)}
            return
        end

        support = Support.new do |e|
            e.email = params[:email]
            e.question = params[:question]
            e.save!
        end

        render json: support
        return
    end

    include Swagger::Blocks
    swagger_path '/support/insert' do
        operation :post do
            key :summary, 'Insert new support'
            key :description, 'Insert new support'
            key :operationId, 'supportinsert'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'support'
            ]
            parameter  name: :email, in: :query, description: 'Email', required: true
            parameter  name: :question, in: :query, description: 'Câu hỏi', required: true
            response 200 do
                key :description, 'country response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
