class Api::ResourceController < ApplicationController
    before_action :check_sign_in, only: []
    layout nil

    def testhoadon
        # params[:e_id]
        str_base64 = Bvdhyd::Hoadon.gethd(params[:e_id])
        if str_base64 && str_base64.length > 0
            raw_pdf_str = Base64.decode64 str_base64
            send_data( raw_pdf_str, :filename => "#{params[:e_id]}.pdf", :type => "application/pdf", :disposition => "inline")
        else
            render json: {status: false}
            return
        end
    end

    def testrabbit
        client = RabbitMain.new("192.168.0.88", 'rpc_queue')
        puts ' [x] Requesting fib(30)'
        response = client.call('Hello how are you?')
        puts " [.] Got #{response}"
        client.stop

        render json: {data: response}
        return
    end

    def daily_report
        render json: Report.count_daily(params[:date])
        return
    end

    def faq
        @faq = Faq.all.order('category_id ASC')
    end

    def ck_payment
        render json: nil
        return
    end

    def cs
        cookies[:p_redirect] = params[:p_redirect]
        @data = ::Cybersource::Main.pay({amount: params[:amount], transaction_code_tt: params[:transaction_code_tt]})
        if !@data
            render plain: "Errors"
            return
        end
    end

    def test
        render json: {data: request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip}
        return
    end

    def testfirebase
        render json: {phone: Firebase::Main.get_phone(params[:firebase_token]).gsub(/\+84/,"0")}
        return
    end

    def testclass
        result = classname.constantize.new.methods.sort if params[:class] && params[:class] != ""
        render json: result
        return
    end

    def testsms
        client = Savon.client(wsdl: 'http://brand.aztech.com.vn/ws/agentSmsApiSoap?wsdl')
        ap client.operations
        # result = client.call(:get_credit, message: {authenticate_user: 'pkh', authenticate_pass: 'pkh@2018', message: 'Hello a Khiet',receiver: '0916883165',type: 1})
        result = client.call(:send_sms, message: {
            authenticate_user: SMS_USER, 
            authenticate_pass: SMS_AUTHEN, 
            message: 'Benh nhan ABC da thanh toan thanh cong so tien 100000. Ma thanh toan: 181018TA-0003, 181018TA-0004',
            brand_name: 'BvDHYD',
            receiver: '0935244240',
            type: 1
        })
        ap result
        render json: result
        return        
    end

    def testtime
        logger.debug(Time.now.to_s)
        render json: Time.now
        return
    end

    def testheader
        # ghi log IPN Cybersource
        render json: request.headers["HTTP_USER_AGENT"]
        return
    end

    def getholiday
        render json: Holiday.all.pluck(:date)
        return
    end

    def testsocket
        schedule = Schedule.with_hospital_subject(
            2, 
            2
        ).find_by(room_id: 1)
    
        if schedule
            slot_time = BookingTime.where(
                "(`from` <= :hour_from AND `to` > :hour_from) 
                OR (`from` >= :hour_from AND `to` <= :hour_to) 
                OR (`from` < :hour_to AND `to` >= :hour_to)", 
                {
                    :hour_from => schedule.ptime.hour_from,
                    :hour_to => schedule.ptime.hour_to
                }
            ).where("room_id = ?", 1)
            slot_time.each do |e|
                e.used_slot = e.booking.where('booking_date IS NOT NULL AND booking_date = ?', "2018-05-14").size
            end
            ActionCable.server.broadcast "slottime_1_2018-05-14", slot_time.as_json
            render json: {status:true}
            return
        else
            render json: {error_code: 1,error_message: "schedule not found"}
            return
        end
    end

    def getconfig
        # params[:platform]
        app_config = AppConfig.where(platform: params[:platform]).where(status: 1)
        render json: app_config
        return
    end

    def checkversion
        # params[:version_number]
        # params[:version_code]
        # params[:platform]
        case params[:platform]
        when 'android'
            app_version = AppVersion.where(platform: params[:platform]).where(status: 1)
            app_version = app_version.order('version_code DESC').first
        when 'ios'
            app_version = AppVersion.where(platform: params[:platform]).where(status: 1)
            app_version = app_version.order('version_number DESC').first
        end
        render json: app_version
        return
    end

    def getallbanner
        banners = Banner.all.pluck(:image)
        banners.each_with_index do |e, k|
            banners[k] = BASE_URL + '/st/banner/' + e.to_s
        end
        render json: banners
        return
    end

    def allbanner
        # params[:type]
        banners = BannerWeb.where.not(status: 0).where(type: params[:type])
        banners.each do |e|
            e.image_url = !e.image_url.nil? ? (WEB_STATIC_URL + e.image_url) : nil
        end
        render json: banners
        return
    end

    def getallcountry
        render json: Country.select(:id,:code,:name).all
        return
    end

    def getcity
        render json: City.select(:id,:name).where(country_code: params[:country_code])
        return
    end

    def getdistrict
        render json: District.on.select(:id,:name).where(city_id: params[:city_id])
        return
    end

    def getward
        render json: Ward.on.select(:id,:name).where(district_id: params[:district_id])
        return
    end

    def getdantoc
        render json: Dantoc.select(:id,:name).all
        return
    end

    def getallprofession
        render json: Profession.select(:id,:name).all.order(:name)
        return
    end

    def getarticle
        # params[:type]
        article = Article.find_by(sub_id: params[:sub_id])
        render json: article
        return
    end

    def barcode
        # params[:bvdhyd_msbn]
        if params[:bvdhyd_msbn]
            width = params[:width].to_i > 0 ? params[:width].to_i: 240
            height = params[:height].to_i > 0 ? params[:height].to_i: 80
            barcode = Barby::Code128.new(params[:bvdhyd_msbn].to_s).to_image(margin:0).resize(width,height)
            @barcode = barcode.to_blob
            send_data @barcode, type: 'image/png', disposition: 'inline'
            return
        else
            render json: nil
            return
        end
    end

    private def _format_name(surname = '', name = '')
        return surname.to_s + ' ' + name.to_s
    end

    private def _give_number(bk)
        if bk
            bk_time = BookingTime.find_by(id: bk.booking_time_id)
            if bk_time && bk_time.step.to_i > 0
                arr_used_number = Booking.where(booking_date: bk.booking_date, schedule_id: bk.schedule_id, booking_time_id: bk.booking_time_id).where('`status` >= 0').order(:booking_number).pluck(:booking_number)
                cursor = bk_time.number_from
                number = 0
                loop do
                    if cursor > bk_time.number_to
                        break
                    end
                    unless arr_used_number.include?(cursor)
                        number = cursor
                        break
                    end
                    cursor = cursor + bk_time.step
                end
                return number
            else
                return nil
            end
        else
            return nil
        end
    end

    private def _send_tracking(bk)
        Dhyd::TrackJob.perform_later(booking_id: bk.id)
    end

    private def _send_socket(bk)
        slot_time = BookingTime.where(
            "(`from` <= :hour_from AND `to` > :hour_from) 
            OR (`from` >= :hour_from AND `to` <= :hour_to) 
            OR (`from` < :hour_to AND `to` >= :hour_to)", 
            {
                hour_from: bk.schedule.ptime.hour_from, 
                hour_to: bk.schedule.ptime.hour_to
            }
        ).where("room_id = ?", bk.schedule.room_id)
        slot_time.each do |e|
            e.used_slot = e.booking.where('booking_date IS NOT NULL AND status >= 0 AND is_deleted != 1 AND booking_date = ?', bk.booking_date).size
        end

        begin
            ActionCable.server.broadcast "slottime_" + bk.schedule.room_id.to_s + "_" + bk.booking_date.to_s, slot_time.as_json
        rescue => e
            ap e
        end
    end

    private def _send_mail(bk)
        patient = Patient.find_by(id: bk.patient_id)
        if patient
            if patient.email && patient.email != ""
                begin
                    Api::BookingMailJob.perform_later(bk.patient.email, "BVDHYD - Xác nhận Phiếu Khám Bệnh", bk.id)
                rescue  => e
                    ap e
                end
            end
        end
    end

    include Swagger::Blocks

    swagger_path '/resource/checkversion' do
        operation :get do
            key :summary, 'Check New Version'
            key :description, 'Check New Version'
            key :operationId, 'checkversion'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :version_number, in: :query, description: 'version_number'
            parameter name: :version_code, in: :query, description: 'version_code'
            parameter name: :platform, in: :query, description: 'platform'

            response 200 do
                key :description, 'app_version response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getconfig' do
        operation :get do
            key :summary, 'Get Config'
            key :description, 'Get Config'
            key :operationId, 'getconfig'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :platform, in: :query, description: 'platform'

            response 200 do
                key :description, 'app_config response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getarticle' do
        operation :get do
            key :summary, 'Get Article'
            key :description, 'Get Article'
            key :operationId, 'getarticle'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :sub_id, in: :query, description: 'Loai ID Bai Viet', required: true
            response 200 do
                key :description, 'article response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getallcountry' do
        operation :get do
            key :summary, 'All Country'
            key :description, 'Returns all country'
            key :operationId, 'allcountry'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            
            response 200 do
                key :description, 'country response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getcity' do
        operation :get do
            key :summary, 'All City in Country'
            key :description, 'Returns all city in country'
            key :operationId, 'allcity'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :country_code, in: :query, description: 'Country Code', required: true
            response 200 do
                key :description, 'city response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :City
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/resource/getdistrict' do
        operation :get do
            key :summary, 'All District in City'
            key :description, 'Returns all district in city'
            key :operationId, 'alldistrict'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :city_id, in: :query, description: 'City ID', required: true
            response 200 do
                key :description, 'district response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :District
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/resource/getward' do
        operation :get do
            key :summary, 'All Ward in District'
            key :description, 'Returns all ward in district'
            key :operationId, 'allward'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            parameter name: :district_id, in: :query, description: 'District ID', required: true
            response 200 do
                key :description, 'ward response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Ward
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getdantoc' do
        operation :get do
            key :summary, 'All Dantoc'
            key :description, 'Returns all dantoc'
            key :operationId, 'alldantoc'
            key :tags, [
                'resource'
            ]
            response 200 do
                key :description, 'dantoc response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Dantoc
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getallbanner' do
        operation :get do
            key :summary, 'All Banners'
            key :description, 'Returns all banners'
            key :operationId, 'getallbanner'
            key :tags, [
                'resource'
            ]
            response 200 do
                key :description, 'banner response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/allbanner' do
        operation :get do
            key :summary, 'All Banners'
            key :description, 'Returns all banners'
            key :operationId, 'allbanner'
            key :tags, [
                'resource'
            ]
            parameter name: :type, in: :query, description: 'Type: 1 - web, 2 - mobile', required: true
            response 200 do
                key :description, 'banner response'
                schema do
                    key :type, :array
                    items do
                    
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getallprofession' do
        operation :get do
            key :summary, 'All Profession'
            key :description, 'Returns all Profession'
            key :operationId, 'allprofession'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            
            response 200 do
                key :description, 'profession response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Profession
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/resource/getholiday' do
        operation :get do
            key :summary, 'All Holidays'
            key :description, 'Returns all Holidays'
            key :operationId, 'allholiday'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'resource'
            ]
            
            response 200 do
                key :description, 'profession response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
