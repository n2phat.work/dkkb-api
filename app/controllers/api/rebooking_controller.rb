class Api::RebookingController < ApplicationController
    before_action :check_sign_in

    def getallpatient
        patients = Patient.joins(:user_patient).where(user_patient: {user_id: params[:user_id], is_for_rebooking: 1})
        patients.each do |e|
            up = UserPatient.find_by(user_id: params[:user_id], patient_id: e.id)
            e.is_default = up.is_default ? up.is_default : 0
        end
        render json: patients
        return
    end

    def getallfromhistory
        # params[:user_id]
        # params[:access_token]
        # params[:patient_id]
        # params[:hospital_id]
        # params[:leminh]
        patient = Patient.joins(:user_patient).find_by(user_patient: {user_id: params[:user_id], patient_id: params[:patient_id], is_for_rebooking: 1})
        if patient
            bvdhyd_rebookings = BvdhydTaikham.where(SoHoSo: patient.bvdhyd_msbn, is_new: 1).where('NgayKham > ?',Time.now.strftime("%Y-%m-%d 00:00:00"))
            bvdhyd_rebookings.each do |e|
                if e.doctor && e.subject
                    Rebooking.find_or_create_by({
                        bvdhyd_msbn: e.SoHoSo,
                        doctor_id: e.doctor.id,
                        booking_date: e.NgayKham,
                        subject_id: e.subject.id,
                        hospital_id: params[:hospital_id] ? params[:hospital_id] : 2
                    }) do |rbk|
                        rbk.is_noticeable = 1
                        rbk.save!
                    end
                end
                e.is_new = 0
                e.save!
            end
            data = []
            rebookings = Rebooking.where(bvdhyd_msbn: patient.bvdhyd_msbn).where('booking_date > ?',Time.now.strftime("%Y-%m-%d"))
            rebookings.each do |e|
                schedule = Schedule.joins(:room).joins(:subject).joins(:ptime).joins(:doctor).joins(:hospital)
                schedule = schedule.where(status: 1) unless params[:leminh]
                schedule = schedule.where({
                    doctor_id: e.doctor_id, 
                    subject: {id: e.subject_id}, 
                    hospital:{id: e.hospital_id},
                    # ptime: {weekday: e.booking_date.to_date.wday},
                    is_old: 0
                })
                rooms = []
                schedule.each do |sch|
                    room = Room.find_by(id: sch.room_id)
                    if room
                        room.ptime = Ptime.find_by(id: sch.ptime_id)
                        room.not_booking = (sch.status - 1).abs
                        rooms << room.as_json(methods: [:ptime,:not_booking], include: [:doctor,:hospital,:subject])
                    end
                end
                rooms.each do |v|
                    pattern = {
                        doctor: {},
                        hospital: {},
                        subject: {},
                        detail: [],
                        rebooking_date: e.booking_date
                    }
                    if data == []
                        temp = nil
                    else
                        temp = data.detect {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                        index = data.index {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                    end
                    if temp
                        data[index][:detail] << v.without("doctor", "hospital", "subject")
                    else
                        pattern[:detail] = [v.without("doctor", "hospital", "subject")]
                        pattern[:doctor] = v["doctor"].without("date_create","date_update")
                        pattern[:hospital] = v["hospital"].without("date_create","date_update")
                        pattern[:subject] = v["subject"].without("date_create","date_update")
                        data << pattern
                    end
                end
            end
            render json: {
                rebooking: data,
                patient: patient
            }
            return
        else
            render json: {error_code: t(:rebooking_patient_cant_be_verified_code),error_message: t(:rebooking_patient_cant_be_verified)}
            return
        end
    end

    def getall
        # include verify, ko can verify neu so dt BN va so dt user nhu nhau
        # check co BN chua roi insert
        # params[:user_id]
        # params[:access_token]
        # params[:msbn]
        # params[:firebase_token]
        # params[:mobile]
        # params[:hospital_id]
        # params[:leminh]
        
        if params[:mobile].nil? || params[:mobile].empty?
            render json: {error_code: 1,error_message: "Invalid params: Mobile!"}
            return
        end
        if params[:firebase_token]
            mobile = Firebase::Main.get_phone(params[:firebase_token])
            if mobile
                mobile = mobile.gsub(/\+84/,"0")
                patient = ::Bvdhyd::Patient.getbyhsbn(params[:msbn])
                if patient
                    if true || params[:test] == "pkh" || patient["DiDong"] == mobile || patient["DienThoai"] == mobile
                        medpro_id = self.make_msbn
                        med_patient = Patient.find_or_create_by(bvdhyd_msbn: patient["SoHS"]) do |p|
                            p.name = patient["Ten"]
                            p.surname = patient["Ho"]
                            p.cmnd = patient["SoCMND"]
                            p.sex = patient["GioiTinh"]
                            p.mobile = patient["DiDong"] ? patient["DiDong"] : patient["DienThoai"]
                            p.email = patient["Email"]
                            p.dantoc_id = patient["IDDanToc"]
                            p.profession_id = patient["IDNgheNghiep"]
                            p.bhyt = nil
                            p.country_code = patient["MaQuocGia"]
                            p.city_id = patient["IDTinh"]
                            p.district_id = patient["IDQuanHuyen"]
                            p.ward_id = patient["IDPhuongXa"]
                            p.address = patient["DiaChi"]
                            p.note = nil
                            p.birthdate = patient["NgaySinh"]
                            p.birthyear = patient["NamSinh"]
                            p.bvdhyd_msbn = patient["SoHS"]
                            p.medpro_id = medpro_id
                            # p.save!
                        end
                        UserPatient.find_or_create_by({
                            user_id: params[:user_id],
                            patient_id: med_patient.id
                        }).update(is_for_rebooking: 1)
                        
                        bvdhyd_rebookings = BvdhydTaikham.where(SoHoSo: params[:msbn], is_new: 1).where('NgayKham > ?',Time.now.strftime("%Y-%m-%d 00:00:00"))
                        bvdhyd_rebookings.each do |e|
                            if e.doctor && e.subject
                                Rebooking.find_or_create_by({
                                    bvdhyd_msbn: e.SoHoSo,
                                    doctor_id: e.doctor.id,
                                    booking_date: e.NgayKham,
                                    subject_id: e.subject.id,
                                    hospital_id: params[:hospital_id] ? params[:hospital_id] : 2
                                }) do |rbk|
                                    rbk.is_noticeable = 1
                                    rbk.save!
                                end
                            end
                            e.is_new = 0
                            e.save!
                        end
                        data = []
                        rebookings = Rebooking.where(bvdhyd_msbn: params[:msbn]).where('booking_date > ?',Time.now.strftime("%Y-%m-%d"))
                        rebookings.each do |e|
                            schedule = Schedule.joins(:room).joins(:subject).joins(:ptime).joins(:doctor).joins(:hospital)
                            schedule = schedule.where(status: 1) unless params[:leminh]
                            schedule = schedule.where({
                                doctor_id: e.doctor_id, 
                                subject: {id: e.subject_id}, 
                                hospital:{id: e.hospital_id},
                                # ptime: {weekday: e.booking_date.to_date.wday},
                                is_old: 0
                            })
                            rooms = []
                            schedule.each do |sch|
                                room = Room.find_by(id: sch.room_id)
                                if room
                                    room.ptime = Ptime.find_by(id: sch.ptime_id)
                                    room.not_booking = (sch.status - 1).abs
                                    rooms << room.as_json(methods: [:ptime, :not_booking], include: [:doctor,:hospital,:subject])
                                end
                            end
                            rooms.each do |v|
                                pattern = {
                                    doctor: {},
                                    hospital: {},
                                    subject: {},
                                    detail: [],
                                    rebooking_date: e.booking_date
                                }
                                if data == []
                                    temp = nil
                                else
                                    temp = data.detect {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                                    index = data.index {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                                end
                                if temp
                                    data[index][:detail] << v.without("doctor", "hospital", "subject")
                                else
                                    pattern[:detail] = [v.without("doctor", "hospital", "subject")]
                                    pattern[:doctor] = v["doctor"].without("date_create","date_update")
                                    pattern[:hospital] = v["hospital"].without("date_create","date_update")
                                    pattern[:subject] = v["subject"].without("date_create","date_update")
                                    data << pattern
                                end
                            end
                        end
                        render json: {
                            rebooking: data,
                            patient: med_patient
                        }
                        return
                    else
                        render json: {error_code: t(:mobile_not_matched_code), error_message: t(:mobile_not_matched)}
                        return
                    end
                else
                    render json: {error_code: t(:patient_not_found_code), error_message: t(:patient_not_found)}
                    return
                end
            else
                render json: {error_code: t(:verified_failed_code),error_message: t(:verified_failed)}
                return
            end
        else
            patient = ::Bvdhyd::Patient.getbyhsbn(params[:msbn])
            user = User.find_by(id: params[:user_id])
            mobile = user ? user.phone.gsub(/\+84/,"0") : -1
            if patient
                if true || params[:test] == "pkh" || patient["DiDong"] == mobile || patient["DienThoai"] == mobile
                    medpro_id = self.make_msbn
                    med_patient = Patient.find_or_create_by(bvdhyd_msbn: patient["SoHS"]) do |p|
                        p.name = patient["Ten"]
                        p.surname = patient["Ho"]
                        p.cmnd = patient["SoCMND"]
                        p.sex = patient["GioiTinh"]
                        p.mobile = patient["DiDong"] ? patient["DiDong"] : patient["DienThoai"]
                        p.email = patient["Email"]
                        p.dantoc_id = patient["IDDanToc"]
                        p.profession_id = patient["IDNgheNghiep"]
                        p.bhyt = nil
                        p.country_code = patient["MaQuocGia"]
                        p.city_id = patient["IDTinh"]
                        p.district_id = patient["IDQuanHuyen"]
                        p.ward_id = patient["IDPhuongXa"]
                        p.address = patient["DiaChi"]
                        p.note = nil
                        p.birthdate = patient["NgaySinh"]
                        p.birthyear = patient["NamSinh"]
                        p.bvdhyd_msbn = patient["SoHS"]
                        p.medpro_id = medpro_id
                        # p.save!
                    end
                    up = UserPatient.find_or_create_by({
                        user_id: params[:user_id],
                        patient_id: med_patient.id
                    }) do |up|
                        up.is_for_rebooking = 1
                        up.save!
                    end
                    
                    bvdhyd_rebookings = BvdhydTaikham.where(SoHoSo: params[:msbn], is_new: 1).where('NgayKham > ?',Time.now.strftime("%Y-%m-%d 00:00:00"))
                    bvdhyd_rebookings.each do |e|
                        if e.doctor && e.subject
                            Rebooking.find_or_create_by({
                                bvdhyd_msbn: e.SoHoSo,
                                doctor_id: e.doctor.id,
                                booking_date: e.NgayKham,
                                subject_id: e.subject.id,
                                hospital_id: params[:hospital_id] ? params[:hospital_id] : 2
                            }) do |rbk|
                                rbk.is_noticeable = 1
                                rbk.save!
                            end
                        end
                        e.is_new = 0
                        e.save!
                    end
                    data = []
                    rebookings = Rebooking.where(bvdhyd_msbn: params[:msbn]).where('booking_date > ?',Time.now.strftime("%Y-%m-%d"))
                    rebookings.each do |e|
                        schedule = Schedule.joins(:room).joins(:subject).joins(:ptime).joins(:doctor).joins(:hospital)
                        schedule = schedule.where(status: 1) unless params[:leminh]
                        schedule = schedule.where({
                            doctor_id: e.doctor_id, 
                            subject: {id: e.subject_id}, 
                            hospital:{id: e.hospital_id},
                            # ptime: {weekday: e.booking_date.to_date.wday},
                            is_old: 0
                        })
                        rooms = []
                        schedule.each do |sch|
                            room = Room.find_by(id: sch.room_id)
                            if room
                                room.ptime = Ptime.find_by(id: sch.ptime_id)
                                room.not_booking = (sch.status - 1).abs
                                rooms << room.as_json(methods: [:ptime, :not_booking], include: [:doctor,:hospital,:subject])
                            end
                        end
                        rooms.each do |v|
                            pattern = {
                                doctor: {},
                                hospital: {},
                                subject: {},
                                detail: [],
                                rebooking_date: e.booking_date
                            }
                            if data == []
                                temp = nil
                            else
                                temp = data.detect {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                                index = data.index {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                            end
                            if temp
                                data[index][:detail] << v.without("doctor", "hospital", "subject")
                            else
                                pattern[:detail] = [v.without("doctor", "hospital", "subject")]
                                pattern[:doctor] = v["doctor"].without("date_create","date_update")
                                pattern[:hospital] = v["hospital"].without("date_create","date_update")
                                pattern[:subject] = v["subject"].without("date_create","date_update")
                                data << pattern
                            end
                        end
                    end
                    render json: {
                        rebooking: data,
                        patient: med_patient
                    }
                    return
                else
                    render json: {error_code: t(:mobile_not_matched_code), error_message: t(:mobile_not_matched)}
                    return
                end
            else
                render json: {error_code: t(:patient_not_found_code), error_message: t(:patient_not_found)}
                return
            end
        end
    end

    def make_msbn(length = 6)
        pattern = '1234567890'
		i = 0
		str = ''
		while i < length
			str = str + pattern[Random.rand(pattern.length - 1)]
			i = i + 1
        end
        str = "MP-" + Time.now.strftime("%y%m%d").to_s + str
        patient = Patient.find_by(medpro_id: str)
        if patient
            self.make_msbn
        else
            return str
        end
    end

    include Swagger::Blocks
    swagger_path '/rebooking/getall' do
        operation :post do
            key :summary, 'All Rebooking'
            key :description, 'Returns all Rebooking'
            key :operationId, 'allrebooking'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'rebooking'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :msbn, in: :query, description: 'MSBN của Bệnh viện: N18-0051453...'
            parameter name: :firebase_token, in: :query, description: 'Firebase Token: ey.....'
            parameter name: :mobile, in: :query, description: 'Số điện thoại'
            parameter name: :hospital_id, in: :query, description: 'HospitalID'

            response 200 do
                key :description, 'Reboking Réponse'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/rebooking/getallfromhistory' do
        operation :post do
            key :summary, 'All Rebooking'
            key :description, 'Returns all Rebooking'
            key :operationId, 'getallfromhistory'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'rebooking'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :patient_id, in: :query, description: 'Patient ID'
            parameter name: :hospital_id, in: :query, description: 'Hospital ID'

            response 200 do
                key :description, 'Reboking Response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/rebooking/getallpatient' do
        operation :get do
            key :summary, 'All Patient Rebooking'
            key :description, 'Returns all Patient Rebooking'
            key :operationId, 'allpatientrebooking'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'rebooking'
            ]
            parameter name: :user_id, in: :query, description: 'UserID'
            parameter name: :access_token, in: :query, description: 'access_token'

            response 200 do
                key :description, 'Reboking Patient Response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
