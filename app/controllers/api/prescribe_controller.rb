class Api::PrescribeController < ApplicationController
    before_action :check_sign_in

    def getall
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            msbn = params[:msbn]
            BvdhydToathuoc.crawl({msbn: msbn})
            data = BvdhydToathuoc.where(SoHS: msbn).group(:NgayThucHien).order('NgayThucHien DESC').pluck(:NgayThucHien)
            render json: {data: data}
            return
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def detail_by_date
        # params["date"]
        # params["msbn"]
        # params["user_id"]
        # params["access_token"]
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            msbn = params[:msbn]
            date = params[:date]
            data = BvdhydToathuoc.where(SoHS: msbn, NgayThucHien: date)
            render json: {data: data}
            return
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def pdf
        # params[:cls_id]
        # params["user_id"]
        # params["access_token"]
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            toathuoc_id = params[:toathuoc_id]
            toathuoc = BvdhydToathuoc.find(toathuoc_id) rescue nil
            if toathuoc
                render layout: 'test', template: 'adm/test/toathuoc_pdf', encoding: "utf-8", locals: {toathuoc: toathuoc} 
            else
                render json: "Không tìm thấy kết quả"
                return
            end
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    include Swagger::Blocks
    swagger_path '/prescribe/getall' do
        operation :get do
            key :summary, 'Get all prescribes'
            key :description, 'Get all prescribes, có dùng jwt để mã hóa'
            key :operationId, 'prescribegetall'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'prescribe - Toa thuốc'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :msbn, in: :query, description: 'msbn', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'prescribes response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/prescribe/detail_by_date' do
        operation :get do
            key :summary, 'Detail prescribes'
            key :description, 'Detail prescribes, có dùng jwt để mã hóa'
            key :operationId, 'detail_by_date'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'prescribe - Toa thuốc'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :date, in: :query, description: 'date', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'prescribes response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/prescribe/pdf' do
        operation :get do
            key :summary, 'Detail prescribes pdf'
            key :description, 'Detail prescribes pdf, có dùng jwt để mã hóa'
            key :operationId, 'prescribepdf'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'prescribe - Toa thuốc'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :toathuoc_id, in: :query, description: 'toathuoc_id', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'prescribes response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
