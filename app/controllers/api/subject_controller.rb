class Api::SubjectController < ApplicationController
    before_action :check_sign_in, except: [:test]

    def test
        params[:_json].each do |e|
            e = e.permit!
            ap e
        end
        return
    end

    def getall
        # render json: params
        # return
        hospital = Hospital.find_by(id: params[:hospital_id])
        if hospital
            render json: hospital.subject
        else
            render json: {status: nil}
        end
        return
    end

    include Swagger::Blocks
    swagger_path '/subject/getall' do
        operation :get do
            key :summary, 'Get detail service by hospital'
            key :description, 'Returns detail service by hospital'
            key :operationId, 'detailsubjecthospital'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'service'
            ]
            parameter name: :hospital_id, in: :query, description: 'ID hospital'
            parameter name: :subject_id, in: :query, description: 'ID subject'

            response 200 do
                key :description, 'hospital response'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Subject
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
