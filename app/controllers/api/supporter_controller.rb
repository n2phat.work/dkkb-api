class Api::SupporterController < ApplicationController

    def getall
        supporters = Supporter.all
        supporters = supporters.where('name LIKE ?', "%#{params[:doctor_name]}%") if params[:doctor_name]
        render json: supporters
        return
    end

    def follow
        # params[:user_id]
        # params[:access_token]
        # params[:supporter_id]
        # params[:remove]
        # check_sign_in
        begin
            if params[:remove].to_i == 1
                SupporterFollow.where(user_id: params[:user_id], supporter_id: params[:supporter_id]).delete_all
            else
                SupporterFollow.new do |e|
                    e.user_id = params[:user_id]
                    e.supporter_id = params[:supporter_id]
                    e.save!
                end
            end
        rescue => exception
            
        end
        render json: {status: true}
        return
    end

    def getfollow
        # params[:user_id]
        # params[:access_token]
        supporters = Supporter.where(id: SupporterFollow.where(user_id: params[:user_id]).pluck(:supporter_id))
        render json: supporters
        return
    end

    def getchatlist
        # params[:user_id]
        # params[:access_token]
        # params[:supporter_id]
        # params[:limit]
        # params[:offset]
        rooms = []
        return_data = []
        limit = params[:limit] ? params[:limit].to_i : 10
        offset = params[:offset] ? params[:offset].to_i : 0
        if params[:user_id] && params[:access_token]
            check_sign_in
            rooms = SupporterRoom.where(user_id: params[:user_id]).order('supporter_room.date_update DESC')
            total = rooms.size
            rooms = rooms.limit(limit).offset(offset)
        elsif params[:supporter_id]
            rooms = SupporterRoom.where(supporter_id: params[:supporter_id]).order('supporter_room.date_update DESC')
            total = rooms.size
            rooms = rooms.limit(limit).offset(offset)
        else

        end
        rooms.each do |r|
            msg = SupporterChat.where(supporter_room_id: r.id).last
            if msg
                e = {
                    id: msg.id,
                    content: msg.content,
                    time: msg.date_create.strftime("%Y-%m-%d %H:%M:%S"),
                    file_url: msg.file_url,
                    from_user_id: msg.from_user_id,
                    from_supporter_id: msg.from_supporter_id,
                    action: nil,
                    read_supporter_id: msg.read_supporter_id,
                    read_user_id: msg.read_user_id
                }
            else
                e = nil
            end
            response = {
                user_id: r.user_id,
                supporter_id: r.supporter_id,
                patient_id: r.patient_id,
                patient: (Patient.find(r.patient_id) rescue nil),
                data: e #last_message
            }
            return_data << response
        end

        render json: {
            total: total,
            data: return_data
        }
        return
    end

    def gethistory
        # params[:user_id]
        # params[:supporter_id]
        # params[:patient_id]
        # params[:read_user_id]
        # params[:read_supporter_id]
        # params[:limit]
        # params[:offset]
        room = SupporterRoom.find_by(user_id: params[:user_id], supporter_id: params[:supporter_id], patient_id: params[:patient_id])
        data = SupporterChat.where(supporter_room_id: (room.id rescue 0)).order('id DESC')
        total = data.size
        limit = params[:limit] ? params[:limit].to_i : 10
        offset = params[:offset] ? params[:offset].to_i : 0
        data.update_all(read_user_id: params[:read_user_id].to_i) if params[:read_user_id].to_i > 0
        data.update_all(read_supporter_id: params[:read_supporter_id].to_i) if params[:read_supporter_id].to_i > 0
        data = data.limit(limit).offset(offset)
        return_data = []
        data.each do |d|
            e = {
                id: d.id,
                content: d.content,
                time: d.date_create.strftime("%Y-%m-%d %H:%M:%S"),
                file_url: d.file_url,
                from_user_id: d.from_user_id,
                from_supporter_id: d.from_supporter_id,
                user_id: params[:user_id],
                supporter_id: params[:supporter_id],
                patient_id: params[:patient_id],
                action: nil,
                read_supporter_id: d.read_supporter_id,
                read_user_id: d.read_user_id
            }
            return_data << e
        end
        # send qua thang kia
        response = {
            action:  'read',
            patient_id: params[:patient_id],
            supporter_id: params[:supporter_id],
            user_id: params[:user_id],
            read_user_id: params[:read_user_id],
            read_supporter_id: params[:read_supporter_id]
        }
        brd((room.uuid rescue nil), response)
        render json: {
            total: total,
            data: return_data
        }
        return
    end

    private def brd(room_id, data)
        begin
          ActionCable.server.broadcast room_id, data
        rescue => exception
          ap "errr"
        end
    end

    include Swagger::Blocks
    swagger_path '/supporter/follow' do
        operation :get do
            key :summary, 'Follow supporter'
            key :description, 'Follow supporter'
            key :operationId, 'follow'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'supporter'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :supporter_id, in: :query, description: 'supporter_id', required: true
            parameter name: :remove, in: :query, description: 'remove, nếu unfollow thì gửi remove = 1'
            response 200 do
                key :description, 'gethistory response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/supporter/getfollow' do
        operation :get do
            key :summary, 'List follow'
            key :description, 'Returns List follow'
            key :operationId, 'getfollow'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'supporter'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            response 200 do
                key :description, 'gethistory response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/supporter/gethistory' do
        operation :get do
            key :summary, 'History chat'
            key :description, 'Returns History chat'
            key :operationId, 'gethistory'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'supporter'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :supporter_id, in: :query, description: 'supporter_id', required: true
            parameter name: :patient_id, in: :query, description: 'patient_id', required: true
            parameter name: :read_user_id, in: :query, description: 'read_user_id'
            parameter name: :read_supporter_id, in: :query, description: 'read_supporter_id'
            parameter name: :limit, in: :query, description: 'limit'
            parameter name: :offset, in: :query, description: 'offset'
            response 200 do
                key :description, 'gethistory response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/supporter/getchatlist' do
        operation :get do
            key :summary, 'Chat List'
            key :description, 'Returns Chat List'
            key :operationId, 'getchatlist'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'supporter'
            ]
            parameter name: :supporter_id, in: :query, description: 'supporter_id', required: true
            parameter name: :limit, in: :query, description: 'limit'
            parameter name: :offset, in: :query, description: 'offset'
            response 200 do
                key :description, 'getchatlist response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/supporter/getall' do
        operation :get do
            key :summary, 'All Supporter'
            key :description, 'Returns all Supporter'
            key :operationId, 'allsupporter'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'supporter'
            ]
            parameter name: :doctor_name, in: :query, description: 'doctor_name'

            response 200 do
                key :description, 'Supporter response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
