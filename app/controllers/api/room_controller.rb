class Api::RoomController < ApplicationController
    before_action :check_sign_in, except: [:checkslot]
    
    private def parse_float_to_time(float = 0.0)
        minute = ((float * 60) % 60).to_i
        hour = float.to_i
        return (hour < 10 ? "0" + hour.to_s : hour.to_s ).to_s + ":" + (minute < 10 ? "0" + minute.to_s : minute).to_s + ":00"
    end

    def detail
        # params[:hospital_id]
        # params[:dayofweek]
        # params[:date] ##Y-m-d
        # params[:subject_id]
        # params[:room_id]
        schedule = Schedule.with_hospital_subject(
                        params[:hospital_id],
                        params[:subject_id]
                    ).find_by(room_id: params[:room_id], is_old: 0, status: 1)
                
        if schedule
            slot_time = BookingTime.where("(`from` <= :hour_from AND `to` > :hour_from) 
                OR (`from` >= :hour_from AND `to` <= :hour_to) 
                OR (`from` < :hour_to AND `to` >= :hour_to)", {
                    hour_from: schedule.ptime.hour_from, 
                    hour_to: schedule.ptime.hour_to
                }).where("is_old = 0 AND room_id = ?", params[:room_id])
            booked_dsk = ::Bvdhyd::Lichkham.getbookedDSK({
                khoa_code: schedule.subject.code,
                doctor_id: schedule.doctor_id, 
                buoi: schedule.ptime.buoi,
                booking_date: C.parse(params[:date]).strftime("%Y-%m-%d")
            })
            slot_time.each do |e|
                e.used_slot = e.booking.where('booking_date IS NOT NULL AND status >= 0 AND is_deleted != 1 AND booking_date = ?',params[:date]).size
                if !booked_dsk.blank? && e.max_slot > 0
                    from = parse_float_to_time(e.from)
                    to = parse_float_to_time(e.to)
                    booked_dsk.each do |dsk|
                        if dsk["GioKham"] >= from && dsk["GioKham"] <= to
                            e.used_slot += 1
                        end
                    end
                    e.used_slot = e.max_slot if e.used_slot > e.max_slot
                end
            end
            render json: slot_time
            return
        else
            render json: { error_code: t(:schedule_not_found_code), error_message: t(:schedule_not_found) }
            return
        end
    end

    def detail2
        # params[:hospital_id]
        # params[:dayofweek]
        # params[:date] ##Y-m-d
        # params[:subject_id]
        # params[:room_id] = 1,2,3
        arr_room_id = params[:room_id].split(",")
        schedule = Schedule.with_hospital_subject(
                        params[:hospital_id], 
                        params[:subject_id]
                    ).where(room_id: arr_room_id, is_old: 0, status: 1)
        slot_times = []
        if !schedule.blank?
            schedule.each do |sche|
                slot_time = BookingTime.where("(`from` <= :hour_from AND `to` > :hour_from) 
                    OR (`from` >= :hour_from AND `to` <= :hour_to) 
                    OR (`from` < :hour_to AND `to` >= :hour_to)", {
                        hour_from: sche.ptime.hour_from, 
                        hour_to: sche.ptime.hour_to
                    }).where("is_old = 0 AND room_id = ?", sche.room_id)
                booked_dsk = ::Bvdhyd::Lichkham.getbookedDSK({
                    khoa_code: sche.subject.code,
                    doctor_id: sche.doctor_id,
                    buoi: sche.ptime.buoi,
                    booking_date: params[:date] ? C.parse(params[:date]).strftime("%Y-%m-%d") : nil
                })
                slot_time.each do |e|
                    e.used_slot = e.booking.where('booking_date IS NOT NULL AND status >= 0 AND is_deleted != 1 AND booking_date = ?',params[:date]).size
                    if !booked_dsk.blank? && e.max_slot > 0
                        from = parse_float_to_time(e.from)
                        to = parse_float_to_time(e.to)
                        booked_dsk.each do |dsk|
                            if dsk["GioKham"] >= from && dsk["GioKham"] <= to
                                e.used_slot += 1
                            end
                        end
                        e.used_slot = e.max_slot if e.used_slot > e.max_slot
                    end
                    slot_times << e
                end
            end
            render json: slot_times
            return
        else
            render json: { error_code: t(:schedule_not_found_code), error_message: t(:schedule_not_found) }
            return
        end
    end

    def detail_from_doctor
        # params[:hospital_id]
        # params[:dayofweek]
        # params[:date] ##Y-m-d
        # params[:subject_id]
        # params[:room_id] = 1,2,3
        arr_room_id = params[:room_id].split(",")
        schedule = Schedule.with_hospital_subject(
                        params[:hospital_id], 
                        params[:subject_id]
                    ).where(room_id: arr_room_id, is_old: 0, status: 1)
        slot_times = []
        if !schedule.blank?
            schedule.each do |sche|
                slot_time = BookingTime.where("(`from` <= :hour_from AND `to` > :hour_from) 
                    OR (`from` >= :hour_from AND `to` <= :hour_to) 
                    OR (`from` < :hour_to AND `to` >= :hour_to)", {
                        hour_from: sche.ptime.hour_from, 
                        hour_to: sche.ptime.hour_to
                    }).where("is_old = 0 AND room_id = ?", sche.room_id)
                booked_dsk = ::Bvdhyd::Lichkham.getbookedDSK({
                    khoa_code: sche.subject.code,
                    doctor_id: sche.doctor_id, 
                    buoi: sche.ptime.buoi,
                    booking_date: C.parse(params[:date]).strftime("%Y-%m-%d")
                })
                slot_time.each do |e|
                    e.used_slot = e.booking.where('booking_date IS NOT NULL AND status >= 0 AND is_deleted != 1 AND booking_date = ?',params[:date]).size
                    if !booked_dsk.blank? && e.max_slot > 0
                        from = parse_float_to_time(e.from)
                        to = parse_float_to_time(e.to)
                        booked_dsk.each do |dsk|
                            if dsk["GioKham"] >= from && dsk["GioKham"] <= to
                                e.used_slot += 1
                            end
                        end
                        e.used_slot = e.max_slot if e.used_slot > e.max_slot
                    end
                    slot_times << e
                end
            end
            tmp_change = BvdhydLichbsthaythe.find_by(Ngay: params[:date], IDBS: schedule.first.doctor_id, MaDonVi: schedule.first.subject.code, RIDThu: schedule.first.ptime.weekday.to_i + 1)
            if tmp_change
                changed_doctor = Doctor.find_by(id: tmp_change.IDBSThayThe)
                return_data = {
                    buoi: tmp_change.RIDBuoiKham,
                    doctor: changed_doctor
                }

            else
                return_data = nil
            end
            render json: {
                changed_doctor: return_data,
                slot_time: slot_times
            }
            return
        else
            render json: { error_code: t(:schedule_not_found_code), error_message: t(:schedule_not_found) }
            return
        end
    end

    def getlist
        # params[:hospital_id]
        # params[:dayofweek]
        # params[:date] ##Y-m-d
        # params[:subject_id]
        if params[:hospital_id] && params[:subject_id]
            schedule = Schedule.with_hospital_subject(params[:hospital_id], params[:subject_id]).joins(:ptime)
            schedule = schedule.where(status: 1)
            schedule = schedule.where(is_old: 0)
            schedule = schedule.where('FIND_IN_SET(?, ptime.`weekday`)',params[:dayofweek]) if params[:dayofweek]
            if params[:date]
                begin
                    month = params[:date].to_date.strftime("%m").to_i
                    day = params[:date].to_date.strftime("%d").to_i
                    schedule = schedule.where("? BETWEEN ptime.day_from AND ptime.day_to", day).where("? BETWEEN ptime.month_from AND ptime.month_to",month)
                rescue ArgumentError
                    render json: {error_code: t(:params_date_invalid), error_message: t(:params_date_invalid)}
                    return
                end
            end
            rooms = []
            schedule.each do |e|
                room = Room.find_by(id: e.room_id)
                if room
                    room.ptime = Ptime.find_by(id: e.ptime_id)
                    tmp_change = BvdhydLichbsthaythe.find_by(Ngay: params[:date], IDBS: e.doctor_id, MaDonVi: e.subject.code, RIDBuoiKham: e.ptime.buoi, RIDThu: e.ptime.weekday.to_i + 1)
                    if tmp_change
                        changed_doctor = Doctor.find_by(id: tmp_change.IDBSThayThe)
                        return_data = {
                            buoi: tmp_change.RIDBuoiKham,
                            doctor: changed_doctor
                        }
                    else
                        return_data = nil
                    end
                    room.changed_doctor = return_data
                    rooms << room.as_json(methods: [:ptime, :changed_doctor], include: [:doctor])
                end
            end
            render json: rooms
            return
        else
            render json: {error_code: t(:params_missing_code), error_message: t(:params_missing)}
            return
        end 
    end

    def checkslot
        number = _give_number_bk(params[:booking_time_id], params[:booking_date],params[:room_id])
        render json: {result: number}
        return
    end

    # function viet rieng cho check so luc booking khi nhan vao slottime
    private def _give_number_bk(booking_time_id, booking_date, room_id)
        number = 0
        bk_time = BookingTime.find_by(id: booking_time_id)
        if bk_time && bk_time.step.to_i > 0
            schedule = Schedule.find_by(room_id: room_id)
            if schedule
                arr_used_number = Booking.where(booking_date: booking_date, schedule_id: schedule.id, booking_time_id: booking_time_id).where('`status` >= 0').order(:booking_number).pluck(:booking_number)
                arr_used_number_bv = []
                result = Bvdhyd::Lichkham.getbookedDSK({
                    khoa_code: schedule.subject.code, 
                    doctor_id: schedule.doctor_id, 
                    buoi: schedule.ptime.buoi, 
                    booking_date: booking_date
                })
                result.each do |e|
                    arr_used_number_bv << e["SoTT"]
                end
                cursor = bk_time.number_from
                loop do
                    if cursor > bk_time.number_to
                        break
                    end
                    unless arr_used_number.include?(cursor) 
                        unless arr_used_number_bv.include?(cursor)
                            number = cursor
                            break
                        end
                    end
                    cursor = cursor + bk_time.step
                end
            end
        end
        return number
    end

    include Swagger::Blocks
    swagger_path '/room/getlist' do
        operation :post do
            key :summary, 'Get room list'
            key :description, 'Returns all rooms'
            key :operationId, 'getlist'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'room'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :hospital_id, in: :query, description: 'ID Hospital', required: true
            parameter name: :dayofweek, in: :query, description: 'Thứ', required: false
            parameter name: :date, in: :query, description: 'date', required: false
            parameter name: :subject_id, in: :query, description: 'ID Khoa', required: true
            
            response 200 do
                key :description, 'Room list'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :Room
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/room/detail' do
        operation :post do
            key :summary, 'All Slot Time'
            key :description, 'Returns all time slots'
            key :operationId, 'alltimeslot'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'room'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :room_id, in: :query, description: 'ID room', required: true
            parameter name: :hospital_id, in: :query, description: 'ID Hospital', required: true
            parameter name: :subject_id, in: :query, description: 'ID Khoa', required: true
            parameter name: :dayofweek, in: :query, description: 'Thứ', required: false
            parameter name: :date, in: :query, description: 'Date', required: false
            response 200 do
                key :description, 'time slot response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :BookingTime
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/room/detail2' do
        operation :post do
            key :summary, 'All Slot Time 2'
            key :description, 'Returns all time slots 2'
            key :operationId, 'alltimeslot2'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'room'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :room_id, in: :query, description: 'List ID room', required: true
            parameter name: :hospital_id, in: :query, description: 'ID Hospital', required: true
            parameter name: :subject_id, in: :query, description: 'ID Khoa', required: true
            parameter name: :dayofweek, in: :query, description: 'Thứ', required: false
            parameter name: :date, in: :query, description: 'Date', required: false
            response 200 do
                key :description, 'time slot response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :BookingTime
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/room/detail_from_doctor' do
        operation :post do
            key :summary, 'All Slot Time 2 with changed doctor'
            key :description, 'All Slot Time 2 with changed doctor'
            key :operationId, 'detail_from_doctor'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'room'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :room_id, in: :query, description: 'List ID room', required: true
            parameter name: :hospital_id, in: :query, description: 'ID Hospital', required: true
            parameter name: :subject_id, in: :query, description: 'ID Khoa', required: true
            parameter name: :dayofweek, in: :query, description: 'Thứ', required: false
            parameter name: :date, in: :query, description: 'Date', required: false
            response 200 do
                key :description, 'time slot response'
                schema do

                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
