class Api::InvoiceController < ApplicationController
    # before_action :check_sign_in

    def getall
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            msbn = data["msbn"]
            BvdhydInvoice.crawl({msbn: msbn})
            data = BvdhydInvoice.where(SoHoSo: msbn).group(:Ngay).order('Ngay DESC').pluck(:Ngay)
            return_data = []
            data.each do |e|
                return_data << Time.strptime(e,'%Y%m%d').localtime.strftime("%Y-%m-%d")
            end
            render json: {data: return_data}
            return
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def detail_by_date
        # params["date"]
        # params["msbn"]
        # params["user_id"]
        # params["access_token"]
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            msbn = data["msbn"]
            date = data["date"].to_date.strftime("%Y%m%d")
            data = BvdhydInvoice.where(SoHoSo: msbn, Ngay: date)
            render json: {data: data.as_json}
            return
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    def pdf
        params[:cls_id]
        params["user_id"]
        params["access_token"]
        begin
            data = JWT.decode params[:sign], 'Medpro.UMC-2018', true, { algorithm: 'HS256' }
        rescue JWT::ExpiredSignature
            data = nil
        rescue
            render json: {error_code: t(:sign_invalid_code), error_message: t(:sign_invalid)}
            return
        end
        if data
            data = data.first
            id = data["invoice_id"]
            e_id = data["e_id"]
            invoice = BvdhydInvoice.find_by(id: id, MaEinvoice: e_id)
            if invoice
                str_base64 = Bvdhyd::Hoadon.gethd(invoice[:MaEinvoice])
                if str_base64 && str_base64.length > 0
                    raw_pdf_str = Base64.decode64 str_base64
                    send_data( raw_pdf_str, :filename => "#{params[:e_id]}.pdf", :type => "application/pdf", :disposition => "inline")
                else
                    render json: {status: false}
                    return
                end
                
            else
                render json: "Không tìm thấy kết quả"
                return
            end
        else
            render json: {error_code: t(:sign_failed_code), error_message: t(:sign_failed)}
            return
        end
    end

    include Swagger::Blocks
    swagger_path '/invoice/getall' do
        operation :get do
            key :summary, 'Get all invoices'
            key :description, 'Get all invoices, có dùng jwt để mã hóa'
            key :operationId, 'invoicegetall'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'Invoice - Hóa đơn'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :msbn, in: :query, description: 'msbn', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'clinics response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/invoice/detail_by_date' do
        operation :get do
            key :summary, 'Get detail invoices by date'
            key :description, 'Get detail invoices by date, có dùng jwt để mã hóa'
            key :operationId, 'detail_by_date'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'Invoice - Hóa đơn'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :msbn, in: :query, description: 'msbn', required: true
            parameter  name: :date, in: :query, description: 'date', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'invoices response'
                schema do
                    key :type, :array
                    items do
                        
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    swagger_path '/invoice/pdf' do
        operation :get do
            key :summary, 'Detail invoice...'
            key :description, 'Detail invoice, có dùng jwt để mã hóa'
            key :operationId, 'invoicepdf'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'invoice - Hóa đơn'
            ]
            parameter  name: :user_id, in: :query, description: 'user_id', required: true
            parameter  name: :access_token, in: :query, description: 'access_token', required: true
            parameter  name: :invoice_id, in: :query, description: 'invoice_id', required: true
            parameter  name: :e_id, in: :query, description: 'e_id lay field MaEinvoice tu api detail_by_date', required: true
            parameter  name: :sign, in: :query, description: 'chuỗi token JWT', required: true
            response 200 do
                key :description, 'clinics response'
                schema do
                    key :type, :array
                    items do
                    key :'$ref', :Country
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
