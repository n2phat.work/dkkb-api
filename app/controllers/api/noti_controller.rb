class Api::NotiController < ApplicationController
    before_action :check_sign_in

    def getall
        # params[:type]
        arr_noti = {
            data: [],
            total_new: 0,
            total: 0
        }
        arr_new = []
        arr_old = []
        arr_new_id = []
        limit = params[:limit] ? params[:limit].to_i : 10       
        offset = params[:offset] ? params[:offset].to_i : 0
        total = 0
        unless params[:user_id].nil? || params[:type].nil?
            type = params[:type].split(",")

            arr_delete = NotiDelete.where(user_id: params[:user_id]).pluck(:notification_id)
            arr_read = NotiRead.where(user_id: params[:user_id]).pluck(:notification_id)

            notis_new = Noti.where(type: type).where('user_id = :user_id OR user_id IS NULL',{user_id: params[:user_id]}).where.not(id: arr_delete).where.not(id: arr_read).order('id DESC')
            arr_new_id = notis_new.pluck(:id)
            notis_new = notis_new.limit(limit).offset(offset)

            notis_read = Noti.where(type: type).where('user_id = :user_id OR user_id IS NULL',{user_id: params[:user_id]}).where.not(id: arr_delete).where.not(id: arr_new_id)
            total = Noti.where(type: type).where('user_id = :user_id OR user_id IS NULL',{user_id: params[:user_id]}).where.not(id: arr_delete).size
            limit_read = limit - notis_new.size
            offset_read = 0

            if notis_new.size == 0
                offset_read = offset - arr_new_id.count
            end
            notis_read = notis_read.limit(limit_read).offset(offset_read).order('id DESC')

            notis_new.each do |e|
                e.is_new = 1
                arr_new << e
            end
            notis_read.each do |e|
                e.is_new = 0
                arr_old << e
            end
        end
        arr_noti[:data] = arr_new + arr_old
        arr_noti[:total_new] = arr_new_id.count
        arr_noti[:total] =  total
        render json: arr_noti
        return
    end

    def getall2
        arr_noti = {
            data: [],
            total_new: 0,
            total: 0
        }
        arr_new = []
        arr_old = []
        arr_new_id = []
        limit = params[:limit] ? params[:limit].to_i : 10       
        offset = params[:offset] ? params[:offset].to_i : 0
        total = 0
        unless params[:user_id].nil? || params[:type].nil?
            type = params[:type].to_s.split(",")

            arr_delete = NotiDelete.where(user_id: params[:user_id]).pluck(:notification_id)
            arr_read = NotiRead.where(user_id: params[:user_id]).pluck(:notification_id)

            notis = Noti.where(type: type).where('user_id = :user_id OR user_id IS NULL',{user_id: params[:user_id]}).where.not(id: arr_delete).order('id DESC')
            total = notis.size
            notis = notis.limit(limit).offset(offset)
            notis.each do |e|
                if arr_read.include?(e.id)
                    e.is_new = 0
                    arr_noti[:total_new] += 1
                else
                    e.is_new = 1
                end
            end
        end
        arr_noti[:data] = notis
        arr_noti[:total] =  total
        render json: arr_noti
        return
    end

    def detail
        noti = Noti.find_by(id: params[:noti_id])
        render json: noti
        return
    end

    def update
        unless params[:user_id].nil? || params[:noti_id].nil?
            arr_noti_id = params[:noti_id].to_s.split(',')
            arr_noti_id.each do |e|
                NotiRead.find_or_create_by(user_id: params[:user_id], notification_id: e) do |e|
                    e.save!
                end
            end
        end
        render json:{status: true}
        return
    end

    def delete
        unless params[:user_id].nil? || params[:noti_id].nil?
            arr_noti_id = params[:noti_id].to_s.split(',')
            arr_noti_id.each do |e|
                NotiDelete.find_or_create_by(user_id: params[:user_id], notification_id: e) do |e|
                    e.save!
                end
            end
        end
        render json:{status: true}
        return
    end

    include Swagger::Blocks
    swagger_path '/noti/getall' do
        operation :get do
            key :summary, 'All Notification'
            key :description, 'Returns all Notification'
            key :operationId, 'allnoti'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'noti'
            ]
            parameter name: :user_id, in: :query, description: 'user_id'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :type, in: :query, description: 'type, được nhận list, tab PKB truyền type = 1,2, type = 3, type = 4'
            parameter name: :limit, in: :query, description: 'limit'
            parameter name: :offset, in: :query, description: 'offset'
            response 200 do
                key :description, 'Noti response'
                schema do
                    key :type, :array
                    items do

                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/noti/getall2' do
        operation :get do
            key :summary, 'All Notification'
            key :description, 'Returns all Notification: giống getall nhưng order theo ngày'
            key :operationId, 'allnoti2'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'noti'
            ]
            parameter name: :user_id, in: :query, description: 'user_id'
            parameter name: :access_token, in: :query, description: 'access_token'
            parameter name: :type, in: :query, description: 'type, được nhận list, tab PKB truyền type = 1,2, type = 3, type = 4'
            parameter name: :limit, in: :query, description: 'limit'
            parameter name: :offset, in: :query, description: 'offset'
            response 200 do
                key :description, 'Noti response'
                schema do
                    key :type, :array
                    items do

                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/noti/detail' do
        operation :get do
            key :summary, 'detail detail'
            key :description, 'Returns detail noti'
            key :operationId, 'detailnoti'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'noti'
            ]
            parameter name: :noti_id, in: :query, description: 'noti_id'
            parameter name: :user_id, in: :query, description: 'user_id'
            parameter name: :access_token, in: :query, description: 'access_token'
            response 200 do
                key :description, 'Noti response'
                schema do
                    key :type, :array
                    items do

                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
    
    swagger_path '/noti/delete' do
        operation :post do
            key :summary, 'delete detail'
            key :description, 'delete noti'
            key :operationId, 'deletenoti'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'noti'
            ]
            parameter name: :noti_id, in: :query, description: 'noti_id, hoặc list noti_id: 1,2,3,4'
            parameter name: :user_id, in: :query, description: 'user_id'
            parameter name: :access_token, in: :query, description: 'access_token'
            response 200 do
                key :description, 'Noti response'
                schema do
                    key :type, :array
                    items do

                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/noti/update' do
        operation :post do
            key :summary, 'update noti read'
            key :description, 'update noti read'
            key :operationId, 'updatenoti'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'noti'
            ]
            parameter name: :noti_id, in: :query, description: 'noti_id, hoặc list noti_id: 1,2,3,4'
            parameter name: :user_id, in: :query, description: 'user_id'
            parameter name: :access_token, in: :query, description: 'access_token'
            response 200 do
                key :description, 'Noti response'
                schema do
                    key :type, :array
                    items do

                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
