class Api::DoctorController < ApplicationController
    before_action :check_sign_in
    
    def detail
        doctor = Doctor.find_by(id: params[:doctor_id])
        render json: doctor
        return
    end

    def getall
        doctors = Doctor.all
        total = doctors.size
        doctors = doctors.limit(params[:limit].to_i) if params[:limit]
        doctors = doctors.offset(params[:offset].to_i) if params[:offset]
        render json: { rows: doctors, total: total }
        return
    end

    def search
        # params[:name]
        # params[:sex]
        # params[:role]
        # params[:subject_id]
        # params[:dayofweek]
        # params[:buoi]
        schedule = Schedule.joins(:room).joins(:subject).joins(:ptime).joins(:doctor)
        schedule = schedule.where(status: 1)
        schedule = schedule.where(is_old: 0)
        schedule = schedule.where('doctor.`name` LIKE ?', "%#{params[:name]}%") if params[:name]
        schedule = schedule.where('doctor.`sex` = ?', params[:sex]) if params[:sex] && params[:sex] != ''
        schedule = schedule.where('doctor.`role` LIKE ?', "%#{params[:role]}%") if params[:role] && params[:role] != ''
        schedule = schedule.where('FIND_IN_SET(?, ptime.`weekday`)',params[:dayofweek]) if params[:dayofweek] && params[:dayofweek] != ''
        schedule = schedule.where('ptime.`buoi` = ?', params[:buoi]) if params[:buoi] && params[:buoi] != ''
        schedule = schedule.where('subject.id = ?', params[:subject_id]) if params[:subject_id] && params[:subject_id] != ''
        schedule = schedule.order('subject.name, doctor.name')
        # schedule = schedule.group('schedule.hospital_subject_id, doctor.id')

        total = schedule.length

        schedule = schedule.limit(params[:limit].to_i) if params[:limit]
        schedule = schedule.offset(params[:offset].to_i) if params[:offset]
        
        rooms = []
        schedule.each do |e|
            room = Room.find_by(id: e.room_id)
            if room
                room.ptime = Ptime.find_by(id: e.ptime_id)
                # room.ptime = Ptime.select("ptime.*").joins(:schedule).where('schedule.doctor_id = ?', e.doctor_id).where('schedule.hospital_subject_id = ?', e.hospital_subject_id)
                rooms << room.as_json(methods: [:ptime], include: [:doctor,:hospital,:subject])
                # rooms << room.as_json(methods: [:ptime])
            end
        end
        
        render json: { rows: rooms, total: total }
        return
    end

    def search2
        # params[:name]
        # params[:sex]
        # params[:role]
        # params[:subject_id]
        # params[:dayofweek]
        # params[:buoi]
        arr_cache_doctor = $redis.get('arr_cache_doctor')
        arr_cache_doctor = arr_cache_doctor ? ::Util.json_decode(arr_cache_doctor, true) : {}
        arr = params.permit!.to_h.without("user_id", "access_token", "controller","action","t","sign","device", "onesignal_id","platform","onesignal_token","doctor")
        arr = arr.sort
        key = arr.join("_").to_sym
        if arr_cache_doctor[key].nil?
            schedule = Schedule.joins(:room).joins(:subject).joins(:ptime).joins(:doctor)
            schedule = schedule.where(status: 1)
            schedule = schedule.where(is_old: 0)
            schedule = schedule.where('doctor.`name` LIKE ?', "%#{params[:name]}%") if params[:name]
            schedule = schedule.where('doctor.`sex` = ?', params[:sex]) if params[:sex] && params[:sex] != ''
            schedule = schedule.where('doctor.`role` LIKE ?', "%#{params[:role]}%") if params[:role] && params[:role] != ''
            schedule = schedule.where('FIND_IN_SET(?, ptime.`weekday`)',params[:dayofweek]) if params[:dayofweek] && params[:dayofweek] != ''
            schedule = schedule.where('ptime.`buoi` = ?', params[:buoi]) if params[:buoi] && params[:buoi] != ''
            schedule = schedule.where('subject.id = ?', params[:subject_id]) if params[:subject_id] && params[:subject_id] != ''
            schedule = schedule.order('doctor.name_only, doctor.name, subject.name')

            rooms = []
            schedule.each do |e|
                room = Room.find_by(id: e.room_id)
                if room
                    room.ptime = Ptime.find_by(id: e.ptime_id)
                    # room.ptime = Ptime.select("ptime.*").joins(:schedule).where('schedule.doctor_id = ?', e.doctor_id).where('schedule.hospital_subject_id = ?', e.hospital_subject_id)
                    rooms << room.as_json(methods: [:ptime], include: [:doctor,:hospital,:subject])
                end
            end
            result = []
            rooms.each do |v|
                pattern = {
                    doctor: {},
                    hospital: {},
                    subject: {},
                    detail: []
                }
                if result == []
                    temp = nil
                else
                    temp = result.detect {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                    index = result.index {|each| each[:doctor]["id"] == v["doctor"]["id"] && each[:subject]["id"] == v["subject"]["id"]}
                end
                if temp
                    result[index][:detail] << v.without("doctor", "hospital", "subject")
                else
                    pattern[:detail] = [v.without("doctor", "hospital", "subject")]
                    pattern[:doctor] = v["doctor"].without("date_create","date_update")
                    pattern[:hospital] = v["hospital"].without("date_create","date_update")
                    pattern[:subject] = v["subject"].without("date_create","date_update")
                    result << pattern
                end
            end
            total = result.length
            return_data = []
            if params[:limit] && params[:offset]
                unless result[params[:offset].to_i].nil?
                    count = 0
                    result.drop(params[:offset].to_i).each do |each|
                        if count < params[:limit].to_i
                            return_data << each
                        else
                            break
                        end
                        count += 1
                    end
                end
            end
            final_data = { rows: return_data, total: total }
            arr_cache_doctor[key] = final_data
            $redis.set('arr_cache_doctor', arr_cache_doctor.to_json)
            $redis.expire('arr_cache_doctor', REDIS_DOCTOR_LIST_CACHE_TIME.minute.to_i)

        else
            final_data = arr_cache_doctor[key]
        end
        render json: final_data
        return
    end
    
    include Swagger::Blocks

    swagger_path '/doctor/search' do
        operation :get do
            key :summary, 'All Doctors'
            key :description, 'Returns all doctors'
            key :operationId, 'alldoctor'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'doctor'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :name, in: :query, description: 'Doctor name'
            parameter name: :sex, in: :query, description: 'Doctor sex'
            parameter name: :role, in: :query, description: 'Doctor role'
            parameter name: :subject_id, in: :query, description: 'ID Khoa'
            parameter name: :dayofweek, in: :query, description: 'Day of week: 1=T2, 2, 3, 4, 5, 6'
            parameter name: :buoi, in: :query, description: 'Buổi sáng chiều: 1,2,3'
            parameter name: :limit, in: :query, description: 'Limit'
            parameter name: :offset, in: :query, description: 'Offset'
            response 200 do
                key :description, 'doctor response'
                schema do
                    property :rows do
                        key :type, :array
                        items do
                            
                        end
                    end
                    property :total do
                        key :type, :integer
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/doctor/search2' do
        operation :get do
            key :summary, 'All Doctors 2'
            key :description, 'Returns all doctors 2'
            key :operationId, 'alldoctor2'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'doctor'
            ]
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :name, in: :query, description: 'Doctor name'
            parameter name: :sex, in: :query, description: 'Doctor sex'
            parameter name: :role, in: :query, description: 'Doctor role'
            parameter name: :subject_id, in: :query, description: 'ID Khoa'
            parameter name: :dayofweek, in: :query, description: 'Day of week: 1=T2, 2, 3, 4, 5, 6'
            parameter name: :buoi, in: :query, description: 'Buổi sáng chiều: 1,2,3'
            parameter name: :limit, in: :query, description: 'Limit'
            parameter name: :offset, in: :query, description: 'Offset'
            response 200 do
                key :description, 'doctor response'
                schema do
                    property :rows do
                        key :type, :array
                        items do
                            
                        end
                    end
                    property :total do
                        key :type, :integer
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/doctor/getall' do
        operation :get do
            key :summary, 'All Doctors'
            key :description, 'Returns all doctors'
            key :operationId, 'alldoctor'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'doctor'
            ]
            
            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :limit, in: :query, description: 'Limit'
            parameter name: :offset, in: :query, description: 'Offset'
            response 200 do
                key :description, 'doctor response'
                schema do
                    property :rows do
                        key :type, :array
                        items do
                            key :'$ref', :Doctor
                        end
                    end
                    property :total do
                        key :type, :integer
                    end
                end
            end
            response :default do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/doctor/detail' do
        operation :get do
            key :summary, 'Get detail doctor'
            key :description, 'Returns detail doctor'
            key :operationId, 'detaildoctor'
            key :consumes, [
                'application/x-www-form-urlencoded'
            ]
            key :tags, [
                'doctor'
            ]

            parameter name: :user_id, in: :query, description: 'user_id', required: true
            parameter name: :access_token, in: :query, description: 'access_token', required: true
            parameter name: :doctor_id, in: :query, description: 'ID doctor', required: true

            response 200 do
                key :description, 'doctor response'
                schema do
                    key :'$ref', :Doctor
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
