class Api::UserController < ApplicationController
    before_action :check_sign_in, only: []

    def fbsmslogin
        if params[:code]
            info = ::Facebook::SMS.get_user_info params[:code]
            if info.nil?
                render json: {error_code: 2,error_message: "no response"}
                return
            elsif info["error"]
                render json: {error_code: 1,error_message: info["error"]["message"]}
                return
            else
                res = {
                    number: "+999999999",
                    username: "+84999999999",
                    fullname: "Lê Văn A",
                    user_id: nil,
                    email: nil,
                    access_token: nil,
                    history_booking_count: 0
                }
                if info["phone"]
                    user = get_user_by_phone(info["phone"]["number"])
                    if !user
                        user = User.new do |e|
                            e.username = info["phone"]["number"]
                            e.phone = info["phone"]["number"]
                            e.salt = self.make_salt
                            e.hashpwd = self.make_hash(e.salt, e.salt)
                            e.save
                        end
                    end
                    
                elsif info["email"]
                    user = get_user_by_email(info["email"]["address"])
                    if !user
                        user = User.new do |e|
                            e.username = info["email"]["address"]
                            e.email = info["email"]["address"]
                            e.salt = self.make_salt
                            e.hashpwd = self.make_hash(e.salt, e.salt)
                            e.save
                        end
                    end
                end
                session = Session.select('user_id, access_token').find_by(user_id: user.id)
                unless session
                    session = Session.new(user_id: user.id, access_token: ::Util.md5(::Util.milisec))
                    session.save
                end

                res[:username] = user.username
                res[:fullname] = user.fullname
                res[:number] = user.phone
                res[:user_id] = user.id
                res[:email] = user.email
                res[:access_token] = session.access_token
                res[:history_booking_count] = user.booking.count
                res[:patient_count] = user.patient.count
                res[:is_admin] = user.is_admin
                render json: res
                return
            end
        else
            render json: {error_code: t(:user_no_code_found_code),error_message: t(:user_no_code_found)}
            return
        end
    end

    def detail
        # params[:is_supporter] #1, 0
        user = User.joins(:session).where(user: {id: params[:user_id]}, session: {access_token: params[:access_token]}).first
        # unless user
        #     render json: {error_code: t(:user_session_expired_code), error_message: t(:user_session_expired)}
        #     return
        # end
        if params[:is_supporter].to_i == 1
            render json: user.to_json(
                :include => [:supporter => {
                    except: [:date_create, :date_update],
                    methods: [:avatar_url]
                }]
            )
            return
        else
            render json: user
            return
        end
    end

    private def get_user_by_phone(phone)
        return User.find_by(phone: phone)
    end

    def get_user_by_email(email)
        return User.find_by(email: email)
    end

    include Swagger::Blocks
    swagger_path '/user/fbsmslogin' do
        operation :post do
            key :summary, 'Login via SMS Facebook Kit'
            key :description, 'Login via SMS Facebook Kit'
            key :operationId, 'fbsmslogin'
            key :tags, [
                'user'
            ]
            parameter name: :code,
                in: :query,
                description: 'code',
                required: true,
                schema:{}
            response 200 do
                key :description, 'login success'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :User
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end

    swagger_path '/user/detail' do
        operation :get do
            key :summary, 'Detail User'
            key :description, 'Detail User'
            key :operationId, 'detail'
            key :tags, [
                'user'
            ]
            parameter name: :user_id,
                in: :query,
                description: 'user_id',
                required: true,
                schema:{}
            parameter name: :access_token,
                in: :query,
                description: 'user_id',
                required: true,
                schema:{}
            parameter name: :is_supporter,
                in: :query,
                description: 'nếu muốn tìm supporter thì lấy cái này bằng 1, ko thì khỏi gắn',
                required: false,
                schema:{}
            response 200 do
                key :description, 'login success'
                schema do
                    key :type, :array
                    items do
                        key :'$ref', :User
                    end
                end
            end
            response :error do
                key :description, 'unexpected error'
                schema do
                    key :'$ref', :ErrorModel
                end
            end
        end
    end
end
