class BookingIndex < Chewy::Index
    settings analysis: {
        analyzer: {
            title: {
                tokenizer: 'standard',
                filter: ['lowercase', 'asciifolding']
            },
            sorted: {
                tokenizer: 'keyword',
                filter: ['lowercase', 'asciifolding']
            }
        }
    }

    define_type Booking.where(is_deleted: 0).includes(:schedule, :booking_time, :patient, :payment, :user) do
        field :id, type: 'integer'
        field :transaction_code_gd
        field :booking_date
        # field :doctor do
        #     field :doctor_id, value: ->(doctor) { doctor.id }
        #     field :doctor_name, value: ->(doctor) { doctor.name }
        # end
        field :hospital do
            field :hospital_id, value: ->(hospital) { hospital.id }
            field :hospital_name, value: ->(hospital) { hospital.name }
        end
        field :subject do
            field :subject_id, value: ->(subject) { subject.id }
            field :subject_name, value: ->(subject) { subject.name }
            field :subject_code, value: ->(subject) { subject.code }
        end
        field :ptime do
            field :buoi, type: 'integer', value: ->(ptime) {ptime.buoi}
            field :day_of_week, type: 'integer', value: ->(ptime) {ptime.weekday.to_i + 1}
        end
        field :payment do
            field :transaction_code_tt, value: ->(payment) { payment.transaction_code_tt }
        end
        field :room do
            field :room_id, value: ->(room) { room.id }
            field :room_name, value: ->(room) { room.name }
            field :room_description, value: ->(room) { room.description }
        end
    end
end