class ScheduleIndex < Chewy::Index
    define_type Schedule.where(is_old: 0).includes(:doctor, :hospital, :subject, :ptime) do
        field :id, type: 'integer'
        field :doctor do
            field :doctor_id, value: ->(doctor) { doctor.id }
            field :doctor_name, value: ->(doctor) { doctor.name }
        end
        field :hospital do
            field :hospital_id, value: ->(hospital) { hospital.id }
            field :hospital_name, value: ->(hospital) { hospital.name }
        end
        field :subject do
            field :subject_id, value: ->(subject) { subject.id }
            field :subject_name, value: ->(subject) { subject.name }
            field :subject_code, value: ->(subject) { subject.code }
        end
        field :ptime do
            field :buoi, type: 'integer', value: ->(ptime) {ptime.buoi}
            field :day_of_week, type: 'integer', value: ->(ptime) {ptime.weekday.to_i + 1}
        end
    end
end