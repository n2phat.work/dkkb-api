class Skin::NotiJob < ApplicationJob
  queue_as :default

  def perform(noti_id)
    # Do something later
    noti = Skin::Noti.find_by(id: noti_id, is_pushed: 0)
    if noti
      case noti.type.to_i
        when NOTI_NORMAL
          title = "BVDL: Thông tin Phiếu khám bệnh"
        when NOTI_SCHEDULE_CHANGE
          title = "BVDL: Thay đổi Phiếu khám bệnh"
        when NOTI_PROMO
          title = "Ưu Đãi"
        when NOTI_SYSTEM
          title = "Hệ thống"
        else
          title = "BVDL Thông Báo"
      end
      if noti.user_id
        device = Skin::PushDevice.where(user_id: noti.user_id).pluck(:onesignal_id)
        ::Onesignal::Skin.sendpush(device, title, noti.title, noti.to_json.to_s)
      else
        ::Onesignal::Skin.sendpush(nil, title, noti.title, noti.to_json.to_s)
      end
      noti.is_pushed = 1
      noti.save!
    end
  end
end
