class Api::CurlJob < ApplicationJob
  queue_as :default

  def perform(server_url, email_params)
    # Do something later
    ::Util.curl_post(server_url,email_params)
  end
end
