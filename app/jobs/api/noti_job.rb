class Api::NotiJob < ApplicationJob
  queue_as :default

  def perform(noti_id)
    # Do something later
    noti = Noti.find_by(id: noti_id, is_pushed: 0)
    if noti
      case noti.type.to_i
        when NOTI_NORMAL
          title = "BVDHYD: Thông tin Phiếu khám bệnh"
        when NOTI_SCHEDULE_CHANGE
          title = "BVDHYD: Thay đổi Phiếu khám bệnh"
        when NOTI_PROMO
          title = "Ưu Đãi"
        when NOTI_SYSTEM
          title = "Hệ thống"
        else
          title = "BVDHYD Thông Báo"
      end
      if noti.user_id
        device = PushDevice.where(user_id: noti.user_id).pluck(:onesignal_id)
        ::Onesignal::Main.sendpush(device, title, noti.title, noti.to_json.to_s)
      else
        ::Onesignal::Main.sendpush(nil, title, noti.title, noti.to_json.to_s)
      end
      noti.is_pushed = 1
      noti.save!
    end
  end
end
