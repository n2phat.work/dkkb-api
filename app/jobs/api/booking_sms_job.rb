class Api::BookingSmsJob < ApplicationJob
  queue_as :default

  def perform(mobile, sms_msg)
    mobile = mobile.gsub(" ","")
    ap ({
        msg: sms_msg,
        mobile: mobile
    })
    # start SMS
    client = Savon.client(wsdl: SMS_SOAP_URL)
    result = client.call(:send_sms, message: {
        authenticate_user: SMS_USER, 
        authenticate_pass: SMS_AUTHEN, 
        brand_name: SMS_BRAND_NAME,
        message: sms_msg,
        receiver: mobile.to_s,
        type: 1
    })
    LogSms.new do |ls|
        ls.mobile = mobile
        ls.content = sms_msg
        ls.response = result
        ls.save!
    end
  end
end
