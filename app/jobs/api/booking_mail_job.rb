class Api::BookingMailJob < ApplicationJob
  queue_as :default

  def perform(to, subject, booking_id)
    Api::BookingMailer.sendmail(to, subject, booking_id).deliver_now
  end
end
