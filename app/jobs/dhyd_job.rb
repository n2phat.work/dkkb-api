class DhydJob < ApplicationJob
  queue_as :default

  def perform(*args)
    require 'open-uri'
    html = Nokogiri::HTML(open("http://www.bvdaihoc.com.vn/Home/ViewLichKham?page=1&pageSize=1000&doctorFullName=&hochamhocvi=0&gioitinh=-1&chuyenKhoa=0&danhmuclichkham=0&sortBy=1&isAsc=0#maincontent"))
    html.at('table').search('tr').each do |row|
      cells = row.search('th, td').map { |cell| (cell.text.strip != "") ? cell.text.strip : cell.search('img').map{ |c| c.attr('src').strip}[0]}
      if cells[0] != "Họ và tên"
        arrSpecial = cells[1].split("\r\n").map{|e| e.strip }.delete_if {|e| e == '' }
        special = arrSpecial ? arrSpecial.count : 0
        pk_role = note = nil
        mp_time = nil
        case special
        when 1
          fullname = arrSpecial[0]
        when 2
          if arrSpecial[0].downcase != 'Phụ trách phòng khám'.downcase
            fullname = arrSpecial[0]
            note = arrSpecial[1]
          else
            fullname = arrSpecial[1]
            pk_role = arrSpecial[0]
          end
        when 3
          if arrSpecial[0].downcase != 'Phụ trách phòng khám'.downcase
            fullname = arrSpecial[0]
            note = arrSpecial[1] + ', ' + arrSpecial[2]
          else
            fullname = arrSpecial[1]
            pk_role = arrSpecial[0]
            note = arrSpecial[2]
          end
        end

        dhyd = Dhyd.find_by(fullname: fullname, subject: cells[4])
        if dhyd
          dhyd.tap do |e|
            e.avatar = cells[0]
            e.webname = cells[1]
            e.role = cells[2]
            e.sex = ((cells[3] ==  'Nam') ? 1: 0)
            e.subject = cells[4]
            e.time = cells[5]
            e.special = special
            e.fullname = fullname
            e.note = note
            e.pk_role = pk_role
            e.mp_time = mp_time
            p e.changes
            e.save
          end
        else
          dhyd = Dhyd.new do |e|
            e.avatar = cells[0]
            e.webname = cells[1]
            e.role = cells[2]
            e.sex = ((cells[3] ==  'Nam') ? 1: 0)
            e.subject = cells[4]
            e.time = cells[5]
            e.special = special
            e.fullname = fullname
            e.note = note
            e.pk_role = pk_role
            e.mp_time = mp_time
            e.save
          end
        end
      end
    end
  end
end
