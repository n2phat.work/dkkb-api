class Nd1::NotiJob < ApplicationJob
  queue_as :default

  def perform(noti_id)
    # Do something later
    noti = Nd1::Noti.find_by(id: noti_id, is_pushed: 0)
    if noti
      case noti.type.to_i
        when NOTI_NORMAL
          title = "BVND1: Thông tin Phiếu khám bệnh"
        when NOTI_SCHEDULE_CHANGE
          title = "BVND1: Thay đổi Phiếu khám bệnh"
        when NOTI_PROMO
          title = "Ưu Đãi"
        when NOTI_SYSTEM
          title = "Hệ thống"
        else
          title = "BVND1 Thông Báo"
      end
      if noti.user_id
        device = Nd1::PushDevice.where(user_id: noti.user_id).pluck(:onesignal_id)
        ::Onesignal::Nd1.sendpush(device, title, noti.title, noti.to_json.to_s)
      else
        ::Onesignal::Nd1.sendpush(nil, title, noti.title, noti.to_json.to_s)
      end
      noti.is_pushed = 1
      noti.save!
    end
  end
end
