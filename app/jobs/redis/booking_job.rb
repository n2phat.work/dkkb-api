class Redis::BookingJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    bookings = Booking.where(user_id: user_id)
    # if params[:patient_id]
    #     booking = booking.where(patient_id: params[:patient_id])
    # end
    bookings = bookings.order('id DESC')
    result = bookings.to_json

    $redis.set('booking_list_' + user_id.to_s, result)
    $redis.expire('booking_list_' + user_id.to_s, REDIS_BOOKING_LIST_CACHE_TIME.minute.to_i)
  end
end
