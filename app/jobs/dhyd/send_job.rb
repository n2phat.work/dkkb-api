class Dhyd::SendJob < ApplicationJob
    queue_as :default

    def perform(*args)
        payments = Payment.where(status_process: 0).where('`status` = 1 OR `status` = -2')
        payments.each do |e|
            tt = {
                transaction_code_tt: e.transaction_code_tt,
                real_price: e.amount,
                bank_code: e.card_code,
                loai_the: e.method_id == 6 ? 3 : e.method_id,
                date_update: e.date_update,
                medpro_price: MEDPRO_SERVICE_FEE,
                status: (e.status < 0) ? -1 : 1,
                total_price: e.amount_original
            }
            if Rails.env == 'production'
                result = ::Bvdhyd::Lichkham.insertTT(tt)
                if result["status"]
                    e.status_process = 1
                    e.save!
                end
            end
        end

        bookings = Booking.where(status_process: 0).where('`status` = 1 OR status = -2')
        bookings.each do |e|
            dl = {
                booking_date: e.booking_date,
                bvdhyd_msbn: e.patient.bvdhyd_msbn,
                buoi: e.schedule.ptime.buoi,
                ma_khoa: e.subject.code,
                doctor_id: e.doctor.id,
                booking_number: e.booking_number,
                surname: e.patient.surname,
                birthyear: e.patient.birthyear,
                sex: e.patient.sex.to_i,
                bhyt: e.bhyt_accept ? 1 : 0,
                note: "",
                name: e.patient.name,
                birthdate: e.patient.birthdate,
                price: e.subject.price,
                status: e.status < 0 ? -1 : 1,
                medpro_id: e.patient.medpro_id,
                bvdhyd_room_id: e.schedule.room.bvdhyd_room_id,
                address: e.patient.address,
                transaction_code_gd: e.transaction_code_gd,
                transaction_code_tt: e.payment.transaction_code_tt,
                mobile: e.patient.mobile,
                country_code: e.patient.country_code,
                city_id: e.patient.city_id,
                district_id: e.patient.district_id,
                ward_id: e.patient.ward_id,
                profession_id: e.patient.profession_id,
                dantoc_id: e.patient.dantoc_id,
                email: e.patient.email,
                bv_booking_time: e.bv_booking_time ? e.bv_booking_time.to_time.strftime("%H:%M:%S") : nil
            }
            if Rails.env == 'production'
                result = ::Bvdhyd::Lichkham.insertDL(dl)
                if result
                    e.status_process = 1
                    e.save!
                end
            end
        end
    end
end
