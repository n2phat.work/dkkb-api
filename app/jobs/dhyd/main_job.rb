class Dhyd::MainJob < ApplicationJob
    queue_as :default

    def perform(*args)
        data = Bvdhyd.where(is_new: 1).order('khoa_code ASC')
        data.each do |e|
            if e.room_id
                khoa = Subject.unscoped.find_or_create_by(code: e.khoa_code) do |k|
                    k.name = e.khoa_name
                    k.price = e.khoa_price
                    k.save!
                end

                hs = HospitalSubject.find_or_create_by(hospital_id: 2, subject_id: khoa.id) do |hss|
                    hss.status = 1
                    hss.save!
                end
                
                room = Room.new do |r|
                    r.bvdhyd_room_id = e.room_id
                    r.hospital_id = 2
                    r.name = e.room_name
                    r.description = e.description
                    r.save!
                end

                doctor = Doctor.find_or_create_by(id: e.IDBS) do |bs|
                    bs.name = e.fullname
                    bs.role = e.role
                    bs.sex = e.sex.to_i
                    bs.save!
                end

                arr_stt = e.list_STT.split(",")
                arr_time = e.list_GioKham.split(",")
                tmp_arr_slot = ::Bvdhyd::Slottime.new({
                            strTime: e.start_time.to_s,
                            start_number: arr_stt.first.to_i,
                            max_slot: arr_stt.count,
                            time_per_slot: e.time_per_slot.to_i,
                            first: e.step.to_i,
                            second: 0
                        }).get_list
                arr_slot = e.buoi == 1 ? tmp_arr_slot[:sang] : tmp_arr_slot[:chieu]
                arr_slot.each do |slot|
                    BookingTime.new do |bk|
                        bk.from = slot[:from]
                        bk.to = slot[:to]
                        bk.number_from = slot[:number_from]
                        bk.number_to = slot[:number_to]
                        bk.step = slot[:step]
                        bk.room_id = room.id
                        bk.max_slot = slot[:max_slot]
                        bk.save!
                    end
                end

                pTime = Ptime.new do |p|
                    p.hour_from = arr_slot.first[:from]
                    p.hour_to = arr_slot.last[:to]
                    p.day_from = 1
                    p.day_to = 31
                    p.month_from = 1
                    p.month_to = 12
                    p.buoi = e.buoi
                    p.weekday = e.dayofweek.to_i > 1 ? (e.dayofweek.to_i - 1) : 1
                    p.save!
                end

                Schedule.new do |sc|
                    sc.hospital_subject_id = hs.id
                    sc.room_id = room.id
                    sc.ptime_id = pTime.id
                    sc.doctor_id = doctor.id
                    sc.save!
                end
            end
        end
    end
end
