class Dhyd::BnGetSohsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Booking.joins(:patient).where(status: [1,2,-2], patient:{bvdhyd_msbn: nil}).where("booking_date = ?", (Time.now - 1.day).strftime("%Y-%m-%d"))
    data = args[0]
    if data[:patient_id]
      patient = Patient.find_by(id: data[:patient_id])
      if patient
        if patient.bvdhyd_msbn.blank?
          data = Bvdhyd::Patient.get_sohs_from_tttk({medpro_id: patient.medpro_id})
          if !data.blank?
            data.each do |e|
              if e["SoHS"]
                old_patient = nil #Patient.find_by(bvdhyd_msbn: e["SoHS"])
                unless old_patient
                  patient.bvdhyd_msbn = e["SoHS"]
                  patient.save!
                end
                return
              end
            end
          end
        end
      end
    end
  end
end