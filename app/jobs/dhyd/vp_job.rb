class Dhyd::VpJob < ApplicationJob
    queue_as :default

    def perform(*args)
        pf = PaymentFee.where(status_process: 0, status: 1).limit(1)
        ap pf
        pf.each do |e|
            tt = {
                transaction_code_tt: e.transaction_code_tt,
                real_price: e.amount,
                loai_the: e.method_id,
                date_update: e.date_create,
                medpro_price: MEDPRO_VP_SERVICE_FEE,
                status: (e.status < 0) ? -1 : e.status,
                total_price: e.amount_original
            }
            if Rails.env == 'production'
                result = ::Bvdhyd::Lichkham.insertTT(tt)
                if result["status"]
                    e.status_process = 1
                    e.save!
                end
            end
            fee = Fee.where(id: e.fee_id.split(","))
            fee.each do |f|
                data = {
                    id: f.id,
                    msbn: f.msbn,
                    msnv: f.msnv,
                    date: f.date_update,
                    subject_code: f.subject_code,
                    amount: f.amount,
                    note: "",
                    status: f.status,
                    transaction_code_tt: e.transaction_code_tt
                }
                
                if Rails.env == 'production'
                    ::Bvdhyd::Vienphi.insert_thanhtoan(data)
                    ::Bvdhyd::Vienphi.update_tamung({id: f.id, status: 1})
                    # if f.ttxv.to_i == 1
                    _sms_ketoan(f.so_dt, f.sohoadon, f.amount, f.msbn)
                    # end
                end
            end
        end
    end

    def _sms_ketoan(mobile = '', shd = '', amount = '', msbn = '')
        mobile = mobile.sub("+84","0")
        mobile = "0" + mobile.to_s
        mobile = mobile.gsub(/^00/,"0")
        client = Savon.client(wsdl: 'http://brand.aztech.com.vn/ws/agentSmsApiSoap?wsdl')
        sms_msg = "Thanh toan thanh cong. SHD: #{shd}"
        sms_msg = "Benh nhan #{msbn} da thanh toan thanh cong so tien #{amount.to_s}. Ma thanh toan: #{shd}"
        result = client.call(:send_sms, message: {
            authenticate_user: SMS_USER, 
            authenticate_pass: SMS_AUTHEN, 
            message: sms_msg,
            brand_name: 'BvDHYD',
            receiver: mobile,
            type: 1
        })
        LogSms.new do |e|
            e.mobile = mobile
            e.content = sms_msg
            e.response = result
            e.save!
        end
    end
end
