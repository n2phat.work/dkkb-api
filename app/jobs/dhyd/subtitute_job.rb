class Dhyd::SubtituteJob < ApplicationJob
  queue_as :default

  def perform(*args)
    subs = BvdhydLichbsthaythe.where(is_notified: 0).where('Ngay >= ? AND Ngay <= ?', (Time.now + 1.days).strftime("%Y-%m-%d"), (Time.now + 30.days).strftime("%Y-%m-%d"))
    subs.each do |e|
      # bookings = Booking.joins(:schedule).joins(:subject).joins(:ptime).where(schedule: {doctor_id: e.IDBS}).where(booking_date: e.Ngay).where(subject: {code: e.MaDonVi}).where(ptime: {buoi: e.RIDBuoiKham})
      bookings = Booking.joins(:schedule).joins(:subject).joins(:ptime).where(schedule: {doctor_id: e.IDBS}).where(booking_date: e.Ngay).where(schedule: {subject: {code: e.MaDonVi}}).where(schedule: {ptime: {buoi: e.RIDBuoiKham}}).where(status: [1])
      if bookings.size > 0
        bookings.each do |bk|
          sub_doctor = Doctor.find_by(id: e.IDBSThayThe)
          Noti.new do |noti|
            noti.type = 2
            noti.user_id = bk.user_id
            noti.booking_id = bk.id
            noti.title = sub_doctor ? "Thay đổi BS khám: #{bk.doctor.name} thay thế bởi BS: #{sub_doctor.name}" : "Thay đổi BS khám: #{bk.doctor.name}"
            noti.content = sub_doctor ? "BS khám bệnh của bạn: <strong>#{bk.doctor.name}</strong> đã được thay thế bởi BS: <strong>#{sub_doctor.name}</strong>" : "BS khám bệnh của bạn: <strong>#{bk.doctor.name}</strong> đã được thay thế."
            noti.save!
          end
        end
      end
      e.is_notified = 1
      e.save!
    end
    return nil
  end
end
