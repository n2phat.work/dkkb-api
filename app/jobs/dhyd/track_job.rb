class Dhyd::TrackJob < ApplicationJob
    queue_as :default

    def perform(*args)
        if Rails.env == 'production'
            data = args.first
            bk = Booking.find(data[:booking_id])
            number = data[:booking_number] ? data[:booking_number] : nil
            dtk = BvdhydDutrukham.find_by({
                MaDonVi: bk.subject.code, 
                RIDBuoiKham: bk.schedule.ptime.buoi,
                RIDThu: bk.booking_date.wday.to_i + 1,
                IDBS: bk.schedule.doctor_id,
                STT: number ? number : bk.booking_number
            })
            data = {
                khoa_code: bk.subject.code,
                buoi: bk.schedule.ptime.buoi,
                doctor_id: bk.schedule.doctor_id,
                booking_date: bk.booking_date.to_s,
                booking_number: number ? number : bk.booking_number,
                bvdhyd_room_id: bk.schedule.room.bvdhyd_room_id,
                gio_kham: dtk.GioKham.strftime("%H:%M"),
                transaction_code_gd: bk.transaction_code_gd,
                transaction_code_tt: bk.payment ? bk.payment.transaction_code_tt : nil,
                loai: (bk.platform == 'web' || bk.platform.empty?) ? 3 : 4
            }
            result = ::Bvdhyd::Lichkham.insertDSK(data)
        end
    end
end
