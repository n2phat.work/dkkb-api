class Dhyd::CrawlJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # :bs
    # :bstt # bs thay the
    # :dtk # du tru kham
    # :room
    # :khoa
    # :doctor
    # :tk # tai kham
    # :cls # danh muc can lam sang

    data = args.length > 0 ? args.first : {}

    resultLichBS = 1
    resultDutru = 1
    resultThaythe = 1
    resultRoom = 1
    resultKhoa = 1
    resultDoctor = 1
    resultTaikham = 1
    resultCls = 1

    if data.key?(:lk)
      offset = 0
      limit = 500
      while resultLichBS != nil
        resultLichBS = ::Bvdhyd::Lichkham.getall_lich_bs(limit, offset)
        if resultLichBS != nil
          resultLichBS.each do |e|
              self.insert_lichbs(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:dtk)
      offset = 0 #BvdhydDutrukham.count
      limit = 1000
      while resultDutru != nil
        resultDutru = ::Bvdhyd::Lichkham.get_dutru_kham(limit, offset)
        if resultDutru != nil
          resultDutru.each do |e|
              self.insert_dutrukham(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:bstt)
      offset = 0
      limit = 500
      while resultThaythe != nil
        resultThaythe = ::Bvdhyd::Lichkham.getall_lich_bsthaythe(limit, offset)
        if resultThaythe != nil
          resultThaythe.each do |e|
              self.insert_lichbs_thaythe(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:room)
      offset = 0
      limit = 500
      while resultRoom != nil
        resultRoom = ::Bvdhyd::Room.getall(limit, offset)
        if resultRoom != nil
          resultRoom.each do |e|
              self.insert_room(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:khoa)
      offset = 0 #BvdhydKhoa.count
      limit = 500
      while resultKhoa != nil
        resultKhoa = ::Bvdhyd::Khoa.getall(limit, offset)
        if resultKhoa != nil
          resultKhoa.each do |e|
              self.insert_khoa(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:bs)
      offset = 0
      limit = 500
      while resultDoctor != nil
        resultDoctor = ::Bvdhyd::Doctor.getall(limit, offset)
        if resultDoctor != nil
          resultDoctor.each do |e|
              self.insert_doctor(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:tk)
      offset = 0
      limit = 500
      while resultTaikham != nil
        resultTaikham = ::Bvdhyd::Taikham.getall(limit, offset)
        if resultTaikham != nil
          resultTaikham.each do |e|
              self.insert_taikham(e)
          end
        end
        offset = offset + limit
      end
    end

    if data.key?(:cls)
      offset = 0
      limit = 500
      while resultCls != nil
        resultCls = ::Bvdhyd::Cls.getallindex(limit, offset)
        if resultCls != nil
          resultCls.each do |e|
              self.insert_cls(e)
          end
        end
        offset = offset + limit
      end
    end
  end

  def insert_cls(params)
    BvdhydClsIndex.find_or_create_by(
      ID_CLS: params["ID_CLS"],
      IDChuongTrinh: params["IDChuongTrinh"]
    ) do |e|
      e.DienGiai = params["DienGiai"]
      e.save!
    end
  end

  def insert_taikham(params)
    BvdhydTaikham.find_or_create_by(
      MaDonVi: params["MaDonVi"], 
      IDBS: params["IDBS"], 
      RIDBuoiKham: params["RIDBuoiKham"],
      SoHoSo: params["SoHoSo"],
      NgayKham: params["NgayKham"],
      BHYT: params["BHYT"]
    ) do |e|
        e.processed_id = params["ID"] ? params["ID"] : nil
        e.response = params.to_json
        e.save!
    end
    return true
  end

  def insert_lichbs(params)
    BvdhydLichbs.find_or_create_by(
          MaDonVi: params["MaDonVi"], 
          IDBS: params["IDBS"], 
          RIDBuoiKham: params["RIDBuoiKham"],
          RIDThu: params["RIDThu"],
          IDDonViCT: params["IDDonViCT"]
    ) do |e|
        e.processed_id = params["ID"] ? params["ID"] : nil
        e.NgungTam = params["NgungTam"]
        e.SlgBNTam = params["SlgBNTam"]
        e.GhiChuTam = params["GhiChuTam"]
        e.response = params.to_json
        e.save!
    end
    return true
  end

  def insert_lichbs_thaythe(params)
    BvdhydLichbsthaythe.find_or_create_by(
      MaDonVi: params["MaDonVi"],
      IDBS: params["IDBS"],
      RIDThu: params["RIDThu"],
      RIDBuoiKham: params["RIDBuoiKham"],
      Ngay: params["Ngay"],
      IDBSThayThe: params["IDBSThayThe"]
    ) do |e|
        e.processed_id = params["IDKhamThe"] ? params["IDKhamThe"] : nil
        e.LyDo = params["LyDo"]
        e.response = params.to_json
        e.save!
    end
    return true
  end

  def insert_dutrukham(params)
    BvdhydDutrukham.new do |e|
        e.MaDonVi = params["MaDonVi"]
        e.IDBS = params["IDBS"]
        e.RIDThu = params["RIDThu"]
        e.RIDBuoiKham = params["RIDBuoiKham"]
        e.STT = params["SoTT"]
        e.processed_id = nil
        e.GioKham = params["GioKham"]
        e.response = params.to_json
        e.save!
    end
    return true
  end

  def insert_room(params)
    BvdhydRoom.find_or_create_by(
      IDDonViCT: params["IDDonViCT"]
    ) do |e|
        e.NgungSD = params["NgungSD"]
        e.IDDonViCTCha = params["IDDonViCTCha"]
        e.TenDonViCT = params["TenDonViCT"]
        e.MaDonVi = params["MaDonVi"]
        e.response = params.to_json
        e.save!
    end
    return true
  end

  def insert_khoa(params)
    BvdhydKhoa.find_or_create_by(
      Ten: params["Ten"],
      MaKhoa: params["MaKhoa"]
    ) do |e|
        e.Stopsd = params["Stopsd"]
        e.Mota = params["Mota"]
        e.SoTien = params["SoTien"]
        e.response = params.to_json
        e.save!
    end
    return true
  end

  def insert_doctor(params)
    BvdhydDoctor.find_or_create_by(
      IDBS: params["IDBS"]
    ) do |e|
        e.HoTen = params["HoTen"]
        e.ChucDanh = params["ChucDanh"]
        e.GioiTinh = params["GioiTinh"]
        e.response = params.to_json
        e.save!
    end
    return true
  end
end
