class Dhyd::BnUpdateJob < ApplicationJob
    queue_as :default

    def perform(*args)
        data = args.length > 0 ? args.first : {}
        Bvdhyd::Patient.insert_bn_update({
          msbn: data[:msbn], 
          mobile: mobile_filter(data[:mobile].to_s),
          email: data[:email],
          medpro_id: data[:medpro_id]
        })
    end

    def mobile_filter(mobile)
        arr_tmp = mobile.split("-")
        mobile = arr_tmp.first.to_s
        mobile = mobile.gsub(/\./,"")
        mobile = mobile.gsub(/\-/,"")
        mobile = mobile.gsub(/\,/,"")
        mobile = mobile.gsub(/\s/,"")
    end
end
