class PaymentLog < ApplicationRecord
    self.table_name = 'payment_log'
    belongs_to :payment, optional: true
end
