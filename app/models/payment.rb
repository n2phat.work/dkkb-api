class Payment < ApplicationRecord
    self.table_name = 'payment'
    has_many :payment_log
    has_many :booking
    has_one :payment_payoo

    before_save do |pm|
        begin
            pml = PaymentLog.find_by(payment_id: pm.id, status: pm.status)
            if pm.status == PAYMENT_CANCEL_LATER
                unless pml
                    pml = PaymentLog.new({
                        payment_id: pm.id,
                        status: pm.status,
                        date_cancel: Time.now
                    })
                    pml.save!
                end
            elsif pm.status == 1
                if pm.result
                    json = ::Util.json_decode(pm.result, true)
                    unless pml
                        pml = PaymentLog.new({
                            payment_id: pm.id,
                            status: pm.status
                        })
                        if pm.method_id == 2
                            date = Time.strptime(json[:auth_time],'%Y-%m-%dT%H%M%S%Z').localtime
                        elsif pm.method_id == 3
                            date = Time.strptime(json[:vnp_PayDate],'%Y%m%d%H%M%S').localtime
                        elsif pm.method_id == 6
                            date = Time.strptime(json[:vnp_PayDate],'%Y%m%d%H%M%S').localtime
                        elsif pm.method_id == 7
                            date = C.parse(json[:vnp_PayDate])
                        else
                            date = Time.now
                        end
                        pml.date_postback_atm = date
                        pml.save!
                    end
                    
                end
            end
        rescue => exception
            SystemException.new({content: exception.to_s, trace: exception.backtrace}).save!
        end
    end

    after_save do |pm|
        unless pm.status == nil
            bookings = Booking.where(payment_id: pm.id)
            bookings.update(status: pm.status)
            if pm.status == PAYMENT_CANCEL_LATER
                bookings.each do |bk|
                    bk.status_process = 0
                    bk.refund_status = 1
                    bk.save!
                    Noti.new do |noti|
                        noti.type = NOTI_NORMAL
                        noti.user_id = bk.user_id
                        noti.booking_id = bk.id
                        noti.title = "Bạn đã hủy thành công phiếu khám bệnh #{bk.transaction_code_gd}."
                        noti.content = "Tiền khám #{bk.subject ? bk.subject.price : 0 }đ sẽ được hoàn vào số tài khoản của bạn theo Quy định. <br/>Trân trọng cảm ơn!"
                        noti.save!
                    end
                    # xoa DSK ben BV
                    Bvdhyd::Lichkham.deleteDSK({transaction_code_gd: bk.transaction_code_gd}) if Rails.env == "production"
                end
            end
        end
    end

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Payment do
        key :required, [:id, :transaction_id]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :transaction_id do
            key :type, :string
        end
        property :amount do
            key :type, :integer
            key :format, :int32
        end
        property :result do
            key :type, :string
        end
        property :status do
            key :type, :integer
            key :format, :int8
        end
    end
end
