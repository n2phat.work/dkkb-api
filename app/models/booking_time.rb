class BookingTime < ApplicationRecord
    self.table_name = 'booking_time'
    belongs_to :room, foreign_key: :room_id, optional: true
    has_many :booking

    attr_accessor :used_slot

    def as_json(options = {})
        super(methods: [:used_slot],except: [:date_create, :date_update])
    end

    include Swagger::Blocks
    swagger_schema :BookingTime do
        key :required, [:id, :from, :to, :number_from, :number_to]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :from do
            key :type, :integer
            key :format, :int32
        end
        property :to do
            key :type, :integer
            key :format, :int32
        end
        property :number_from do
            key :type, :integer
            key :format, :int32
        end
        property :number_to do
            key :type, :integer
            key :format, :int32
        end
        property :number_skip do
            key :type, :integer
            key :format, :int32
        end
        property :max_slot do
            key :type, :integer
            key :format, :int32
        end
    end
end
