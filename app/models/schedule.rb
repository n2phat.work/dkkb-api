class Schedule < ApplicationRecord
    self.table_name = 'schedule'
    # default_scope { where(status: 1) }
    scope :with_hospital_subject, -> (hospital_id, subject_id) {
        joins(:hospital_subject).where(
            :hospital_subject => {
                hospital_id: hospital_id,
                subject_id: subject_id
            }
        )
    }
    belongs_to :hospital_subject, foreign_key: 'hospital_subject_id', primary_key: :id, optional: true
    belongs_to :doctor, foreign_key: 'doctor_id', primary_key: :id, optional: true
    belongs_to :ptime, foreign_key: 'ptime_id', primary_key: :id, optional: true
    belongs_to :room, foreign_key: 'room_id', primary_key: :id, optional: true

    has_one :hospital, through: :hospital_subject
    has_one :subject, through: :hospital_subject

    # update_index 'schedule#schedule', :self

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
end
