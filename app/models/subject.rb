class Subject < ApplicationRecord
    self.table_name = 'subject'
    default_scope { where(is_internal: 0) }
    has_many :hospital_subject
    has_many :hospital, through: :hospital_subject
    has_many :room, through: :hospital_subject
    has_many :schedule, through: :hospital_subject

    default_scope { order('name ASC') }

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Subject do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :is_cls do
            key :type, :integer
            key :format, :int8
            key :note, '0 or 1'
        end
    end
end
