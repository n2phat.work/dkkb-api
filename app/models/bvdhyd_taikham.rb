class BvdhydTaikham < ApplicationRecord
    self.table_name = 'bvdhyd_taikham'
    has_one :doctor, class_name: "Doctor", primary_key: :IDBS, foreign_key: :id
    has_one :subject, class_name: "Subject", primary_key: :MaDonVi, foreign_key: :code
    
    def as_json(options={})
        super(options)
    end
end
