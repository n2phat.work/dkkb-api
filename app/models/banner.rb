class Banner < ApplicationRecord
    self.table_name = 'banner'

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :Banner do
        key :required, [:id, :image]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :imgage do
            key :type, :string
        end
    end
end
