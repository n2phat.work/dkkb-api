class Supporter < ApplicationRecord
    self.table_name = 'supporter'

    belongs_to :user, foreign_key: :user_id, optional: true

    def avatar_url
        self.avatar.url rescue nil
    end

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        if options[:methods]
            options[:methods] += [:avatar_url]
        else
            options[:methods] = [:avatar_url]
        end
        super(options)
    end
end