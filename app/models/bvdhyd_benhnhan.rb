class BvdhydBenhnhan < ApplicationRecord
    self.table_name = 'bvdhyd_benhnhan'

    def as_json(options={})
        super(options)
    end

    def self.crawl
        offset = 0
        limit = 1000
        result = 1
        while result != nil
            result = ::Bvdhyd::Patient.getall(limit, offset)
            if result != nil
                result.each do |e|
                    self.find_or_create_by(data: e.to_s) do |f|
                        f.save
                    end
                end
            end
            offset = offset + limit
        end 
    end
end
