class Report
    def self.count_daily(date = nil)
        date = date.to_date.strftime("%Y-%m-%d") if date
        begin
            data = StoredProcedureService.new.execute("count_daily", date.to_s)
        rescue => exception
            data = []
            SystemException.new({content: exception.to_s, trace: exception.backtrace}).save!
        end
        return_data = []
        i = 1
        data.each do |e|
            if !e[:field].nil? && !e[:value].nil?
                return_data << {
                  id: i,
                  field: e[:field],
                  total: e[:value]
                }
                i += 1
            end
        end
        return return_data
    end
end