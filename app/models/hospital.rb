class Hospital < ApplicationRecord
    self.table_name = 'hospital'
    has_many :hospital_subject
    has_many :subject, through: :hospital_subject
    has_many :room, through: :hospital_subject
    has_many :doctor, through: :hospital_subject
    has_many :ptime, through: :hospital_subject
    has_many :schedule, through: :hospital_subject

    belongs_to :city, foreign_key: :city_id, optional: true

    # attr_accessor :khoa
    
    # def attributes
    #     super.merge({
    #         'khoa' => self.subject
    #     })
    # end

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Hospital do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :status do
            key :type, :integer
            key :format, :int8
        end
        property :city_id do
            key :type, :integer
            key :format, :int32
        end
        property :address do
            key :type, :string
        end
    end
end
