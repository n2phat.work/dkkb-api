class Patient < ApplicationRecord
    self.table_name = 'patient'
    has_one :relative
    has_many :user_patient
    has_many :user, through: :user_patient
    belongs_to :country, -> {select(:id, :name, :code)}, foreign_key: :country_code, primary_key: :code, optional: true
    belongs_to :city, -> {select(:id, :name)}, foreign_key: :city_id, optional: true
    belongs_to :district, -> {select(:id, :name)}, foreign_key: :district_id, optional: true
    belongs_to :ward, -> {select(:id, :name)}, foreign_key: :ward_id, optional: true
    belongs_to :dantoc, -> {select(:id, :name)}, foreign_key: :dantoc_id, optional: true
    belongs_to :profession, -> {select(:id, :name)}, foreign_key: :profession_id, optional: true
    belongs_to :booking, foreign_key: :patient_id, optional: true

    attr_accessor :is_default

    include ActiveModel::Dirty

    after_update do |patient|
        if patient.saved_change_to_email? || patient.saved_change_to_mobile?
            if Rails.env == "production" && !patient.bvdhyd_msbn.nil?
                Dhyd::BnUpdateJob.perform_later({
                    msbn: patient.bvdhyd_msbn,
                    mobile: patient.mobile,
                    email: patient.email,
                    medpro_id: patient.medpro_id
                })
            end
        end
    end

    def self.update_msbn
        bookings = Booking.joins(:patient).where("patient.bvdhyd_msbn IS NULL OR patient.bvdhyd_msbn = ''").where(status: [1,2,-2]).where('DATE(booking_date) = ?', (Time.now - 1.days).strftime("%Y-%m-%d"))
        bookings.each do |b|
            Dhyd::BnGetSohsJob.perform_now({patient_id: b.patient.id})
        end
    end

    def rebooking_count
        if self.bvdhyd_msbn
            return BvdhydTaikham.where('NgayKham > ?', Time.now).where(SoHoSo: self.bvdhyd_msbn).count
        end
        return 0
    end

    def mobile_censored
        if self.mobile
            mobile = self.mobile.to_s
            if mobile.length >= 9
                mobile = mobile.gsub(/^(.*)(\d{4})(\d{3})$/,'\1****\3')
                return mobile
            else
                return nil
            end
        end
        return nil
    end

    def mobile_secure
        if self.mobile
            mobile = self.mobile
            unless self.mobile.include? "+"
                mobile = mobile.gsub(/^0/,"")
                mobile = "+84" + mobile.to_s
            end
            return ::Util.md5(mobile)
        end
        return nil
    end

    def as_json(options = nil)
        super(
            methods: [
                :is_default,
                :rebooking_count,
                :relative,
                :mobile_censored,
                :mobile_secure
            ],
            include: [
                :country,
                :city,
                :district,
                :ward,
                :dantoc,
                :profession,
                relative: {
                    include: [:loai],
                    except: [:date_create, :date_update]
                }
            ],
            except: [
                :date_create,
                :date_update,
                # :mobile
            ])
    end

    include Swagger::Blocks
    swagger_schema :Patient do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :bvdhyd_msbn do
            key :type, :string
        end
        property :medpro_id do
            key :type, :string
        end
        property :name do
            key :type, :string
        end
        property :surname do
            key :type, :string
        end
        property :job do
            key :type, :string
        end
        property :sex do
            key :type, :integer
            key :format, :int8
            key :note, '1 - nam; 0 - nữ'
        end
        property :birthyear do
            key :type, :integer
            key :format, :int32
        end
        property :birthdate do
            key :type, :date
        end
        property :bhyt do
            key :type, :string
        end
        property :mobile do
            key :type, :string
        end
        property :email do
            key :type, :string
        end
        property :country_code do
            key :type, :string
        end
        property :city_id do
            key :type, :integer
            key :format, :int32
        end
        property :district_id do
            key :type, :integer
            key :format, :int32
        end
        property :ward_id do
            key :type, :integer
            key :format, :int32
        end
        property :user_id do
            key :type, :integer
            key :format, :int32
        end
        property :is_default do
            key :type, :integer
            key :format, :int8
        end
        property :address do
            key :type, :string
        end
    end
end
