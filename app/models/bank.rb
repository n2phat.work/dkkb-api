class Bank < ApplicationRecord
    self.table_name = 'bank'

    attr_accessor :rate
    attr_accessor :const_rate
    attr_accessor :medpro_fee

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        if options[:methods]
            options[:methods] += [:rate, :const_rate, :medpro_fee]
        else
            options[:methods] = [:rate, :const_rate, :medpro_fee]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :Bank do
        key :required, [:id, :name, :code]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :code do
            key :type, :string
        end
        property :priority do
            key :type, :int32
        end
        property :image do
            key :type, :string
        end
    end
end
