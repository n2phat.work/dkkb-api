class Support < ApplicationRecord
    self.table_name = 'support'
    
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :Support do
        key :required, [:id]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :email do
            key :type, :string
        end
        property :question do
            key :type, :string
        end
    end
end
