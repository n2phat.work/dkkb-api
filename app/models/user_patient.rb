class UserPatient < ApplicationRecord
    self.table_name = 'user_patient'
    belongs_to :patient, foreign_key: :patient_id, optional: true
    belongs_to :user, foreign_key: :user_id, optional: true
end