class Room < ApplicationRecord
    self.table_name = 'room'
    has_one :schedule
    has_one :doctor, through: :schedule
    has_one :hospital, through: :schedule
    has_one :subject, through: :schedule
    # has_one :ptime, through: :schedule

    attr_accessor :ptime
    attr_accessor :changed_doctor
    attr_accessor :not_booking

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Room do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :hospital_id do
            key :type, :integer
            key :format, :int32
        end
        property :description do
            key :type, :string
        end
    end
end
