class City < ApplicationRecord
    self.table_name = 'dm_city'
    default_scope { where(status: 1).order(:priority) }

    has_many :patient
    has_many :hospital
    has_many :district

    belongs_to :country, -> {select(:id,:name)}, foreign_key: :country_code, primary_key: :code, optional: true
    
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :City do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :country_code do
            key :type, :string
        end
    end
end
