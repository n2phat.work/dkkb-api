class Country < ApplicationRecord
    self.table_name = 'dm_country'
    
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    has_many :city, foreign_key: :country_code, primary_key: :code
    include Swagger::Blocks
    swagger_schema :Country do
        key :required, [:id, :code, :name]
        property :id do
            key :type, :int32
        end
        property :code do
            key :type, :string
        end
        property :name do
            key :type, :string
        end
    end
end
