class Bvdhyd < ApplicationRecord
    self.table_name = 'lichkham'

    def as_json(options={})
        super(options)
    end

    def time_per_slot
        if self.list_GioKham
            arr_GioKham = self.list_GioKham.split(',')
            if arr_GioKham.length > 1
                return ((Time.parse(arr_GioKham[1]) - Time.parse(arr_GioKham[0])).round.to_i / 60)
            elsif arr_GioKham.length > 0
                return 30
            else
                return 0
            end
        else
            return 0
        end
    end

    def step
        if self.list_STT
            arr_STT = self.list_STT.split(',')
            if arr_STT.length > 1
                return arr_STT[1].to_i - arr_STT[0].to_i
            else
                return 0
            end
        else
            return 0
        end
    end
end
