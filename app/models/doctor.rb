class Doctor < ApplicationRecord
    self.table_name = 'doctor'
    has_many :schedule
    has_many :ptime, through: :schedule
    has_many :room, through: :schedule
    has_many :hospital_subject, through: :schedule

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Doctor do
        key :required, [:id]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :birthyear do
            key :type, :integer
            key :format, :int32
        end
        property :sex do
            key :type, :integer
            key :format, :int32
            key :note, '1 - nam; 0 - nữ'
        end
    end
end
