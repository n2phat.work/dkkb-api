class BvdhydToathuoc < ApplicationRecord
    self.table_name = 'bvdhyd_toathuoc'

    def self.crawl(params)
        if params[:msbn]
            data = Bvdhyd::Toathuoc.get_by_sohs(params[:msbn])
            data.each do |e|
                cls = self.find_or_create_by({
                    MaToaThuoc: e["MaToaThuoc"],
                    SoHS: e["SoHS"],
                    IDBS: e["IDBS"],
                    NgayThucHien: e["NgayThucHien"],
                    SoTheBHYT: e["SoTheBHYT"],
                    LoaiBenhNhan: e["LoaiBenhNhan"]
                })
                cls.FileXML = e["FileXML"]
                cls.save!
            end
        end
    end

    def result
        # Ox.load(self.FileXML, mode: :hash_no_attrs) rescue nil
        obj = Nokogiri::XML::Document.parse(self.FileXML) rescue nil
        if obj
            return Hash.from_xml(obj.to_xml)
        else
            return nil
        end
    end

    def as_json(options={})
        if options[:methods]
            # options[:methods] += [:result]
        else
            # options[:methods] = [:result]
        end
        if options[:except]
            options[:except] += [:FileXML]
        else
            options[:except] = [:FileXML]
        end
        super(options)
    end
end
