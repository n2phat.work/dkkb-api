module Nd1
    class User < ApplicationRecord
        self.table_name = 'user'
        has_many :user_patient, class_name: "Nd1::UserPatient", foreign_key: :user_id
        has_many :patient, through: :user_patient
        # has_many :booking, class_name: "Nd1::Booking", foreign_key: :user_id

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end