module Nd1
    class Session < ApplicationRecord
        self.table_name = 'nd1_session'

        belongs_to :user, class_name: "Nd1::User", foreign_key: :user_id, optional: true
        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end