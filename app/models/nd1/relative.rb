class Nd1::Relative < ApplicationRecord
    self.table_name = 'nd1_relative'

    belongs_to :patient, foreign_key: :skin_patient_id, primary_key: :id, optional: true
    belongs_to :loai, class_name: 'Nd1::RelativeType', foreign_key: :nd1_relative_type_id, primary_key: :id, optional: true

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        if options[:methods]
            options[:methods] += [:loai]
        else
            options[:methods] = [:loai]
        end
        if options[:include]
            options[:include] += [:loai]
        else
            options[:include] = [:loai]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :Relative do
        key :required, [:id]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :mobile do
            key :type, :string
        end
        property :email do
            key :type, :string
        end
        property :patient_id do
            key :type, :integer
        end
        property :relative_type_id do
            key :type, :integer
        end
    end
end
