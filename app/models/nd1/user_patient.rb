module Nd1
    class UserPatient < ApplicationRecord
        self.table_name = 'nd1_user_patient'

        belongs_to :user, class_name: "Nd1::User",foreign_key: :user_id, optional: true
        belongs_to :patient, class_name: "Nd1::Patient", foreign_key: :nd1_patient_id, optional: true
    end
end