module Nd1
    class Noti < ApplicationRecord
        self.table_name = 'nd1_notification'
        self.inheritance_column = nil

        attr_accessor :is_new

        after_commit do |b|
            # Skin::NotiJob.perform_later(b.id) if b.is_pushed == 0
        end

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_update]
                options[:methods] += [:is_new]
            else
                options[:except] = [:date_update]
                options[:methods] = [:is_new]
            end
            super(options)
        end

        include Swagger::Blocks
        swagger_schema :Notification do
            key :required, [:id]
            property :id do
                key :type, :integer
                key :format, :int32
            end
            property :content do
                key :type, :string
            end
            property :user_id do
                key :type, :integer
                key :format, :int32
            end
            property :type do
                key :type, :integer
                key :format, :int8
            end
            property :url do
                key :type, :string
            end
        end
    end
end