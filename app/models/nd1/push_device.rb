module Nd1
    class PushDevice < ApplicationRecord
        self.table_name = 'nd1_push_device'

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end
