class BvdhydCls < ApplicationRecord
    self.table_name = 'bvdhyd_cls'
    belongs_to :bvdhyd_cls_index, foreign_key: :ID_CLS, optional: true

    def self.crawl(params)
        if params[:msbn]
            data = Bvdhyd::Cls.get_by_sohs(params[:msbn])
            data.each do |e|
                cls = self.find_or_create_by({
                    SoBienNhan: e["SoBienNhan"],
                    SoChungTu: e["SoChungTu"],
                    SoHS: e["SoHS"],
                    NgayThucHien: e["NgayThucHien"].to_date,
                    DVCode: e["DVCode"],
                    LoaiBenhNhan: e["LoaiBenhNhan"],
                    ID_CLS: e["ID_CLS"]
                })
                cls.FileXML = e["FileXML"]
                cls.save!
            end
        end
    end

    def cls_name
        self.bvdhyd_cls_index.DienGiai rescue nil
    end

    def result
        Ox.load(self.FileXML, mode: :hash_no_attrs) rescue nil
    end

    def as_json(options={})
        if options[:methods]
            # options[:methods] += [:result]
        else
            # options[:methods] = [:result]
        end
        if options[:except]
            options[:except] += [:FileXML]
        else
            options[:except] = [:FileXML]
        end
        super(options)
    end
end
