class Dantoc < ApplicationRecord
    self.table_name = 'dm_dantoc'
    has_many :patient
    
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Dantoc do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :note do
            key :type, :string
        end
    end
end
