class User < ApplicationRecord
    self.table_name = 'user'

    has_one :supporter
    has_one :session
    has_many :user_patient
    has_many :patient, through: :user_patient
    has_many :booking

    def is_admin
        mobile = self.phone.sub("+84","0")
        if FeeMposAdmin.find_by(mobile: mobile)
            return true
        else
            return false
        end
    end

    def is_superadmin
        return false
    end

    def as_json(options={})
        if options[:except]
            options[:except] += [:hashpwd, :salt, :date_create, :date_update]
        else
            options[:except] = [:hashpwd, :salt, :date_create, :date_update]
        end
        if options[:methods]
            options[:methods] += [:is_admin,:is_superadmin]
        else
            options[:methods] = [:is_admin,:is_superadmin]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :User do
        key :required, [:id, :username]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :access_token do
            key :type, :string
        end
        property :username do
            key :type, :string
        end
        property :hash do
            key :type, :string
        end
        property :salt do
            key :type, :string
        end
        property :email do
            key :type, :string
        end
        property :fullname do
            key :type, :string
        end
        property :phone do
            key :type, :string
        end
    end
end