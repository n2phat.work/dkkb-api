class Fee < ApplicationRecord
    self.table_name = 'fee'

    attr_accessor :patient
    attr_accessor :subject

    def ngay
        self.date_create.strftime("%Y-%m-%d %H:%M:%S")
    end

    def crawl(*args)
        data = args[0]
        if data[:msbn] && data[:msnv]
            bv_data = Bvdhyd::Vienphi.searchmulti(data[:msbn], data[:msnv])
            patient = Patient.find_by(bvdhyd_msbn: data[:msbn])
        elsif data[:sohoadon]
            bv_data = Bvdhyd::Vienphi.searchsingle(data[:sohoadon])
        else
            bv_data = nil
        end
        return_data = []
        if bv_data
            bv_data.each do |f|
                tmp = Fee.find_or_create_by({
                    id: f["Autoid"].to_i
                })
                tmp.msbn = f["SoHS"]
                tmp.msnv = f["SoNV"]
                tmp.sohoadon = f["SoPhieu"]
                tmp.amount = f["SoTien"].to_i
                tmp.subject_code = f["TraiBenh"]
                tmp.ttxv = f["TTXV"].to_i
                tmp.so_dt = f["SoDT"]
                tmp.date_create = f["Ngay"]
                tmp.tenphong = f["TenPhong"]
                tmp.chandoan = f["ChanDoan"]
                tmp.tengiuong = f["TenGiuong"]
                tmp.content = f["NoiDung"]
                tmp.save!
                if tmp.status == data[:paid_status]
                    patient ||= Patient.find_by(bvdhyd_msbn: f["SoHS"])
                    if patient

                    else
                        data_patient = ::Bvdhyd::Patient.getbyhsbn(f["SoHS"])
                        if data_patient
                            medpro_id = self.make_msbn
                            patient = Patient.new do |e|
                                e.name = data_patient["Ten"]
                                e.surname = data_patient["Ho"]
                                e.cmnd = data_patient["SoCMND"]
                                e.sex = data_patient["GioiTinh"]
                                e.mobile = data_patient["DiDong"] ? data_patient["DiDong"] : data_patient["DienThoai"]
                                e.email = nil
                                e.dantoc_id = data_patient["IDDanToc"]
                                e.profession_id = data_patient["IDNgheNghiep"]
                                e.bhyt = nil
                                e.country_code = data_patient["MaQuocGia"]
                                e.city_id = data_patient["IDTinh"]
                                e.district_id = data_patient["IDQuanHuyen"]
                                e.ward_id = data_patient["IDPhuongXa"]
                                e.address = data_patient["DiaChi"]
                                e.note = nil
                                e.birthdate = data_patient["NgaySinh"]
                                e.birthyear = data_patient["NamSinh"]
                                e.bvdhyd_msbn = data_patient["SoHS"]
                                e.medpro_id = medpro_id
                                e.save!
                            end
                        end
                    end
                    tmp.patient = patient
                    return_data << tmp
                end
            end
        end
        return return_data
    end

    def make_msbn(length = 6)
        pattern = '1234567890'
		i = 0
		str = ''
		while i < length
			str = str + pattern[Random.rand(pattern.length - 1)]
			i = i + 1
        end
        str = "MP-" + Time.now.strftime("%y%m%d").to_s + str
        patient = Patient.find_by(medpro_id: str)
        if patient
            self.make_msbn
        else
            return str
        end
    end

    def subject
        Subject.unscoped.find_by(code: self.subject_code)
    end
    
    def as_json(options={})
        if options[:methods]
            options[:methods] += [:ngay, :patient, :subject]
        else
            options[:methods] = [:ngay, :patient, :subject]
        end
        if options[:except]
            options[:except] += [:date_update]
        else
            options[:except] = [:date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :City do
        key :required, [:id]
        property :id do
            key :type, :integer
            key :format, :int32
        end
    end
end
