class PaymentPayoo < ApplicationRecord
    self.table_name = 'payment_payoo'
    belongs_to :payment, optional: true
end
