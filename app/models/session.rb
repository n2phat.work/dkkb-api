class Session < ApplicationRecord
    self.table_name = 'session'

    belongs_to :user, foreign_key: :user_id, optional: true
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Session do
        key :required, [:user_id, :access_token]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :access_token do
            key :type, :string
        end
        property :user_id do
            key :type, :int32
        end
    end
end
