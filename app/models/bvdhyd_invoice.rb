class BvdhydInvoice < ApplicationRecord
    self.table_name = 'bvdhyd_invoice'

    def self.crawl(params)
        if params[:msbn]
            data = Bvdhyd::Hoadon.get_by_sohs(params[:msbn])
            data.each do |e|
                hoadon = self.find_or_create_by({
                    MaPhieu: e["MaPhieu"],
                    Ngay: e["Ngay"],
                    SoHoSo: e["SoHoSo"],
                    SoNhapVien: e["SoNhapVien"],
                    MaHoaDon: e["MaHoaDon"],
                    SoTien: e["SoTien"],
                    IDTinhTrang: e["IDTinhTrang"],
                    MaEinvoice: e["MaEinvoice"]
                })
                hoadon.save!
            end
        end
    end

    def as_json(options={})
        if options[:methods]
            # options[:methods] += [:result]
        else
            # options[:methods] = [:result]
        end
        if options[:except]
            options[:except] += [:FileXML]
        else
            options[:except] = [:FileXML]
        end
        super(options)
    end
end
