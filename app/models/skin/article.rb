module Skin
    class Article < ApplicationRecord
        self.table_name = 'skin_article'

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end