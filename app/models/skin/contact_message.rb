class Skin::ContactMessage < ApplicationRecord
    self.table_name = 'skin_contact_message'
    self.inheritance_column = nil

    after_commit do |cm|
        # ActiveRecord::Base.connection.execute("INSERT INTO medproadmindb.`error` (`contact_message_id`) VALUE ('#{cm.id}')")
    end
    
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :ContactMessage do
        key :required, [:id]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :company_name do
            key :type, :string
        end
        property :email do
            key :type, :string
        end
        property :content do
            key :type, :string
        end
    end
end
