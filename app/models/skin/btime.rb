module Skin
    class Btime < ApplicationRecord
        self.table_name = 'skin_time'
        
        attr_accessor :used_slot

        belongs_to :subject, class_name: "Skin::Room", foreign_key: :skin_subject_id, optional: true
        

        def as_json(options={})
            super(methods: [:used_slot],except: [:date_create, :date_update])
        end
    end
end