module Skin
    class NotiRead < ApplicationRecord
        self.table_name = 'skin_notification_read'
        
        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end

        include Swagger::Blocks
        swagger_schema :Notification do
            key :required, [:id]
            property :id do
                key :type, :integer
                key :format, :int32
            end
            property :content do
                key :type, :string
            end
            property :user_id do
                key :type, :integer
                key :format, :int32
            end
            property :type do
                key :type, :integer
                key :format, :int8
            end
            property :url do
                key :type, :string
            end
        end
    end
end
