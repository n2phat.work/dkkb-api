module Skin
    class Payment < ApplicationRecord
        self.table_name = 'skin_payment'

        before_save do |pm|
            begin
                pml = Skin::PaymentLog.find_by(skin_payment_id: pm.id, status: pm.status)
                if pm.status == PAYMENT_CANCEL_LATER
                    unless pml
                        pml = Skin::PaymentLog.new({
                            skin_payment_id: pm.id,
                            status: pm.status,
                            date_cancel: Time.now
                        })
                        pml.save!
                    end
                elsif pm.status == 1
                    if pm.result
                        json = ::Util.json_decode(pm.result, true)
                        unless pml
                            pml = Skin::PaymentLog.new({
                                skin_payment_id: pm.id,
                                status: pm.status
                            })
                            if pm.method_id == 2
                                date = Time.strptime(json[:auth_time],'%Y-%m-%dT%H%M%S%Z').localtime
                            elsif pm.method_id == 3
                                date = Time.strptime(json[:vnp_PayDate],'%Y%m%d%H%M%S').localtime
                            elsif pm.method_id == 6
                                date = Time.strptime(json[:vnp_PayDate],'%Y%m%d%H%M%S').localtime
                            elsif pm.method_id == 7
                                date = C.parse(json[:responseTime])
                            else
                                date = Time.now
                            end
                            pml.date_success = date
                            pml.save!
                        end
                        
                    end
                end
            rescue => exception
                SystemException.new({content: exception.to_s, trace: exception.backtrace}).save!
            end
        end

        after_save do |pm|
            unless pm.status == nil
                bookings = Skin::Booking.where(skin_payment_id: pm.id)
                bookings.update(status: pm.status)
                if pm.status == PAYMENT_CANCEL_LATER
                    bookings.each do |bk|
                        bk.refund_status = 1
                        bk.save!
                        Skin::Noti.new do |noti|
                            noti.type = NOTI_NORMAL
                            noti.user_id = bk.user_id
                            noti.skin_booking_id = bk.id
                            noti.title = "Bạn đã hủy thành công phiếu khám bệnh #{bk.transaction_code_gd}."
                            noti.content = "Tiền khám #{bk.subject ? bk.subject.price : 0 }đ sẽ được hoàn vào số tài khoản của bạn theo Quy định. <br/>Trân trọng cảm ơn!"
                            noti.save!
                        end
                    end
                end
            end
        end

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end