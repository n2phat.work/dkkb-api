module Skin
    class Patient < ApplicationRecord
        self.table_name = 'skin_patient'
        attr_accessor :is_default

        belongs_to :city, -> {select(:id, :name)}, foreign_key: :city_id, optional: true
        belongs_to :district, -> {select(:id, :name)}, foreign_key: :district_id, optional: true
        belongs_to :ward, -> {select(:id, :name)}, foreign_key: :ward_id, optional: true
        belongs_to :country, -> {select(:id, :name, :code)}, foreign_key: :country_code, primary_key: :code, optional: true
        belongs_to :dantoc, -> {select(:id, :name)}, foreign_key: :dantoc_id, optional: true
        belongs_to :profession, -> {select(:id, :name)}, foreign_key: :profession_id, optional: true

        has_one :relative, class_name: "Skin::Relative", foreign_key: :skin_patient_id
        has_many :user_patient, class_name: "Skin::UserPatient", foreign_key: :skin_patient_id

        def mobile_censored
            if self.mobile
                mobile = self.mobile.to_s
                if mobile.length >= 9
                    mobile = mobile.gsub(/^(.*)(\d{4})(\d{3})$/,'\1****\3')
                    return mobile
                else
                    return nil
                end
            end
            return nil
        end

        def mobile_secure
            if self.mobile
                mobile = self.mobile
                unless self.mobile.include? "+"
                    mobile = mobile.gsub(/^0/,"")
                    mobile = "+84" + mobile.to_s
                end
                return ::Util.md5(mobile)
            end
            return nil
        end
        
        def as_json(options={})
            super(
                methods: [
                    :is_default,
                    :mobile_censored,
                    :mobile_secure
                ],
                include: [
                    :country,
                    :city,
                    :district,
                    :ward,
                    :dantoc,
                    :profession,
                    :relative => {
                        include: [:loai],
                        except: [:date_create, :date_update]
                    }
                ],
                except: [
                    :date_create,
                    :date_update,
                    # :mobile
                ])
        end
    end
end