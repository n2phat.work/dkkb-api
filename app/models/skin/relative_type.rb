class Skin::RelativeType < ApplicationRecord
    self.table_name = 'relative_type'

    has_many :relative, class_name: "Skin::Relative"

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
end
