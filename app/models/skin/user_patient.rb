module Skin
    class UserPatient < ApplicationRecord
        self.table_name = 'skin_user_patient'

        belongs_to :user, class_name: "Skin::User", foreign_key: :user_id, optional: true
        belongs_to :patient, class_name: "Skin::Patient", foreign_key: :skin_patient_id, optional: true

    end
end