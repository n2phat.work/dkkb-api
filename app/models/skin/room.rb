module Skin
    class Room < ApplicationRecord
        self.table_name = 'skin_room'

        has_many :booking_time, class_name: 'Skin::Btime', foreign_key: :skin_room_id

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end