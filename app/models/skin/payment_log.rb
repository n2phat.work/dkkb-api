module Skin
    class PaymentLog < ApplicationRecord
        self.table_name = 'skin_payment_log'

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end