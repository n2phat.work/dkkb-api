module Skin
    class Booking < ApplicationRecord
        self.table_name = 'skin_booking'
        attr_accessor :related
        
        belongs_to :schedule, class_name: "Skin::Schedule", foreign_key: :skin_schedule_id, optional: true
        belongs_to :booking_time, class_name: "Skin::Btime", foreign_key: :skin_time_id, optional: true
        belongs_to :patient, class_name: "Skin::Patient", foreign_key: :skin_patient_id, optional: true
        belongs_to :payment, class_name: "Skin::Payment", foreign_key: :skin_payment_id, optional: true
        belongs_to :user, class_name: "User", foreign_key: :user_id, optional: true
        has_one :hospital, through: :schedule
        has_one :subject, through: :schedule
        has_one :doctor, through: :schedule
        has_many :log_sms, class_name: "Skin::LogSms", foreign_key: :skin_booking_id

        def sms_count
            self.log_sms.count
        end

        def bv_booking_time
            if [1,2].include? self.status
                if self.subject
                    count = Skin::Booking.where('status IN (1,2) AND date_create <= :date_create AND booking_date = :booking_date AND skin_schedule_id = :skin_schedule_id', {date_create: self.date_create, booking_date: self.booking_date, skin_schedule_id: self.skin_schedule_id}).count
                    es_each_time = self.subject.each_time.to_f / self.subject.room_amount.to_f rescue 0
                    es_time = Time.new(Time.now.year, Time.new.month, Time.new.day, self.booking_time.from, 0, 0) + (count * (es_each_time) * 60).seconds
                    return es_time.strftime("%H:%M")
                else
                    nil
                end
            else
                nil
            end
        end

        def as_json(options={})
            super(
                methods: [:sms_count, :related, :bv_booking_time],
                include: [
                    {
                        patient: {
                            include: [:country, :city, :district, :ward, :dantoc, :profession],
                            except: [
                                :date_create,
                                :date_update,
                                # :mobile
                            ],
                            methods: [
                                :mobile_censored,
                                :mobile_secure
                            ]
                        }
                    },{
                        booking_time:{
                            methods: [:used_slot],
                            only: [:from, :to, :buoi]
                        }
                    },{
                        payment:{
                            except: [:result,:date_create, :date_update]
                        }
                    },{
                        user: {
                            except: [:hashpwd, :salt, :date_create, :date_update]
                        }
                    },{
                        hospital:{
                            except: [:date_create, :date_update]
                        }
                    },{
                        subject:{
                            except: [:date_create, :date_update]
                        }
                    },{
                        doctor:{
                            except: [:date_create, :date_update]
                        }
                    }
                ],
                except:[
                    :date_create, :date_update
                ]
            )
        end
    end
end