module Skin
    class Schedule < ApplicationRecord
        self.table_name = 'skin_schedule'

        belongs_to :hospital, class_name: "Hospital", foreign_key: :hospital_id, optional: true
        belongs_to :subject, class_name: "Skin::Subject", foreign_key: :skin_subject_id, optional: true
        belongs_to :room, class_name: "Skin::Room", foreign_key: :skin_room_id, optional: true
        belongs_to :doctor, class_name: "Skin::Doctor", foreign_key: :skin_doctor_id, optional: true

        def as_json(options={})
            if options[:except]
                options[:except] += [:date_create, :date_update]
            else
                options[:except] = [:date_create, :date_update]
            end
            super(options)
        end
    end
end