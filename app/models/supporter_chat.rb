class SupporterChat < ApplicationRecord
    self.table_name = 'supporter_chat'
    mount_uploader :file, AttachmentUploader

    belongs_to :supporter_room, foreign_key: "supporter_room_id", optional: true

    def file_url
        if self.file
            BASE_URL + self.file.url rescue nil
        else
            nil
        end
    end
    
    def as_json(options={})
        if options[:except]
            options[:except] += [:date_update]
        else
            options[:except] = [:date_update]
        end
        super(options)
    end
end