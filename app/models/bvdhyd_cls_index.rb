class BvdhydClsIndex < ApplicationRecord
    self.table_name = 'bvdhyd_cls_index'
    has_many :bvdhyd_cls
    
    def as_json(options={})
        if options[:methods]
            # options[:methods] += [:result]
        else
            # options[:methods] = [:result]
        end
        if options[:except]
            options[:except] += [:FileXML]
        else
            options[:except] = [:FileXML]
        end
        super(options)
    end
end
