class PatientPromo < ApplicationRecord
    self.table_name = 'patient_promo'

    def self.crawl(date = nil)
        date = date.to_date.strftime("%Y-%m-%d %H:%M:%S") if date
        arr_old_msbn = Patient.where('bvdhyd_msbn IS NOT NULL AND bvdhyd_msbn != ""').pluck(:bvdhyd_msbn)
        arr_msbn = BvdhydTaikham.where(NgayKham: date).select('SoHoSo').where.not(SoHoSo: arr_old_msbn)
        arr_msbn.each do |e|
            data = ::Bvdhyd::Patient.getbyhsbn(e.SoHoSo)
            if data
                patient = self.find_by(bvdhyd_msbn: e.SoHoSo)
                if patient
                
                else
                    patient = PatientPromo.new do |e|
                        e.name = data["Ten"]
                        e.surname = data["Ho"]
                        e.cmnd = data["SoCMND"]
                        e.sex = data["GioiTinh"]
                        e.mobile = data["DiDong"] ? data["DiDong"] : data["DienThoai"]
                        e.email = nil
                        e.dantoc_id = data["IDDanToc"]
                        e.profession_id = data["IDNgheNghiep"]
                        e.bhyt = nil
                        e.country_code = data["MaQuocGia"]
                        e.city_id = data["IDTinh"]
                        e.district_id = data["IDQuanHuyen"]
                        e.ward_id = data["IDPhuongXa"]
                        e.address = data["DiaChi"]
                        e.note = nil
                        e.birthdate = data["NgaySinh"]
                        e.birthyear = data["NamSinh"]
                        e.bvdhyd_msbn = data["SoHS"]
                        e.save!
                    end
                end
            end
        end
    end

    def self.send_sms_tk(date = nil)
        date = date.to_date.strftime("%Y-%m-%d %H:%M:%S") if date
        tks = BvdhydTaikham.where(NgayKham: date).select('SoHoSo, MaDonVi').where.not(IDBS: 9).where.not(MaDonVi: ['CKQT','TQQT'])
        tks.each do |e|
            patient = PatientPromo.where(bvdhyd_msbn: e.SoHoSo).where(is_sent: 0).first
            if patient
                subject = Subject.find_by(code: e.MaDonVi)
                if subject
                    sms_msg = "Ban co lich hen tai kham ngay #{date.to_date.strftime("%d/%m/%Y")}, khoa #{::Util.remove_dau(subject.name)}. Vui long dat kham qua ung dung UMC hoac umc.medpro.com.vn de khoi xep hang cho doi!"
                    if patient.mobile && patient.mobile != ""
                        mobile = patient.mobile.split("-").first
                        if mobile
                            mobile = mobile.gsub(" ","")
                            ap ({
                                msg: sms_msg,
                                mobile: mobile
                            })
                            # start SMS
                            client = Savon.client(wsdl: SMS_SOAP_URL)
                            result = client.call(:send_sms, message: {
                                authenticate_user: SMS_USER, 
                                authenticate_pass: SMS_AUTHEN, 
                                brand_name: SMS_BRAND_NAME,
                                message: sms_msg,
                                receiver: mobile.to_s,
                                type: 1
                            })
                            LogSms.new do |ls|
                                ls.mobile = mobile
                                ls.content = sms_msg
                                ls.response = result
                                ls.save!
                            end
                            patient.is_sent = 1
                            patient.save!
                        end
                    end
                end
            end
        end
    end
end
