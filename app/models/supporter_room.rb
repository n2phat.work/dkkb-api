class SupporterRoom < ApplicationRecord
    self.table_name = 'supporter_room'

    has_many :supporter_chat
    belongs_to :patient,  foreign_key: "patient_id", optional: true
 
    before_create do |sr|
        sr.uuid = SecureRandom.uuid
    end

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
end