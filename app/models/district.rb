class District < ApplicationRecord
    self.table_name = 'dm_district'
    has_many :ward
    
    belongs_to :city, -> {select(:id,:name)}, foreign_key: :city_id, optional: true

    scope :on, -> (){
        where(status: 1)
    }

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :District do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :name do
            key :type, :string
        end
        property :city_id do
            key :type, :string
        end
    end
end
