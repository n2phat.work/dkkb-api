class Ptime < ApplicationRecord
    self.table_name = 'ptime'
    has_many :schedule
    has_many :doctor, through: :schedule
    has_many :room, through: :schedule
    has_many :subject, through: :schedule

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end

    include Swagger::Blocks
    swagger_schema :Ptime do
        key :required, [:id, :name]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :hour_from do
            key :type, :integer
            key :format, :int8
        end
        property :hour_to do
            key :type, :integer
            key :format, :int8
        end
        property :day_from do
            key :type, :integer
            key :format, :int8
        end
        property :day_to do
            key :type, :integer
            key :format, :int8
        end
        property :month_from do
            key :type, :integer
            key :format, :int8
        end
        property :month_from do
            key :type, :integer
            key :format, :int8
        end
        property :month_to do
            key :type, :integer
            key :format, :int8
        end
        property :weekday do
            key :type, :integer
            key :format, :int8
            key :note, '0 - CN, 6 - T7'
        end
    end
end
