class Faq < ApplicationRecord
    self.table_name = 'faq'

    belongs_to :faq_category, foreign_key: 'category_id', primary_key: :id, optional: true

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
    
    include Swagger::Blocks
    swagger_schema :Faq do
        key :required, [:id, :question, :answer]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :question do
            key :type, :string
        end
        property :answer do
            key :type, :string
        end
        property :category_id do
            key :type, :integer
            key :format, :int32
        end
    end
end
