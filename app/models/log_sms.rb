class LogSms < ApplicationRecord
    self.table_name = 'log_sms'

    belongs_to :booking, foreign_key: :booking_id, optional: true

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
end
