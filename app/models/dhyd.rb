class Dhyd < ApplicationRecord
    self.table_name = 'dhyd'
    include ActiveModel::Dirty

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
end