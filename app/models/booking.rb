class Booking < ApplicationRecord
    self.table_name = 'booking'
    default_scope { where(is_deleted: 0) }
    belongs_to :booking_time, -> {select(:id,:from,:to)}, foreign_key: :booking_time_id, optional: true
    belongs_to :patient, foreign_key: :patient_id, optional: true
    belongs_to :payment, foreign_key: :payment_id, optional: true
    belongs_to :user, foreign_key: :user_id, optional: true
    belongs_to :schedule, foreign_key: :schedule_id, optional: true

    has_one :hospital, through: :schedule
    has_one :subject, through: :schedule
    has_one :room, through: :schedule
    has_one :ptime, through: :schedule
    has_one :doctor, through: :schedule
    has_many :log_sms, class_name: "LogSms"

    # update_index 'booking#booking', :self

    attr_accessor :related

    after_commit do |b|
        # $redis.del('booking_list_' + b.user_id.to_s)
        # Redis::BookingJob.perform_later(b.user_id)
    end

    def self.clean_outdate
        Te.new do |e|
            e.value = Time.now
            e.save!
        end
        # xoa PKB thuong
        bookings = Booking.joins(:payment).where(status: 0).where(payment: {method_id: [1,2,3]}).where('booking.date_update < ?', (Time.now - 20.minutes))
        bookings.each do |e|
            e.is_deleted = 1
            e.save!
            # xoa ben bvdhyd
            Bvdhyd::Lichkham.deleteDSK({transaction_code_gd: e.transaction_code_gd})
            # end
        end
        # xoa PKB thanh toan dai ly payoo
        bookings = Booking.joins(:payment).where(status: 0).where(payment: {method_id: [5,6]}).where('date_expired <= ?', Time.now)
        bookings.each do |e|
            e.is_deleted = 1
            e.save!
            # xoa ben bvdhyd
            Bvdhyd::Lichkham.deleteDSK({transaction_code_gd: e.transaction_code_gd})
            # end
        end
        # noti payment failed
        # remember: update is_notified before deploy

        # payments = Payment.where(method_id: [1,2,3]).where('date_create <= ?', (Time.now - 25.minutes)).where(is_notified: 0).where(status: 0)
        # payments.each do |e|
        #     e.is_notified = 1
        #     e.save!

        #     Noti.new do |noti|
        #         noti.user_id = e.user_id
        #         noti.title = "Thanh toán KHÔNG THÀNH CÔNG."
        #         noti.content = "Thanh toán <b>không thành công.</b><br/>Vui lòng thử lại hoặc liên hệ <b>19002267</b> để được hỗ trợ nếu gặp khó khăn trong quá trình thanh toán hoặc đã bị trừ tiền mà không nhận được phiếu khám bệnh."
        #         noti.type = 4
        #         noti.save!
        #     end
        # end
        
        # update da di kham
        bookings = Booking.where(status: 1).where('booking_date <= ?', (Time.now - 1.days).strftime("%Y-%m-%d"))
        bookings.each do |e|
            # e.status = -3 # qua ngay kham
            e.status = 2 # qua ngay kham
            e.save!
            # UserPatient.where(user_id: e.user_id, patient_id: e.patient_id).update(is_banned_rebooking: 0)
        end
    end

    def sms_count
        self.log_sms.count
    end

    def bv_booking_time
        bv_dutrukham = self.subject ? BvdhydDutrukham.find_by(is_new: 1, MaDonVi: self.subject.code, RIDBuoiKham: self.schedule.ptime.buoi, IDBS: self.doctor.id, RIDThu: self.schedule.ptime.weekday.to_i + 1, STT: self.booking_number) : nil
        begin
            return bv_dutrukham ? bv_dutrukham.GioKham.strftime("%H:%M") : _strtotime(self.booking_time.to)
        rescue  => e
            return nil
        end
    end

    private def _strtotime(str)
        num = str.to_f
        hour = (num * 60 / 60).to_i
        min = (num * 60 % 60).to_i
        return hour.to_s + ":" + (min != 0 ? min.to_s : "00")
    end

    def self.email_reminder
        bookings = Booking.where(status: 1).where(booking_date: (Time.now + 1.day).strftime("%Y-%m-%d"))
        bookings.each do |e|
            Noti.new do |noti|
                noti.type = NOTI_NORMAL
                noti.user_id = e.user_id
                noti.booking_id = e.id
                noti.title = "Bạn có lịch khám bệnh vào ngày mai (Mã phiếu: #{e.transaction_code_gd})"
                noti.content = "Bệnh nhân: #{e.patient.surname.to_s + ' ' + e.patient.name.to_s}<br/>Chuyên khoa: #{e.subject ? e.subject.name : ""}<br/>Ngày khám: #{e.booking_date ? e.booking_date.to_date.strftime("%d-%m-%Y") : ""}<br/>Số thứ tự: #{e.booking_number.to_i}<br/><i>#{e.bhyt_accept >= 1 ? 'Vui lòng xác minh bảo hiểm y tế tại các quầy 12, 13, 14 trước khi vào phòng khám.' : 'Vui lòng in phiếu khám bệnh tại ki-ốt trước khi vào phòng khám.'}</i>"
                noti.save!
            end
            unless e.patient_id.nil?
                if e.patient.email && e.patient.email != ""
                    begin
                        Api::BookingMailJob.perform_later(e.patient.email, "MedPro - Nhắc lại Phiếu Khám Bệnh: #{e.transaction_code_gd}", e.id)
                    rescue  => err
                        pp err
                    end
                end
                #sms
                if e.patient.mobile && e.patient.mobile != ""
                    begin
                        sms_msg = "Ban co lich kham tai BV DHYD TPHCM, khoa #{::Util.remove_dau(e.subject.name)}, vao ngay mai #{e.booking_date.to_date.strftime("%d/%m/%Y")}. Vui long den BV truoc gio kham 15 phut. Tran trong!"
                        Api::BookingSmsJob.perform_later(e.patient.mobile, sms_msg)
                    rescue  => err
                        pp err
                    end
                end
            end
        end
    end

    def as_json(options = nil)
        super(
                methods: [:sms_count,:related,:bv_booking_time],
                include: [
                {
                    booking_time:{
                        only: [:from, :to]
                    }
                }, 
                {
                    patient: {
                        include: [:country, :city, :district, :ward, :dantoc, :profession],
                        except: [
                            :date_create,
                            :date_update,
                            # :mobile
                        ],
                        methods: [
                            :mobile_censored,
                            :mobile_secure
                        ]
                    }
                },
                {
                    payment:{
                        except: [:result,:date_create, :date_update]
                    }
                },
                {
                    user: {
                        except: [:hashpwd, :salt, :date_create, :date_update]
                    }
                },
                {
                    hospital:{
                        except: [:date_create, :date_update]
                    }
                },
                {
                    subject:{
                        except: [:date_create, :date_update]
                    }
                },
                {
                    room:{
                        except: [:date_create, :date_update]
                    }
                },
                {
                    doctor:{
                        except: [:date_create, :date_update]
                    }
                },
                {
                    ptime:{
                        except: [:date_create, :date_update]
                    }
                }
            ],
            except:[
                :date_create, :date_update
            ])
    end

    include Swagger::Blocks
    swagger_schema :Booking do
        key :required, [:id, :hospital_subject_doctor_room_time_id, :patient_id, :booking_phone]
        property :id do
            key :type, :integer
            key :format, :int32
        end
        property :hospital_subject_doctor_room_time_id do
            key :type, :integer
            key :format, :int32
        end
        property :patient_id do
            key :type, :integer
            key :format, :int32
        end
        property :booking_date do
            key :type, :date
            key :format, 'Y-m-d'
        end
        property :booking_time_id do
            key :type, :integer
            key :format, :int32
        end
        property :booking_number do
            key :type, :integer
            key :format, :int32
        end
        property :bhyt_accept do
            key :type, :integer
            key :format, :int8
        end
        property :email do
            key :type, :string
        end
        property :booking_phone do
            key :type, :string
        end
        property :status do
            key :type, :integer
            key :format, :int8
        end
    end
end
