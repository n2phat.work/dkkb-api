class BookingDay < ApplicationRecord
    self.table_name = 'booking_day'

    def self.truncate_all
        self.connection.execute("TRUNCATE booking_day")
    end

    def as_json(options={})
        super(options)
    end
end
