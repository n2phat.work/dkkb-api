class HospitalSubject < ApplicationRecord
    self.table_name = 'hospital_subject'
    belongs_to :hospital, foreign_key: 'hospital_id', optional: true
    belongs_to :subject, foreign_key: 'subject_id', optional: true

    has_many :schedule
    has_many :room, through: :schedule
    has_many :doctor, through: :schedule
    has_many :ptime, through: :schedule

    def as_json(options={})
        if options[:except]
            options[:except] += [:date_create, :date_update]
        else
            options[:except] = [:date_create, :date_update]
        end
        super(options)
    end
end
