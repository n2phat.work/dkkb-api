module Adm::ReportHelper
    def format_name(surname, name)
        return surname.to_s + " " + name.to_s
    end

    def format_sex(id)
        return id.to_i == 1 ? "Nam" : "Nữ"
    end

    def format_date(str)
        return C.parse(str).strftime("%d-%m-%Y")
    end

    def format_buoi(id)
        if id == 1
            return "Buổi Sáng"
        elsif id ==2
            return "Buổi Chiều"
        else
            return "Cả Ngày"
        end
    end

    def format_booking_time(from, to)
        self.strtotime(from) + " - " + self.strtotime(to)
    end

    def strtotime(str)
        num = str.to_f
        hour = (num * 60 / 60).to_i
        min = (num * 60 % 60).to_i
        return hour.to_s + ":" + (min < 10 ? ("0" + min.to_s) : min.to_s)
    end

    def format_status( status = nil)
        pattern = {
            0 => "Chưa thanh toán",
            1 => "Đã thanh toán",
            2 => "Đã khám",
            -1 => "Hủy lúc thanh toán",
            -2 => "Hủy không khám"
        }
        pattern_color = {
            0 => "grey",
            1 => "green",
            2 => "blue",
            -1 => "grey",
            -2 => "red"
        }
        return "<span style=\"font-weight:600; color: #{pattern_color[status]}\">#{pattern[status]}</span>"
    end

    def format_refund( status = nil )
        pattern = {
            0 => "",
            1 => "Cần hoàn",
            2 => "Đã hoàn",
        }
        pattern_color = {
            0 => "grey",
            1 => "red",
            2 => "blue"
        }
        return "<span style=\"font-weight:600; color: #{pattern_color[status]}\">#{pattern[status]}</span>"
    end
end