module Api::BookingMailerHelper
    def format_name(surname, name)
        return surname.to_s + " " + name.to_s
    end

    def format_sex(id)
        return id.to_i == 1 ? "Nam" : "Nữ"
    end

    def format_date(str)
        return C.parse(str).strftime("%d-%m-%Y")
    end

    def format_buoi(id)
        if id == 1
            return "Buổi Sáng"
        elsif id ==2
            return "Buổi Chiều"
        else
            return "Cả Ngày"
        end
    end

    def format_booking_time(from, to)
        self.strtotime(from) + " - " + self.strtotime(to)
    end

    def strtotime(str)
        num = str.to_f
        hour = (num * 60 / 60).to_i
        min = (num * 60 % 60).to_i
        return hour.to_s + ":" + (min < 10 ? ("0" + min.to_s) : min.to_s)
    end
end