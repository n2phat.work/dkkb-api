class MessageChannel < ApplicationCable::Channel
  def subscribed
    ap params
    # stream_from "message_#{params[:user_id]}"
    stream_from "message"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    p data
    # ActionCable.server.broadcast "message_#{params[:user_id]}", message: 'clicked', user: 'PP', time: Time.now.to_i, data: data
    ActionCable.server.broadcast "message", message: 'clicked', user: 'PP', time: Time.now.to_i, data: data
  end
end
