class SlottimeChannel < ApplicationCable::Channel
  def subscribed
    ap params
    if params[:data]
      stream_from 'slottime_' + params[:data][:room_id].to_s + "_" + params[:data][:date].to_s
    else
      stream_from 'slottime_' + params[:room_id].to_s + "_" + params[:date].to_s
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    p data
    if params[:data]
      socket_room_id = 'slottime_' + params[:data][:room_id].to_s + "_" + params[:data][:date].to_s
    else
      socket_room_id = 'slottime_' + params[:room_id].to_s + "_" + params[:date].to_s
    end
    # ActionCable.server.broadcast "message_#{params[:user_id]}", message: 'clicked', user: 'PP', time: Time.now.to_i, data: data
    ActionCable.server.broadcast socket_room_id, message: 'clicked', user: 'PP', time: Time.now.to_i, data: data
  end
end
