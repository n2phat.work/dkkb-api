class ChatChannel < ApplicationCable::Channel
  def subscribed
    # params[:user_id]
    # params[:access_token]
    # params[:supporter_id]
    # params[:patient_id]
    begin
      if params[:user_id] && params[:supporter_id] && params[:patient_id]
        sroom = SupporterRoom.find_by(user_id: params[:user_id], supporter_id: params[:supporter_id], patient_id: params[:patient_id])
        unless sroom
          sroom = SupporterRoom.new do |sr|
            sr.user_id = params[:user_id]
            sr.supporter_id = params[:supporter_id]
            sr.patient_id = params[:patient_id]
            sr.save!
          end
        end
        stream_from sroom.uuid
      else
  
      end
    rescue => exception
      
    end
  end

  def speak(data)
    # data["user_id"]
    # data["access_token"]
    # data["supporter_id"]
    # data["patient_id"]
    # data["from_user_id"]
    # data["from_supporter_id"]
    # data["action"]
    # data["content"]
    # data["file_base64"]
    # data["filename"]
    begin
      data = data["data"]
      uuid = nil
      response = nil
      sroom = SupporterRoom.find_by(user_id: data["user_id"], supporter_id: data["supporter_id"], patient_id: data["patient_id"])
      if sroom
        uuid = sroom.uuid
        case data["action"].to_s.downcase
        when 'message'
          if data["file_base64"] && data["filename"]
            ext = data["filename"].split(".").last
            file = decode_base64_file(data["file_base64"], ext)
          else
            file = nil
          end
          msg = SupporterChat.new do |chat|
            chat.from_user_id = data["from_user_id"]
            chat.from_supporter_id = data["from_supporter_id"]
            chat.content = data["content"]
            chat.read_user_id = data["read_user_id"] ? data["read_user_id"] : 0
            chat.read_supporter_id = data["read_supporter_id"] ? data["read_supporter_id"] : 0
            chat.file = file
            chat.save
          end
          sroom.date_update = Time.now
          sroom.supporter_chat << msg
          sroom.save!
          response = {
            id: msg.id,
            content: msg.content,
            time: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
            file_url: msg.file_url,
            from_user_id: msg.from_user_id,
            from_supporter_id: msg.from_supporter_id,
            user_id: data["user_id"],
            supporter_id: data["supporter_id"],
            patient_id: data["patient_id"],
            action:  data["action"],
            read_supporter_id: msg.read_supporter_id,
            read_user_id: msg.read_user_id
          }
          self.brd('supporter_' + data["supporter_id"].to_s, {
            user_id: data["user_id"],
            supporter_id: data["supporter_id"],
            patient_id: data["patient_id"],
            patient: Patient.find_by(id: data["patient_id"]),
            data: response
          })
        when 'typing'
          response = {
            content: "",
            time: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
            from_user_id: data["from_user_id"],
            from_supporter_id: data["from_supporter_id"],
            patient_id: data["patient_id"],
            action:  data["action"]
          }
          self.brd('supporter_' + data["supporter_id"].to_s, {
            user_id: data["user_id"],
            supporter_id: data["supporter_id"],
            patient_id: data["patient_id"],
            patient: Patient.find_by(id: data["patient_id"])
          })
        when 'read'
          # data[:patient_id]
          # data[:supporter_id]
          # data[:user_id]
          # data[:read_user_id]
          # data[:read_supporter_id]
          sroom.supporter_chat.update_all({
            read_user_id: data["read_user_id"].to_i
          }) if data["read_user_id"].to_i > 0
          sroom.supporter_chat.update_all({
            read_supporter_id: data["read_supporter_id"].to_i
          }) if data["read_supporter_id"].to_i > 0
          response = {
            action:  data["action"],
            patient_id: data["patient_id"],
            supporter_id: data["supporter_id"],
            user_id: data["user_id"],
            read_user_id: data["read_user_id"],
            read_supporter_id: data["read_supporter_id"]
          }
        else
          response = {
            content: "",
            time: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
            from_user_id: data["from_user_id"],
            from_supporter_id: data["from_supporter_id"],
            patient_id: data["patient_id"],
            action:  data["action"]
          }
          self.brd('supporter_' + data["supporter_id"].to_s, {
            user_id: data["user_id"],
            supporter_id: data["supporter_id"],
            patient_id: data["patient_id"],
            patient: Patient.find_by(id: data["patient_id"])
          })
        end
      end
      self.brd(uuid, response)
    rescue => exception
      self.unsubscribed
    end
  end

  def brd(room_id, data)
    begin
      ActionCable.server.broadcast room_id, data
    rescue => exception
      ap "errr"
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  private def decode_base64_file(str, ext)
    str = str.split(",").last
    decoded_file = Base64.decode64(str)
    file = Tempfile.new(['tmp', ("." + ext)])
    file.binmode
    file.write decoded_file
    return file
  end
end
