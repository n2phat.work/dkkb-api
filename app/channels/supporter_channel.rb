class SupporterChannel < ApplicationCable::Channel
  def subscribed
    # params["supporter_id"]
    Supporter.where(id: params["supporter_id"]).update(is_online: 1)
    stream_from 'supporter_' + params["supporter_id"].to_s
  end

  def unsubscribed
    Supporter.where(id: params["supporter_id"]).update(is_online: 0)
  end
end
