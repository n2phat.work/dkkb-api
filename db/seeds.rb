# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

data = [
    [6.5,7],
    [7,8],
    [8,9],
    [9,10],
    [10,11],
    [11,12],
    [13,14],
    [14,15],
    [15,16],
    [16,17]
]

[*20..60].each do |e|
    data.each do |v|
        BookingTime.find_or_create_by(from: v[0], to: v[1], room_id: e)
    end
end