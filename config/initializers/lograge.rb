require 'colorized_string'

class ColorKeyValue < Lograge::Formatters::KeyValue
  FIELDS_COLORS = {
    method: :red,
    path: :red,
    format: :red,
    controller: :green,
    action: :white,
    status: :green,
    duration: :magenta,
    view: :magenta,
    db: :magenta,
    time: :cyan,
    ip: :red,
    host: :red,
    params: :yellow
  }

  def format(key, value)
    line = super(key, value)

    color = FIELDS_COLORS[key] || :default
    ColorizedString.new(line).public_send(color)
  end
end

Rails.application.configure do
    config.lograge.enabled = true
    if Rails.env == 'production'
      config.active_record.logger = nil
      config.log_level = :warn
    else
      config.log_level = :info
    end
    # config.log_tags = [lambda {|r| Time.now.strftime('%Y-%m-%d %H:%M:%S.%3N') }, :remote_ip]
    # add time to lograge
    config.lograge.ignore_actions = ['Griddler::EmailsController#create']
    config.lograge.custom_options = lambda do |event|
        exceptions = %w(controller action)
        {
          params: event.payload[:params].except(*exceptions).to_json,
          time: event.time.strftime('%Y-%m-%d %H:%M:%S.%3N'),
          ip: event.payload[:remote_ip]
        }
    end
    # config.lograge.formatter = ColorKeyValue.new
    config.lograge.formatter = Lograge::Formatters::Logstash.new
end