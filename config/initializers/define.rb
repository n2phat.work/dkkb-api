USER_SECRET_KEY = "@M31D|P1r0!"
# booking status
BOOKING_NEW = 0
BOOKING_PAYMENT_DONE = 1
BOOKING_PAYMENT_CANCEL = -1
BOOKING_DONE = 2
BOOKING_CANCEL = -2

#redis
REDIS_BOOKING_LIST_CACHE_TIME = 15 #minute(s)
REDIS_DOCTOR_LIST_CACHE_TIME = 1440 #minute(s)

# payment
PAYMENT_CANCEL_LATER = -2
PAYMENT_CANCEL = -1
PAYMENT_NEW = 0
PAYMENT_SUCCESS = 1

#notification
NOTI_NORMAL = 1
NOTI_SCHEDULE_CHANGE = 2
NOTI_PROMO = 3
NOTI_SYSTEM = 4

# other
SCHEME = ENV.fetch('SCHEME') {'http://'}
HOST = ENV.fetch('HOST') { '192.168.0.50:3001' }
BASE_URL = SCHEME + HOST
BASE_URL_PAYOO = ENV.fetch('BASE_URL_PAYOO') {'http://localhost:8080'}
USER_MAX_PATIENT = 10
C = Chronic

# payment
MEDPRO_SERVICE_FEE = 10000
MEDPRO_VP_SERVICE_FEE = 7000
PAYMENT_POSTBACK_URL = ENV.fetch('PAYMENT_POSTBACK_URL') {SCHEME + HOST +  '/api/payment/postback'}

VNPAY_PAYMENT_VERSION = ENV.fetch('VNPAY_PAYMENT_VERSION') {'2.0.0'}
VNPAY_PAYMENT_URL = ENV.fetch('VNPAY_PAYMENT_URL') {'http://sandbox.vnpayment.vn/paymentv2/vpcpay.html'}
VNPAY_TMN_CODE = ENV.fetch('VNPAY_TMN_CODE') {'PKH00001'}
VNPAY_TMN_CODE_TKB = ENV.fetch('VNPAY_TMN_CODE_TKB') {'PKH00002'}
VNP_HASHSECRET = ENV.fetch('VNP_HASHSECRET') {"ULAOUCWEEQXCUDIESXTPMMWVIGLESIOG"}
VNP_HASHSECRET_TKB = ENV.fetch('VNP_HASHSECRET_TKB') {"LGMFDJQQKPJFXDSVRLCWBZITRCJZJSFW"}
VNPAY_MULTIPLIER = 100
MAIL_SERVER_URL = ENV.fetch('MAIL_SERVER_URL') {"http://util.pkh.vn/api/mail/send"}
SIDEKIQ_ADMIN = ENV.fetch('SIDEKIQ_ADMIN') {"admin"}
SIDEKIQ_PASSWORD = ENV.fetch('SIDEKIQ_PASSWORD') {"mbkoat"}

MOMO_END_POINT = ENV.fetch('MOMO_END_POINT') {'https://test-payment.momo.vn/gw_payment/transactionProcessor'}
MOMO_SDK_PAYMENT = ENV.fetch('MOMO_SDK_PAYMENT') {'https://test-payment.momo.vn/pay/app'}

MOMO_PARTNER_CODE = ENV.fetch('MOMO_PARTNER_CODE') {'MOMO1QNV20190417'}
MOMO_ACCESS_KEY = ENV.fetch('MOMO_ACCESS_KEY') {'qykhMyNXn9Enu3tA'}
MOMO_SECRET_KEY = ENV.fetch('MOMO_SECRET_KEY') {'3YA5bVNxhlFfpNIwMcTnld9Yl66pWrBW'}
MOMO_APP_SCHEME = ENV.fetch('MOMO_APP_SCHEME') {'momo1qnv20190417'}
MOMO_RETURN_URL = ENV.fetch('') {'http://api.pkh.vn/api/payment/momopostback'}
MOMO_NOTIFY_URL = ENV.fetch('') {'http://api.pkh.vn/api/payment/momonoti'}

MOMO_PARTNER_CODE_DL = ENV.fetch('MOMO_PARTNER_CODE_DL') {'MOMO1QNV20190417'}
MOMO_ACCESS_KEY_DL = ENV.fetch('MOMO_ACCESS_KEY_DL') {'qykhMyNXn9Enu3tA'}
MOMO_SECRET_KEY_DL = ENV.fetch('MOMO_SECRET_KEY_DL') {'3YA5bVNxhlFfpNIwMcTnld9Yl66pWrBW'}
MOMO_APP_SCHEME_DL = ENV.fetch('MOMO_APP_SCHEME_DL') {'momo1qnv20190417'}
MOMO_RETURN_URL_DL = ENV.fetch('MOMO_RETURN_URL_DL') {'http://api.pkh.vn/skin/payment/momopostback'}
MOMO_NOTIFY_URL_DL = ENV.fetch('MOMO_NOTIFY_URL_DL') {'http://api.pkh.vn/skin/payment/momonoti'}

PAYOO_CUS_NAME = ENV.fetch('PAYOO_CUS_NAME') {"PP"}
PAYOO_CUS_PHONE = ENV.fetch('PAYOO_CUS_PHONE') {"01698665336"}
PAYOO_CUS_ADDRESS = ENV.fetch('PAYOO_CUS_ADDRESS') {"97 Trần Quang Diệu"}
PAYOO_CUS_CITY = ENV.fetch('PAYOO_CUS_CITY') {"24000"}
PAYOO_CUS_EMAIL = ENV.fetch('PAYOO_CUS_EMAIL') {"phatn1@yahoo.com"}
PAYOO_POSTBACK_PAYMENT = ENV.fetch('PAYOO_POSTBACK_PAYMENT') {SCHEME + HOST +  '/api/payment/payoopostback'}
PAYOO_POSTBACK_PAYMENT_VP = ENV.fetch('PAYOO_POSTBACK_PAYMENT_VP') {SCHEME + HOST +  '/api/payment/payoopostbackvp'}
PAYOO_NOTI_URL = ENV.fetch('PAYOO_NOTI_URL') {'http://api.pkh.vn/api/payment/payoonoti'}
PAYOO_ACCOUNT_NAME = ENV.fetch('PAYOO_ACCOUNT_NAME') {'iss_pkh'}
PAYOO_SHOP_DOMAIN = ENV.fetch('PAYOO_SHOP_DOMAIN') {BASE_URL}
PAYOO_SHOP_ID = ENV.fetch('PAYOO_SHOP_ID') {764}
PAYOO_SHOP_TITLE = ENV.fetch('PAYOO_SHOP_TITLE') {'pkh'}
PAYOO_SECRET_KEY = ENV.fetch('PAYOO_SECRET_KEY') {'cb3c4b398451886d42bcf2fcccfb3ec0'}
PAYOO_PAYMENT_URL = ENV.fetch('PAYOO_PAYMENT_URL') {'https://newsandbox.payoo.com.vn/v2/paynow/order/create'}
VP_FINAL_URL = ENV.fetch('VP_FINAL_URL') {'http://medpro.pkh.vn/vp-payment-success'}

PAYOO_BUSINESS_URL = ENV.fetch('PAYOO_BUSINESS_URL') {'https://bizsandbox.payoo.com.vn/BusinessRestAPI.svc'}
PAYOO_API_USERNAME = ENV.fetch('PAYOO_API_USERNAME') {'iss_pkh_BizAPI'}
PAYOO_API_PASSWORD = ENV.fetch('PAYOO_API_PASSWORD') {'VTfJsdK8SeA+Uma/'}
PAYOO_API_SIGNATURE = ENV.fetch('PAYOO_API_SIGNATURE') {'qILbQHxJ0GAjogm3xTXFXKZeEU2Ww+Duit8h0Ka0IyE='}

PKB_PAYOO_CUS_NAME = ENV.fetch('PKB_PAYOO_CUS_NAME') {"PP"}
PKB_PAYOO_CUS_PHONE = ENV.fetch('PKB_PAYOO_CUS_PHONE') {"01698665336"}
PKB_PAYOO_CUS_ADDRESS = ENV.fetch('PKB_PAYOO_CUS_ADDRESS') {"97 Trần Quang Diệu"}
PKB_PAYOO_CUS_CITY = ENV.fetch('PKB_PAYOO_CUS_CITY') {"24000"}
PKB_PAYOO_CUS_EMAIL = ENV.fetch('PKB_PAYOO_CUS_EMAIL') {"phatn1@yahoo.com"}
PKB_PAYOO_POSTBACK_PAYMENT = ENV.fetch('PKB_PAYOO_POSTBACK_PAYMENT') {SCHEME + HOST +  '/api/payment/payoo_postback_pkb'}
PKB_PAYOO_POSTBACK_PAYMENT_VP = ENV.fetch('PKB_PAYOO_POSTBACK_PAYMENT_VP') {SCHEME + HOST +  '/api/payment/payoopostbackvp'}
PKB_PAYOO_NOTI_URL = ENV.fetch('PKB_PAYOO_NOTI_URL') {'http://api.pkh.vn/api/payment/payoo_noti_pkb'}
PKB_PAYOO_ACCOUNT_NAME = ENV.fetch('PKB_PAYOO_ACCOUNT_NAME') {'iss_pkh_paystore'}
PKB_PAYOO_SHOP_DOMAIN = ENV.fetch('PKB_PAYOO_SHOP_DOMAIN') {BASE_URL}
PKB_PAYOO_SHOP_ID = ENV.fetch('PKB_PAYOO_SHOP_ID') {830}
PKB_PAYOO_SHOP_TITLE = ENV.fetch('PKB_PAYOO_SHOP_TITLE') {'pkh_paystore'}
PKB_PAYOO_SECRET_KEY = ENV.fetch('PKB_PAYOO_SECRET_KEY') {'d6f1c587353d3af639ceb747be8b5d8d'}
PKB_PAYOO_PAYMENT_URL = ENV.fetch('PKB_PAYOO_PAYMENT_URL') {'https://newsandbox.payoo.com.vn/v2/paynow/order/create'}
PKB_PAYOO_BUSINESS_URL = ENV.fetch('PKB_PAYOO_BUSINESS_URL') {'https://bizsandbox.payoo.com.vn/BusinessRestAPI.svc'}
PKB_PAYOO_API_USERNAME = ENV.fetch('PKB_PAYOO_API_USERNAME') {'iss_pkh_paystore_BizAPI'}
PKB_PAYOO_API_PASSWORD = ENV.fetch('PKB_PAYOO_API_PASSWORD') {'3fTQMHEXEGZDKdmi'}
PKB_PAYOO_API_SIGNATURE = ENV.fetch('PKB_PAYOO_API_SIGNATURE') {'rr3os7WC3yS3YV9FRtABGh6K70eX7gJ7ACu3sOjkTg9TQ0Z9yXPHH9uoKzglZvwx'}

CYBS_PROFILE_ID = ENV.fetch('CYBS_PROFILE_ID') {'E7C5DD45-1D59-4C37-9020-8D92EC980955'}
CYBS_ACCESS_KEY = ENV.fetch('CYBS_ACCESS_KEY') {'370069f8660c37c08495bc7da9390762'}
CYBS_SECRET = ENV.fetch('CYBS_SECRET') {'d7b5044424a245efad78e5ac4b671b4c3588418e43d0463aac34652695e8f3ea0b6249d3af054bd79f2827b4baf3f1ab867dfc95875c4b24a799913ccd2f9bb30806e405a5c34067bbe3257ef4cd4814111ceecbf11d44d493a2d0f7d609c0698d68b3795c844c348154efec1731213511d4fe8258da411ca10a3800ff559827'}
CYBS_URL = ENV.fetch('CYBS_URL') {'https://testsecureacceptance.cybersource.com/pay'}

RATE_ATM = 0.01
ADD_ATM = 1650
VP_RATE_ATM = 0.0075
VP_ADD_ATM = 0

RATE_INT_CARD = 0.02
ADD_INT_CARD = 2000
VP_RATE_INT_CARD = 0.0175
VP_ADD_INT_CARD = 0

VP_MPOS_INTERNAL = 0.0033
VP_MPOS_INTER_VN = 0.0135
VP_MPSO_INTER_EX = 0.0235

VP_RATE_OFFLINE = 0.0065

WEB_FINAL_STEP = ENV.fetch('WEB_FINAL_STEP') {"http://localhost:3000/booking-new-final-step"}
WEB_FINAL_STEP_ERROR = ENV.fetch('WEB_FINAL_STEP_ERROR') {"http://192.168.0.73:3000/payment_error"}
WEB_STATIC_URL = ENV.fetch('WEB_STATIC_URL') {"http://192.168.0.73"}

DL_WEB_FINAL_STEP = ENV.fetch('DL_WEB_FINAL_STEP') {"http://localhost:3000/booking-new-final-step"}
DL_WEB_FINAL_STEP_ERROR = ENV.fetch('DL_WEB_FINAL_STEP_ERROR') {"http://192.168.0.73:3000/payment_error"}
DL_WEB_STATIC_URL = ENV.fetch('DL_WEB_STATIC_URL') {"http://192.168.0.73"}

#sms
SMS_SOAP_URL = 'http://brand.aztech.com.vn/ws/agentSmsApiSoap?wsdl'
SMS_BRAND_NAME = ENV.fetch('SMS_BRAND_NAME') {"Medpro"}
SMS_USER = ENV.fetch('SMS_USER') {"pkh"}
SMS_AUTHEN = ENV.fetch('SMS_AUTHEN') {"pkh@2018"}

#define CK string
if Rails.env == 'production'
    PKH_TRANSFER_TEXT = '<p>Vui lòng liên hệ <b><a href="https://zalo.me/0916883165"><font color="blue">Zalo 0916883165</font></a></b> hoặc tổng đài <b>19002267</b> để nhận thông tin chuyển khoản.</p><p>Mã TT: '
else
    PKH_TRANSFER_TEXT = '<p>Vui lòng liên hệ <b><a href="https://zalo.me/0916883165"><font color="blue">Zalo 0916883165</font></a></b> hoặc tổng đài <b>19002267</b> để nhận thông tin chuyển khoản. </p><p>Mã TT: '
end