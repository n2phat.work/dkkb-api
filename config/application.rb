require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
Dotenv::Railtie.load

module PkhApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.time_zone = 'Hanoi'
    config.active_record.default_timezone = :local
    if Rails.env == 'production'
      config.i18n.default_locale = :vi
    else
      config.i18n.default_locale = :en
    end
    config.exceptions_app = self.routes
    # config.autoload_paths << Rails.root.join('lib')
    config.eager_load_paths << Rails.root.join('lib')
    config.action_cable.allowed_request_origins = [
      'http://192.168.0.50:3001', 
      %r{http://api.pkh.*}, 
      %r{http://localhost*},
      %r{https://alpha.medpro.com.vn},
      %r{https://*medpro.com.vn*},
      %r{http://pkh.vn*},
      %r{https://umc.medpro.com.vn},
      %r{http://bvdhyd.online},
      %r{http://www.bvdhyd.online},
      %r{http://medpro.pkh.vn},
      %r{http://192.168.0.50:3555},
      %r{http://192.168.0.50:1323}
    ]
    config.web_console.whitelisted_ips = ['10.0.0.0/8', '172.16.0.0/12', '192.168.0.0/16'] if Rails.env != 'production'
    config.action_dispatch.default_headers = {
      'Access-Control-Allow-Origin' => '*',
      'Access-Control-Request-Method' => %w{GET POST PUT PATCH DELETE OPTIONS}.join(",")
    }
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, credentials: false, :methods => [:get, :post, :options]
      end
    end
    config.cache_store = :redis_store, "redis://#{ENV.fetch('REDIS_HOST') { 'localhost' }}:6379/1/cache"
  end
end
