# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
case @environment
    when 'production'
        set :environment, 'production'
    when 'development'
        set :environment, 'development'
    else
        set :environment, 'development'
end

set :chronic_options, hours24: true
set :output, "/opt/ruby/pkh-api/log/cron.log"

every 1.minute do
    runner "Booking.clean_outdate"
end

every 1.minute do
    runner "Dhyd::VpJob.perform_now"
end

every 1.hour do
    runner "Dhyd::CrawlJob.perform_now({bstt: 1})"
end

every '0 */6 * * *' do
    runner "Dhyd::CrawlJob.perform_now({tk: 1})"
end

every 1.day, at: '09:00' do
    runner "Dhyd::SubtituteJob.perform_now"
end

every 1.day, at: '16:55' do
    runner "Booking.email_reminder"
end

every 1.day, at: '16:45' do
    runner "Report::DailyMailer.sendmail({}).deliver_now"
end

every 5.minute do
    runner "Report::DailyMailer.checknumber.deliver_now"
end

every 1.day, at: '00:00' do
    runner "BookingDay.truncate_all"
end

every 1.day, at: '16:35' do
    runner "Dhyd::SendJob.perform_now"
end

every 1.day, at: '18:00' do
    runner "Dhyd::SendJob.perform_now"
end

every 1.day, at: '23:59' do
    runner "Dhyd::SendJob.perform_now"
end

every 1.day, at: '04:00' do
    runner "Patient.update_msbn"
end