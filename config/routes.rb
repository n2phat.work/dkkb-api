Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "index#index"
  # email reader
  post "/email_processor" => "griddler/emails#create"
  # socket route
  mount ActionCable.server => '/cable'
  # match '/abc' => 'index#test', via: [:get]
  # docs routes
  if Rails.env != "production"
    get '/docs-ui' => redirect('/swagger-ui/dist/index.html?url=/apidocs')
    get '/skindocs-ui' => redirect('/swagger-ui/dist/index.html?url=/skindocs')
    get '/nd1docs-ui' => redirect('/swagger-ui/dist/index.html?url=/nd1docs')
    resources :apidocs, only: [:index]
    resources :skindocs, only: [:index]
    resources :nd1docs, only: [:index]
  end
  match '/apitest' => 'api/test#test', via: [:get]
  match '/fbcallbacksms' => 'fblogin#fbcallbacksms', via: [:get]

  #sidekiq monitor
  require 'sidekiq/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    # Protect against timing attacks:
    # - See https://codahale.com/a-lesson-in-timing-attacks/
    # - See https://thisdata.com/blog/timing-attacks-against-string-comparison/
    # - Use & (do not use &&) so that it doesn't short circuit.
    # - Use digests to stop length information leaking (see also ActiveSupport::SecurityUtils.variable_size_secure_compare)
    ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest(SIDEKIQ_ADMIN)) &
    ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(SIDEKIQ_PASSWORD))
  end
  
  mount Sidekiq::Web => '/skmonitor'
  mount RedisBrowser::Web => '/redis-browser'

  # error
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  # api routes
  match 'api/hospital/getall' => 'api/hospital#getall', via: [:all]
  match 'api/hospital/detail' => 'api/hospital#detail', via: [:all]

  match 'api/service/getall' => 'api/subject#getall', via: [:all]
  match 'api/service/test' => 'api/subject#test', via: [:all]

  match 'api/room/getall' => 'api/room#getall', via: [:all]
  match 'api/room/getlist' => 'api/room#getlist', via: [:all]
  match 'api/room/detail' => 'api/room#detail', via: [:all]
  match 'api/room/detail2' => 'api/room#detail2', via: [:all]
  match 'api/room/detail_from_doctor' => 'api/room#detail_from_doctor', via: [:all]
  match 'api/room/checkslot' => 'api/room#checkslot', via: [:all]

  match 'api/doctor/getall' => 'api/doctor#getall', via: [:all]
  match 'api/doctor/detail' => 'api/doctor#detail', via: [:all]
  match 'api/doctor/search' => 'api/doctor#search', via: [:all]
  match 'api/doctor/search2' => 'api/doctor#search2', via: [:all]
  
  match 'api/schedule/getall' => 'api/schedule#getall', via: [:all]
  
  match 'api/booking/insert' => 'api/booking#insert', via: [:all]
  match 'api/booking/insertmulti' => 'api/booking#insertmulti', via: [:all]
  match 'api/booking/detail' => 'api/booking#detail', via: [:all]
  match 'api/booking/update' => 'api/booking#update', via: [:all]
  match 'api/booking/edit' => 'api/booking#edit', via: [:all]
  match 'api/booking/getavailable' => 'api/booking#getavailable', via: [:all]
  match 'api/booking/getbyuserid' => 'api/booking#getbyuserid', via: [:all]
  match 'api/booking/cancel' => 'api/booking#cancel', via: [:all]
  match 'api/booking/resend_code' => 'api/booking#resend_code', via: [:all]
  match 'api/booking/back_delete' => 'api/booking#back_delete', via: [:all]

  match 'api/rebooking/getall' => 'api/rebooking#getall', via: [:all]
  match 'api/rebooking/getallfromhistory' => 'api/rebooking#getallfromhistory', via: [:all]
  match 'api/rebooking/getallpatient' => 'api/rebooking#getallpatient', via: [:all]

  match 'api/patient/search' => 'api/patient#search', via: [:all]
  match 'api/patient/edit' => 'api/patient#edit', via: [:all]
  match 'api/patient/detail' => 'api/patient#detail', via: [:all]
  match 'api/patient/delete' => 'api/patient#delete', via: [:all]
  match 'api/patient/getbyuserid' => 'api/patient#getbyuserid', via: [:all]
  match 'api/patient/getbymsbn' => 'api/patient#getbymsbn', via: [:all]
  match 'api/patient/insert' => 'api/patient#insert', via: [:all]
  match 'api/patient/update' => 'api/patient#update', via: [:all]
  match 'api/patient/setdefault' => 'api/patient#setdefault', via: [:all]
  match 'api/patient/test' => 'api/patient#test', via: [:all]
  match 'api/patient/searchbyinfo' => 'api/patient#searchbyinfo', via: [:all]

  match 'api/user/fbsmslogin' => 'api/user#fbsmslogin', via: [:all]
  match 'api/user/detail' => 'api/user#detail', via: [:all]

  match 'api/supporter/getall' => 'api/supporter#getall', via: [:all]
  match 'api/supporter/getchatlist' => 'api/supporter#getchatlist', via: [:all]
  match 'api/supporter/gethistory' => 'api/supporter#gethistory', via: [:all]
  match 'api/supporter/getchatlist' => 'api/supporter#getchatlist', via: [:all]
  match 'api/supporter/follow' => 'api/supporter#follow', via: [:all]
  match 'api/supporter/getfollow' => 'api/supporter#getfollow', via: [:all]

  match 'api/city/getall' => 'api/city#getall', via: [:all]

  match 'api/payment/create' => 'api/payment#create', via: [:all]
  match 'api/payment/getallpayment' => 'api/payment#getallpayment', via: [:all]
  match 'api/payment/getlistbank' => 'api/payment#getlistbank', via: [:all]
  match 'api/payment/getlistvisa' => 'api/payment#getlistvisa', via: [:all]
  match 'api/payment/postback' => 'api/payment#postback', via: [:all]
  match 'api/payment/vnpayipn' => 'api/payment#vnpayipn', via: [:all]
  match 'api/payment/getinfobooking' => 'api/payment#getinfobooking', via: [:all]
  match 'api/payment/cybersourcepostback' => 'api/payment#cybersourcepostback', via: [:all]
  match 'api/payment/payoopostback' => 'api/payment#payoopostback', via: [:all]
  match 'api/payment/payoopostbackvp' => 'api/payment#payoopostbackvp', via: [:all]
  match 'api/payment/payoonoti' => 'api/payment#payoonoti', via: [:all]
  match 'api/payment/payoo_postback_pkb' => 'api/payment#payoo_postback_pkb', via: [:all]
  match 'api/payment/payoo_noti_pkb' => 'api/payment#payoo_noti_pkb', via: [:all]
  match 'api/payment/momopostback' => 'api/payment#momopostback', via: [:all]
  match 'api/payment/momonoti' => 'api/payment#momonoti', via: [:all]

  match 'api/noti/getall2' => 'api/noti#getall2', via: [:all]
  match 'api/noti/getall' => 'api/noti#getall', via: [:all]
  match 'api/noti/detail' => 'api/noti#detail', via: [:all]
  match 'api/noti/update' => 'api/noti#update', via: [:all]
  match 'api/noti/delete' => 'api/noti#delete', via: [:all]

  match 'api/resource/getallcountry' => 'api/resource#getallcountry', via: [:all]
  match 'api/resource/getcity' => 'api/resource#getcity', via: [:all]
  match 'api/resource/getdistrict' => 'api/resource#getdistrict', via: [:all]
  match 'api/resource/getward' => 'api/resource#getward', via: [:all]
  match 'api/resource/getdantoc' => 'api/resource#getdantoc', via: [:all]
  match 'api/resource/getarticle' => 'api/resource#getarticle', via: [:all]
  match 'api/resource/getallbanner' => 'api/resource#getallbanner', via: [:all]
  match 'api/resource/allbanner' => 'api/resource#allbanner', via: [:all]
  match 'api/resource/getallprofession' => 'api/resource#getallprofession', via: [:all]
  match 'api/resource/barcode' => 'api/resource#barcode', via: [:all]
  match 'api/resource/checkversion' => 'api/resource#checkversion', via: [:all]
  match 'api/resource/getconfig' => 'api/resource#getconfig', via: [:all]
  match 'api/resource/faq' => 'api/resource#faq', via: [:all]
  match 'api/resource/daily_report' => 'api/resource#daily_report', via: [:all]
  match 'api/resource/ck_payment' => 'api/resource#ck_payment', via: [:all]
  match 'api/resource/getholiday' => 'api/resource#getholiday', via: [:all]
  
  match 'api/resource/test' => 'api/resource#test', via: [:all]
  match 'api/resource/testsocket' => 'api/resource#testsocket', via: [:all]
  match 'api/resource/testheader' => 'api/resource#testheader', via: [:all]
  # match 'api/resource/testheader' => 'api/payment#cybersourcepostback', via: [:all]
  match 'api/resource/testtime' => 'api/resource#testtime', via: [:all]
  match 'api/resource/testsms' => 'api/resource#testsms', via: [:all]
  match 'api/resource/testclass' => 'api/resource#testclass', via: [:all]
  match 'api/resource/testfirebase' => 'api/resource#testfirebase', via: [:all]
  match 'api/resource/testrabbit' => 'api/resource#testrabbit', via: [:all]
  match 'api/resource/testhoadon' => 'api/resource#testhoadon', via: [:all]
  match 'api/resource/cs' => 'api/resource#cs', via: [:all]

  match 'api/faq/getall' => 'api/faq#getall', via: [:all]

  match 'api/fee/search' => 'api/fee#search', via: [:all]
  match 'api/fee/getorder' => 'api/fee#getorder', via: [:all]
  match 'api/fee/getbank' => 'api/fee#getbank', via: [:all]
  match 'api/fee/history' => 'api/fee#history', via: [:all]
  match 'api/fee/search_history' => 'api/fee#search_history', via: [:all]
  match 'api/fee/getresult' => 'api/fee#getresult', via: [:all]
  match 'api/fee/detail' => 'api/fee#detail', via: [:all]
  match 'api/fee/getordermpos' => 'api/fee#getordermpos', via: [:all]
  match 'api/fee/getorderoffline' => 'api/fee#getorderoffline', via: [:all]
  match 'api/fee/resultmpos' => 'api/fee#resultmpos', via: [:all]
  #for payoo partner
  match 'api/fee/search_by_code' => 'api/fee#search_by_code', via: [:all]
  match 'api/fee/bill_code_ipn' => 'api/fee#bill_code_ipn', via: [:all]
  match 'api/fee/check_transaction' => 'api/fee#check_transaction', via: [:all]
  #for momo partner
  match 'api/fee/momo_search_code' => 'api/fee#momo_search_code', via: [:all]
  match 'api/fee/momo_ipn' => 'api/fee#momo_ipn', via: [:all]

  match 'api/support/insert' => 'api/support#insert', via: [:all]
  match 'api/cmessage/insert' => 'api/cmessage#insert', via: [:all]
  match 'api/relative/getalltype' => 'api/relative#getalltype', via: [:all]
  
  match 'api/clinic/getall' => 'api/clinic#getall', via: [:all]
  match 'api/clinic/detail_by_date' => 'api/clinic#detail_by_date', via: [:all]
  match 'api/clinic/pdf' => 'api/clinic#pdf', via: [:all]

  match 'api/invoice/getall' => 'api/invoice#getall', via: [:all]
  match 'api/invoice/detail_by_date' => 'api/invoice#detail_by_date', via: [:all]
  match 'api/invoice/pdf' => 'api/invoice#pdf', via: [:all]

  match 'api/prescribe/getall' => 'api/prescribe#getall', via: [:all]
  match 'api/prescribe/detail_by_date' => 'api/prescribe#detail_by_date', via: [:all]
  match 'api/prescribe/pdf' => 'api/prescribe#pdf', via: [:all]

  match 'adm/booking/:command' => 'adm/booking#index', via: [:all]
  match 'adm/booking' => 'adm/booking#index', via: [:all]

  match 'adm/report' => 'adm/report#index', via: [:all]
  match 'adm/report/:command' => 'adm/report#index', via: [:all]

  match 'cls/test' => 'adm/test#cls', via: [:all]

  namespace :skin do
    root 'index#index'

    match 'user/fbsmslogin' => 'user#fbsmslogin', via: [:all]

    match 'subject/getall' => 'subject#getall', via: [:all]
    match 'subject/detail' => 'subject#detail', via: [:all]

    match 'booking/insertmulti' => 'booking#insertmulti', via: [:all]
    match 'booking/detail' => 'booking#detail', via: [:all]
    match 'booking/getbyuserid' => 'booking#getbyuserid', via: [:all]
    match 'booking/cancel' => 'booking#cancel', via: [:all]

    match 'patient/insert' => 'patient#insert', via: [:all]
    match 'patient/update' => 'patient#update', via: [:all]
    match 'patient/detail' => 'patient#detail', via: [:all]
    match 'patient/delete' => 'patient#delete', via: [:all]
    match 'patient/getbyuserid' => 'patient#getbyuserid', via: [:all]
    match 'patient/getbymsbn' => 'patient#getbymsbn', via: [:all]

    match 'payment/getallpayment' => 'payment#getallpayment', via: [:all]
    match 'payment/getinfobooking' => 'payment#getinfobooking', via: [:all]
    match 'payment/momopostback' => 'payment#momopostback', via: [:all]
    match 'payment/momonoti' => 'payment#momonoti', via: [:all]
    match 'payment/momo_sdk_payment' => 'payment#momo_sdk_payment', via: [:all]

    match 'noti/getall2' => 'noti#getall2', via: [:all]
    match 'noti/getall' => 'noti#getall', via: [:all]
    match 'noti/detail' => 'noti#detail', via: [:all]
    match 'noti/update' => 'noti#update', via: [:all]
    match 'noti/delete' => 'noti#delete', via: [:all]

    match 'resource/getallcountry' => 'resource#getallcountry', via: [:all]
    match 'resource/getcity' => 'resource#getcity', via: [:all]
    match 'resource/getdistrict' => 'resource#getdistrict', via: [:all]
    match 'resource/getward' => 'resource#getward', via: [:all]
    match 'resource/getdantoc' => 'resource#getdantoc', via: [:all]
    match 'resource/getarticle' => 'resource#getarticle', via: [:all]
    match 'resource/getallbanner' => 'resource#getallbanner', via: [:all]
    match 'resource/allbanner' => 'resource#allbanner', via: [:all]
    match 'resource/getallprofession' => 'resource#getallprofession', via: [:all]
    match 'resource/barcode' => 'resource#barcode', via: [:all]
    match 'resource/checkversion' => 'resource#checkversion', via: [:all]
    match 'resource/getconfig' => 'resource#getconfig', via: [:all]
    match 'resource/faq' => 'resource#faq', via: [:all]
    match 'resource/daily_report' => 'resource#daily_report', via: [:all]
    match 'resource/ck_payment' => 'resource#ck_payment', via: [:all]
    match 'resource/getholiday' => 'resource#getholiday', via: [:all]

    match 'faq/getall' => 'faq#getall', via: [:all]

    match 'relative/getalltype' => 'relative#getalltype', via: [:all]
    match 'cmessage/insert' => 'cmessage#insert', via: [:all]
  end

  namespace :nd1 do
    root 'index#index'

    match 'user/fbsmslogin' => 'user#fbsmslogin', via: [:all]
  end
  #partner
  match 'partner/patient_update' => 'partner#patient_update', via: [:all]
  match 'partner/patient_detail' => 'partner#patient_detail', via: [:all]

  match 'partner/booking_update' => 'partner#booking_update', via: [:all]

  match 'partner/payment_detail' => 'partner#payment_detail', via: [:all]
  match 'partner/payment_getlist' => 'partner#payment_getlist', via: [:all]
end