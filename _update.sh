cp /opt/ruby/pkh-api/.env ./
cp /opt/ruby/pkh-api/config/database.yml ./config

docker build --add-host rubygems.org:151.101.192.70 --add-host dkkhambenh.umc.edu.vn:125.234.101.182 -t pkh-api-live .
docker stop pkh-api-live
docker rm pkh-api-live
docker run -d --restart=always --add-host dkkhambenh.umc.edu.vn:125.234.101.182 --name pkh-api-live --link pkh-db --link pkh-redis -p 3001:3001 -v /opt/ruby/pkh-api/public/uploads:/pkh-api/public/uploads -v /opt/ruby/pkh-api/log:/pkh-api/log -v /opt/ruby/pkh-api/public/swagger-ui:/pkh-api/public/swagger-ui -v /opt/ruby/pkh-api/tmp:/pkh-api/tmp -t pkh-api-live

docker build --add-host rubygems.org:151.101.192.70 --add-host dkkhambenh.umc.edu.vn:125.234.101.182 -t pkh-api-sidekiq-live -f Dockerfile_sidekiq .
docker stop pkh-api-sidekiq-live
docker rm pkh-api-sidekiq-live
docker run -d --restart=always --add-host dkkhambenh.umc.edu.vn:125.234.101.182 --name pkh-api-sidekiq-live --link pkh-db --link pkh-redis -v /opt/ruby/pkh-api/public/uploads:/pkh-api/public/uploads -v /opt/ruby/pkh-api/log:/pkh-api/log -v /opt/ruby/pkh-api/public/swagger-ui:/pkh-api/public/swagger-ui -v /opt/ruby/pkh-api/tmp:/pkh-api/tmp -t pkh-api-sidekiq-live